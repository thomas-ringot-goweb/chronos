"use strict";

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

jQuery(document).ready(function ($) {
  $(document).ready(function () {
    initCarousel();
    initNav({
      openSelectorList: [".category-picker", ".js-header__hamburger"],
      closeSelectorList: [".nav__close"]
    });
    initNewsletter({
      openSelectorList: [".js-newsletter__open"],
      closeSelectorList: [".js-newsletter__close", ".js-newsletter__cancel"]
    });
    initFooterForm();

    if (isOnDesktop()) {
      initSidebar();
    }
  });

  function initNewsletter(_ref) {
    var openSelectorList = _ref.openSelectorList,
        closeSelectorList = _ref.closeSelectorList;
    var $body = $("body");
    var $newsletter = $(".js-newsletter");
    var $promises = $(".js-newsletter__promises");
    var $submitButton = $(".acysubbuttons");
    var defaultCssForTriggers = {
      cursor: "pointer",
      outline: "0"
    }; // BINDING SHOW EVENTS

    openSelectorList.forEach(function (openSelector) {
      var $open = $(openSelector);
      $open.css(defaultCssForTriggers);
      $newsletter.css({
        outline: "0"
      });
      $open.on("click", openNewsletter);
    }); // BINDING CLOSE EVENTS
    // click on triggers

    closeSelectorList.forEach(function (closeSelector) {
      var $close = $(closeSelector);
      $close.css(defaultCssForTriggers);
      $close.on("click", closeNewsletter);
    }); // reordering layout

    $promises.insertAfter(".acysubbuttons");
    $submitButton.insertAfter(".newsletter__acymailing"); // METHODs

    function openNewsletter() {
      $newsletter.addClass("is-active");
      $body.addClass("overflow-hidden");
    }

    function closeNewsletter() {
      $newsletter.removeClass("is-active");
      $body.removeClass("overflow-hidden");
    }
  }

  function initNav(_ref2) {
    var openSelectorList = _ref2.openSelectorList,
        closeSelectorList = _ref2.closeSelectorList;
    var $nav = $(".js-nav");
    var $body = $("body");
    var $categoryItemList = $(".js-nav__category-list-item");
    var defaultCssForTriggers = {
      cursor: "pointer",
      outline: "0"
    }; // BINDING SHOW EVENTS

    openSelectorList.forEach(function (openSelector) {
      var $open = $(openSelector);
      $open.css(defaultCssForTriggers);
      $open.on("click", openNav);
    }); // BINDING CLOSE EVENTS
    // click on triggers

    closeSelectorList.forEach(function (closeSelector) {
      var $close = $(closeSelector);
      $close.css(defaultCssForTriggers);
      $close.on("click", closeNav);
    }); // BINDING SUBCATEGORY TOGGLE

    $categoryItemList.each(function (i, $categoryItem) {
      $categoryItem.addEventListener("click", openCategoryItem);
    }); // METHODs

    function openNav() {
      $body.addClass("overflow-hidden");
      $nav.addClass("is-active");
    }

    function closeNav() {
      $body.removeClass("overflow-hidden");
      $nav.removeClass("is-active");
    }

    function openCategoryItem(e) {
      $(".nav__subcategory.is-active").removeClass("is-active");
      e.target.nextElementSibling.classList.toggle("is-active");
    }
  }

  function initCarousel() {
    var baseSettings = {
      autoplay: true,
      autoplaySpeed: 5000,
      infinite: true,
      adaptiveHeight: true
    };
    $(".js-carousel").slick(_objectSpread({}, baseSettings, {
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [{
        breakpoint: 992,
        settings: _objectSpread({}, baseSettings, {
          centerMode: true,
          centerPadding: "2rem",
          slidesToShow: 2,
          slidesToScroll: 2,
          arrows: false
        })
      }, {
        breakpoint: 768,
        settings: _objectSpread({}, baseSettings, {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: false,
          arrows: false
        })
      }]
    }));
  }

  function initFooterForm() {
    var $mainForm = $(".footer__form");
    var $form1 = $(".footer__form--part1");
    var $form2 = $(".footer__form--part2");
    var $form3 = $(".footer__form--part3");
    var $button1 = $(".footer__form--part1 button");
    var $button2 = $(".footer__form--part2 button");
    var $indicatorList = $(".footer__indicator-item");
    $button1.on("click", goPart2);
    $button2.on("click", goPart3);

    function goPart2() {
      $form1.addClass("hidden");
      $form2.removeClass("hidden");
      $indicatorList[0].classList.remove("is-active");
      $indicatorList[1].classList.add("is-active");
    }

    function goPart3() {
      $form2.addClass("hidden");
      $form3.removeClass("hidden");
      $indicatorList[1].classList.remove("is-active");
      $indicatorList[2].classList.add("is-active");
      sendFormData();
    }

    function sendFormData() {
      var formData = $mainForm.serializeArray();
      var data = {
        name: $('[name="name"').val(),
        mail: $('[name="mail"').val(),
        message: $('[name="message"').val(),
        tel: $('[name="tel"').val()
      };
      $.ajax({
        url: window.location.origin + "/wp-content/themes/chronos/scripts/form-footer.php",
        type: "post",
        data: data
      }).done(function () {
        console.log("done");
      });
    }
  }

  function initSidebar() {
    var $sticky = $(".widget-container");
    var $stickylimit = $(".article__navigation");

    if (!$stickylimit.length) {
      $stickylimit = $(".footer");
    }

    var $articleExcerpt = $(".article__excerpt");
    var offset;

    if ($articleExcerpt.length) {
      offset = $articleExcerpt.offset().top;
    } else {
      offset = $(".article__content").offset().top;
    }

    $sticky.css({
      position: "absolute",
      top: offset
    });

    if (!!$sticky.offset()) {
      var stickyHeight = $sticky.innerHeight();
      var stickyOffsetTop = $sticky.offset().top;
      var stickyOffset = offset;
      var stickyStopperPosition = $stickylimit.offset().top;
      var stopStickyPoint = stickyStopperPosition - stickyHeight - stickyOffset;
      var stickyDifference = stopStickyPoint + stickyOffset - 48;
      $(window).scroll(function () {
        var windowTop = $(window).scrollTop();

        if (stopStickyPoint < windowTop) {
          $sticky.css({
            position: "absolute",
            top: stickyDifference
          });
        } else if (stickyOffsetTop < windowTop + stickyOffset) {
          $sticky.css({
            position: "fixed",
            top: stickyOffset
          });
        } else {// $sticky.css({ position: "absolute", top: "40%" });
        }
      });
    }
  } // Utilities


  function isOnDesktop() {
    // console.log($(document).width());
    return $(document).width() > 768;
  }
});
//# sourceMappingURL=main.js.map
