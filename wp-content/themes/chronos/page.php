<?php
get_header();
get_template_part('template-parts/menu');
get_template_part('template-parts/newsletter');
?>

<main class="main bg-grey-light mt-24 pt-8">
    <div class="main__container single-article container px-4 flex flex-wrap">

			<article class="article flex-1">
				<header>
					<h1 class="article__title  md:w-3/4 text-2xl my-8 text-center md:text-left">
						<?php the_title()?>
					</h1>
        </header>

        <div class="article__body bg-white my-8">

          <div class="article__content p-8 ">
            <?php
$default_styled_content = get_post_field('post_content');
$clean_content = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $default_styled_content);

echo ($clean_content)
?>
          </div>
        </div>

			</article>

      <?php get_sidebar();?>
		</div>
</main>

<?php get_template_part('template-parts/footer-single');?>

<?php wp_footer();?>
</body>
</html>
