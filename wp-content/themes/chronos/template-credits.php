<?php
/*
Template Name: Modèle crédits
Template Post Type: page
 */

get_header();
get_template_part('template-parts/menu');
get_template_part('template-parts/newsletter');
?>

<main class="main mt-24 pt-8">
    <div class="main__container single-article container px-4 flex flex-wrap">

			<article class="flex-1">
				<header>
					<h1 class="home-title">
						<?php the_title()?>
					</h1>
        </header>

        <div class="article__body bg-white my-8">

          <div class="article__content p-8 text-center">
            <?php
							$default_styled_content = get_post_field('post_content');
							$clean_content = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $default_styled_content);

							echo ($clean_content)
						?>
          </div>
        </div>

			</article>
		</div>
</main>

<?php get_template_part('template-parts/footer');?>

<!-- BANNER -->
<?php wp_footer();?>
</body>
</html>
