jQuery(document).ready(function($) {
	$(document).ready(function() {
		initCarousel();

		initNav({
			openSelectorList: [".category-picker", ".js-header__hamburger"],
			closeSelectorList: [".nav__close"]
		});

		initNewsletter({
			openSelectorList: [".js-newsletter__open"],
			closeSelectorList: [".js-newsletter__close", ".js-newsletter__cancel"]
		});

		initFooterForm();

		if (isOnDesktop()) {
			initSidebar();
		}
	});

	function initNewsletter({ openSelectorList, closeSelectorList }) {
		const $body = $("body");
		const $newsletter = $(".js-newsletter");
		const $promises = $(".js-newsletter__promises");
		const $submitButton = $(".acysubbuttons");

		const defaultCssForTriggers = {
			cursor: "pointer",
			outline: "0"
		};

		// BINDING SHOW EVENTS
		openSelectorList.forEach(openSelector => {
			const $open = $(openSelector);
			$open.css(defaultCssForTriggers);
			$newsletter.css({ outline: "0" });

			$open.on("click", openNewsletter);
		});

		// BINDING CLOSE EVENTS
		// click on triggers
		closeSelectorList.forEach(closeSelector => {
			const $close = $(closeSelector);
			$close.css(defaultCssForTriggers);

			$close.on("click", closeNewsletter);
		});

		// reordering layout
		$promises.insertAfter(".acysubbuttons");
		$submitButton.insertAfter(".newsletter__acymailing");

		// METHODs
		function openNewsletter() {
			$newsletter.addClass("is-active");
			$body.addClass("overflow-hidden");
		}

		function closeNewsletter() {
			$newsletter.removeClass("is-active");
			$body.removeClass("overflow-hidden");
		}
	}

	function initNav({ openSelectorList, closeSelectorList }) {
		const $nav = $(".js-nav");
		const $body = $("body");
		const $categoryItemList = $(".js-nav__category-list-item");

		const defaultCssForTriggers = {
			cursor: "pointer",
			outline: "0"
		};

		// BINDING SHOW EVENTS
		openSelectorList.forEach(openSelector => {
			const $open = $(openSelector);
			$open.css(defaultCssForTriggers);

			$open.on("click", openNav);
		});

		// BINDING CLOSE EVENTS
		// click on triggers
		closeSelectorList.forEach(closeSelector => {
			const $close = $(closeSelector);
			$close.css(defaultCssForTriggers);

			$close.on("click", closeNav);
		});

		// BINDING SUBCATEGORY TOGGLE
		$categoryItemList.each((i, $categoryItem) => {
			$categoryItem.addEventListener("click", openCategoryItem);
		});

		// METHODs
		function openNav() {
			$body.addClass("overflow-hidden");
			$nav.addClass("is-active");
		}

		function closeNav() {
			$body.removeClass("overflow-hidden");
			$nav.removeClass("is-active");
		}

		function openCategoryItem(e) {
			$(".nav__subcategory.is-active").removeClass("is-active");
			e.target.nextElementSibling.classList.toggle("is-active");
		}
	}

	function initCarousel() {
		const baseSettings = {
			autoplay: true,
			autoplaySpeed: 5000,
			infinite: true,
			adaptiveHeight: true
		};

		$(".js-carousel").slick({
			...baseSettings,
			slidesToShow: 3,
			slidesToScroll: 1,
			responsive: [
				{
					breakpoint: 992,
					settings: {
						...baseSettings,
						centerMode: true,
						centerPadding: "2rem",
						slidesToShow: 2,
						slidesToScroll: 2,
						arrows: false
					}
				},
				{
					breakpoint: 768,
					settings: {
						...baseSettings,
						slidesToShow: 1,
						slidesToScroll: 1,
						centerMode: false,
						arrows: false
					}
				}
			]
		});
	}

	function initFooterForm() {
		const $mainForm = $(".footer__form");

		const $form1 = $(".footer__form--part1");
		const $form2 = $(".footer__form--part2");
		const $form3 = $(".footer__form--part3");

		const $button1 = $(".footer__form--part1 button");
		const $button2 = $(".footer__form--part2 button");

		const $indicatorList = $(".footer__indicator-item");

		$button1.on("click", goPart2);
		$button2.on("click", goPart3);

		function goPart2() {
			$form1.addClass("hidden");
			$form2.removeClass("hidden");
			$indicatorList[0].classList.remove("is-active");
			$indicatorList[1].classList.add("is-active");
		}

		function goPart3() {
			$form2.addClass("hidden");
			$form3.removeClass("hidden");
			$indicatorList[1].classList.remove("is-active");
			$indicatorList[2].classList.add("is-active");

			sendFormData();
		}

		function sendFormData() {
			const formData = $mainForm.serializeArray();
			const data = {
				name: $('[name="name"').val(),
				mail: $('[name="mail"').val(),
				message: $('[name="message"').val(),
				tel: $('[name="tel"').val()
			};
			$.ajax({
				url:
					window.location.origin +
					"/wp-content/themes/chronos/scripts/form-footer.php",
				type: "post",
				data: data
			}).done(function() {
				console.log("done");
			});
		}
	}

	function initSidebar() {
		var $sticky = $(".widget-container");
		var $stickylimit = $(".article__navigation");
		if (!$stickylimit.length) {
			$stickylimit = $(".footer");
		}

		var $articleExcerpt = $(".article__excerpt");
		var offset;
		if ($articleExcerpt.length) {
			offset = $articleExcerpt.offset().top;
		} else {
			offset = $(".article__content").offset().top;
		}

		$sticky.css({ position: "absolute", top: offset });

		if (!!$sticky.offset()) {
			var stickyHeight = $sticky.innerHeight();
			var stickyOffsetTop = $sticky.offset().top;
			var stickyOffset = offset;
			var stickyStopperPosition = $stickylimit.offset().top;
			var stopStickyPoint = stickyStopperPosition - stickyHeight - stickyOffset;
			var stickyDifference = stopStickyPoint + stickyOffset - 48;

			$(window).scroll(function() {
				var windowTop = $(window).scrollTop();
				if (stopStickyPoint < windowTop) {
					$sticky.css({ position: "absolute", top: stickyDifference });
				} else if (stickyOffsetTop < windowTop + stickyOffset) {
					$sticky.css({ position: "fixed", top: stickyOffset });
				} else {
					// $sticky.css({ position: "absolute", top: "40%" });
				}
			});
		}
	}

	// Utilities
	function isOnDesktop() {
		// console.log($(document).width());
		return $(document).width() > 768;
	}
});
