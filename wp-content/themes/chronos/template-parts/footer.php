<div class="footer footer--home">
	<footer class="footer-container container">
		<div class="footer__websites">
			<h3 class="footer__websites__title">vivaldi chronos c'est aussi...</h3>
			<ul class="footer__websites__list">
				<li class="footer__websites__item">
					<a class="footer__websites__link" href="#" >
						<div class="footer__websites__logo-box">
							<img class="footer__websites__logo" src="/wp-content/uploads/2018/12/home__amiral-logo.png"/>
						</div>
						<p class="footer__websites__content">
							<strong>Vivaldi Avocats</strong><br>
							Avocat de l'entreprise et du patrimoine
						</p>
					</a>
				</li>

				<li class="footer__websites__item">
					<a class="footer__websites__link" href="#" >
						<div class="footer__websites__logo-box">
							<img class="footer__websites__logo" src="/wp-content/uploads/2018/12/home__immo-logo.png"/>
						</div>
						<p class="footer__websites__content">
							<strong>Vivaldi Immo</strong><br>
							Portail des enchères immobilières
						</p>
					</a>
				</li>

				<li class="footer__websites__item">
					<a class="footer__websites__link" href="#" >
						<div class="footer__websites__logo-box">
							<img class="footer__websites__logo" src="/wp-content/uploads/2018/12/home__tempo-logo.png"/>
						</div>
						<p class="footer__websites__content">
							<strong>Vivaldi Tempo</strong><br>
							Suivi en temps réel de votre dossier
						</p>
					</a>
				</li>
			</ul>
		</div>
	</footer>

	<ul class="footer__links__list">
		<li class="footer__links__item">
			vivaldi chronos © <?php echo date("Y"); ?>
		</li>

		<li class="footer__links__item">
			<a class="footer__links__link" href="/mentions-legales">
				mention legales
			</a>
		</li>

		<li class="footer__links__item">
			<a class="footer__links__link" href="/credits">
				crédits
			</a>
		</li>
	</ul>
</div>
