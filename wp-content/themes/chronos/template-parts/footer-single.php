<footer class="footer footer--single bg-white py-12">
	<div class="footer__container container">
		<form class="footer__form margin-auto text-center">
			<h3 class="font-bold uppercase mb-4">
				vous êtes concerné par cette actualité ? parlons-en !
			</h3>

			<div class="footer__form--part1">
				<p class="mb-8">
					Entrons en contact, laissez-nous vos coordonnées !
				</p>
				<div class="footer__inputs flex flex-wrap justify-center mb-8">
					<input class="bg-grey md:rounded-l-full border-0 p-4" name="name" type="text" placeholder="Votre nom" required>
					<input class="bg-grey border-0 p-4" type="mail" name="mail"placeholder="Votre email" required>
					<button class="text-right bg-grey md:rounded-r-full border-0 p-4" type="button">
						<svg class="md:flex md:justify-center md:items-center" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="0.292in" height="0.194in">
											<image x="0px" y="0px" width="21px" height="14px" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAOCAQAAABqD59FAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfiChcOIDs+N4ciAAAAT0lEQVQoz2P4z4AdMjAwMDBkMIgyIAAepTkM/xmuICnGo1SM4QqKYrwOEEVTjBeQp5gRZhdBkMtEpEKiAMK1eEMA1VskBBZ1ooCEiEVLLgAXr4kwZTWFRgAAAABJRU5ErkJggg=="></image>
									</svg>
					</button>
				</div>
				<p>
					<strong>Notre engagement :</strong>
					Vos informations sont strictement condidentielles, elles ne seront jamis communiqués ni vendues à des tiers.
				</p>
			</div>

			<div class="footer__form--part2 m-auto md:w-1/2 hidden">
				<div class="footer__inputs flex flex-wrap mb-8">
					<textarea class="bg-grey border-0 p-4 border-b border-white" name="message" placeholder="Votre message" rows="5" required></textarea>
					<input class="bg-grey border-0 p-4" name="tel" type="tel" placeholder="Votre téléphone" required>
					<button class="text-right bg-grey border-0 p-4 self-end" type="button">
						<svg class="md:flex md:justify-center md:items-center" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="0.292in" height="0.194in">
											<image x="0px" y="0px" width="21px" height="14px" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAOCAQAAABqD59FAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfiChcOIDs+N4ciAAAAT0lEQVQoz2P4z4AdMjAwMDBkMIgyIAAepTkM/xmuICnGo1SM4QqKYrwOEEVTjBeQp5gRZhdBkMtEpEKiAMK1eEMA1VskBBZ1ooCEiEVLLgAXr4kwZTWFRgAAAABJRU5ErkJggg=="></image>
									</svg>
					</button>
				</div>
			</div>

			<div class="footer__form--part3 hidden">
				<div class="footer__inputs mb-8">
					<p class="text-lg">Votre message a bien été envoyé !</p><br>
					<button class="button p-4 bg-blue text-white uppercase">suivez-nous sur linkedin</button>
				</div>
			</div>

			<ul class="footer__indicator-list list-reset flex justify-center mt-8">
				<li class="footer__indicator-item is-active bg-grey h-4 w-4 mr-4"></li>
				<li class="footer__indicator-item bg-grey h-4 w-4 mr-4"></li>
				<li class="footer__indicator-item bg-grey h-4 w-4 mr-4"></li>
			</ul>
		</form>
	</div>
</footer>
