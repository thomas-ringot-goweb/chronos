<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Vivaldi_Chronos
 */

?>
<section class="banner bg-grey pt-8">
    <div class="banner__container container-lg lg:px-8 pb-4 pb-4 flex flex-wrap justify-around">
        <div class="banner__top w-full mb-12 text-center md:text-left lg:mb-0 md:w-auto">
            <p class="mb-4 text-base">
                Ne ratez plus l'actualité de Vivaldi chronos<br>
                <strong>Inscrivez-vous à notre newsletter !</strong>
            </p>

            <?php v_show_cta("Je m'inscris", "default", "js-newsletter__open");?>
        </div>

        <ul class="banner__bottom list-reset my-8 md:mt-0 flex flex-wrap justify-center text-sm">
            <li class="flex flex-col items-center justify-center text-center mb-4 md:mb-0">
                <svg
                    class="pl-2 flex-1"
                    xmlns="http://www.w3.org/2000/svg"
                    xmlns:xlink="http://www.w3.org/1999/xlink"
                    width="0.722in" height="0.639in">
                    <image  x="0px" y="0px" width="52px" height="46px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAADQAAAAuCAMAAABQ68okAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAACslBMVEUAAAAsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzM+LC9/OUmWGx6WGx6WGx6WGx6WGx6VHCAsLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzOWGx6WGx6WGx6WGx4sLzMsLzMsLzMsLzMsLzOWGx6WGx6WGx6WGx4sLzMsLzMsLzMsLzMsLzMsLzMsLzOWGx4sLzMsLzMsLzMsLzMsLzMsLzMsLzMsLzOWGx6WGx6WGx4sLzMsLzMsLzMsLzMsLzMsLzOWGx6WGx6WGx6WGx4sLzMsLzOWGx6WGx6WGx6WGx4sLzOWGx6WGx6WGx6WGx4sLzMsLzMsLzOWGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx4sLzMsLzOWGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx4sLzOWGx7///8QP1X7AAAA43RSTlMAAl257vrbkCEezHAY4ppHJyA0adV6teoEmTZCv7aHwD/7DSJKYFQzsH4DMDfH5WbwQFXh0NedASX3xSsRjqDUCozr7Fc7CyTPWxC3fwd7KpPyCQ/gb2NQ4wwsXNy0BVE43nLorbw852iI+SkEyscCT+paddFx/D6RFNMONLCYi57k8WrE7ctQ5k4b9DquGvJZlwhfoRlu7eWR/BOqzd1Dw17NLwT4lGjAIBAycrMWHVOAooCpDSaMnzCWkPQ/oIXsR3vg1nS6cNNgYmnQWN5OsehAqIrxWgG7k8L6vw98BqX4YYjvoMkAAAABYktHROVYZQu/AAAAB3RJTUUH4goYDgEpGXLz3wAAArxJREFUSMedk/lfTFEYxk8xJJKp1EhMNUJJkkhkDFJEiKmEIlu2rAkRRUSRZJd9K2TNvu/7vu/b8/4hbt25c+9kPnWm94d7nud5z/d+zj33HMZqKzv7Bg0BVaPGDoy7mjjCVKqmvEwzJ6iaO7dQu7i6AS05IXd4uIhK0wrw5GI8gdbmj/NCGy6oLbSy8YaPr/VpunZ+7TvYSa4j/OVWgAqdrCGBnas3KqiLyWuh3LJgdLXChKiAbqHdhWePMF6oZzh69RbGiD6AKy+kR1+DqPqhfwAnNACRJuUbjoF8UBQQLelBGMwHxQBqSWvhV29oCIbK/dhwBPJAw+AxXIrCRiBuJA80yggvKfIG4hkPxBIQKkWJGP0/w5KAMZJ2w9jq0U8JGa1AbBySU0TlKvF1Q+OBCaJKhZ5xQvKPMEqiXtBEJTTJwANFTwamSM2pQNq0gLog3+kzAO3MWemz57C58+azBcFARs2DFLswE4tEudhxCXN2ArKcGVtKtIxl03LhR64QrmdOjJJJTAVyIyQXowdWrtJUyTxavYYov0rq3IG168xIQTJQ6J8k2fVGYEORqPNJqI2mRrEHsKlE1JuF9W7Zan7FNh9sD5FMPu3YSbskV7ob2FN13KKEtezdJ6/UH9hfYHYH6OAhosNmn3AEuUWsNAPQR8mMQyaOpsi2jMqPER2XgxNBwoHJQaG9ckvikRWmsBV08hTRaUVyBsEsDsUWe38W55S2ks5fILqoSHQAA3Q1IL3SXqLLjOiKIlFXQ+raoKt0jV2nG7ZBN+kWu013bIOy6S67J54IBXQ/LckCisQDpX1Ij9hjeqJIDEFuTJPCLKvEwuXRU5ZOz5SRxsoVsazn9IKVU0Vd0yzrJb1ir6nSNugNvWXv6L1t0Af6+OkzfbEN+krfvhOV2Qb9EBD6aRvD2K/ff/7WzP4BIVxZtFVE96YAAAAASUVORK5CYII=" />
                </svg>

                <p>
                    <strong>Newsletter qualitative</strong><br>
                    <span class="text-grey-dark px-2">rédigée par des professionnels</span>
                </p>
            </li>

            <li class="flex flex-col items-center justify-center text-center mb-4 md:mb-0">
                <svg
                    class="pl-2 flex-1"
                    xmlns="http://www.w3.org/2000/svg"
                    xmlns:xlink="http://www.w3.org/1999/xlink"
                    width="0.5in" height="0.556in">
                    <image  x="0px" y="0px" width="36px" height="40px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAoCAMAAAChHKjRAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAB5lBMVEUAAACWGx4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACWGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACWGx7///8wo2T/AAAAn3RSTlMAAF3N/PnBSJjakeh1WpoEC7Y3yhDypQwG+42z2GHtjLHZI4BlzAIP76hgjwewPQGi0ojgfmpSCAUDAWHS/MdOm9mMk+N7W5kIsDvI3AQO76f6ka52ehq5HXcZuh59vL14+dpxGLNzHG6xcBbbqVqXPZqNkuLR/v1k1lab3YOXA0HG3uut95Sp3NCG9para8Bzwg5UnwUJO5LkeVvLw0ow5XOzAAAAAWJLR0ShKdSONgAAAAd0SU1FB+IKGA4GNUIyOVcAAAGNSURBVDjLtZRlU8NAEIYPKVC0OC2FYsWhuFtx11BaaHEo7u6uxa3o5qeSa2CGgc6GD/B82J3MPTN7914SQoitnb3IwZGgOIHY2ckFXDHHDdw9CJF4ghcieYs9aPPx9bM8+vv8JIBIZbwcKKc1CKwQTKQKXgoJpTUs3AoRRCmPpKtR0THInmIhjrPiEyARkVRJkJwiS4U0NCdVuhxAmUGEyAwQVP4Fm298XcvKFpJycvPY/IJCXCpi1cUlpWwZJpWzFdywyiq2GpFqautoq29oRKSmZr63qKnEtGratLrW9g49Y9B06nUGTZe+20B6PqVeKvUx/QPaQa4Yh5j+YeMIMzps7BskBWMf48aRcRPsJL/xKSyCaVY9MzuHR8CFOb+wWCMQ5q+u5bcX/HcIv76qpWUAuxVcWoXlFMUarGOOG2xsch9nDGwh0vaO5a8i2d1DpP0Dvh8e0ep6bIUTcmripTMxrecXVrgkV6mW819HHyLjbuD2jpD7EHjAjmeGHbPpEZ7woJ5fpKLXN8x4B25SfADN0dTaAAAAAElFTkSuQmCC" />
                </svg>

                <p>
                    <strong>Personnalisable</strong><br>
                    <span class="text-grey-dark px-2">ne recevez que ce qui vous intéresse</span>
                </p>
            </li>

            <li class="flex flex-col items-center justify-center text-center mb-4 md:mb-0">
                <svg
                    class="pl-2 flex-1"
                    xmlns="http://www.w3.org/2000/svg"
                    xmlns:xlink="http://www.w3.org/1999/xlink"
                    width="0.778in" height="0.569in">
                    <image  x="0px" y="0px" width="56px" height="41px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAADgAAAApCAMAAABX0hoSAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAC3FBMVEUAAACWGx6WGx6WGx6WGx6WGx6WGx6WGx4AAAAAAAAAAAAAAAAAAAAAAACWGx6WGx6WGx6WGx4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACWGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx4AAAAAAAAAAAAAAAAAAAAAAAAAAACWGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACWGx6WGx6WGx6WGx6WGx6WGx4AAAAAAAAAAAAAAAAAAAAAAAAAAACWGx6WGx6WGx6WGx4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACWGx6WGx4AAAAAAAAAAAAAAAAAAAAAAACWGx6WGx6WGx4AAAAAAAAAAAAAAAAAAACWGx6WGx6WGx4AAAAAAAAAAACWGx4AAAAAAAAAAACWGx6WGx6WGx6WGx4AAAAAAAAAAAAAAAAAAAAAAACWGx6WGx6WGx4AAAAAAAAAAAAAAACWGx6WGx6WGx6WGx4HAQEAAAAAAAAAAAAJAgIAAAAAAAAAAAAAAACWGx6WGx4AAAAAAAAAAAAAAAAAAACWGx6WGx4AAAAAAAAAAAAAAAAAAACWGx6WGx4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACWGx6WGx4JAgIAAAAAAAAIAQIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACWGx4AAAAAAAAAAAAAAAAAAAAAAACWGx6WGx4AAAAAAAAAAAAAAACWGx6WGx6WGx6WGx4LAgIAAACWGx6WGx6WGx6WGx4LAgIAAAAAAACWGx6WGx6WGx6WGx6WGx4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACWGx4AAAD///8qS1bAAAAA8XRSTlMAPZ7f+/qcPDW88OKPDBWxrhMNkOO5OvrGBBrcrFAnT6vbGAbJ+Tg+Zl4CxexAP+vDAWD4YT/Kwf0yoJhc+TY0+FmamTf79k67yY2LxpK1VPSkjbQ9Jf6x8qEW2ITy7zKJ1hQKQC7I4EEJ4C/hNBC8wMwSEfznvTYgA83L5ANPJThrYG0qwusdIBiMXwJISYsXI+iifH2lIW6RUri6BVVvrUWr7x/8SyOoRii43IMOZIEqrwHPsu4onNDEDQxyxd4bQUf0ORstCeThBytDMCq78LkpHMgvFX4xzXa/n/FWvlqbljP+wFdlY1PMCAdCnRKeFBNIFAAAAAFiS0dE86yxvu4AAAAHdElNRQfiChgOBzrLlhWHAAADNElEQVRIx62V+VtMURjHX7JkSrTQhLJkRCSZlsmSlEKRGEv2MEQq0rTY10KLnYQkCZGdyJrs+5oKWcuenPcvcO/cO8+cme4wPXx/Oue+3885z3nfc98DoEf16hs1aGjUqDHolXETkYlpU52PZs0Ir+bmOiELSytRC2OAltbISGyjFWzVmpA2tnZ2bdsR0r6DVsi+I+uXdAIxih06S7ALHXTsSro5ccPuzqSHCx3riVJXN3f0ABl6AvTC3nSwD+nrpR738yb96ZgP+gIMQD/wx4EAg9CUDg4mAZpJIBlCx4ZiEMAwDIbhKBohRxxJxcwJsdPMRpHRdH4cEMeYhuBYGDeePewEelEvMpGeTiKT6Wko658yFWBaKIoUWombTmbQ0zDipBWeKcLQWexAgeE6pZpt9icQwpHbqDaorf8HRvBgJPpF0TkNmDNXS94kOnAeZYhRsqVnZBOLcfEJKjELzF9ABLTQkQE4T3wcLlrMLbFEhrykS2EZ8V6+Qkcr+zIVWSVRu2QD1ZsrEvmPSavBloQJH3NNEueReDgIpkIvWEtrkxPlKakCoEvaOhcBMNU1yGP9BoCNm9j9NwuAWwjZKgBuY/2x6TAGrbZnIO6IYmVDgzsJ2UWDmSrLbsSMrEW4BySYDbCXz1fOPgpMy83dT4EHQnjTQYA89Ac/dOO358pBJSdMM2LAQ1LexBzLDaVwGPNTjsjQVVVcC9APwlGV5RjKjqfkoxxO5LCrnDwlUI7o0wLlSD3D+gvOAqSfKzyfpWlyFHjholAdMy9dLvRJ+JcLwChSniRS6UoRXCUTi510VHyNXIcbVzhPkjxSzWVbq6+v8ibcui30d9y5C/eUapd1Nn/ccBTff6BSETN9+Mj5sY6cnzxlAs84T95zjOBuaAnKxkEd9EKJJaqBAk3qwmlax996Ti0Z2qz0gfalWGBcF66sAEvtmRZUzma41HDuJesvj2GeLf9zrxBbGsrFI3q8lqIPVOAbgLdoaSiYjIMA3mEIKLEMwBffGwr64geAj1jJPOVVnp/88bOh4BcM/lpWhd9A4ccetvC7oeCPQlVPrmaeWLGy4meN4Vmt+RVS6V4NvwF0fwy4I4eZsgAAAABJRU5ErkJggg==" />
                </svg>

                <p>
                    <strong>Personnalisable</strong><br>
                    <span class="text-grey-dark px-2">ne recevez que ce qui vous intéresse</span>
                </p>
            </li>
        </ul>
    </div>
</section>
