<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Vivaldi_Chronos
 */
?>

<li class="article-item">
  <article class="article article--news">
    <ul class="article__category-list">
      <?php
        v_show_categories_tags();
      ?>
    </ul>

    <div class="article__card">
      <header class="article__header">
        <p class="article__publish">
          <span class="article__publish-label">Publié le </span>
          <?php v_show_date() ?>
        </p>
      </header>

      <?php v_show_title() ?>

      <?php v_show_excerpt() ?>

      <?php v_show_read_more() ?>
    </div>
  </article>
</li>
