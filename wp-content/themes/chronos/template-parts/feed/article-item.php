<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Vivaldi_Chronos
 */

  $is_important = in_category("important");
  $article_classes = "article" . ($is_important ? " article--important" : "" );

  $has_thumbnail = get_the_post_thumbnail_url();
  $article_classes .= $has_thumbnail ? " article--thumbnail" : "" ;

  $thumbnail_styles = "";
  if($has_thumbnail) {
    $thumbnail_styles = '
      background: linear-gradient(
          to bottom,
          rgba(0, 0, 0, 0),
          rgba(0, 0, 0, 0.6)
        ),
        url("' . $has_thumbnail . '");
      background-size: cover;
    ';
  }
?>

<li class="article-item " style='<?php echo($thumbnail_styles)?>'>
  <article class="<?= $article_classes?>">
    <ul class="article__category-list">
      <?php
        v_show_categories_tags();
      ?>
    </ul>


    <div class="article__card">
      <header class="article__header">
        <p class="article__publish">
          <span class="article__publish-label">Publié le </span>
          <?php v_show_date() ?>
        </p>

        <?php if($is_important) echo('<p class="article__important">important</p>') ?>
      </header>

      <?php v_show_title() ?>

      <?php v_show_excerpt() ?>

      <?php v_show_read_more() ?>
    </div>
  </article>
</li>
