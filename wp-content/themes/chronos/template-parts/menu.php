
<nav class="js-nav nav fixed h-screen w-screen pin z-20">
  <button class="nav__close fixed pin-r pin-t pt-8 pr-8" type="button">
    <svg class="nav__close-icon w-8 h-8"width="64" version="1.1" xmlns="http://www.w3.org/2000/svg" height="64" viewBox="0 0 64 64" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 64 64">
      <g>
        <path fill="#1D1D1B" d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59   c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59   c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0   L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z"/>
      </g>
    </svg>
  </button>

  <div class="nav__body flex-1 p-8 flex flex-col justify-around items-center">
    <ul class="nav__category-list list-reset">
    <?php
    $excluded_categories = array("Non classé", "A la une", "important");
    $categories = get_categories(array(
        'orderby' => 'name',
        'order'   => 'ASC',
        'parent' => 0
    ));

    foreach($categories as $category) {
      if(!in_array($category->name, $excluded_categories)) {
        $subcategories = get_categories(array(
            'orderby' => 'name',
            'order'   => 'ASC',
            'parent' => $category->term_id
        ));

        echo('
        <li class="nav__category-list-item js-nav__category-list-item mt-4">
          '.$category->name.'
        </li>'
        );

        echo('
        <li class="js-nav__subcategory nav__subcategory">
          <ul class="list-reset p-4 border-l-2 border-red">');

        foreach($subcategories as $subcategory) {
          echo('
            <li class="nav__subcategory-list-item">
              <a class="nav__subcategory-link" href="/'.$subcategory->slug.'">'.
                $subcategory->name
              .'</a>
            </li>
          ');
        }

        echo('
          </ul>
        <li>');
      }
    }
    ?>
    </ul>

    <a class="header__discover-link md:hidden md:block w-64 text-center lg:text-right mt-4 font-bold uppercase text-red no-underline hover:underline" href="/qu-est-ce-que-chronos">
        qu'est-ce que chronos ?
    </a>

    <form role="search" method="get" class="header__search md:hidden self-center m-0 border-b border-t py-4 border-black mt-4" action="<?php echo(get_site_url())?>">
        <input class="header__seach-input w-64" type="search" placeholder="Rechercher un article…" value="" name="s">
        <button type="submit">
            <svg class="header__glass-icon align-text-top" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                width="0.229in" height="0.243in">
                <path fill-rule="evenodd" stroke="rgb(5, 7, 8)" stroke-width="1px" stroke-linecap="butt"
                    stroke-linejoin="miter" fill="none" d="M15.389,15.182 L14.682,15.889 L10.935,12.142 C9.840,12.981 8.486,13.500 7.000,13.500 C3.410,13.500 0.500,10.590 0.500,7.000 C0.500,3.410 3.410,0.500 7.000,0.500 C10.590,0.500 13.500,3.410 13.500,7.000 C13.500,8.742 12.805,10.316 11.690,11.483 L15.389,15.182 Z" />
            </svg>
        </button>
    </form>
  </div>
</nav>
