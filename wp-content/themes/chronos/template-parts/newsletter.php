<aside class="newsletter fixed pin-t pin-l js-newsletter w-full h-full bg-grey z-20 flex flex-col overflow-y-auto">
  <div class="newsletter__header-wrapper bg-white py-4">
    <header class="newsletter__header container flex justify-around md:justify-between flex-wrap items-center">
      <?php echo(v_logo()) ?>

      <h2 class="newsletter__title pb-2">Inscription à la newsletter</h2>

      <button class="js-newsletter__close newsletter__close-button font-bold uppercase text-sm p-2" type="button">
        fermer
        <svg
          class="align-top"
          xmlns="http://www.w3.org/2000/svg"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          width="0.222in" height="0.222in">
          <path fill-rule="evenodd"  fill="rgb(0, 0, 0)"
          d="M15.778,14.364 L14.364,15.778 L8.000,9.414 L1.636,15.778 L0.222,14.364 L6.586,8.000 L0.222,1.636 L1.636,0.222 L8.000,6.586 L14.364,0.222 L15.778,1.636 L9.414,8.000 L15.778,14.364 Z"/>
        </svg>
      </button>
    </header>
  </div>

  <div class="newsletter__content-wrapper container flex-1 py-8">
      <p class="text-red font-bold w-full text-center mb-8">Personnalisez votre newsletter en fonction de vos préférences</p>

      <div class="w-full mb-8 flex flex-wrap">
        <!-- A PROPOS DE VOUS -->
        <div class="md:w-1/2 flex items-center my-4">
          <svg
            class="mr-4"
            xmlns="http://www.w3.org/2000/svg"
            xmlns:xlink="http://www.w3.org/1999/xlink"
            width="0.569in" height="0.403in">
            <image  x="0px" y="0px" width="41px" height="29px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACkAAAAdCAMAAAAAVbV/AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAACClBMVEX///8AAACVGx4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABODhBSDxFQDxAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABLDg9xFRd7FhlPDhAAAAAAAAAAAAAAAAAAAAAAAAAAAABlEhR2FRhjEhRvFBZxFRcAAAAAAAAAAAAAAAAAAAAAAAB4FhhLDg9/FxqKGRwAAAAAAAAAAAAAAACNGhxNDhBLDg8AAAAAAAAAAAAAAACGGBtNDg8AAAAAAABhEhRsFBZmExVNDhBPDhBNDg+JGRxODhBdERNnExViEhRLDg8AAAAAAACOGh15FhgAAAAAAAAAAAAAAABwFBZbERJODhBLDg9PDhBWEBF+FxkAAAAAAAAAAAAAAAAAAABSDxGPGh1cERJ1FRhYEBIAAAAAAAAAAAAAAABiEhRwFBdjEhR6Fhh1FRgAAAAAAABVDxFrExZLDg9LDg+IGRthEhQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACVGx6RGh2PGh2SGh2QGh2TGx7///8R/pCjAAAApnRSTlMAAABIsOTw7M+BECpXOAOb/tTAxe/lMRTm8zGCJAZz++cTxu3A4eUW+d0NcoXvCvf9Wm0F0v4fDnlDyO78JYBAtdzKGzwj/TCnz7wIfsP+8V4c8tPjmzcEOnn3Cts8v2BV/pzrhHFQ0OG55L/y7HDgcNgGAv24Kk3O9mYEFZf8vQ5i10kBX+3MQor4rRkYuWQiuFlG6qudC0799WUdwtgvplE+fDDN/Jmr4gAAAAFiS0dEAIgFHUgAAAAHdElNRQfiCxQKEBKLHwc/AAABvklEQVQ4y2NggAJGRmYWVjZ2Dk4uRjBg4uZBArxMDHDAx79MQFBIWGSZqBhIobjEchQgKQVXKb1MWAaoQlZOXkERSCstV1ZBAqrL1WAK1TU0tSDWai/TAZK6K/SYkIG+AUyl4TIjiEJGYxETkEpTJiY9MygwZ2LSNYWptFhmCVXJaGUNVWkDc+RKWySVdsvsYSqF2KAqHRyhwEmPgEpnFyhwZSKg0g1m+yp3Aio9PKHAyxtJpQ9CpS9YpR9KIDH5w1UGgEMRBAKDgoFkyPJQlzAmpvAIiEsjV0fBVEbHLIuFqIxbFg8kExKB7ksST4Y5dU0KPDZTl6VBVKZngCnezKzl2TnLcyEuzU5ApJA8uMp8SFpiKli+ZnlhEcyh+FUudypmIkalS0kpkufhCsvilpVDVFZUVlUzYgKoupraumX1DRChRrZlTc04VLa0ti1r74CLcXV2Levuwaayt2+ZdX8LsuiEicvYJmGqnDxl2VR1UI5DBpOsl02bjqZyxsxZsyF5EwXM8V02lw9V5bxZ8xmwqGTkW7BsIarKZX0MWFUyLlq2GE0lCw6VS5bZka2SXQg7aEdXuZQNF2hD9REAmLqX6HNEKTEAAAAASUVORK5CYII=" />
          </svg>
          <p>
            <strong class="font-bold capitalize">a propos de vous</strong><br>
            Vos informations ne seront <span class="underline">jamais</span> communiquées à des tiers
          </p>
        </div>

        <!-- VOS PREFERENCES -->
        <div class="md:w-1/2 flex items-center my-4">
          <svg
            class="mr-4"
            xmlns="http://www.w3.org/2000/svg"
            xmlns:xlink="http://www.w3.org/1999/xlink"
            width="0.389in" height="0.417in">
            <image  x="0px" y="0px" width="28px" height="30px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAeCAMAAAAIG46tAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABlVBMVEX///+VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx4AAAAAAAAAAACVGx6VGx6VGx6VGx4AAAAAAACVGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx4AAAAAAACVGx6VGx6VGx4AAAAAAACVGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx4AAAAAAACVGx6VGx4AAAAAAACVGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx6VGx4AAAD///8uBGmcAAAAhHRSTlMACleBeD1O5ztDwWj63Bou9hIfhf0csPlJNWXTGfWxo1tp0hVCkHGfyQ9ZgdbiBxbQe2SnLPcBmezDVVK35NugCVN8djo/5bsr6CLMBHddskBvnvGshNfhBjmCJB7qVLzeoU1wM7gTKvTrjsit+/OiPGr+8Inc1uAFNoNr1/ghplhcqS94riMjAAAAAWJLR0QAiAUdSAAAAAd0SU1FB+ILFAorGSB/MY8AAAFPSURBVCjPdZBpWwFhFIYPqREzEYNKISrRooRWkgqtSopK+6pN+753+t+ZS1/MHM+X8+E+7/Ne5wYAmbxCUQlQxShBkmoGC1GpWa5GwjRaRl6r0/PIGaQPjaa6eoAG3twoZdBksfK2ZrQ7AFpaS9MGvNPA8djucuug47c0nWDv6vb0qFxK7CVqvdgnA/Bxfh0BA/04YBnk2SEgMzwSDI36aAbhscj4RBk2GRUMxRwUi2unbJ5pPT8zS0AjGxfGHM5ThhLFj01WWBAZWgQ+WVxaSsGyyNAKmNMZga3iGlGrRmsYYD3kz1KGnLiRirGmTfpQ+RazvVPOUGZXsbdfhinTgqGDQ4odHee8hvgJnlKGzs41woigm4AXRUMyNg+XIkNXcP1v6CYFtyJDd3D/8CiwJ3wmal8wWfCXfX17pwzlMfqR8LOf5J2BL1Uu+P1DoT/Ep1mEnmSkHAAAAABJRU5ErkJggg==" />
          </svg>

          <p>
            <strong class="font-bold capitalize">vos préférences</strong><br>
            Cochez les catégories qui vous intéressent
          </p>
        </div>
      </div>
      <?php if ( is_active_sidebar( 'newsletter' ) ) : ?>
      <?php dynamic_sidebar( 'newsletter' ); ?>
      <?php endif; ?>
 </div>

  <!-- NOS ENGAGEMENTS -->
  <div class="js-newsletter__promises" style="margin: 2rem 0 !important;">
    <div class="flex items-center">
      <svg
        class="mr-4"
        xmlns="http://www.w3.org/2000/svg"
        xmlns:xlink="http://www.w3.org/1999/xlink"
        width="0.486in" height="0.486in">
        <image  x="0px" y="0px" width="35px" height="35px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAjCAMAAAApB0NrAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABnlBMVEX///8AAACWGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx4AAAAAAAAAAAAAAAAAAACWGx6WGx4AAAAAAAAAAAAAAAAAAAAAAACWGx4AAAAAAAAAAAAAAACWGx6WGx4AAAAAAAAAAACWGx6WGx6WGx4AAAAAAACWGx6WGx6WGx6WGx6WGx6WGx4AAACWGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx4AAACWGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx6WGx4AAAAAAABLDg8OAgMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACWGx4AAAD///9MpP7zAAAAh3RSTlMAAAbM9eKEAxCUkP2AQI3mD15/gGRQC0/v/uTguhM0+q8bMje10AWJ9Ar1b0v7pqCDIFEZh30ok68EvcDC7PY4UJXQ+vB2ysHgpA5o83nox5zvcxZNPiRk1sirUhIef1Ewt6y6NEz8XwILq4/KYGX7e5aVkI5VlMJMrM1p2bRbAXiIgn3T1Wux1SpkAAAAAWJLR0QAiAUdSAAAAAd0SU1FB+ILFAoYLIKnkJwAAAFySURBVDjLhdP3U8IwGIDhxAHIcANuQa2iiIgbwQG4B6K4B+4JuHAvcCZ/tqkKNKWS94e0d3nu+vXSAvAXFJaRmZUtkwNxlFGgHKVKzTIakJsn2M0vKCyiKv4xWqRLEH1JaVk5VQWsJEaDFHFSVW0wpjyrBtUKTR1XnzpPg6lRaJrMqTM3W2SQmBYrqdUGAG5LmvYOpVLZ2dWt6rFDYOt1kJx9/QMADyaNy+R2uz1oaFgOYfw5upFRylgdZBkbn+DvE+89OSVh1JAy0162sTrZZsbHNrNzTONH80yzgBaZZsljZ5rcZSBhXJRZWaWNz0sO0dS/tq6ItxFAm7TZ2ianuLOLBFkCgDZ/7WkT7fv5vYNDiW8+Oc/RMVm4k7QmGCJLGJ+mMfqzc/5ygUOXkciV8WfnOiLs5hYbfr/2u/sHjsOHPHnET5yw55eo4EfEYULMOBgVzQNEJoZfU2YWGYpImzf8DhnmA8cgy4iIlPn8gv+abzgYZyY13rBFAAAAAElFTkSuQmCC" />
      </svg>
      <p>
        <strong class="font-bold capitalize">nos engagements</strong><br>
        Une newsletter conçue pour <span class="underline">vous</span>
      </p>
    </div>
    <p class="mt-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus, quisquam quod. Possimus quasi delectus unde asperiores iure obcaecati explicabo voluptatibus repellat, laboriosam magni, repellendus vero blanditiis saepe tenetur, sint accusantium.</p>
  </div>
</aside>
