<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Vivaldi_Chronos
 */
get_header();
?>

<main class="main bg-grey-light">
    <div class="main__container single-article container px-4 flex flex-wrap">
			<article class="article flex-1">
				<header>
					<h1 class="article__title  md:w-3/4 text-2xl my-8 text-center md:text-left">
						<?php the_title() ?>
					</h1>

        <div class="article__body bg-white my-8">
					<header class="article__excerpt bg-grey p-8">
						<?php v_show_excerpt(); ?>
					</header>

					<div class="article__content p-8 ">
						<?php the_content() ?>
					</div>
				</div>
			</article>

      <?php get_sidebar(); ?>
		</div>
</main>

<?php get_footer(); ?>
