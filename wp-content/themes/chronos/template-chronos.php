<?php
/*
Template Name: Modèle chronos
Template Post Type: page
 */
get_header();
get_template_part('template-parts/menu');
get_template_part('template-parts/newsletter');
?>

<main class="main bg-grey-light mt-12 md:mt p-8">
    <div class="main__container container md:flex flex-wrap p-0">

			<article class="article flex-1">
				<header>
					<h1 class="article__title  md:w-3/4 text-2xl my-8 text-center md:text-left">
						Qu'est-ce que Chronos ?
					</h1>
        </header>

        <div class="article__body bg-white my-8">
				  <header class="article__excerpt bg-grey p-8">
						"Se doter d'un outil de veille qui correspond à mes secteurs d'activités."
          </header>

          <div class="article__content p-8 pt-0">
						<div class="chronos__animation pb-8">
							<span class="chronos__animated-word">Affaires et patrimoine</span>
							<span class="chronos__animated-word">Assurances</span>
							<span class="chronos__animated-word">Banque</span>
							<span class="chronos__animated-word">Bourse</span>
							<span class="chronos__animated-word">Crédit</span>
							<span class="chronos__animated-word">Baux commerciaux</span>
							<span class="chronos__animated-word">Cession</span>
							<span class="chronos__animated-word">Acquisition</span>
							<span class="chronos__animated-word">Transmission d'Entreprise</span>
							<span class="chronos__animated-word">Concurrence</span>
							<span class="chronos__animated-word">Consommation</span>
							<span class="chronos__animated-word">Construction</span>
							<span class="chronos__animated-word">Urbanisme</span>
							<span class="chronos__animated-word">Droit des sociétés</span>
							<span class="chronos__animated-word">Droit des sûretés</span>
							<span class="chronos__animated-word">Mesure d'exécution</span>
							<span class="chronos__animated-word">Recouvrement</span>
							<span class="chronos__animated-word">Droit fiscal</span>
							<span class="chronos__animated-word">Droit public économique</span>
							<span class="chronos__animated-word">Entreprises en difficulté</span>
							<span class="chronos__animated-word">Environnement</span>
							<span class="chronos__animated-word">Energie</span>
							<span class="chronos__animated-word">Immobilier</span>
							<span class="chronos__animated-word">Pénal</span>
							<span class="chronos__animated-word">Propriété intellectuelle</span>
							<span class="chronos__animated-word">Nouvelles technologies</span>
							<span class="chronos__animated-word">Ressources humaines</span>
							<span class="chronos__animated-word">Vivaldi Chronos</span>
						</div>

						<p class="mb-8">
							Plus qu’une simple newsletter, Chronos est un véritable <strong>outil de veille juridique personnalisé</strong> au service des professionnels du droit, du chiffre mais également de tous ceux, qui régulièrement confrontés aux normes juridiques, doivent prendre des décisions dans le cadre de leur activité professionnelle.
						</p>

						<p class="mb-8">
							Conçu sous un format « 20 minutes », Chronos vous offre <strong>l’essentiel de l’information</strong> dans les domaines que vous décidez de choisir.
						</p>

						<p class="mb-16">
							Vous économisez ainsi du temps en filtrage de l’information, en analyse de considérations juridiques qui ne sont pas nécessairement indispensables au stade de la transmission de l’information.
						</p>

						<div class="flex flex-wrap">
							<div class="w-full md:w-1/2 md:pr-4">
								<p>
									Les informations collectées sont un outil d’aide à la prise de décision que vous pourrez compléter soit en recherchant les articles de fond publiés sur notre site Internet, soit en contactant notre cabinet.
								</p>

								<form role="search" method="get" class="header__search m-0 border-b border-black mt-4" action="<?php echo (get_site_url()) ?>">
									<input class="header__seach-input" type="search" placeholder="Rechercher un article…" value="" name="s">
									<button class="float-right" type="submit">
											<svg class="header__glass-icon align-text-top" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
													width="0.229in" height="0.243in">
													<path fill-rule="evenodd" stroke="rgb(5, 7, 8)" stroke-width="1px" stroke-linecap="butt"
															stroke-linejoin="miter" fill="none" d="M15.389,15.182 L14.682,15.889 L10.935,12.142 C9.840,12.981 8.486,13.500 7.000,13.500 C3.410,13.500 0.500,10.590 0.500,7.000 C0.500,3.410 3.410,0.500 7.000,0.500 C10.590,0.500 13.500,3.410 13.500,7.000 C13.500,8.742 12.805,10.316 11.690,11.483 L15.389,15.182 Z" />
											</svg>
									</button>
								</form>
							</div>

							<p class="w-full md:w-1/2 mt-5 md:mt-0 md:pl-4">
								Enfin, si vous souhaitez bénéficier de plus amples renseignements sur une décision, un article de loi, avoir la copie intégrale d’un texte, vous pouvez nous contacter.

								<br>

								<a class="block mt-4 border border-black p-4 font-bold text-black no-underline text-center" href="mailto:contact@vivaldi-avocats.com">Contactez-nous<a>
							</p>
						</div>
          </div>
        </div>

			</article>

      <?php get_sidebar();?>
		</div>
</main>

<?php get_template_part('template-parts/footer-single');?>

<?php wp_footer();?>
</body>
</html>
