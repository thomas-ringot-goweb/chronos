<?php
function v_logo($classes = null)
{
    $custom_logo_id = get_theme_mod('custom_logo');
    $html = sprintf('<a href="%1$s" class="hover:zoom ' . $classes . '" rel="home" itemprop="url">%2$s</a>',
        esc_url(get_site_url()),
        wp_get_attachment_image($custom_logo_id, 'full', false)
    );
    return $html;
}

function v_show_categories_tags()
{
    $categories = get_the_category();

    foreach ($categories as $category) {
        $category_link = sprintf(
            '<li class="article__category-item">' .
            '  <a class="article__category-link" href="%1$s" alt="%2$s">%3$s</a>' .
            '</li>',
            esc_url(get_category_link($category->term_id)),
            esc_attr(sprintf(__('Voir tout les articles de la catégorie "%s"', 'textdomain'), $category->name)),
            esc_html($category->name)
        );

        echo ($category_link);
    }
}

function v_show_date()
{
    echo ('<time class="article__publish-date" datetime="2008-02-14 20:00">' . get_the_date() . '</time>');
}

function v_show_title()
{
    echo ('<h2 class="article__title">' . get_the_title() . '</h2>');
}

function v_show_excerpt()
{
    echo ('<p class="article__excerpt max-8lines">' . wp_strip_all_tags(get_the_excerpt()) . '</p>');
}

function v_show_read_more()
{
    echo ('
        <a class="article__read-more hvr-underline-from-left pb-1" href="' . get_permalink() . '">
          Lire l\'article
            <svg
                class="article__read-more-icon"
                xmlns="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                width="0.292in" height="0.194in">
                <image  x="0px" y="0px" width="21px" height="14px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAOCAQAAABqD59FAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfiChcOIDs+N4ciAAAAT0lEQVQoz2P4z4AdMjAwMDBkMIgyIAAepTkM/xmuICnGo1SM4QqKYrwOEEVTjBeQp5gRZhdBkMtEpEKiAMK1eEMA1VskBBZ1ooCEiEVLLgAXr4kwZTWFRgAAAABJRU5ErkJggg==" />
            </svg>
        </a>');
}

function v_show_cta($content, $style = "default", $sup_classes = "", $href = "#")
{
    $classes = "cta no-underline hvr-float-shadow" . ($style == "grey" ? " border border-grey bg-grey font-black" : "") . ' ' . $sup_classes;

    echo ('
        <a class="' . $classes . '" href="' . $href . '">
            ' . $content . '
            <svg
                class="cta-icon ml-4 align-middle"
                xmlns="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                width="0.292in" height="0.194in">
                <image  x="0px" y="0px" width="21px" height="14px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAOCAQAAABqD59FAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfiChcOIDs+N4ciAAAAT0lEQVQoz2P4z4AdMjAwMDBkMIgyIAAepTkM/xmuICnGo1SM4QqKYrwOEEVTjBeQp5gRZhdBkMtEpEKiAMK1eEMA1VskBBZ1ooCEiEVLLgAXr4kwZTWFRgAAAABJRU5ErkJggg==" />
            </svg>
        </a>');
}

function v_show_category_picker($style = "default")
{
    $classes = "category-picker hvr-outline-in" . ($style == "with_border" ? " border border-black" : "");
    echo ('
        <div class="' . $classes . '">
            Catégories
            <svg
                class="category-picker__icon ml-4"
                xmlns="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                width="0.167in" height="0.097in">
                <image  x="0px" y="0px" width="12px" height="7px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAHCAMAAAALbFwWAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAS1BMVEX///8FBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwgFBwj///9Y64XuAAAAF3RSTlMAMroTtNEWDMDWGQrZHQi14gaxswXDxM5+zsUAAAABYktHRACIBR1IAAAAB3RJTUUH4goXESgtFUBcNgAAAERJREFUCNctizcSwCAQxJYjZ3Da///UA7YqFRKUaGy0KBhat9xZGvjAmIAUGTyQhaXWQskraJ1jsLdvnQd5Tvxc97P9BWV/ApI6af/OAAAAAElFTkSuQmCC" />
            </svg>
        </div>
        ');
}

function v_show_article_navigation()
{
    // data
    $previous_post = get_previous_post(true);
    $next_post = get_next_post(true);

    // markup
    $previous_post_link = $previous_post
    ?
    '<a class="article__previous flex  border-b border-black md:border-0 pb-4 md:pb-0 mb-4 md:mb-0 no-underline text-black hover:zoom" href="' . $previous_post->guid . '">
            <div class="article__previous-icon-box hidden md:block mr-4">
                <img src="/wp-content/uploads/2018/12/single__arrow-rleft.png" alt="" />
            </div>

            <div class="article__previous-label md:w-3/4">
                <strong>Article précédent</strong><br>
                ' . $previous_post->post_title . '
            </div>
        </a>'
    : '';

    $next_post_link = $next_post
    ?
    '<a class="article__previous flex no-underline text-black hover:zoom" href="' . $next_post->guid . '">
            <div class="article__previous-label md:w-3/4">
                <strong>Article suivant</strong><br>
                ' . $next_post->post_title . '
            </div>

            <div class="article__previous-icon-box hidden md:block">
                <img src="/wp-content/uploads/2018/12/single__arrow-right.png" alt="" />
            </div>
        </a>'
    : '';

    echo ('
        <div class="article__navigation flex flex-wrap justify-between my-12 px-4">
            <div class="w-full md:w-1/2 pr-4 flex">'
        . $previous_post_link .
        '</div>

            <div class="w-full md:justify-end md:w-1/2 pr-4 flex">'
        . $next_post_link .
        '</div>
        </div>'
    );
}

function v_show_article_sources()
{
    $source_list = CFS()->get("sources");

    if ($source_list) {
        $sources_markup = '';

        foreach ($source_list as $key => $source_item) {
            $url = ($source_item['source']['url']);
            $text = ($source_item['source']['text']);
            $target = ($source_item['source']['target']);

            $sources_markup .= '<a class="article__source-link text-sm text-grey-dark italic" href="' . $url . '" target=' . $target . '>' . $text . '</a>';
        }

        echo ('
      <div class="article__source-content flex flex-col text-right flex-end mr-4">

      <div class="article__source-label font-bold">Source</div>
      ' . $sources_markup . '
      </div>

      <div class="article__source-icon-box flex items-center justify-center">
      <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      width="0.625in" height="0.625in">
      <image  x="0px" y="0px" width="45px" height="45px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC0AAAAtCAQAAACQEyoRAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfiChkRKii3w70IAAADXUlEQVRIx8WXTWhcVRTHf/My+WiTiQrVQrW2MZlQnIqUiOJCsEFqXbmQgsSPhbhxVVyVuDMLU+rGVReKH61K0ZWCgiDRjQ1BTacISts4TdPGRduFmfOm7Xudee/vIpNJKu8rY4Lnre7j3N8779z/vfecnEg292FGGWEPO+njLqrUuMI5Zvmh8EfyzFw82t3Na7zkPNRBBw4OkENASEhAQHiRz/mocCkWoMjHhuykW/fUULw15Mmt20kbimZEYXvsbdfzFSrdQvlyPZuwngxoK1r5ZibsKv6mrGzFFLQ9a9Xb68Cu2G1Z1Q4moO2Q6zfaAC9n3vXtUAzaRl0/aBMsSYFc30Yj0Fa0pXYjXo3clmz4X2jrsTPt5Dgi52dW1OI05T2e39fJf7dO8vt4a82WsSHXW4/ckiyU6y1vojwAE13ducyRLfI11ynyPH1RJwdd3f4EY4CwQbeRPeb31dVcpx2aiYs7sEGBsEkvM/i40DM6rTkdU6e26+9IL092dBl9Kaua31O33tGK96TQJ5F+gWxBOG7J2eVkzPK9XGC8JaoDwHykn4PzoFvK83RH5gUcu2N0FRiI8ewg3J/nsSzoRT5lnt28ws7mmxrjbOeFWHR9BJtO395T6muqYmtTFUt6Uo6+jZ3RkE1jf6UJb0pbtEUfqqaK3tQHkq5pr9CJhDmhbBGrJqNn1K9eTa15s6i9QscTZ4WyKpbo8pvuVr9mJP2qV1VWoJ80KDSZmkRTYtSXdZ+2qSxJuq6B1lF8NBUcyixPjf44XbzBLU7zCADbOMvHnOUeXuTxLEp1ExRSETqSGl+0NWTTDufDmM9eBu5fM77FCYKMmyuE8w6zce47gO9bo+8o8hVZd24As1ipFvtbTwkd1kXN6bDQmPzMCanJSghbiDv5rujRlipeV/ZLOZAtWC4PnKof6Y78rQf4hW/4ma0c4ImMqQCow6mCcsIdzF3odbJfYGkmboQaLlQcKFT0RX3DwFBHXxYqbNKNXmzVIYU/dczboJg99G5hrpmZZvVU3qDqqbxSPa3WfMObVPNtaqW6qfW1EHaw7a7A7Ln/q5dpqmViXR2Yn7EDu7NvTFrWIKVvTOt2X3YGYrvdeT5L6nZzqT16if2MsIdd9NKPcYMFzjHLj4Xfk2f+A3LmTPeS6sl0AAAAAElFTkSuQmCC" />
      </svg>
      </div>
    ');
    }
}

function v_show_authors()
{
    global $wpdb;

    // On récupère la liste des auteurs dans un Array
    $author_name_list = explode(',', get_the_author());

    // On prépare la requête SQL selon le nombre d'auteurs
    $query = "SELECT * FROM $wpdb->users WHERE ";
    foreach ($author_name_list as $id => $author_name) {
        if ($id != 0) {
            $query .= "OR ";
        }

        $query .= "display_name = '" . trim($author_name) . "' ";
    }

    $query .= "LIMIT 1";

    // FIRE
    $author_meta_list = $wpdb->get_results($query);

    // On affiche chaque auteur avec son markup
    foreach ($author_meta_list as $author_meta) {

        $author_url = get_user_meta($author_meta->ID, 'molongui_author_image_url', true);
        if (empty($author_url)) {
            $author_url = "/wp-content/uploads/2018/11/avatar_par_defaut-1.png";
        }

        $author_name = $author_meta->display_name;

        echo ('<div class="mb-2 flex items-center">');
        echo ('  <img class="w-16 h-16 rounded-full float-left border border-grey" src="' . $author_url . '" alt="Avatar de l\'auteur" />');
        echo ('  <p class="ml-4 font-bold text-sm">' . $author_name . '</p>');
        echo ('</div>');
    }
}

function newsletter_widget_init()
{

    register_sidebar(array(

        'name' => 'Newsletter',
        'id' => 'newsletter',
        'before_widget' => '<div class="newsletter__acymailing">',
        'after_widget' => '</div>',
    ));
}

add_action('widgets_init', 'newsletter_widget_init');
