<?php
get_header();
get_template_part('template-parts/menu');
get_template_part('template-parts/newsletter');
?>

<main class="search-feed mb-16 mt-24 pt-8">
	<div class="search__container  mt-8 container px-4">
		<h1 class="home-title">
		<?php printf( esc_html__('Résultat de recherche pour : %s', 'chronos' ), '<span>' . get_search_query() . '</span>' ); ?>
		</h1>

		<?php
			if (have_posts())
			{
				echo('<ul class="article-list flex flex-wrap justify-center md:justify-between list-reset">');
				while (have_posts())
				{
					the_post();
					get_template_part( 'template-parts/feed/article-item');
				}
				echo('</ul>');
			}
			else
			{
				echo('
					<p class="text-center text-lg font-bold mb-32">Aucun résultat trouvé</p>
				');
			}
		?>
		</div> <!-- main__container -->
	<footer class="main__footer container px-4 text-center">
			<?php v_show_category_picker("with_border") ?>

			<?php v_show_cta("Tout les articles", "grey", "ml-4") ?>
	</footer>
</main><!-- #main -->

<!-- BANNER -->
<?php get_template_part('template-parts/footer'); ?>

<?php wp_footer();?>
</body>
</html>
