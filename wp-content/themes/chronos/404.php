<?php
get_header();
?>

<!-- MAIN FEED -->
<!-- show 9 last post with category "à la une" / .article .main-feed -->
<main class="mb-16 min-h mt-16 pt-8">
	<div class="mt-8 container px-4 flex-1">
		<h1 class="home-title">
			404
		</h1>

		<p class="text-center mb-8"> Cette page n'existe pas.</p>
		</div> <!-- main__container -->
	<footer class="main__footer container px-4 text-center">
			<?php v_show_category_picker("with_border")?>

			<?php v_show_cta("Tout les articles", "grey", "ml-4")?>
	</footer>
</main><!-- #main -->

<!-- BANNER -->
<?php get_template_part('template-parts/footer');?>

<?php wp_footer();?>
</body>
</html>
