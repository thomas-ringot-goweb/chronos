var gulp = require("gulp");
var sourcemaps = require("gulp-sourcemaps");
var sass = require("gulp-sass");
var postcss = require("gulp-postcss");
var babel = require("gulp-babel");

gulp.task("css", function() {
	var tailwindcss = require("tailwindcss");

	return gulp
		.src("sass/**/*.scss")
		.pipe(sass().on("error", sass.logError))
		.pipe(postcss([tailwindcss("./tailwind.js"), require("autoprefixer")]))
		.pipe(gulp.dest("./"));
});

gulp.task("js", function() {
	return gulp
		.src("js/main.js")
		.pipe(sourcemaps.init())
		.pipe(babel())
		.pipe(sourcemaps.write("."))
		.pipe(gulp.dest("./"));
});

// css
gulp.task("wcss", function() {
	gulp.watch("sass/**/*.scss", ["css"]);
});

// js
gulp.task("wjs", function() {
	gulp.watch("js/main.js", ["js"]);
});
