<?php
get_header();
get_template_part('template-parts/menu');
get_template_part('template-parts/newsletter');
?>

<main class="main bg-grey-light mt-24">
    <div class="main__container single-article container px-4 flex flex-wrap">

			<article class="article flex-1">
				<header>
					<h1 class="article__title  md:w-3/4 text-2xl my-8 text-center md:text-left">
						<?php the_title()?>
					</h1>

					<div class="article__meta flex flex-wrap items-center">
						<p class="article__publish w-full md:w-auto text-sm">
							<span class="article__publish-label">Publié le </span>
							<?php v_show_date()?>
						</p>

						<span class="article__dot hidden md:block text-grey-dark mx-4">•</span>

						<ul class="article__category-list text-sm list-reset flex-wrap inline-flex items-center">
							<?php	v_show_categories_tags()?>
						</ul>
					</div>
        </header>

        <div class="article__body bg-white my-8">
          <header class="article__excerpt bg-grey p-8">
            <?php v_show_excerpt();?>
          </header>

          <div class="article__content p-8 ">
            <?php
$default_styled_content = get_post_field('post_content');
$clean_content = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $default_styled_content);

echo ($clean_content)
?>
          </div>

          <div class="article__excerpt--end p-8 pb-12 border-b border-grey font-bold">
            <?php v_show_excerpt();?>
          </div>

          <footer class="article__footer flex">

            <div class="article__author-list w-1/2 p-8">
              <p class="text-sm font-thin mb-4">Article écrit par</p>

              <?php v_show_authors()?>
            </div>

            <div class="article__source w-1/2 p-8 flex justify-end">
              <?php
v_show_article_sources();
?>
            </div>
          </footer>
        </div>

        <?php v_show_article_navigation()?>
			</article>

      <?php get_sidebar();?>
		</div>
</main>

<?php get_template_part('template-parts/footer-single');?>

<!-- BANNER -->
<?php wp_footer();?>
</body>
</html>
