<!-- HEADER && MENU -->
<?php
get_header();
get_template_part('template-parts/menu');
get_template_part('template-parts/newsletter');
?>

<!-- MASHEAD -->
<section class="mashead bg-grey mt-24">
    <div class="mashead__container container pt-4 md:pt-8">
        <header class="mashead__header flex justify-between mt-8">
            <div class="mashead__news-label h-3-5r py-3 px-6 bg-red text-xs sm:text-base md:text-2xl flex justify-center items-center text-white font-bold">En ce moment</div>

            <?php v_show_category_picker()?>
        </header>

        <!-- ~show 9 last post with category "à la une" in article-item markup -->
        <ul class="js-carousel article--news-list list-reset pt-8">
            <?php

$query = new WP_Query(array('category_name' => 'a-la-une', 'posts_per_page' => 9));
if ($query->have_posts()) {
    while ($query->have_posts()) {
        $query->the_post();
        get_template_part('template-parts/feed/article-item--news');
    }
    wp_reset_postdata();
}
?>
        </ul> <!-- article--news-list -->
    </div> <!-- mashead__container -->
</section> <!-- mashead -->

<!-- BANNER -->
<?php get_template_part('template-parts/home/banner');?>

<!-- MAIN FEED -->
<!-- show 9 last post with category "à la une" / .article .main-feed -->
<main class="main-feed mb-16">
    <div class="main__container container px-4">

        <h1 class="home-title">
            L'actualité sur chronos
        </h1>

        <ul class="article-list flex flex-wrap justify-center md:justify-between list-reset">
            <?php
if (have_posts()):
    while (have_posts()):
        the_post();
        get_template_part('template-parts/feed/article-item');
    endwhile;
endif;
?>
        </ul> <!-- article-list -->
    </div> <!-- main__container -->


    <footer class="main__footer container px-4 text-center">
        <?php v_show_category_picker("with_border")?>

        <?php v_show_cta("Tout les articles", "grey", "home__read-all md:ml-4", "/tous-les-articles")?>
    </footer>
</main>

<?php get_template_part('template-parts/footer');?>
<?php wp_footer();?>

<!-- cookies scrips -->
<script type="text/javascript" id="cookieinfo"
	src="//cookieinfoscript.com/js/cookieinfo.min.js"
	data-bg="#E6E6E6"
    data-font-family="Futura"
    data-message="Nous utilisons les cookies sur ce site poyr améliorer votre experience utilisateur"
	data-fg="#000"
	data-link="#951C1F"
	data-divlink="#FFF"
    data-divlinkbg="#951C1F"
	data-cookie="CookieInfoScript"
	data-linkmsg="en savoir plus"
	data-text-align="center"
    data-accept-on-scroll="true"
    data-close-text="J'accepte">
</script>
</body>
</html>
