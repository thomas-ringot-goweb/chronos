<!doctype html>
<html lang="fr-FR">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <!-- HOVER.CSS LIB -->
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/chronos/css/hover.min.css"/>
    <!-- CAROUSEL LIB -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css"/>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-128070956-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-128070956-1');
    </script>

	<?php wp_head();?>
</head>

<body>

<header class="header bg-white w-full fixed pin-t pin-l z-10 h-24 border-b border-grey">
    <div class="header__container container flex py-4 items-center">
        <?php echo(v_logo("mr-auto")) ?>

        <form role="search" method="get" class="header__search hidden md:block m-0 border-b border-black mt-4" action="<?php echo(get_site_url())?>">
            <input class="header__seach-input w-64" type="search" placeholder="Rechercher un article…" value="" name="s">
            <button type="submit">
                <svg class="header__glass-icon align-text-top" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                    width="0.229in" height="0.243in">
                    <path fill-rule="evenodd" stroke="rgb(5, 7, 8)" stroke-width="1px" stroke-linecap="butt"
                        stroke-linejoin="miter" fill="none" d="M15.389,15.182 L14.682,15.889 L10.935,12.142 C9.840,12.981 8.486,13.500 7.000,13.500 C3.410,13.500 0.500,10.590 0.500,7.000 C0.500,3.410 3.410,0.500 7.000,0.500 C10.590,0.500 13.500,3.410 13.500,7.000 C13.500,8.742 12.805,10.316 11.690,11.483 L15.389,15.182 Z" />
                </svg>
            </button>
        </form>

        <a class="header__discover-link hidden md:block w-64 text-center lg:text-right mt-4 font-bold uppercase text-black no-underline hover:text-red" href="/qu-est-ce-que-chronos">
            qu'est-ce que chronos ?
        </a>

        <button class="header__hamburger js-header__hamburger md:hidden" type="button">
            <svg height="32px" id="Layer_1" style="enable-background:new 0 0 32 32;" version="1.1" viewBox="0 0 32 32" width="32px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M4,10h24c1.104,0,2-0.896,2-2s-0.896-2-2-2H4C2.896,6,2,6.896,2,8S2.896,10,4,10z M28,14H4c-1.104,0-2,0.896-2,2  s0.896,2,2,2h24c1.104,0,2-0.896,2-2S29.104,14,28,14z M28,22H4c-1.104,0-2,0.896-2,2s0.896,2,2,2h24c1.104,0,2-0.896,2-2  S29.104,22,28,22z"/></svg>
        </button>
    </div>
</header>
