<?php
/*
Template Name: Modèle Tous les articles
Template Post Type: page
*/
get_header();
get_template_part('template-parts/menu');
get_template_part('template-parts/newsletter');
?>

<!-- MAIN FEED -->
<!-- show 9 last post with category "à la une" / .article .main-feed -->
<main class="search-feed mb-16 mt-24 pt-8">
	<div class="search__container  mt-8 container px-4">
		<h1 class="home-title capitalize">
			tout les articles
		</h1>

		<?php
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			$args = array('post_type' => 'post', 'posts_per_page' => 9, 'paged' => $paged);
			$wp_query = new WP_Query($args);

			if (have_posts())
			{
				echo('<ul class="article-list flex flex-wrap justify-center md:justify-between list-reset">');
				while (have_posts())
				{
					the_post();
					get_template_part( 'template-parts/feed/article-item');
				}
				echo('</ul>');
			}
			else
			{
				echo('
					<p class="text-center text-lg font-bold mb-32">Aucun résultat trouvé</p>
				');
			}
		?>
		</div> <!-- main__container -->
	<footer class="main__footer container px-4 text-center">
			<?php v_show_category_picker("with_border") ?>
	</footer>
</main><!-- #main -->

<!-- BANNER -->
<?php get_template_part('template-parts/footer'); ?>
<?php wp_footer();?>
</body>
</html>
