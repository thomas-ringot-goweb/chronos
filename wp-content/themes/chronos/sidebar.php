<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Vivaldi_Chronos
 */
?>

<aside class="w-full md:w-2xs md:p-8 text-center md:text-left">
	<?php v_show_category_picker()?>

	<div class="widget-container">
		<section class="newsletter-widget">
			<header class="p-4 bg-grey-dark text-white font-bold text-center">
				Restez informé par mail de<br>
				l'actualité Chronos
			</header>

			<div class="newsletter-widget__body bg-white p-8 text-center">
				<div class="newsletter-widget__content">
					<div class="newsletter-widget__icon-box">
						<svg
							class="newsletter-widget__icon"
							xmlns="http://www.w3.org/2000/svg"
							xmlns:xlink="http://www.w3.org/1999/xlink"
							width="0.694in" height="0.361in">
							<image  x="0px" y="0px" width="50px" height="26px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAaCAQAAABfycdhAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfiChsWAzamzMGOAAACHklEQVRIx63WOWhUURTG8V+MMZqISiCKhSSFBBsXCDaCS++CClq4IW6goGAliGihwcoFbAQNiLVVFAQLl5CIiqIogRQWUdRELHQEJyExHotMYjLOzJs3+t3mcvh4//Puee+cS7J2eW5EpF7DumyGqkTENQfw048y0pmq2arR5lSScY+QsUNNagQzHZQV1icZ3wibKgCMa59wv7SlThjI7deZm+Lh860GNQYNlbY2C69Aq9BnRZmIVfqFFtAnyoXUeyRk7SwDccSwcMeMP5DXRT7ArikQalwWwmXTSwBqtQvhrGm5SJ/gWRHIgzwI7DYoPNBYBLHIU+G7LZNiKY5rXK3eCe+tLOBe67PQa8mUaAUQGt0XhuzNix81InSYkxevCEK1C0K4MvGLznJT+OV0gQ4yCdLia15NHhaFIId5ZAGavBDCpYLOKZBveZDOEpBthoWM8NFhX4RPwqiDpSFpjuuYUeGMBvdyCd0y23EhnPwfkCrnhFGHctVpM+hErhJ7/BQu5tUlNaRauzBoaxH/BlnhhupSkAu+Tay7f0Hq3BYy1pRIa7WM0KGuOOTqpMI/zoM06BY+WFr65S0zIHRO9OwUxzVPj9CrSbIWeyu8NCstZLnwpGjPytdCr43mEkqE1Av9uX2L2jIRUKcZ1MgmDS16/nH87k0ev2OmjO0VXSRq7ZMVNiZfia7bjxHZ1JB603G+QBcooMovd91j4+s3y9c7OXpi8wQAAAAASUVORK5CYII=" />
						</svg>
					</div>

					<p class="font-bold text-sm md:text-lg text-left float-right mb-8 inline-block">
						Inscrivez-vous<br>à notre newsletter !
					</p>
				</div>

				<?php v_show_cta("Je m'inscris", "default", "js-newsletter__open")?>
			</div>
		</section>
	</div>
</aside>
