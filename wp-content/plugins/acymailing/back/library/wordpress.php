<?php
/**
 * @package	AcyMailing for WordPress
 * @version	6.0.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

defined('ABSPATH') or die('Restricted access');
?><?php

define('ACYM_CMS', 'WordPress');
define('ACYM_COMPONENT', 'acymailing');
define('ACYM_DEFAULT_LANGUAGE', 'en-US');

define('ACYM_BASE', '');
define('ACYM_ROOT', rtrim(ABSPATH, DS.'/').DS);
define('ACYM_FOLDER', WP_PLUGIN_DIR.DS.ACYM_COMPONENT.DS);
define('ACYM_FRONT', ACYM_FOLDER.'front'.DS);
define('ACYM_BACK', ACYM_FOLDER.'back'.DS);
define('ACYM_VIEW', ACYM_BACK.'views'.DS);
define('ACYM_VIEW_FRONT', ACYM_FRONT.'views'.DS);
define('ACYM_HELPER', ACYM_BACK.'helpers'.DS);
define('ACYM_CLASS', ACYM_BACK.'classes'.DS);
define('ACYM_LIBRARY', ACYM_BACK.'library'.DS);
define('ACYM_TYPE', ACYM_BACK.'types'.DS);
define('ACYM_CONTROLLER', ACYM_BACK.'controllers'.DS);
define('ACYM_CONTROLLER_FRONT', ACYM_FRONT.DS.'controllers'.DS);
define('ACYM_MEDIA', ACYM_FOLDER.'media'.DS);
define('ACYM_LANGUAGE', ACYM_FOLDER.'language'.DS);
define('ACYM_INC', ACYM_FRONT.'inc'.DS);

define('ACYM_MEDIA_URL', plugins_url().'/'.ACYM_COMPONENT.'/media/');
define('ACYM_IMAGES', ACYM_MEDIA_URL.'images/');
define('ACYM_CSS', ACYM_MEDIA_URL.'css/');
define('ACYM_JS', ACYM_MEDIA_URL.'js/');
define('ACYM_DYNAMICS_URL', plugins_url().'/'.ACYM_COMPONENT.'/back/dynamics/');

define('ACYM_MEDIA_FOLDER', str_replace(ABSPATH, '', WP_PLUGIN_DIR).'/'.ACYM_COMPONENT.'/media');

define('ACYM_J30', true);
define('ACYM_J37', true);
define('ACYM_J40', true);

define('ACYM_ALLOWRAW', 2);
define('ACYM_ALLOWHTML', 4);

include_once(rtrim(__DIR__, DS).DS.'punycode.php');


global $acymLanguages;

function acym_getTime($date)
{
    static $timeoffset = null;
    if ($timeoffset === null) {
        $timeoffset = acym_getCMSConfig('offset');

        if (!is_numeric($timeoffset)) {
            $timezone = new DateTimeZone($timeoffset);
            $timeoffset = $timezone->getOffset(new DateTime);
        }
    }

    return strtotime($date) - $timeoffset + date('Z');
}

function acym_fileGetContent($url, $timeout = 10)
{
    ob_start();
    $data = '';

    if (strpos($url, 'http') === 0 && class_exists('WP_Http') && method_exists('WP_Http', 'request')) {
        $args = array('timeout' => $timeout);
        $request = new WP_Http();
        $data = $request->request($url, $args);
        $data = (empty($data) || !is_array($data) || empty($data['body'])) ? '' : $data['body'];
    }

    if (empty($data) && function_exists('file_get_contents')) {
        if (!empty($timeout)) {
            ini_set('default_socket_timeout', $timeout);
        }
        $streamContext = stream_context_create(array('ssl' => array('verify_peer' => false, 'verify_peer_name' => false)));
        $data = file_get_contents($url, false, $streamContext);
    }

    if (empty($data) && function_exists('fopen') && function_exists('stream_get_contents')) {
        $handle = fopen($url, "r");
        if (!empty($timeout)) {
            stream_set_timeout($handle, $timeout);
        }
        $data = stream_get_contents($handle);
    }
    $warnings = ob_get_clean();

    if (acym_isDebug()) {
        echo $warnings;
    }

    return $data;
}

function acym_formToken()
{
    return '<input type="hidden" name="_wpnonce" value="'.wp_create_nonce('acymnonce').'">';
}

function acym_checkToken()
{
    $token = acym_getVar('cmd', '_wpnonce');
    if (!wp_verify_nonce($token, 'acymnonce')) {
        die('Invalid Token');
    }
}

function acym_getFormToken()
{
    $token = acym_getVar('cmd', '_wpnonce', '');
    if (empty($token)) {
        $token = wp_create_nonce('acymnonce');
    }
    acym_setVar('_wpnonce', $token);

    return '_wpnonce='.$token;
}

function acym_setTitle($name, $picture = '', $link = '')
{
    return;
}

function acym_translation($key, $jsSafe = false, $interpretBackSlashes = true)
{
    global $acymLanguages;
    if (empty($acymLanguages['currentLanguage'])) {
        acym_getLanguageTag();
    }
    if (!isset($acymLanguages[$acymLanguages['currentLanguage']])) {
        acym_loadLanguage();
    }

    $translation = $key;
    foreach ($acymLanguages[$acymLanguages['currentLanguage']] as $fileContent) {
        if (isset($fileContent[$key])) {
            $translation = $fileContent[$key];
        }
    }
    if ($translation == $key && $acymLanguages['currentLanguage'] != ACYM_DEFAULT_LANGUAGE && isset($acymLanguages[ACYM_DEFAULT_LANGUAGE])) {
        foreach ($acymLanguages[ACYM_DEFAULT_LANGUAGE] as $fileContent) {
            if (isset($fileContent[$key])) {
                $translation = $fileContent[$key];
                break;
            }
        }
    }

    if ($jsSafe) {
        $translation = addslashes($translation);
    } elseif ($interpretBackSlashes && strpos($translation, '\\') !== false) {
        $translation = str_replace(array('\\\\', '\t', '\n'), array("\\", "\t", "\n"), $translation);
    }

    return str_replace('Joomla', 'WordPress', $translation);
}

function acym_translation_sprintf()
{
    $args = func_get_args();
    $args[0] = acym_translation($args[0]);
    $return = "return sprintf('".str_replace("'", "\\'", array_shift($args))."'";
    foreach ($args as $oneArg) {
        $return .= ",'".str_replace("'", "\\'", $oneArg)."'";
    }
    $return .= ');';

    return eval($return);
}

function acym_route($url, $xhtml = true, $ssl = null)
{
    return acym_baseURI().$url;
}

function acym_getVar($type, $name, $default = null, $hash = 'REQUEST', $mask = 0)
{
    $hash = strtoupper($hash);

    switch ($hash) {
        case 'GET':
            $input = &$_GET;
            break;
        case 'POST':
            $input = &$_POST;
            break;
        case 'FILES':
            $input = &$_FILES;
            break;
        case 'COOKIE':
            $input = &$_COOKIE;
            break;
        case 'ENV':
            $input = &$_ENV;
            break;
        case 'SERVER':
            $input = &$_SERVER;
            break;
        default:
            $hash = 'REQUEST';
            $input = &$_REQUEST;
            break;
    }

    if (!isset($input[$name])) {
        return $default;
    }

    $result = $input[$name];
    unset($input);
    if ($type == 'array') {
        $result = (array)$result;
    }

    if (in_array($hash, array('POST', 'REQUEST', 'GET', 'COOKIE'))) {
        $result = acym_stripslashes($result);
    }

    return acym_cleanVar($result, $type, $mask);
}

function acym_stripslashes($element)
{
    if (is_array($element)) {
        foreach ($element as &$oneCell) {
            $oneCell = acym_stripslashes($oneCell);
        }
    } elseif (is_string($element)) {
        $element = stripslashes($element);
    }

    return $element;
}

function acym_cleanVar($var, $type, $mask)
{
    if (is_array($var)) {
        foreach ($var as $i => $val) {
            $var[$i] = acym_cleanVar($val, $type, $mask);
        }

        return $var;
    }

    switch ($type) {
        case 'string':
            $var = (string)$var;
            break;
        case 'int':
            $var = (int)$var;
            break;
        case 'float':
            $var = (float)$var;
            break;
        case 'boolean':
            $var = (boolean)$var;
            break;
        case 'word':
            $var = preg_replace('#[^a-zA-Z_]#', '', $var);
            break;
        case 'cmd':
            $var = preg_replace('#[^a-zA-Z0-9_\.-]#', '', $var);
            $var = ltrim($var, '.');
            break;
        default:
            break;
    }

    if (!is_string($var)) {
        return $var;
    }

    $var = trim($var);

    if ($mask & ACYM_ALLOWRAW) {
        return $var;
    }

    if (!preg_match('//u', $var)) {
        $var = htmlspecialchars_decode(htmlspecialchars($var, ENT_IGNORE, 'UTF-8'));
    }

    if (!($mask & ACYM_ALLOWHTML)) {
        $var = preg_replace('#<[a-zA-Z/]+[^>]*>#Uis', '', $var);
    }

    return $var;
}

function acym_setVar($name, $value = null, $hash = 'REQUEST', $overwrite = true)
{
    $hash = strtoupper($hash);

    switch ($hash) {
        case 'GET':
            $input = &$_GET;
            break;
        case 'POST':
            $input = &$_POST;
            break;
        case 'FILES':
            $input = &$_FILES;
            break;
        case 'COOKIE':
            $input = &$_COOKIE;
            break;
        case 'ENV':
            $input = &$_ENV;
            break;
        case 'SERVER':
            $input = &$_SERVER;
            break;
        default:
            $input = &$_REQUEST;
            break;
    }

    if (!isset($input[$name]) || $overwrite) {
        $input[$name] = $value;
    }
}

function acym_raiseError($level, $code, $msg, $info = null)
{
    acym_display($code.': '.$msg, 'error');
    wp_die();
}

function acym_getGroupsByUser($userid = null, $recursive = null, $names = false)
{
    if ($userid === null) {
        $user = wp_get_current_user();
    } else {
        $user = new WP_User($userid);
    }

    return $user->roles;
}

function acym_getGroups()
{
    $groups = acym_loadResult('SELECT option_value FROM #__options WHERE option_name = "#__user_roles"');
    $groups = unserialize($groups);

    $usersPerGroup = acym_loadObjectList('SELECT meta_value, COUNT(meta_value) AS nbusers FROM #__usermeta WHERE meta_key = "#__capabilities" GROUP BY meta_value');

    $nbUsers = array();
    foreach ($usersPerGroup as $oneGroup) {
        $oneGroup->meta_value = unserialize($oneGroup->meta_value);
        $nbUsers[key($oneGroup->meta_value)] = $oneGroup->nbusers;
    }

    foreach ($groups as $key => $group) {
        $newGroup = new stdClass();
        $newGroup->id = $key;
        $newGroup->value = $key;
        $newGroup->parent_id = 0;
        $newGroup->text = $group['name'];
        $newGroup->nbusers = empty($nbUsers[$key]) ? 0 : $nbUsers[$key];
        $groups[$key] = $newGroup;
    }

    return $groups;
}

function acym_getLanguages($installed = false)
{
    $result = array();

    require_once(ABSPATH.'wp-admin/includes/translation-install.php');
    $wplanguages = wp_get_available_translations();

    $languages = get_available_languages();
    foreach ($languages as $oneLang) {
        $langTag = str_replace('_', '-', $oneLang);

        $lang = new stdClass();
        $lang->sef = empty($wplanguages[$oneLang]['iso'][1]) ? null : $wplanguages[$oneLang]['iso'][1];
        $lang->language = strtolower($langTag);
        $lang->name = empty($wplanguages[$oneLang]) ? $langTag : $wplanguages[$oneLang]['native_name'];
        $lang->exists = file_exists(ACYM_LANGUAGE.$langTag.'.'.ACYM_LANGUAGE_FILE.'.ini');
        $lang->content = true;

        $result[$langTag] = $lang;
    }

    if (!in_array('en-US', array_keys($result))) {
        $lang = new stdClass();
        $lang->sef = 'en';
        $lang->language = 'en-us';
        $lang->name = 'English (United States)';
        $lang->exists = file_exists(ACYM_LANGUAGE.'en-US.'.ACYM_LANGUAGE_FILE.'.ini');
        $lang->content = true;

        $result['en-US'] = $lang;
    }

    return $result;
}

function acym_languageFolder($code)
{
    return ACYM_LANGUAGE;
}

function acym_cleanSlug($slug)
{
    $slug = str_replace('-', ' ', $slug);

    $UTF8_LOWER_ACCENTS = array(
        'à' => 'a',
        'ô' => 'o',
        'ď' => 'd',
        'ḟ' => 'f',
        'ë' => 'e',
        'š' => 's',
        'ơ' => 'o',
        'ß' => 'ss',
        'ă' => 'a',
        'ř' => 'r',
        'ț' => 't',
        'ň' => 'n',
        'ā' => 'a',
        'ķ' => 'k',
        'ŝ' => 's',
        'ỳ' => 'y',
        'ņ' => 'n',
        'ĺ' => 'l',
        'ħ' => 'h',
        'ṗ' => 'p',
        'ó' => 'o',
        'ú' => 'u',
        'ě' => 'e',
        'é' => 'e',
        'ç' => 'c',
        'ẁ' => 'w',
        'ċ' => 'c',
        'õ' => 'o',
        'ṡ' => 's',
        'ø' => 'o',
        'ģ' => 'g',
        'ŧ' => 't',
        'ș' => 's',
        'ė' => 'e',
        'ĉ' => 'c',
        'ś' => 's',
        'î' => 'i',
        'ű' => 'u',
        'ć' => 'c',
        'ę' => 'e',
        'ŵ' => 'w',
        'ṫ' => 't',
        'ū' => 'u',
        'č' => 'c',
        'ö' => 'oe',
        'è' => 'e',
        'ŷ' => 'y',
        'ą' => 'a',
        'ł' => 'l',
        'ų' => 'u',
        'ů' => 'u',
        'ş' => 's',
        'ğ' => 'g',
        'ļ' => 'l',
        'ƒ' => 'f',
        'ž' => 'z',
        'ẃ' => 'w',
        'ḃ' => 'b',
        'å' => 'a',
        'ì' => 'i',
        'ï' => 'i',
        'ḋ' => 'd',
        'ť' => 't',
        'ŗ' => 'r',
        'ä' => 'ae',
        'í' => 'i',
        'ŕ' => 'r',
        'ê' => 'e',
        'ü' => 'ue',
        'ò' => 'o',
        'ē' => 'e',
        'ñ' => 'n',
        'ń' => 'n',
        'ĥ' => 'h',
        'ĝ' => 'g',
        'đ' => 'd',
        'ĵ' => 'j',
        'ÿ' => 'y',
        'ũ' => 'u',
        'ŭ' => 'u',
        'ư' => 'u',
        'ţ' => 't',
        'ý' => 'y',
        'ő' => 'o',
        'â' => 'a',
        'ľ' => 'l',
        'ẅ' => 'w',
        'ż' => 'z',
        'ī' => 'i',
        'ã' => 'a',
        'ġ' => 'g',
        'ṁ' => 'm',
        'ō' => 'o',
        'ĩ' => 'i',
        'ù' => 'u',
        'į' => 'i',
        'ź' => 'z',
        'á' => 'a',
        'û' => 'u',
        'þ' => 'th',
        'ð' => 'dh',
        'æ' => 'ae',
        'µ' => 'u',
        'ĕ' => 'e',
        'œ' => 'oe',
    );
    $slug = str_replace(array_keys($UTF8_LOWER_ACCENTS), array_values($UTF8_LOWER_ACCENTS), $slug);

    $UTF8_UPPER_ACCENTS = array(
        'À' => 'A',
        'Ô' => 'O',
        'Ď' => 'D',
        'Ḟ' => 'F',
        'Ë' => 'E',
        'Š' => 'S',
        'Ơ' => 'O',
        'Ă' => 'A',
        'Ř' => 'R',
        'Ț' => 'T',
        'Ň' => 'N',
        'Ā' => 'A',
        'Ķ' => 'K',
        'Ŝ' => 'S',
        'Ỳ' => 'Y',
        'Ņ' => 'N',
        'Ĺ' => 'L',
        'Ħ' => 'H',
        'Ṗ' => 'P',
        'Ó' => 'O',
        'Ú' => 'U',
        'Ě' => 'E',
        'É' => 'E',
        'Ç' => 'C',
        'Ẁ' => 'W',
        'Ċ' => 'C',
        'Õ' => 'O',
        'Ṡ' => 'S',
        'Ø' => 'O',
        'Ģ' => 'G',
        'Ŧ' => 'T',
        'Ș' => 'S',
        'Ė' => 'E',
        'Ĉ' => 'C',
        'Ś' => 'S',
        'Î' => 'I',
        'Ű' => 'U',
        'Ć' => 'C',
        'Ę' => 'E',
        'Ŵ' => 'W',
        'Ṫ' => 'T',
        'Ū' => 'U',
        'Č' => 'C',
        'Ö' => 'Oe',
        'È' => 'E',
        'Ŷ' => 'Y',
        'Ą' => 'A',
        'Ł' => 'L',
        'Ų' => 'U',
        'Ů' => 'U',
        'Ş' => 'S',
        'Ğ' => 'G',
        'Ļ' => 'L',
        'Ƒ' => 'F',
        'Ž' => 'Z',
        'Ẃ' => 'W',
        'Ḃ' => 'B',
        'Å' => 'A',
        'Ì' => 'I',
        'Ï' => 'I',
        'Ḋ' => 'D',
        'Ť' => 'T',
        'Ŗ' => 'R',
        'Ä' => 'Ae',
        'Í' => 'I',
        'Ŕ' => 'R',
        'Ê' => 'E',
        'Ü' => 'Ue',
        'Ò' => 'O',
        'Ē' => 'E',
        'Ñ' => 'N',
        'Ń' => 'N',
        'Ĥ' => 'H',
        'Ĝ' => 'G',
        'Đ' => 'D',
        'Ĵ' => 'J',
        'Ÿ' => 'Y',
        'Ũ' => 'U',
        'Ŭ' => 'U',
        'Ư' => 'U',
        'Ţ' => 'T',
        'Ý' => 'Y',
        'Ő' => 'O',
        'Â' => 'A',
        'Ľ' => 'L',
        'Ẅ' => 'W',
        'Ż' => 'Z',
        'Ī' => 'I',
        'Ã' => 'A',
        'Ġ' => 'G',
        'Ṁ' => 'M',
        'Ō' => 'O',
        'Ĩ' => 'I',
        'Ù' => 'U',
        'Į' => 'I',
        'Ź' => 'Z',
        'Á' => 'A',
        'Û' => 'U',
        'Þ' => 'Th',
        'Ð' => 'Dh',
        'Æ' => 'Ae',
        'Ĕ' => 'E',
        'Œ' => 'Oe',
    );
    $slug = str_replace(array_keys($UTF8_UPPER_ACCENTS), array_values($UTF8_UPPER_ACCENTS), $slug);

    $slug = trim(strtolower($slug));
    $slug = preg_replace('/(\s|[^A-Za-z0-9\-])+/', '-', $slug);
    $slug = trim($slug, '-');

    return $slug;
}

function acym_punycode($email, $method = 'emailToPunycode')
{
    if (empty($email)) {
        return $email;
    }

    $explodedAddress = explode('@', $email);
    $newEmail = $explodedAddress[0];

    if (!empty($explodedAddress[1])) {
        $domainExploded = explode('.', $explodedAddress[1]);
        $newdomain = '';
        $puc = new acympunycode();

        foreach ($domainExploded as $domainex) {
            $domainex = $puc->$method($domainex);
            $newdomain .= $domainex.'.';
        }

        $newdomain = substr($newdomain, 0, -1);
        $newEmail = $newEmail.'@'.$newdomain;
    }

    return $newEmail;
}

function acym_isAdmin()
{
    $page = acym_getVar('string', 'page', '');

    return !in_array($page, array(ACYM_COMPONENT.'_front', 'front'));
}

function acym_getCMSConfig($varname, $default = null)
{
    $map = array(
        'offset' => 'timezone_string',
        'list_limit' => 'posts_per_page',
        'sitename' => 'blogname',
        'mailfrom' => 'new_admin_email',
        'feed_email' => 'new_admin_email',
    );

    if (!empty($map[$varname])) {
        $varname = $map[$varname];
    }
    $value = get_option($varname, $default);

    if ($varname == 'timezone_string' && empty($value)) {
        $value = acym_getCMSConfig('gmt_offset');

        if (empty($value)) {
            $value = 'UTC';
        } elseif ($value < 0) {
            $value = 'GMT'.$value;
        } else {
            $value = 'GMT+'.$value;
        }
    }

    if ($varname == 'posts_per_page') {
        $possibilities = array(5, 10, 15, 20, 25, 30, 50, 100);
        $closest = 5;
        foreach ($possibilities as $possibility) {
            if (abs($value - $closest) > abs($value - $possibility)) {
                $closest = $possibility;
            }
        }
        $value = $closest;
    }

    return $value;
}

function acym_getCMSPosts($category, $keyword, $offset = 0)
{
    $args = array(
        's' => $keyword,
        'posts_per_page' => "10",
        'post_type' => 'post',
        'category__in' => $category,
        'offset' => $offset,
    );
    $the_query = new WP_Query($args);

    if ($the_query->have_posts()) {
        foreach ($the_query->posts as $post) {
            echo "<div class='cell acym__wysid__cms__post margin-bottom-1 padding-1'>";
            echo "<div class='cell acym__wysid__cms__post__title'><h3>".$post->post_title."</h3></div>";
            echo "<div class='cell acym__wysid__cms__post__content'><p>".$post->post_content."</p></div>";
            echo "</div>";
        }
    }
}

function acym_getCMSCategories()
{
    $args = array(
        'orderby' => 'name',
        'order' => 'ASC',
    );
    $categories = get_categories($args);

    echo "<option value=''>All categories</option>";
    foreach ($categories as $category) {
        echo "<option value='".$category->term_id."'>".$category->name."</option>";
    }
}

function acym_addPageParam($url, $ajax = false, $front = false)
{
    preg_match('#^([a-z]+)(?:[^a-z]|$)#Uis', $url, $ctrl);

    if ($front) {
        if ($ajax) {
            $link = 'admin-ajax.php?page='.ACYM_COMPONENT.'_front&ctrl='.$url.'&action='.ACYM_COMPONENT.'_frontrouter&'.acym_noTemplate();
        } else {
            $link = 'admin.php?page='.ACYM_COMPONENT.'_front&ctrl='.$url;
        }
        $link = 'wp-admin/'.$link;
    } else {
        if ($ajax) {
            $link = 'admin-ajax.php?page='.ACYM_COMPONENT.'_'.$ctrl[1].'&ctrl='.$url.'&action='.ACYM_COMPONENT.'_router&'.acym_noTemplate();
        } else {
            $link = 'admin.php?page='.ACYM_COMPONENT.'_'.$ctrl[1].'&ctrl='.$url;
        }
    }

    return $link;
}

function acym_redirect($url, $msg = '', $msgType = 'message')
{
    if (acym_isAdmin() && substr($url, 0, 4) != 'http' && substr($url, 0, 4) != 'www.') {
        $url = acym_addPageParam($url);
    }
    @ob_get_clean();
    if (empty($url)) {
        $url = acym_rootURI();
    }
    wp_redirect($url);
    exit;
}

function acym_getLanguageTag()
{
    global $acymLanguages;
    if (!isset($acymLanguages['currentLanguage'])) {
        $acymLanguages['currentLanguage'] = get_bloginfo("language");
        if (strpos($acymLanguages['currentLanguage'], '-') === false) {
            $acymLanguages['currentLanguage'] = $acymLanguages['currentLanguage'].'-'.strtoupper($acymLanguages['currentLanguage']);
        }
    }

    return $acymLanguages['currentLanguage'];
}

function acym_getLanguageLocale()
{
    return array(get_locale());
}

function acym_setLanguage($lang)
{
    global $acymLanguages;
    $acymLanguages['currentLanguage'] = $lang;
}

function acym_baseURI($pathonly = false)
{
    if (acym_isAdmin()) {
        return acym_rootURI().'wp-admin/';
    }

    return acym_rootURI();
}

function acym_rootURI($pathonly = false, $path = 'siteurl')
{
    return get_option($path).'/';
}

function acym_generatePassword($length = 8)
{
    $salt = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $pass = array();
    $alphaLength = strlen($salt) - 1;
    for ($i = 0; $i < $length; $i++) {
        $n = mt_rand(0, $alphaLength);
        $pass[] = $salt[$n];
    }

    return implode($pass);
}

function acym_currentUserId()
{
    return get_current_user_id();
}

function acym_currentUserName($userid = null)
{
    if (!empty($userid)) {
        $special = get_user_by('id', $userid);

        return $special->display_name;
    }

    $current_user = wp_get_current_user();

    return $current_user->display_name;
}

function acym_currentUserEmail($userid = null)
{
    if (!empty($userid)) {
        $special = get_user_by('id', $userid);

        return $special->user_email;
    }

    $current_user = wp_get_current_user();

    return $current_user->user_email;
}

function acym_authorised($action, $assetname = null)
{
    return acym_isAdmin();
}

function acym_loadLanguageFile($extension, $basePath = null, $lang = null, $reload = false, $default = true)
{
    global $acymLanguages;
    $currentLanguage = acym_getLanguageTag();
    if (isset($acymLanguages[$currentLanguage][$extension]) && !$reload) {
        return true;
    }

    if (!file_exists(ACYM_FOLDER.'language'.DS.$currentLanguage.'.'.$extension.'.ini')) {
        $currentLanguage = ACYM_DEFAULT_LANGUAGE;
    }
    if (!file_exists(ACYM_FOLDER.'language'.DS.$currentLanguage.'.'.$extension.'.ini')) {
        return false;
    }

    $data = acym_fileGetContent(ACYM_FOLDER.'language'.DS.$currentLanguage.'.'.$extension.'.ini');
    $data = str_replace('"_QQ_"', '"', $data);
    $separate = explode("\n", $data);
    foreach ($separate as $raw) {
        if (strpos($raw, '=') === false) {
            continue;
        }
        $keyval = explode('=', $raw);
        $acymLanguages[$acymLanguages['currentLanguage']][$extension][$keyval[0]] = trim($keyval[1], "\"\r\n\t ");
    }

    if ($currentLanguage == ACYM_DEFAULT_LANGUAGE) {
        return true;
    }

    $data = acym_fileGetContent(ACYM_FOLDER.'language'.DS.ACYM_DEFAULT_LANGUAGE.'.'.$extension.'.ini');
    $data = str_replace('"_QQ_"', '"', $data);
    $separate = explode("\n", $data);
    foreach ($separate as $raw) {
        if (strpos($raw, '=') === false) {
            continue;
        }
        $keyval = explode('=', $raw);
        $acymLanguages[ACYM_DEFAULT_LANGUAGE][$extension][$keyval[0]] = trim($keyval[1], "\"\r\n\t ");
    }
}

function acym_escapeDB($value)
{
    return "'".esc_sql($value)."'";
}

function acym_query($query)
{
    global $wpdb;
    $query = acym_prepareQuery($query);

    $result = $wpdb->query($query);

    return $result === false ? null : $result;
}

function acym_loadObjectList($query, $key = '', $offset = null, $limit = null)
{
    global $wpdb;
    $query = acym_prepareQuery($query);

    if (isset($offset)) {
        $query .= ' LIMIT '.$offset.','.$limit;
    }

    $results = $wpdb->get_results($query);
    if (empty($key)) {
        return $results;
    }

    $sorted = array();
    foreach ($results as $oneRes) {
        $sorted[$oneRes->$key] = $oneRes;
    }

    return $sorted;
}

function acym_prepareQuery($query)
{
    global $wpdb;
    $query = str_replace('#__', $wpdb->prefix, $query);
    if (is_multisite()) {
        $query = str_replace($wpdb->prefix.'users', $wpdb->base_prefix.'users', $query);
    }

    return $query;
}

function acym_date($time = 'now', $format = null, $useTz = true, $gregorian = false)
{
    if ($time == 'now') {
        $time = time();
    }

    if (is_numeric($time)) {
        $time = date('Y-m-d H:i:s', $time);
    }

    if (!$format || (strpos($format, 'ACYM_DATE_FORMAT') !== false && acym_translation($format) == $format)) {
        $format = 'ACYM_DATE_FORMAT_LC1';
    }
    $format = acym_translation($format);

    if ($useTz === false) {
        $date = new DateTime($time);

        return acym_translateDate($date->format($format));
    } else {
        $cmsOffset = acym_getCMSConfig('offset');

        $timezone = new DateTimeZone($cmsOffset);

        if (!is_numeric($cmsOffset)) {
            $cmsOffset = $timezone->getOffset(new DateTime);
        }

        return acym_translateDate(date($format, strtotime($time) + $cmsOffset));
    }
}

function acym_loadObject($query)
{
    global $wpdb;
    $query = acym_prepareQuery($query);

    return $wpdb->get_row($query);
}

function acym_loadResult($query)
{
    global $wpdb;
    $query = acym_prepareQuery($query);

    return $wpdb->get_var($query);
}

function acym_loadResultArray($query)
{
    global $wpdb;
    $query = acym_prepareQuery($query);

    return $wpdb->get_col($query);
}

function acym_getEscaped($text, $extra = false)
{
    $result = esc_sql($text);
    if ($extra) {
        $result = addcslashes($result, '%_');
    }

    return $result;
}

function acym_getDBError()
{
    global $wpdb;

    return $wpdb->last_error;
}

function acym_insertObject($table, $element)
{
    global $wpdb;
    $element = get_object_vars($element);
    $table = acym_prepareQuery($table);
    $wpdb->insert($table, $element);

    return $wpdb->insert_id;
}

function acym_updateObject($table, $element, $pkey)
{
    global $wpdb;
    $element = get_object_vars($element);
    $table = acym_prepareQuery($table);

    if (!is_array($pkey)) {
        $pkey = array($pkey);
    }

    $where = array();
    foreach ($pkey as $onePkey) {
        $where[$onePkey] = $element[$onePkey];
    }

    $nbUpdated = $wpdb->update($table, $element, $where);

    return $nbUpdated !== false;
}

function acym_getPrefix()
{
    global $wpdb;

    return $wpdb->prefix;
}

function acym_insertID()
{
    global $wpdb;

    return $wpdb->insert_id;
}

function acym_getTableList()
{
    global $wpdb;

    return acym_loadResultArray("SELECT table_name FROM information_schema.tables WHERE table_schema='".$wpdb->dbname."' AND table_name LIKE '".$wpdb->prefix."%'");
}

function acym_completeLink($link, $popup = false, $redirect = false, $forceNoPopup = false)
{
    if (($popup || acym_isNoTemplate()) && $forceNoPopup == false) {
        $link .= '&'.acym_noTemplate();
    }

    $link = acym_addPageParam($link);

    return acym_route($link);
}

function acym_noTemplate()
{
    return 'noheader=1';
}

function acym_isNoTemplate()
{
    return acym_getVar('cmd', 'noheader') == '1';
}

function acym_setNoTemplate($status = true)
{
    if ($status) {
        acym_setVar('noheader', '1');
    } else {
        unset($_REQUEST['noheader']);
    }
}

function acym_cmsLoaded()
{
    defined('ABSPATH') or die('Restricted access');
}

function acym_formOptions($token = true, $task = '', $currentStep = null, $currentCtrl = '')
{
    if (!empty($currentStep)) {
        echo '<input type="hidden" name="step" value="'.$currentStep.'"/>';
        echo '<input type="hidden" name="nextstep" value=""/>';
        echo '<input type="hidden" name="edition" value="'.acym_getVar('cmd', 'edition', '0').'"/>';
    }
    echo '<input type="hidden" name="task" value="'.$task.'"/>';
    echo '<input type="hidden" name="nextstep" value=""/>';
    echo '<input type="hidden" name="page" value="'.acym_getVar('cmd', 'page', '').'"/>';
    echo empty($currentCtrl) ? '<input type="hidden" name="ctrl" value="'.acym_getVar('cmd', 'ctrl', '').'"/>' : '<input type="hidden" name="ctrl" value="'.$currentCtrl.'"/>';
    if ($token) {
        echo acym_formToken();
    }
    echo '<button type="submit" class="is-hidden" id="formSubmit"></button>';
}

function acym_enqueueMessage($message, $type = 'success')
{
    $result = is_array($message) ? implode('<br/>', $message) : $message;

    $type = str_replace(array('notice', 'message'), array('info', 'success'), $type);
    acym_session();
    if (empty($_SESSION['acymessage'.$type]) || !in_array($result, $_SESSION['acymessage'.$type])) {
        $_SESSION['acymessage'.$type][] = $result;
    }
}

function acym_displayMessages()
{
    $types = array('success', 'info', 'warning', 'error', 'notice', 'message');
    acym_session();
    foreach ($types as $type) {
        if (empty($_SESSION['acymessage'.$type])) {
            continue;
        }
        acym_display($_SESSION['acymessage'.$type], $type);
        unset($_SESSION['acymessage'.$type]);
    }
}

function acym_editCMSUser($userid)
{
    return ACYM_LIVE.'wp-admin/profile.php?wp_http_referer='.urlencode(ACYM_LIVE.'wp-admin/user.php?update=add&id='.$userid);
}

function acym_prepareAjaxURL($url)
{
    return htmlspecialchars_decode(acym_route(acym_addPageParam($url, true)));
}

function acym_cmsACL()
{
    return '';
}

function acym_isDebug()
{
    return defined('WP_DEBUG') && WP_DEBUG;
}

function acym_sendMail($to, $subject, $body, $attachments = array(), $headers = array())
{
    return wp_mail($to, $subject, $body, $headers, $attachments);
}

function acym_translateDate($date)
{
    $map = array(
        'January' => 'ACYM_JANUARY',
        'February' => 'ACYM_FEBRUARY',
        'March' => 'ACYM_MARCH',
        'April' => 'ACYM_APRIL',
        'May' => 'ACYM_MAY',
        'June' => 'ACYM_JUNE',
        'July' => 'ACYM_JULY',
        'August' => 'ACYM_AUGUST',
        'September' => 'ACYM_SEPTEMBER',
        'October' => 'ACYM_OCTOBER',
        'November' => 'ACYM_NOVEMBER',
        'December' => 'ACYM_DECEMBER',
        'Monday' => 'ACYM_MONDAY',
        'Tuesday' => 'ACYM_TUESDAY',
        'Wednesday' => 'ACYM_WEDNESDAY',
        'Thursday' => 'ACYM_THURSDAY',
        'Friday' => 'ACYM_FRIDAY',
        'Saturday' => 'ACYM_SATURDAY',
        'Sunday' => 'ACYM_SUNDAY',
    );

    foreach ($map as $english => $translationKey) {
        $translation = acym_translation($translationKey);
        if ($translation == $translationKey) {
            continue;
        }

        $date = preg_replace('#'.preg_quote($english).'( |,|$)#i', $translation.'$1', $date);
        $date = preg_replace('#'.preg_quote(substr($english, 0, 3)).'( |,|$)#i', mb_substr($translation, 0, 3).'$1', $date);
    }

    return $date;
}

function acym_addScript($raw, $script, $type = "text/javascript", $defer = false, $async = false)
{
    if ($raw) {
        echo '<script type="'.$type.'">'.$script.'</script>';
    } else {
        echo '<script type="'.$type.'" src="'.$script.'"'.($async ? ' async' : '').($defer ? ' defer' : '').'></script>';
    }
}

function acym_addStyle($raw, $style, $type = 'text/css', $media = null, $attribs = array())
{
    if ($raw) {
        echo '<style type="'.$type.'"'.(empty($media) ? '' : ' media="'.$media.'"').'>'.$style.'</style>';
    } else {
        echo '<link rel="stylesheet" href="'.$style.'" type="'.$type.'"'.(empty($media) ? '' : ' media="'.$media.'"').'>';
    }
}

global $acymMetaData;
function acym_addMetadata($meta, $data, $name = 'name')
{
    global $acymMetaData;

    $tag = new stdClass();
    $tag->meta = $meta;
    $tag->data = $data;
    $tag->name = $name;

    $acymMetaData[] = $tag;
}

add_action('wp_head', 'acym_head_wp');
add_action('admin_head', 'acym_head_wp');
function acym_head_wp()
{
    global $acymMetaData;

    if (!empty($acymMetaData)) {
        foreach ($acymMetaData as $metadata) {
            if (empty($metadata->data)) {
                continue;
            }
            echo '<meta '.$metadata->name.'="'.$metadata->meta.'" content="'.$metadata->data.'"/>';
        }
    }

    $acymMetaData = array();
}

function acym_userEditLink()
{
    return acym_rootURI().'wp-admin/user-edit.php?user_id=';
}

function acym_getLanguagePath($basePath, $language = null)
{
    return ACYM_FOLDER.'language';
}

function acym_askLog($current = true, $message = 'ACYM_NOTALLOWED', $type = 'error')
{
    $url = acym_rootURI().'wp-login.php';
    if ($current) {
        $url .= '&redirect_to='.base64_encode(acym_currentURL());
    }

    acym_redirect($url, acym_translation($message), $type);
}

function acym_frontendLink($link, $complete = true, $popup = false)
{
    return acym_rootURI().acym_addPageParam($link, true, true);
}

function acym_getMenu()
{
    return null;
}

function acym_getTitle()
{
    ob_start();
    wp_title('');
    $title = ob_get_clean();

    return trim($title);
}

function acym_extractArchive($archive, $destination)
{
    if (substr($archive, strlen($archive) - 4) !== '.zip') {
        return false;
    }

    $zip = new ZipArchive;
    $res = $zip->open($archive);

    if ($res !== true) {
        return false;
    }

    $zip->extractTo($destination);
    $zip->close();

    return true;
}

function acym_getDefaultConfigValues()
{
    $allPref = array();

    $allPref['from_name'] = get_option('fromname', '');
    $allPref['from_email'] = get_option('admin_email', '');
    $allPref['bounce_email'] = $allPref['from_email'];
    $allPref['sendmail_path'] = '';
    $allPref['smtp_port'] = get_option('mailserver_port', '');
    $allPref['smtp_secured'] = $allPref['smtp_port'] == 465 ? 'ssl' : '';
    $allPref['smtp_auth'] = 1;
    $allPref['smtp_username'] = get_option('mailserver_login', '');
    $allPref['smtp_password'] = get_option('mailserver_pass', '');
    $allPref['mailer_method'] = empty($allPref['smtp_host']) ? 'phpmail' : 'smtp';
    $allPref['smtp_host'] = get_option('mailserver_url', '');
    $allPref['uploadfolder'] = ACYM_MEDIA_FOLDER.'/upload';
    $allPref['cron_savepath'] = ACYM_MEDIA_FOLDER.'/logs/report{year}_{month}.log';

    return $allPref;
}

function acym_enqueueNotification_front($message, $type = 'info', $time = 0)
{
    $notif = '';

    if ($type == 'success') {
        $notif .= '<div class="acy_callout acym__callout__confirm"';
    } elseif ($type == 'warning') {
        $notif .= '<div class="acy_callout acym__callout__warning"';
    } elseif ($type == 'error') {
        $notif .= '<div class="acy_callout acym__callout__error"';
    } else {
        $notif .= '<div class="acy_callout acym__callout__information"';
    }
    $notif .= $time > 0 ? ' callout-timer="'.$time.'" style="display: none"><div class="progress" role="progressbar"><div class="progress-meter" style="width: 0"></div></div>' : '">';

    if (is_array($message)) {
        $message = implode('<br />', $message);
    }
    $notif .= '<div class="acym_callout_color acym_callout_front_'.$type.'"><div></div></div><p>'.$message.'<img class="acy_callout_close" src="'.ACYM_MEDIA_FOLDER.DS.'images'.DS.'close.png"/></p></div>';

    $type = str_replace(array('notice', 'message'), array('info', 'success'), $type);
    acym_session();
    if (empty($_SESSION['acymessage'.$type]) || !in_array($notif, $_SESSION['acymessage'.$type])) {
        $_SESSION['acymessage'.$type][] = $notif;
    }
}

function acym_addBreadcrumb($title, $link = '')
{
}

function acym_setPageTitle($title)
{
}

global $acymCmsUserVars;
$acymCmsUserVars = new stdClass();
$acymCmsUserVars->table = '#__users';
$acymCmsUserVars->name = 'display_name';
$acymCmsUserVars->username = 'user_login';
$acymCmsUserVars->id = 'id';
$acymCmsUserVars->email = 'user_email';
$acymCmsUserVars->registered = 'user_registered';
$acymCmsUserVars->blocked = 'user_status';

class JFormField
{
}
