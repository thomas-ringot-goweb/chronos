<?php
/**
 * @package	AcyMailing for WordPress
 * @version	6.0.1
 * @author	acyba.com
 * @copyright	(C) 2009-2018 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

defined('ABSPATH') or die('Restricted access');
?><?php

class acym_subscriptionform_widget extends WP_Widget
{
    function __construct()
    {
        require_once(rtrim(dirname(dirname(__DIR__)), DS).DS.'back'.DS.'helpers'.DS.'helper.php');

        parent::__construct(
            'acym_subscriptionform_widget',
            acym_translation_sprintf('ACYM_MENU', acym_translation('ACYM_MENU_FORM')),
            array('description' => acym_translation('ACYM_MENU_FORM_DESC'))
        );
    }

    public function form($instance)
    {
        require_once(rtrim(dirname(dirname(__DIR__)), DS).DS.'back'.DS.'helpers'.DS.'helper.php');

        acym_addStyle(false, ACYM_CSS.'widget.min.css?v='.filemtime(ACYM_MEDIA.'css'.DS.'widget.min.css'));

        $listClass = acym_get('class.list');
        $fieldClass = acym_get('class.field');
        $allFields = $fieldClass->getAllfieldsNameId();
        $fields = array();
        foreach ($allFields as $field) {
            $fields[$field->id] = $field->name;
        }
        $lists = $listClass->getAll();
        foreach ($lists as $i => $oneList) {
            if ($oneList->active == 0) {
                unset($lists[$i]);
            }
        }

        $params = array(
            'title' => 'Receive our newsletters',
            'displists' => '',
            'hiddenlists' => implode(',', array_keys($lists)),
            'fields' => 'name,email',
            'textmode' => '1',
            'subtext' => 'Subscribe',
            'unsub' => '0',
            'unsubtext' => 'Unsubscribe',
            'introtext' => '',
            'posttext' => '',
            'userinfo' => '1',
            'mode' => 'tableless',
            'source' => 'widget __i__',
            'redirect' => '',
        );

        foreach ($params as $oneParam => &$value) {
            if (isset($instance[$oneParam])) {
                $value = $instance[$oneParam];
            }

            if (is_array($value)) {
                $value = implode(',', $value);
            }

            $value = esc_attr($value);
        }

        if (!isset($instance['hiddenlists']) && !empty($params['displists'])) {
            $params['hiddenlists'] = '';
        }

        echo '<div class="acyblock widget" id="mainopt_acywidget">
                <div class="widget-top">
                    <div class="widget-title-action">
                        <button type="button" class="widget-action hide-if-no-js" aria-expanded="false">
                            <span class="toggle-indicator" aria-hidden="true"></span>
                        </button>
                    </div>
                    <div class="widget-title"><h3>'.acym_translation('ACYM_MAIN_OPTIONS').'</h3></div>
                </div>
                <div class="widget-inside">';

        echo '<p><label class="acyWPconfig" for="'.$this->get_field_id('title').'">'.acym_translation('ACYM_TITLE').'</label>
			<input type="text" class="widefat" id="'.$this->get_field_id('title').'" name="'.$this->get_field_name('title').'" value="'.$params['title'].'" /></p>';

        echo '<p><label class="acyWPconfig" title="'.acym_translation('ACYM_DISPLAYED_LISTS_DESC').'">'.acym_translation('ACYM_DISPLAYED_LISTS').'</label>';
        echo acym_selectMultiple($lists, $this->get_field_name('displists'), explode(',', $params['displists']), ['id' => $this->get_field_id('displists')], 'id', 'name');

        echo '<p><label class="acyWPconfig" title="'.acym_translation('ACYM_AUTO_SUBSCRIBE_TO_DESC').'">'.acym_translation('ACYM_AUTO_SUBSCRIBE_TO').'</label>';
        echo acym_selectMultiple($lists, $this->get_field_name('hiddenlists'), explode(',', $params['hiddenlists']), ['id' => $this->get_field_id('hiddenlists')], 'id', 'name');

        echo '<p><label class="acyWPconfig" title="'.acym_translation('ACYM_TEXT_MODE_DESC').'">'.acym_translation('ACYM_TEXT_MODE').'</label>';
        echo acym_boolean($this->get_field_name('textmode'), $params['textmode'], $this->get_field_id('textmode'), array(), 'ACYM_TEXT_INSIDE', 'ACYM_TEXT_OUTSIDE').'</p>';

        echo '<p><label class="acyWPconfig" for="'.$this->get_field_id('subtext').'" title="'.acym_translation('ACYM_SUBSCRIBE_TEXT_DESC').'">'.acym_translation('ACYM_SUBSCRIBE_TEXT').'</label>
			<input type="text" class="widefat" id="'.$this->get_field_id('subtext').'" name="'.$this->get_field_name('subtext').'" value="'.$params['subtext'].'" /></p>';

        echo '<p><label class="acyWPconfig" title="'.acym_translation('ACYM_FIELDS_TO_DISPLAY_DESC').'">'.acym_translation('ACYM_FIELDS_TO_DISPLAY').'</label>';
        echo acym_selectMultiple($fields, $this->get_field_name('fields'), explode(',', $params['fields']));

        $options = array();
        $options[] = acym_selectOption('inline', acym_translation('ACYM_MODE_HORIZONTAL'));
        $options[] = acym_selectOption('vertical', acym_translation('ACYM_MODE_VERTICAL'));
        $options[] = acym_selectOption('tableless', acym_translation('ACYM_MODE_TABLELESS'));
        echo '<p><label class="acyWPconfig" title="'.acym_translation('ACYM_DISPLAY_MODE_DESC').'">'.acym_translation('ACYM_DISPLAY_MODE').'</label>';
        echo acym_radio($options, $this->get_field_name('mode'), $params['mode'], $this->get_field_id('mode')).'</p>';

        echo '</div>
            </div>
            <div class="acyblock widget" id="advopt_acywidget">
                <div class="widget-top">
                    <div class="widget-title-action">
                        <button type="button" class="widget-action hide-if-no-js" aria-expanded="false">
                            <span class="toggle-indicator" aria-hidden="true"></span>
                        </button>
                    </div>
                    <div class="widget-title"><h3>'.acym_translation('ACYM_ADVANCED_OPTIONS').'</h3></div>
                </div>
                <div class="widget-inside">';

        echo '<p><label class="acyWPconfig">'.acym_translation('ACYM_DISPLAY_UNSUB_BUTTON').'</label>';
        $onclick = "var disp = 'none'; if(this.value == 1){disp = 'block';} document.getElementById('".$this->get_field_id('unsubtextrow')."').style.display = disp;";
        echo acym_boolean($this->get_field_name('unsub'), $params['unsub'], $this->get_field_id('unsub'), ['onclick' => $onclick]).'</p>';

        echo '<p id="'.$this->get_field_id('unsubtextrow').'" '.($params['unsub'] == '0' ? 'style="display:none;"' : '').'><label class="acyWPconfig" for="'.$this->get_field_id('unsubtext').'" title="'.acym_translation('ACYM_UNSUBSCRIBE_TEXT_DESC').'">'.acym_translation('ACYM_UNSUBSCRIBE_TEXT').'</label>
			<input type="text" class="widefat" id="'.$this->get_field_id('unsubtext').'" name="'.$this->get_field_name('unsubtext').'" value="'.$params['unsubtext'].'" /></p>';

        echo '<p><label class="acyWPconfig" for="'.$this->get_field_id('introtext').'" title="'.acym_translation('ACYM_INTRO_TEXT_DESC').'">'.acym_translation('ACYM_INTRO_TEXT').'</label>
			<textarea class="widefat" id="'.$this->get_field_id('introtext').'" name="'.$this->get_field_name('introtext').'" >'.$params['introtext'].'</textarea></p>';

        echo '<p><label class="acyWPconfig" for="'.$this->get_field_id('posttext').'" title="'.acym_translation('ACYM_POST_TEXT_DESC').'">'.acym_translation('ACYM_POST_TEXT').'</label>
			<textarea class="widefat" id="'.$this->get_field_id('posttext').'" name="'.$this->get_field_name('posttext').'" >'.$params['posttext'].'</textarea></p>';

        echo '<p><label class="acyWPconfig" title="'.acym_translation('ACYM_FORM_AUTOFILL_ID_DESC').'">'.acym_translation('ACYM_FORM_AUTOFILL_ID').'</label>';
        echo acym_boolean($this->get_field_name('userinfo'), $params['userinfo'], $this->get_field_id('userinfo')).'</p>';

        echo '<p><label class="acyWPconfig" for="'.$this->get_field_id('source').'" title="'.acym_translation('ACYM_SOURCE_DESC').'">'.acym_translation('ACYM_SOURCE').'</label>
			<input type="text" class="widefat" id="'.$this->get_field_id('source').'" name="'.$this->get_field_name('source').'" value="'.$params['source'].'" /></p>';

        echo '<p><label class="acyWPconfig" for="'.$this->get_field_id('redirect').'" title="'.acym_translation('ACYM_REDIRECT_LINK_DESC').'">'.acym_translation('ACYM_REDIRECT_LINK').'</label>
			<input type="text" class="widefat" id="'.$this->get_field_id('redirect').'" name="'.$this->get_field_name('redirect').'" value="'.$params['redirect'].'" /></p>';

        echo '</div></div>';
    }

    public function widget($args, $instance)
    {
        require_once(rtrim(dirname(dirname(__DIR__)), DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'back'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'helper.php');

        echo $args['before_widget'];

        $params = new acymParameter($instance);
        acym_initModule($params);

        $title = apply_filters('widget_title', $instance['title']);
        if (!empty($title)) {
            echo $args['before_title'].$title.$args['after_title'];
        }

        $identifiedUser = null;
        $currentUserEmail = acym_currentUserEmail();
        if ($params->get('userinfo', '1') == '1' && !empty($currentUserEmail)) {
            $userClass = acym_get('class.user');
            $identifiedUser = $userClass->getOneByEmail($currentUserEmail);
        }

        $visibleLists = $params->get('displists', array());
        $hiddenLists = $params->get('hiddenlists', array());
        $allfields = $params->get('fields', array());
        if (!in_array('2', $allfields)) {
            $allfields[] = 2;
        }
        acym_arrayToInteger($visibleLists);
        acym_arrayToInteger($hiddenLists);
        acym_arrayToInteger($allfields);

        $listClass = acym_get('class.list');
        $fieldClass = acym_get('class.field');
        $allLists = $listClass->getAll();
        $allfields = $fieldClass->getFieldsByID($allfields);
        $fields = array();
        foreach ($allfields as $field) {
            $fields[$field->id] = $field;
        }

        if (empty($visibleLists) && empty($hiddenLists)) {
            $hiddenLists = array_keys($allLists);
        }

        if (!empty($visibleLists) && !empty($hiddenLists)) {
            $visibleLists = array_diff($visibleLists, $hiddenLists);
        }

        if (!empty($identifiedUser->id)) {
            $userLists = $userClass->getUserSubscriptionById($identifiedUser->id);

            $countSub = 0;
            $countUnsub = 0;
            $formLists = array_merge($visibleLists, $hiddenLists);
            foreach ($formLists as $idOneList) {
                if (empty($userLists[$idOneList]) || $userLists[$idOneList]->status == 0) {
                    $countSub++;
                } else {
                    $countUnsub++;
                }
            }
        }

        $config = acym_config();
        $displayOutside = $params->get('textmode') == '0';

        $formName = acym_getModuleFormName();
        $formAction = htmlspecialchars_decode(acym_rootURI().acym_addPageParam('frontusers', true, true));

        $js = "\n"."acymModule['excludeValues".$formName."'] = Array();";
        $fieldsToDisplay = array();
        foreach ($fields as $field) {
            $fieldsToDisplay[$field->id] = $field->name;
            $js .= "\n"."acymModule['excludeValues".$formName."']['".$field->id."'] = '".$field->name."';";
        }
        echo "<script type=\"text/javascript\">
                <!--
                $js
                //-->
                </script>";

        ?>
        <div class="acym_module" id="acym_module_<?php echo $formName; ?>">
            <div class="acym_fulldiv" id="acym_fulldiv_<?php echo $formName; ?>">
                <form id="<?php echo $formName; ?>" name="<?php echo $formName ?>" method="POST" action="<?php echo $formAction; ?>" onsubmit="return submitAcymForm('subscribe','<?php echo $formName; ?>')">
                    <div class="acym_module_form">
                        <?php
                        $introText = $params->get('introtext', '');
                        if (!empty($introText)) {
                            echo '<div class="acym_introtext">'.$introText.'</div>';
                        }
                        if ($params->get('mode', 'tableless') == 'tableless') {
                            include(__DIR__.DS.'tmpl'.DS.'tableless.php');
                        } else {
                            $displayInline = $params->get('mode', 'tableless') != 'vertical';
                            include(__DIR__.DS.'tmpl'.DS.'default.php');
                        }
                        ?>
                    </div>

                    <input type="hidden" name="page" value="front"/>
                    <input type="hidden" name="ctrl" value="frontusers"/>
                    <input type="hidden" name="task" value="notask"/>
                    <input type="hidden" name="option" value="<?php echo ACYM_COMPONENT ?>"/>

                    <?php
                    $redirectURL = $params->get('redirect', '');
                    if (empty($redirectURL)) {
                        echo '<input type="hidden" name="ajax" value="1"/>';
                    } else {
                        echo '<input type="hidden" name="ajax" value="0"/>';
                        echo '<input type="hidden" name="redirect" value="'.htmlspecialchars($redirectURL).'"/>';
                    }
                    ?>
                    <input type="hidden" name="acy_source" value="<?php echo htmlspecialchars($params->get('source', '')); ?>"/>
                    <input type="hidden" name="hiddenlists" value="<?php echo implode(',', $hiddenLists); ?>"/>
                    <input type="hidden" name="acyformname" value="<?php echo $formName; ?>"/>

                    <?php
                    $postText = $params->get('posttext', '');
                    if (!empty($postText)) {
                        echo '<div class="acym_posttext">'.$postText.'</div>';
                    }
                    ?>
                </form>
            </div>
        </div>
        <?php
        echo $args['after_widget'];
    }
}
