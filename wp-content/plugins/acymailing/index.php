<?php
/*
Plugin Name: AcyMailing
Description: Manage your contact lists and send newsletters from your site.
Author: Acyba
Author URI: https://www.acyba.com
License: GPLv3
Version: 6.0.1
Text Domain: acymailing
Domain Path: /language
*/
defined('ABSPATH') || die('Restricted Access');

// Load defines
if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}

include_once(__DIR__.DS.'widgets'.DS.'profile'.DS.'widget.php');
include_once(__DIR__.DS.'widgets'.DS.'subscriptionform'.DS.'widget.php');

class acymInit
{
    function __construct()
    {
        // Install Acy DB and sample data on first activation (not installation because of FTP install)
        register_activation_hook(__FILE__, array($this, 'install'));

        // Enable ajax calls
        add_action('wp_ajax_acymailing_router', array($this, 'router'));
        add_action('wp_ajax_acymailing_frontrouter', array($this, 'frontRouter'));
        add_action('wp_ajax_nopriv_acymailing_frontrouter', array($this, 'frontRouter'));

        // Make sure we can redirect / download if needed after some checks (me from the future: find better if possible, please u_u)
        $pages = array('users', 'lists', 'fields', 'automation');
        foreach ($pages as $page) {
            add_action('load-acymailing_page_acymailing_'.$page, array($this, 'waitHeaders'));
        }

        add_action('widgets_init', array($this, 'loadWidgets'));

        // Add "settings" link for acym on plugins listing
        add_filter('plugin_action_links_'.plugin_basename(__FILE__), array($this, 'addPluginLinks'));

        if (defined('WP_ADMIN') && WP_ADMIN) {
            // Add AcyMailing menu in the back-end's left menu of WordPress
            add_action('admin_menu', array($this, 'addMenus'), 99);
        } else {
            add_action('wp', array($this, 'frontMessages'));
            add_action('wp_footer', array($this, 'messagingSystem'));
        }

        // Update system
        add_filter('pre_set_site_transient_update_plugins', array($this, 'checkUpdates'));
        add_filter('plugins_api', array($this, 'hookUpdate'), 10, 3);
    }

    public function checkUpdates($transient)
    {
        if ((empty($_REQUEST['force-check']) || $_REQUEST['force-check'] != 1) && $transient->last_checked > time() - 86400) {
            return $transient;
        }

        // Load Acy library
        require_once(rtrim(__DIR__, DS).DS.'back'.DS.'helpers'.DS.'helper.php');

        // Get latest version
        $config = acym_config();


        $url = ACYM_UPDATEMEURL.'updatexml&component=acymailing&cms=wp&level='.$config->get('level').'&version='.$config->get('version');
        if (acym_level(1)) {
            $url .= '&li='.urlencode(base64_encode(ACYM_LIVE));
        }
        $updateInformation = acym_fileGetContent($url, 10);
        $updateInformation = substr($updateInformation, strpos($updateInformation, '<?xml'));

        try {
            $xml = new SimpleXMLElement($updateInformation);
        } catch (Exception $err) {
            return $transient;
        }

        $latestVersion = (string)$xml->update[0]->version;
        $required = (string)$xml->update[0]->min_wp_version;
        $tested = (string)$xml->update[0]->tested_wp_version;
        $requiredPHP = (string)$xml->update[0]->min_php_version;
        $downloadURL = (string)$xml->update[0]->downloadurl;
        $updateBanner = (string)$xml->update[0]->banner;
        // Add the update to transient if any
        if (!empty($downloadURL) && !empty($latestVersion) && version_compare($latestVersion, $config->get('version', ''), '>')) {
            $plugin_slug = plugin_basename(__FILE__);
            $transient->response[$plugin_slug] = (object)array(
                'new_version' => $latestVersion,
                'package' => str_replace('.tar.gz', '.zip', $downloadURL),
                'slug' => $plugin_slug,
                'icons' => array(
                    '1x' => ACYM_ACYWEBSITE.'images/logo_icon.png',
                ),
            );
        }

        // Save the latest version and the license expiration date to warn the user when something's wrong
        $newConfig = new stdClass();
        $newConfig->latestversion = $latestVersion;
        $newConfig->requiredversion = $required;
        $newConfig->testedversion = $tested;
        $newConfig->requiredphp = $requiredPHP;
        $newConfig->updatebanner = ACYM_ACYWEBSITE.$updateBanner;

        if (acym_level(1)) {
            $url = ACYM_UPDATEMEURL.'update&task=loadUserInformation&component=acymailing&level='.strtolower($config->get('level', 'starter'));
            if (acym_level(1)) {
                $url .= '&domain='.urlencode(rtrim(ACYM_LIVE, '/'));
            }
            ob_start();
            $userInformation = acym_fileGetContent($url, 30);
            $warnings = ob_get_clean();

            // Could not load the user information
            if (!empty($userInformation)) {
                $decodedInformation = json_decode($userInformation, true);

                $newConfig->expirationdate = $decodedInformation['expiration'];
            }
        }

        $config->save($newConfig);

        return $transient;
    }

    public function hookUpdate($result, $action = null, $args = null)
    {
        $acySlug = basename(__DIR__).'/index.php';

        if (empty($action) || $action != 'plugin_information' || empty($args->slug) || $args->slug != $acySlug) {
            return $result;
        }

        // Load Acy library
        require_once(rtrim(__DIR__, DS).DS.'back'.DS.'helpers'.DS.'helper.php');
        $config = acym_config();

        $pluginInfo = new stdClass();
        $pluginInfo->banners = array(
            'low' => $config->get('updatebanner'),
        );
        $pluginInfo->name = ACYM_NAME;
        $pluginInfo->slug = $acySlug;
        $pluginInfo->version = $config->get('latestversion');
        $pluginInfo->author = '<a href="'.ACYM_ACYWEBSITE.'" target="_blank">Acyba</a>';
        $pluginInfo->homepage = ACYM_ACYWEBSITE;
        $pluginInfo->requires = $config->get('requiredversion');
        $pluginInfo->tested = $config->get('testedversion');
        $pluginInfo->requires_php = $config->get('requiredphp');

        $changelog = acym_fileGetContent(ACYM_ACYWEBSITE.'support/change-log.html?tmpl=component', 10);
        $changelog = preg_replace('#<head>.*</head>#Uis', '', $changelog);
        $changelog = preg_replace('#<div class="page-header">.*</div>#Uis', '', $changelog);
        $changelog = str_replace(array('<h2', '</h2>'), array('<h1', '</h1>'), $changelog);

        $pluginInfo->sections = array(
            _x('Changelog', 'Plugin installer section title') => $changelog,
        );

        return $pluginInfo;
    }

    /**
     * Meh.
     */
    function frontRouter()
    {
        $this->router('_front');
    }

    function router($suffix = '')
    {
        if (empty($suffix)) {
            auth_redirect();
        }

        require_once(rtrim(__DIR__, DS).DS.'back'.DS.'helpers'.DS.'helper.php');

        $this->updateAcym();

        if (file_exists(ACYM_FOLDER.'update.php')) {
            unlink(ACYM_FOLDER.'update.php');
        }

        // Get controller. If not found, take it from the page
        $ctrl = acym_getVar('cmd', 'ctrl', '');
        if (empty($ctrl)) {
            $ctrl = str_replace(ACYM_COMPONENT.'_', '', acym_getVar('cmd', 'page', ''));

            if (empty($ctrl)) {
                echo 'Page not found';

                return;
            }

            acym_setVar('ctrl', $ctrl);
        }

        if (!file_exists(constant('ACYM_CONTROLLER'.strtoupper($suffix)).$ctrl.'.php')) {
            echo 'Controller not found: '.$ctrl;

            return;
        }

        // Install language files if needed
        /*$config = acym_config();
        $installLanguages = $config->get('installlang', '');
        if (!empty($installLanguages)) {
            $newConfig = new stdClass();
            $newConfig->installlang = '';
            $config->save($newConfig);

            acym_setVar('languages', $installLanguages);
            $updateHelper = acym_get('controller.file');
            $updateHelper->installLanguages(false);
            global $acymLanguages;
            $acymLanguages = array();
        }*/

        $controller = acym_get('controller'.$suffix.'.'.$ctrl);
        $task = acym_getVar('cmd', 'task', '');
        if (empty($task)) {
            $task = acym_getVar('cmd', 'defaulttask', $controller->defaulttask);
        }

        if ((!defined('DOING_AJAX') || !DOING_AJAX)) {
            $this->writeScripts($ctrl, $task);
            $controller->loadScripts($task);
        }

        $controller->$task();
    }

    function writeScripts($ctrl, $task)
    {
        acym_addScript(
            true,
            'var TOGGLE_URL_ACYM = "admin-ajax.php?action='.ACYM_COMPONENT.'_router&'.acym_noTemplate().'&page='.ACYM_COMPONENT.'_toggle&ctrl=toggle&'.acym_getFormToken().'";
                var AJAX_URL_ACYM = "admin-ajax.php?action='.ACYM_COMPONENT.'_router&'.acym_noTemplate().'&'.acym_getFormToken().'";
                var MEDIA_URL_ACYM = "'.ACYM_MEDIA_URL.'";
                var CMS_ACYM = "'.ACYM_CMS.'";
                var FOUNDATION_FOR_EMAIL = "'.ACYM_CSS.'libraries/foundation_email.min.css?v='.filemtime(ACYM_MEDIA.'css'.DS.'libraries'.DS.'foundation_email.min.css').'";
                var ACYM_FIXES_FOR_EMAIL = "'.ACYM_CSS.'email.min.css?v='.filemtime(ACYM_MEDIA.'css'.DS.'email.min.css').'";
                var ACYM_REGEX_EMAIL = /^'.acym_getEmailRegex(true).'$/i;
                var ACYM_JS_TXT = '.acym_getJSMessages().';'
        );

        // Without this line the image insertion and dtexts button don't work
        wp_enqueue_media();

        wp_enqueue_script('jquery');
        wp_footer();

        wp_enqueue_script('jquery-ui-draggable');
        wp_enqueue_script('jquery-ui-droppable');
        wp_enqueue_script('jquery-ui-sortable');
        wp_enqueue_script('jquery-effects-slide');

        acym_addScript(false, ACYM_JS.'global.min.js?v='.filemtime(ACYM_MEDIA.'js'.DS.'global.min.js'));

        if (acym_isAdmin()) {

            acym_addScript(false, ACYM_JS.'libraries/foundation.min.js?v='.filemtime(ACYM_MEDIA.'js'.DS.'libraries'.DS.'foundation.min.js'));
            acym_addScript(false, ACYM_JS.'libraries/select2.min.js?v='.filemtime(ACYM_MEDIA.'js'.DS.'libraries'.DS.'select2.min.js'));

            //Font icons
            acym_addStyle(false, 'https://fonts.googleapis.com/icon?family=Material+Icons');
            acym_addStyle(false, 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');

            acym_addScript(false, ACYM_JS.'back_global.min.js?v='.filemtime(ACYM_MEDIA.'js'.DS.'back_global.min.js'));
            acym_addStyle(false, ACYM_CSS.'back_global.min.css?v='.filemtime(ACYM_MEDIA.'css'.DS.'back_global.min.css'));

            //introjs
            acym_addStyle(false, ACYM_CSS.'libraries/introjs.min.css?v='.filemtime(ACYM_MEDIA.'css'.DS.'libraries'.DS.'introjs.min.css'));
            acym_addScript(false, ACYM_JS.'libraries/intro.min.js?v='.filemtime(ACYM_MEDIA.'js'.DS.'libraries'.DS.'intro.min.js'));

            if (file_exists(ACYM_MEDIA.'js'.DS.'back'.DS.$ctrl.'.min.js')) {
                acym_addScript(false, ACYM_JS.'back/'.$ctrl.'.min.js?v='.filemtime(ACYM_MEDIA.'js'.DS.'back'.DS.$ctrl.'.min.js'));
            }
        } else {
            acym_addScript(false, ACYM_JS.'front_global.min.js?v='.filemtime(ACYM_MEDIA.'js'.DS.'front_global.min.js'));
            acym_addStyle(false, ACYM_CSS.'front_global.min.css?v='.filemtime(ACYM_MEDIA.'css'.DS.'front_global.min.css'));

            if (file_exists(ACYM_MEDIA.'js'.DS.'front'.DS.$ctrl.'.min.js')) {
                acym_addScript(false, ACYM_JS.'front/'.$ctrl.'.min.js?v='.filemtime(ACYM_MEDIA.'js'.DS.'front'.DS.$ctrl.'.min.js'));
            }
        }
    }

    function loadWidgets()
    {
        register_widget('acym_profile_widget');
        register_widget('acym_subscriptionform_widget');
    }

    // Add AcyMailing menu to WP left menu and define controllers
    function addMenus()
    {
        require_once(rtrim(__DIR__, DS).DS.'back'.DS.'helpers'.DS.'helper.php');

        // Everyone in WordPress can read, the real test is made above
        $capability = 'read';

        add_menu_page(
            acym_translation('ACYM_DASHBOARD'),
            'AcyMailing',
            $capability,
            ACYM_COMPONENT.'_dashboard',
            array($this, 'router'),
            'data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAB8lBMVEUAeNIAetMAedMAetQAwP8AnfAAuf8Aq/8AjugAmO4Aw/8Ar/8Aof0Aj+kAkekAnvEAx/8Avf8AoP0AjekAme0AoPIAwv8Ayf8Aq/wAkO0An/EA6/8A3v8Av/4AnvIAnfEArfoAtf4AedMAedMAedMAedMAedMAedMAaM0Abs8AedMAedMAedMAedMAc84Acs4AwO0AedMAedMAn+wA//8Awf8A6v8AtP4A6/8Atf4Ax/8Avf8AtP8AluwAm+8AoPIAx/8AtP8Arf8AkOgAluwAoPIAxf8AxP8Apf0AkOoAn/IAn/IAxf8Awv8AnvEAn/IA6/8Axf8Awf8AnfEAn/IAtf4A6/8A3/8Awf8AnfEArvoAtf4A0v8Axf8Awf8AnfEAn/IApvUAe9QAedMAetQAedQAZMoAdNEAd9EAbssAeNMA3vwAneQAbc4Aa8sAcs0Ac84Ai+EArfkA6v8A5f8AuvYAhtwAbMoAdtAAgNkAme0Ar/0AtP4A3v8A0v8A0/8AyP8An+0AbskAc80Ai+QAoPQApvcApvUArvoA3/8Axf8Av/8Awf8Arf4AjOYAgdsAkeoAnPIAnPAAoPIAxv8Avf8As/8Asf8Arf8AlO4AkusAlusAm+8AtP8Apf4AkOoAkOkAluwA6/8Atf4An/IAAADmtUTHAAAAYnRSTlMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAXs2Dfc5iNLvWUErSvTX+rqr+/Pzy8vLy44Lt8ITe6hKeoBXi8Qw/QAbq8AwH6ebuDAfm5hWUCwWWFj+wHRevQyzPqKYAAAABYktHRKUuuUovAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH4gMBDxMfj/8GxgAAAPJJREFUGNNjYGBUUlZhAgNVNXUGZgYNzaTkZC1tJiYd3eTkFD19BoPUtPSU5GRDIyA3IzPLmMEkOyc3LxkolJycX1BYZMpgVlxSWlaen5JSkV9ZVV1jzmBRW1ff0JjS1JzS0trW3mHJYNFZ19Xd09vXP2HipMlQgSlTp02fMXPW7DkIgbkz5s1fsBAuYGVtAxKwtbOHCjiwODrNm+/swuoKFXBzZ2P38OTg9PIGCSzqrPPx5eLm4eXj9/Nv71jMENBZFxgkICgkLCIaHNLeEcoQFl7XFSEmLiEpxR+5pD0qmkFaJiY2TkxAVk6ePz4hUUERAHayWPxHOSt0AAAAAElFTkSuQmCC',
            42
        );

        $menus = array(
            'ACYM_USERS' => 'users',
            'ACYM_CUSTOM_FIELDS' => 'fields',
            'ACYM_LISTS' => 'lists',
            'ACYM_CAMPAIGNS' => 'campaigns',
            'ACYM_TEMPLATES' => 'mails',
            'ACYM_AUTOMATION' => 'automation',
            'ACYM_QUEUE' => 'queue',
            'ACYM_STATISTICS' => 'stats',
            'ACYM_CONFIGURATION' => 'configuration',
        );
        foreach ($menus as $title => $ctrl) {
            add_submenu_page(ACYM_COMPONENT.'_dashboard', acym_translation($title), acym_translation($title), $capability, ACYM_COMPONENT.'_'.$ctrl, array($this, 'router'));
        }

        // Declare invisible menus
        $controllers = array('dynamics', 'file', 'language');
        foreach ($controllers as $oneCtrl) {
            add_submenu_page(null, $oneCtrl, $oneCtrl, $capability, ACYM_COMPONENT.'_'.$oneCtrl, array($this, 'router'));
        }

        // For the front ajax
        add_submenu_page(
            null,
            'front',
            'front',
            $capability,
            ACYM_COMPONENT.'_front',
            array($this, 'frontRouter')
        );

        // In WordPress, the first submenu is called "AcyMailing" instead of "Dashboard" so we rename it manually
        global $submenu;
        if (isset($submenu[ACYM_COMPONENT.'_dashboard'])) {
            $submenu[ACYM_COMPONENT.'_dashboard'][0][0] = acym_translation('ACYM_DASHBOARD');
        }
    }

    function waitHeaders()
    {
        ob_start();
    }

    // Add links on the plugins listing
    function addPluginLinks($links)
    {
        $settings_link = '<a href="admin.php?page='.ACYM_COMPONENT.'_configuration">'.__('Settings').'</a>';
        $links = array_merge(array($settings_link), $links);

        return $links;
    }

    // Install DB and sample data
    function install()
    {
        $file_name = rtrim(__DIR__, DS).DS.'back'.DS.'tables.sql';
        $handle = fopen($file_name, 'r');
        $queries = fread($handle, filesize($file_name));
        fclose($handle);

        require_once(rtrim(__DIR__, DS).DS.'back'.DS.'helpers'.DS.'helper.php');

        if (is_multisite()) {
            $currentBlog = get_current_blog_id();
            $sites = function_exists('get_sites') ? get_sites() : wp_get_sites();

            foreach ($sites as $site) {
                if (is_object($site)) {
                    $site = get_object_vars($site);
                }
                switch_to_blog($site['blog_id']);
                $this->sampledata($queries);
            }

            switch_to_blog($currentBlog);
        } else {
            $this->sampledata($queries);
        }

        if (file_exists(ACYM_FOLDER.'update.php')) {
            unlink(ACYM_FOLDER.'update.php');
        }
    }

    function sampledata($queries)
    {
        global $wpdb;
        $prefix = acym_getPrefix();

        $acytables = str_replace('#__', $prefix, $queries);
        $tables = explode('CREATE TABLE IF NOT EXISTS', $acytables);

        foreach ($tables as $oneTable) {
            $oneTable = trim($oneTable);
            if (empty($oneTable)) {
                continue;
            }
            $wpdb->query('CREATE TABLE IF NOT EXISTS'.$oneTable);
        }

        $this->updateAcym();
    }

    function updateAcym()
    {
        $config = acym_config();
        if (!file_exists(ACYM_FOLDER.'update.php') && $config->get('installcomplete', 0) != 0) {
            return;
        }

        require_once(ACYM_FOLDER.'install.class.php');

        if (!class_exists('acymInstall')) {
            return;
        }

        //First we increase the perfs so that we won't have any surprise.
        acym_increasePerf();

        $installClass = new acymInstall();
        $installClass->addPref();
        $installClass->updatePref();
        $installClass->updateSQL();

        $updateHelper = acym_get('helper.update');
        $updateHelper->fromLevel = $installClass->fromLevel;
        $updateHelper->fromVersion = $installClass->fromVersion;
        $updateHelper->installLanguages(false);

        $newConfig = new stdClass();
        $newConfig->installcomplete = 1;
        $config->save($newConfig);

        // Reload conf
        acym_config(true);

        $updateHelper = acym_get('helper.update');
        $updateHelper->installNotifications();
        $updateHelper->installLanguages(false);
        $updateHelper->installFields();
    }


    function frontMessages()
    {
        $sessionID = session_id();
        if (empty($sessionID)) {
            @session_start();
        }
        $output = '';

        $types = array('success', 'info', 'warning', 'error', 'notice', 'message');
        foreach ($types as $type) {
            if (empty($_SESSION['acymessage'.$type])) {
                continue;
            }

            $messages = $_SESSION['acymessage'.$type];
            if (!is_array($messages)) {
                $messages = array($messages);
            }

            $output .= implode(' ', $messages);

            unset($_SESSION['acymessage'.$type]);
        }

        $_SESSION['acymessages'] = $output;
    }

    function messagingSystem()
    {
        $sessionID = session_id();
        if (empty($sessionID)) {
            @session_start();
        }

        if (empty($_SESSION['acymessages'])) {
            return;
        }

        echo '<div id="acym__callout__container">'.$_SESSION['acymessages'].'</div>';

        $script = '
        setCallouts();
        
        function setCallouts(){
            var callouts = document.getElementsByClassName("acy_callout");
            for(var i = 0; i < callouts.length; i++){
            var callout = callouts[i];
            setTimeout(function(){
                callout.style["margin-right"] = "0px"; 
                callout.style["margin-left"] = "0px"; 
            }, 500);
            var test = callouts[i]
                callouts[i].children[1].children[0].onclick = function(){
                    closeCallout(test);
                }
            }
        }
        
        function closeCallout(callout){
            callout.style["margin-left"] = "640px";
            callout.style["margin-right"] = "-640px";
            setTimeout(function(){ callout.remove() }, 1000);
        }';

        $css = ' 
                
                #acym__callout__container .acy_callout_close {
                    width: 17px;
                    height: 18px;
                    margin-left: 40px;
                    cursor: pointer;
                    vertical-align: middle;
                }
                
                #acym__callout__container .acym_callout_color{
                    margin: auto !important;
                    border-radius: 50px;
                    height: 50px;
                    width: 50px;
                    line-height: 50px;
                    text-align: center;
                    font-size: 16px;
                    position: relative;
                }
                
                #acym__callout__container .acym_callout_front_info{
                    background-color: #eef1ff;
                }
                #acym__callout__container .acym_callout_front_info div{
                    background-color: #5d77ff;
                }
                
                #acym__callout__container .acym_callout_front_confirm{
                    background-color: #ebfcf4;
                }
                #acym__callout__container .acym_callout_front_confirm div{
                    background-color: #3dea91;
                }
                
                #acym__callout__container .acym_callout_front_warning{
                    background-color: #fff6e9;
                }
                #acym__callout__container .acym_callout_front_warning div{
                    background-color: #ffab15;
                }
                
                #acym__callout__container .acym_callout_front_error{
                    background-color: #ffedef;
                }
                #acym__callout__container .acym_callout_front_error div{
                    background-color: #ff5259;
                }
                
                #acym__callout__container .acym_callout_color div{
                    width: 15px;
                    height: 15px;
                    margin: 0 auto;
                    border-radius: 50px;
                    position: absolute;
                    top: 50%;
                    left: 50%;
                    transform: translate(-50%,-50%);
                }
                
                #acym__callout__container {
                  right: 0;
                  top: 45px;
                  position: fixed;
                  width: 400px;
                  max-width: 100%;
                  z-index: 1000000;
                }
                 #acym__callout__container .acy_callout {
                  padding: 15px 0;
                  background-color: #ffffff;
                  margin-left: 640px;
                  margin-right: -640px;
                  max-width: 100%;
                  -moz-transition-property: margin-left, margin-right;
                  -o-transition-property: margin-left, margin-right;
                  -webkit-transition-property: margin-left, margin-right;
                  transition-property: margin-left, margin-right;
                  -moz-transition-duration: 1s;
                  -o-transition-duration: 1s;
                  -webkit-transition-duration: 1s;
                  transition-duration: 1s;
                  width: 400px;
                  overflow: hidden;
                  border-radius: 4px 0 0 4px;
                  border: none;
                  box-shadow: 2px 2px 15px 2px #ebebeb;
                  float: right;
                  display: flex;
                  opacity: 1;
                  word-wrap: break-word;
                  word-break: break-word;
                }
                 #acym__callout__container .acy_callout i {
                  margin: auto !important;
                  border-radius: 50px;
                  height: 50px;
                  width: 50px;
                  line-height: 50px;
                  text-align: center;
                  font-size: 16px;
                }
                #acym__callout__container .acy_callout p {
                    float: right;
                    font-size: 14px;
                    width: calc(100% - 120px);
                    margin: 1rem 0 1rem 1rem;
                }
                @media screen and (max-width: 639px) {
                  #acym__callout__container .acy_callout {
                    margin-bottom: 0;
                    width: 100%;
                  }
                }
                @media screen and (max-width: 639px) {
                  #acym__callout__container {
                    width: 100%;
                    top: 0;
                  }
                }';

        echo '<script type="text/javascript">'.$script.'</script>';
        echo '<style >'.$css.'</style>';
    }
}

new acymInit();