=== AcyMailing ===
Contributors: Acyba
Tags: newsletter, email, campaigns, marketing, marketing automation
Requires at least: 4.4
Tested up to: 4.9.8
Stable tag: trunk
Requires PHP: 5.4.0
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

AcyMailing is a reliable Newsletter and email marketing plugin for Wordpress.
Create subscribers lists, test and send perfect emails easily.
 
== Description ==

AcyMailing is the perfect automation tool for your marketing via email. 
Keep your customer happy by sending them the right content at the right time. 
Test your newsletter content thanks to the Mail-tester tool so won't be considered as SPAM.

No restrictions : Manage as many users as you want and send unlimited emails monthly. 

More information here : https://www.acyba.com/acymailing.html

Here are some other features you need to know :
 
* No HTML knowledge required, enjoy the Newsletter creation with our drag and drop editor
* Real time send process using a queue system with throttling enabling you to overcome ANY server limitation!
* Subscription Module with cool effects (popup, slide effect...)
* Handle the subscription of registered users and visitors
* Record the User IP to stay compliant with all countries laws
* Double opt-in and automatic unsubscribe link
* CAN-SPAM compliant
* GDPR compliant
* Validation and Confirmation of the email address
* Mass Subscription using filters and mass actions
* Import your users from any format

* Easily include Wordpress Articles in your newsletter

* Easily include any user information in your newsletter (name, e-mail, username...)
* Facebook share and Twitter share plugins
* Display your lists on your Wordpress registration form
* Automatically subscribe your users to one or several lists during their registration process
* Handle unlimited subscribers, lists and newsletters
* Newsletter Statistics management (Number of Sent / Opened, HTML or text version)
* Advanced Newsletter statistics (who opened your Newsletter and when)

== Installation ==
 
There are 3 ways to install this plugin:

= 1. First solution =
1. In your Admin click on your Plugin then "Add New"
1. Search for `AcyMailing`
1. Find it, install it, enable it
1. You're done! 

= 2. Second way =
2. Download the ZIP from this page
2. In your Admin click on your Plugin then "Add New"
2. Click on upload plugin 
2. Upload the ZIP, enable the extension
2. You're done! 


== Frequently Asked Questions ==
 
= How to contact us? =
 
 Feel free to do it via this form : https://www.acyba.com/contact-us.html
 
= Where can I find documentation? =
 
https://www.acyba.com/support/documentation.html
 
== Screenshots ==
 
1. Dashboard
2. Drag & Drop editor
3. Statistics
4. User Edition page
 
== Changelog ==

= 6.0.1 =
* Fixed ajax calls (list selection on campaigns)
* Much better "Send a test" report message on campaign's "test" tab
* Fixed default value in configuration when it hasn't been saved yet
* Added multiple language files
* Template thumbnails are now correctly generated
* We now use the core WordPress method to get file contents
* Greately improved performances for the database table dedicated to the Tags feature
* The email field is now always required

= 6.0.0 =

This is the first version of AcyMailing available on the WordPress plugin directory
It includes the following features:
* User management (import / export / creation)
* Lists management (creation and user subscription)
* Campaigns management (creation with a drag&drop editor or with the default WP editor)
* Custom fields for users
* User fields insertion in any campaign
* WordPress posts insertion in campaigns 