<?php

use Molongui\Authorship\Includes\Activator;
use Molongui\Authorship\Includes\Deactivator;
use Molongui\Authorship\Includes\Core;
use Molongui\Fw\Includes\Compatibility;

// Deny direct access to this file
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * The plugin bootstrap file.
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * Plugin Name:       Molongui Authorship
 * Plugin URI:        https://molongui.amitzy.com/product/authorship
 * Description:       Complete suite on all about authors and authorship: Multiple authors, guest authors and author box. Three plugins in one... 3 in 1!
 * Text Domain:       molongui-authorship
 * Domain Path:       /i18n/
 * Version:           3.1.2
 * Author:            Amitzy
 * Author URI:        https://molongui.amitzy.com/
 * Plugin Base:       _boilerplate 2.0.0
 * License:           GPL-3.0+
 * License URI:       http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * This plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this plugin. If not, see http://www.gnu.org/licenses/.
 *
 * @author     Amitzy
 * @category   plugin
 * @package    Authorship
 * @license    GPL-3.0+
 * @since      1.0.0
 * @version    2.0.7
 */

if( !class_exists( 'molongui_authorship' ) )
{
    class molongui_authorship
    {
        /**
         * Framework compatibility class.
         *
         * @since   1.0.0
         * @version 1.0.0
         */
        private $compatibility;

        /**
         * The constructor.
         *
         * Initializes the plugin by setting localization, filters, and administration functions.
         *
         * @since   1.0.0
         * @version 1.0.0
         */
        function __construct()
        {
            /**
             * Sanity check.
             *
             * Do not let the plugin be activated when another instance is already installed.
             *
             * @since   1.0.0
             * @version 1.0.0
             */
            require_once( plugin_dir_path( __FILE__ ) . 'fw/includes/fw-class-compatibility.php' );
            $this->compatibility = new Compatibility( __FILE__ );
            if ( !$this->compatibility->compatible_version() ) return;

            /**
			 * Loads configuration.
             *
             * NOTE: Load order is important => First load plugin config, then fw config.
             *
             * @since   1.0.0
             * @version 1.0.0
             */
            require_once( plugin_dir_path( __FILE__ ) . 'config/plugin.php' );
            require_once( plugin_dir_path( __FILE__ ) . 'fw/config/fw.php'  );

            /**
			 * Loads common functions.
             *
             * @since   1.0.0
             * @version 1.0.0
             */
            require_once MOLONGUI_AUTHORSHIP_FW_DIR . 'includes/fw-helper-functions.php';

	        /**
	         * Loads plugin activation library.
	         *
	         * @since   2.0.7
	         * @version 2.0.7
	         */
	        require_once MOLONGUI_AUTHORSHIP_DIR . 'includes/plugin-class-activator.php';

	        /**
             * Registers hooks that are fired when the plugin is activated and deactivated,
             * respectively.
             *
             * @since   1.0.0
             * @version 1.0.0
             */
            register_activation_hook( __FILE__, array( $this, 'activate' ) );
            register_deactivation_hook( __FILE__, array( $this, 'deactivate' ) );

	        /**
	         * Initializes the plugin on every new blog is created (if plugin is network-activated).
	         *
	         * @since   2.0.7
	         * @version 2.0.7
	         */
	        add_action( 'wpmu_new_blog', array( $this, 'activate_on_new_blog' ), 10, 6 );

            /**
			 * Loads the class that is used to define internationalization,
             * admin-specific hooks and public-facing site hooks.
             *
             * @since   1.0.0
             * @version 1.0.0
             */
            require MOLONGUI_AUTHORSHIP_DIR . 'includes/plugin-class-core.php';

            /**
             * Begins execution of the plugin.
             *
             * Since everything within the plugin is registered via hooks,
             * then kicking off the plugin from this point in the file does
             * not affect the page life cycle.
             *
             * @since   1.0.0
             * @version 1.0.0
             */
            $plugin = new Core();
            $plugin->run();
        }

        /**
         * Fires all required actions during plugin activation.
         *
         * This action is documented in includes/plugin-class-activator.php
         *
         * @access  public
         * @param   bool    $network_wide   Whether to enable the plugin for all sites in the network or just the current site. Multisite only. Default is false.
         * @since   1.0.0
         * @version 2.0.7
         */
        public function activate( $network_wide )
        {
            $this->compatibility->activation_check();
            Activator::activate( $network_wide );
        }

        /**
         * Fires all required actions during plugin deactivation.
         *
         * This action is documented in includes/plugin-class-deactivator.php
         *
         * @access  public
         * @param   bool    $network_wide   Whether to disable the plugin for all sites in the network or just the current site. Multisite only. Default is false.
         * @since   1.0.0
         * @version 2.0.7
         */
        public function deactivate( $network_wide )
        {
            require_once MOLONGUI_AUTHORSHIP_DIR . 'includes/plugin-class-deactivator.php';
            Deactivator::deactivate( $network_wide );
        }

	    /**
	     * Initializes the plugin on every new blog is created (if plugin is network-activated).
	     *
	     * @access  public
	     * @param   int    $blog_id Blog ID.
	     * @param   int    $user_id User ID.
	     * @param   string $domain  Site domain.
	     * @param   string $path    Site path.
	     * @param   int    $site_id Site ID. Only relevant on multi-network installs.
	     * @param   array  $meta    Meta data. Used to set initial site options.
	     * @since   2.0.7
	     * @version 2.0.7
	     */
	    public function activate_on_new_blog( $blog_id, $user_id, $domain, $path, $site_id, $meta )
	    {
		    Activator::activate_on_new_blog( $blog_id, $user_id, $domain, $path, $site_id, $meta );
	    }

    } // End class
} // End if

/**
 * Run the plugin.
 *
 * @since   1.0.0
 * @version 1.0.0
 */
if ( class_exists( 'molongui_authorship' ) )
{
    $plugin = new molongui_authorship();
}