<?php

namespace Molongui\Authorship\Includes;

// Deny direct access to this file.
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Class DB_Update.
 *
 * Holds all the update functions to update db schema.
 *
 * @author     Amitzy
 * @package    Authorship
 * @subpackage /includes
 * @since      1.3.0
 * @version    3.0.0
 */
class DB_Update
{
    /**
	 * Updates data to be compatible with version 1.3.0 and higher.
	 *
	 * Changes made on version 1.3.0:
	 *
	 *  - removed db key named 'molongui_authorship_config' (aka MOLONGUI_AUTHORSHIP_CONFIG_KEY)
	 *  - removed db key named 'molongui_authorship_deactivate_checkbox'
	 *  + added db key name 'molongui_authorship_main' (aka MOLONGUI_AUTHORSHIP_MAIN_SETTINGS)
	 *  + added db key name 'molongui_authorship_box' (aka MOLONGUI_AUTHORSHIP_BOX_SETTINGS)
	 *  + added db key name 'molongui_authorship_strings' (aka MOLONGUI_AUTHORSHIP_STRING_SETTINGS)
	 *  - removed 'molongui_authorship' prefix from the array indexes names
	 *  ~ changed 'molongui_authorship_related_show' setting name to 'show_related'
	 *  ~ changed 'layout-1' layout name to 'ribbon'
	 *  ~ changed 'layout-1-rtl' layout name to 'ribbon-rtl'
	 *  ~ changed 'molongui_guest_author_link' array index name to 'molongui_guest_author_blog'
	 *  ~ changed 'molongui_guest_author_xxxxxx' metadata option names to '_molongui_guest_author_xxxxxx' (to hide them from the backend)
	 *  ~ changed 'molongui_author_box_display' metadata option names to '_molongui_author_box_display' (to hide it from the backend)
	 *
	 * @access  public
	 * @since   1.3.0
	 * @version 1.3.0
	 */
	public function db_update_2()
	{
		global $wpdb;

		// Get current settings.
		// 'molongui_authorship_config' was the db key used to store the plugin settings prior to version 1.3.0.
		$settings = get_option( 'molongui_authorship_config' );

		// Convert plugin settings.
		$main_settings = array(
			'show_related'       => $settings['molongui_authorship_related_show'],
			'related_order_by'   => $settings['molongui_authorship_related_order_by'],
			'related_order'      => $settings['molongui_authorship_related_order'],
			'related_items'      => $settings['molongui_authorship_related_items'],
			'show_tw'            => $settings['molongui_authorship_show_social_networks_tw'],
			'show_fb'            => $settings['molongui_authorship_show_social_networks_fb'],
			'show_in'            => $settings['molongui_authorship_show_social_networks_in'],
			'show_gp'            => $settings['molongui_authorship_show_social_networks_gp'],
			'show_yt'            => $settings['molongui_authorship_show_social_networks_yt'],
			'show_pi'            => $settings['molongui_authorship_show_social_networks_pi'],
			'show_tu'            => $settings['molongui_authorship_show_social_networks_tu'],
			'show_ig'            => $settings['molongui_authorship_show_social_networks_ig'],
			'show_xi'            => $settings['molongui_authorship_show_social_networks_xi'],
			'show_re'            => $settings['molongui_authorship_show_social_networks_re'],
			'show_vk'            => $settings['molongui_authorship_show_social_networks_vk'],
			'show_fl'            => $settings['molongui_authorship_show_social_networks_fl'],
			'show_vi'            => $settings['molongui_authorship_show_social_networks_vi'],
			'show_me'            => $settings['molongui_authorship_show_social_networks_me'],
			'show_we'            => $settings['molongui_authorship_show_social_networks_we'],
			'show_de'            => $settings['molongui_authorship_show_social_networks_de'],
			'show_st'            => $settings['molongui_authorship_show_social_networks_st'],
			'show_my'            => $settings['molongui_authorship_show_social_networks_my'],
			'show_ye'            => $settings['molongui_authorship_show_social_networks_ye'],
			'show_mi'            => $settings['molongui_authorship_show_social_networks_mi'],
			'show_so'            => $settings['molongui_authorship_show_social_networks_so'],
			'show_la'            => $settings['molongui_authorship_show_social_networks_la'],
			'show_fo'            => $settings['molongui_authorship_show_social_networks_fo'],
			'show_sp'            => $settings['molongui_authorship_show_social_networks_sp'],
			'show_vm'            => $settings['molongui_authorship_show_social_networks_vm'],
			'add_opengraph_meta' => $settings['molongui_authorship_add_opengraph_meta'],
			'add_google_meta'    => $settings['molongui_authorship_add_google_meta'],
			'add_facebook_meta'  => $settings['molongui_authorship_add_facebook_meta'],
			'admin_menu_level'   => $settings['molongui_authorship_admin_menu_level'],
			'keep_config'        => $settings['molongui_authorship_keep_config'],
			'keep_data'          => $settings['molongui_authorship_keep_data'],
		);

		$box_settings = array(
			'display'             => $settings['molongui_authorship_display'],
			'position'            => $settings['molongui_authorship_position'],
			'hide_if_no_bio'      => $settings['molongui_authorship_hide_if_no_bio'],
			'layout'              => ( $settings['molongui_authorship_layout'] == 'layout-1' ? 'ribbon' : ( $settings['molongui_authorship_layout'] == 'layout-1-rtl' ? 'ribbon-rtl' : $settings['molongui_authorship_layout'] ) ),
			'box_shadow'          => $settings['molongui_authorship_box_shadow'],
			'box_border'          => $settings['molongui_authorship_box_border'],
			'box_border_color'    => $settings['molongui_authorship_box_border_color'],
			'box_background'      => $settings['molongui_authorship_box_background'],
			'img_style'           => $settings['molongui_authorship_img_style'],
			'img_default'         => $settings['molongui_authorship_img_default'],
			'name_size'           => $settings['molongui_authorship_name_size'],
			'name_color'          => $settings['molongui_authorship_name_color'],
			'meta_size'           => $settings['molongui_authorship_meta_size'],
			'meta_color'          => $settings['molongui_authorship_meta_color'],
			'bio_size'            => $settings['molongui_authorship_bio_size'],
			'bio_color'           => $settings['molongui_authorship_bio_color'],
			'bio_align'           => $settings['molongui_authorship_bio_align'],
			'bio_style'           => $settings['molongui_authorship_bio_style'],
			'show_icons'          => $settings['molongui_authorship_icons_show'],
			'icons_size'          => $settings['molongui_authorship_icons_size'],
			'icons_color'         => $settings['molongui_authorship_icons_color'],
			'icons_style'         => $settings['molongui_authorship_icons_style'],
			'bottom_bg'           => $settings['molongui_authorship_bottom_bg'],
			'bottom_border'       => $settings['molongui_authorship_bottom_border'],
			'bottom_border_color' => $settings['molongui_authorship_bottom_border_color'],
		);

		// Insert new settings.
		add_option( MOLONGUI_AUTHORSHIP_MAIN_SETTINGS, $main_settings );
		add_option( MOLONGUI_AUTHORSHIP_BOX_SETTINGS, $box_settings );

		// Remove old entries.
		delete_option( 'molongui_authorship_config' );
		delete_option( 'molongui_authorship_deactivate_checkbox' );

		// Change 'molongui_guest_author_link' to 'molongui_guest_author_blog'.
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = 'molongui_guest_author_blog' WHERE meta_key = 'molongui_guest_author_link';" );

		// Change 'molongui_guest_author_xxxxx' to '_molongui_guest_author_xxxxx'.
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_guest_author_id' WHERE meta_key = 'molongui_guest_author_id';" );
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_guest_author_mail' WHERE meta_key = 'molongui_guest_author_mail';" );
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_guest_author_link' WHERE meta_key = 'molongui_guest_author_blog';" );
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_guest_author_job' WHERE meta_key = 'molongui_guest_author_job';" );
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_guest_author_company' WHERE meta_key = 'molongui_guest_author_company';" );
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_guest_author_company_link' WHERE meta_key = 'molongui_guest_author_company_link';" );
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_guest_author_twitter' WHERE meta_key = 'molongui_guest_author_twitter';" );
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_guest_author_facebook' WHERE meta_key = 'molongui_guest_author_facebook';" );
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_guest_author_linkedin' WHERE meta_key = 'molongui_guest_author_linkedin';" );
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_guest_author_gplus' WHERE meta_key = 'molongui_guest_author_gplus';" );
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_guest_author_youtube' WHERE meta_key = 'molongui_guest_author_youtube';" );
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_guest_author_pinterest' WHERE meta_key = 'molongui_guest_author_pinterest';" );
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_guest_author_tumblr' WHERE meta_key = 'molongui_guest_author_tumblr';" );
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_guest_author_instagram' WHERE meta_key = 'molongui_guest_author_instagram';" );
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_guest_author_xing' WHERE meta_key = 'molongui_guest_author_xing';" );
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_guest_author_renren' WHERE meta_key = 'molongui_guest_author_renren';" );
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_guest_author_vkg' WHERE meta_key = 'molongui_guest_author_vk';" );
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_guest_author_flickr' WHERE meta_key = 'molongui_guest_author_flickr';" );
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_guest_author_vine' WHERE meta_key = 'molongui_guest_author_vine';" );
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_guest_author_meetup' WHERE meta_key = 'molongui_guest_author_meetup';" );
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_guest_author_weibo' WHERE meta_key = 'molongui_guest_author_weibo';" );
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_guest_author_deviantart' WHERE meta_key = 'molongui_guest_author_deviantart';" );
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_guest_author_stumbleupon' WHERE meta_key = 'molongui_guest_author_stumbleupon';" );
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_guest_author_myspace' WHERE meta_key = 'molongui_guest_author_myspace';" );
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_guest_author_yelp' WHERE meta_key = 'molongui_guest_author_yelp';" );
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_guest_author_mixi' WHERE meta_key = 'molongui_guest_author_mixi';" );
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_guest_author_soundcloud' WHERE meta_key = 'molongui_guest_author_soundcloud';" );
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_guest_author_lastfm' WHERE meta_key = 'molongui_guest_author_lastfm';" );
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_guest_author_foursquare' WHERE meta_key = 'molongui_guest_author_foursquare';" );
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_guest_author_spotify' WHERE meta_key = 'molongui_guest_author_spotify';" );
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_guest_author_vimeo' WHERE meta_key = 'molongui_guest_author_vimeo';" );

		// Change 'molongui_author_box_display' to '_molongui_author_box_display'.
		$wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_author_box_display' WHERE meta_key = 'molongui_author_box_display';" );
	}

    /**
     * Updates data to be compatible with version 1.3.5 and higher.
     *
     * Changes made on version 1.3.5:
     *
     *  ~ renamed 'enable_guest_archives' array index name to 'enable_guest_archive'
     *  ~ renamed '_molongui_guest_author_gplus' metadata option name to '_molongui_guest_author_googleplus'
     *  ~ renamed 'molongui_author_gplus' metadata option name to 'molongui_author_googleplus'
     *
     * @access  public
     * @since   1.3.5
     * @version 1.3.5
     */
    public function db_update_3()
    {
        global $wpdb;

        // Get current 'main' settings.
        $settings = get_option( MOLONGUI_AUTHORSHIP_MAIN_SETTINGS );

        // Convert plugin 'main' settings.
        $main_settings = array(
            'show_related'            => $settings['show_related'],
            'related_order_by'        => $settings['related_order_by'],
            'related_order'           => $settings['related_order'],
            'related_items'           => $settings['related_items'],
            'enable_guest_archive'    => $settings['enable_guest_archives'],
            'guest_archive_permalink' => $settings['guest_archive_permalink'],
            'guest_archive_slug'      => $settings['guest_archive_slug'],
            'guest_archive_tmpl'      => $settings['guest_archive_tmpl'],
            'show_facebook'           => $settings['show_fb'],
            'show_twitter'            => $settings['show_tw'],
            'show_linkedin'           => $settings['show_in'],
            'show_googleplus'         => $settings['show_gp'],
            'show_youtube'            => $settings['show_yt'],
            'show_pinterest'          => $settings['show_pi'],
            'show_tumblr'             => $settings['show_tu'],
            'show_instagram'          => $settings['show_ig'],
            'show_slideshare'         => $settings['show_ss'],
            'show_xing'               => $settings['show_xi'],
            'show_renren'             => $settings['show_re'],
            'show_vk'                 => $settings['show_vk'],
            'show_flickr'             => $settings['show_fl'],
            'show_vine'               => $settings['show_vi'],
            'show_meetup'             => $settings['show_me'],
            'show_weibo'              => $settings['show_we'],
            'show_deviantart'         => $settings['show_de'],
            'show_stubmleupon'        => $settings['show_st'],
            'show_myspace'            => $settings['show_my'],
            'show_yelp'               => $settings['show_ye'],
            'show_mixi'               => $settings['show_mi'],
            'show_soundcloud'         => $settings['show_so'],
            'show_lastfm'             => $settings['show_la'],
            'show_foursquare'         => $settings['show_fo'],
            'show_spotify'            => $settings['show_sp'],
            'show_vimeo'              => $settings['show_vm'],
            'show_dailymotion'        => $settings['show_dm'],
            'show_reddit'             => $settings['show_rd'],
            'enable_sc_text_widgets'  => $settings['enable_sc_text_widgets'],
            'add_opengraph_meta'      => $settings['add_opengraph_meta'],
            'add_google_meta'         => $settings['add_google_meta'],
            'add_facebook_meta'       => $settings['add_facebook_meta'],
            'admin_menu_level'        => $settings['admin_menu_level'],
            'keep_config'             => $settings['keep_config'],
            'keep_data'               => $settings['keep_data'],
        );

        // Update converted settings.
        update_option( MOLONGUI_AUTHORSHIP_MAIN_SETTINGS, $main_settings );

        // Rename '_molongui_guest_author_gplus' to '_molongui_guest_author_googleplus'.
        $wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_guest_author_googleplus' WHERE meta_key = '_molongui_guest_author_gplus';" );

        // Rename '_molongui_guest_author_gplus' to '_molongui_guest_author_googleplus'.
        $wpdb->query( "UPDATE {$wpdb->prefix}usermeta SET meta_key = 'molongui_author_googleplus' WHERE meta_key = 'molongui_author_gplus';" );
    }

    /**
     * Updates data to be compatible with version 2.0.0 and higher.
     *
     * Changes made on version 2.0.0:
     *
     *  ~ moved setting 'shortcodes' from 'main' tab to 'advanced' tab
     *  ~ moved setting 'uninstalling' from 'main' tab to 'advanced' tab
     *  + added '_molongui_author' key
     *  + added '_molongui_main_author' key
     *  - removed '_molongui_guest_author' key
     *  - removed '_molongui_guest_author_id' key
     *
     * @access  public
     * @since   2.0.0
     * @version 2.0.0
     */
    public function db_update_4()
    {
        global $wpdb;

        // Get current 'main' settings.
        $main_settings = get_option( MOLONGUI_AUTHORSHIP_MAIN_SETTINGS );

        // Initialize 'advanced' settings copying moved values from 'main' object.
        $advanced_settings = array(
            'extend_to_post'         => '1',
            'extend_to_page'         => '1',
            'enable_sc_text_widgets' => $main_settings['enable_sc_text_widgets'],
            'keep_config'            => $main_settings['keep_config'],
            'keep_data'              => $main_settings['keep_data'],
        );

        // Update 'advanced' settings object into db.
        update_option( MOLONGUI_AUTHORSHIP_ADVANCED_SETTINGS, $advanced_settings );

        // Remove moved settings from 'main' object.
        unset( $main_settings['enable_sc_text_widgets'] );
        unset( $main_settings['keep_config'] );
        unset( $main_settings['keep_data'] );

        // Update 'main' settings object into db.
        update_option( MOLONGUI_AUTHORSHIP_MAIN_SETTINGS, $main_settings );

        // Replace '_molongui_guest_author' and '_molongui_guest_author_id' keys with '_molongui_author' and '_molongui_main_author'.
        // Replace keys on guest authored posts.
        $rows = $wpdb->get_results( "SELECT meta_id, post_id, meta_value FROM {$wpdb->prefix}postmeta WHERE meta_id IN (SELECT meta_id FROM {$wpdb->prefix}postmeta WHERE meta_key = '_molongui_guest_author_id' AND meta_value IS NOT NULL)" );
        foreach ( $rows as $row )
        {
            // Author reference value to insert.
            $author_value = 'guest-'.$row->meta_value;

            // Re-use existing '_molongui_guest_author_id' entry modifying its meta_key and meta_value values to store '_molongui_author' entries.
            $wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_author', meta_value = '{$author_value}' WHERE meta_id = {$row->meta_id};" );

            // Re-use existing '_molongui_guest_author' entry modifying its meta_key and meta_value values to store '_molongui_main_author' entries.
            $wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_main_author', meta_value = '{$author_value}' WHERE meta_key = '_molongui_guest_author' AND post_id = {$row->post_id};" );
        }

        // Replace keys on non-guest authored posts.
        $rows = $wpdb->get_results( "SELECT meta_id, post_id FROM {$wpdb->prefix}postmeta WHERE meta_key = '_molongui_guest_author' AND meta_value = '0'" );
        foreach ( $rows as $row )
        {
            // Get user id.
            $author_id    = $wpdb->get_col( "SELECT post_author FROM {$wpdb->prefix}posts WHERE ID = {$row->post_id}" );
            $author_value = 'user-'.$author_id[0];

            // Re-use existing '_molongui_guest_author' entry modifying its meta_key and meta_value values to store '_molongui_author' entries.
            $wpdb->query( "UPDATE {$wpdb->prefix}postmeta SET meta_key = '_molongui_author', meta_value = '{$author_value}' WHERE meta_id = {$row->meta_id};" );

            // Create new entry for '_molongui_main_author' key.
            $wpdb->query( "INSERT INTO {$wpdb->prefix}postmeta (post_id, meta_key, meta_value) VALUES ({$row->post_id}, '_molongui_main_author', '{$author_value}');" );
        }

        // Remove all remaining '_molongui_guest_author' entries (there should be none).
        $wpdb->query( "DELETE FROM {$wpdb->prefix}postmeta WHERE meta_key = '_molongui_guest_author';" );

        // Remove all remaining '_molongui_guest_author_id' entries (there should be none).
        $wpdb->query( "DELETE FROM {$wpdb->prefix}postmeta WHERE meta_key = '_molongui_guest_author_id';" );
    }

	/**
	 * Updates data to be compatible with version 2.0.7 and higher.
	 *
	 * Changes made on version 2.0.7:
	 *
	 *  ~ dropped use of 'molongui_author_bio' user meta field in favor of 'description' user field.
	 *  - removed 'molongui_author_bio' user meta.
	 *
	 * @access  public
	 * @since   2.0.7
	 * @version 2.0.7
	 */
	public function db_update_5()
	{
		// Get all users.
		$users = get_users();

		foreach ( $users as $user )
		{
			// Re-set WP user "description" field.
			if ( $bio = get_user_meta( $user->ID, 'molongui_author_bio', true ) ) update_user_meta( $user->ID, 'description', $bio );

			// Remove 'molongui_author_bio' user meta.
			delete_user_meta( $user->ID, 'molongui_author_bio' );
		}
	}

	/**
	 * Updates data to be compatible with version 2.1.0 and higher.
	 *
	 * Changes made on version 2.1.0:
	 *
	 *  - removed db key 'molongui_authorship_main' (aka MOLONGUI_AUTHORSHIP_MAIN_SETTINGS).
	 *  + added 'molongui_authorship_byline', 'molongui_authorship_guest' and 'molongui_authorship_archives' db keys.
	 *  ~ dropped use of predefined size values for font-size and border widths in favour of open values.
	 *  ~ renamed several author box settings keys.
	 *  ~ renamed author box layout names.
	 *  ~ moved settings from 'molongui_authorship_main' to other db keys.
	 *
	 * @access  public
	 * @since   2.1.0
	 * @version 2.1.0
	 */
	public function db_update_6()
	{
		// Get current settings.
		$main_settings     = get_option( 'molongui_authorship_main' );
		$box_settings      = get_option( MOLONGUI_AUTHORSHIP_BOX_SETTINGS );
		$advanced_settings = get_option( MOLONGUI_AUTHORSHIP_ADVANCED_SETTINGS );

		// Replace old box settings values by their numeric equivalent.
		$text_styles = array
		(
			'normal'   => 'normal',
		    'bold'     => 'bold',
		    'italics'  => 'italic',
		    'itbo'     => 'italic,bold',
		);
		$font_sizes = array
		(
			'biggest'  => 26,
		    'bigger'   => 22,
		    'big'      => 18,
		    'normal'   => 14,
		    'small'    => 12,
		    'smaller'  => 11,
		    'smallest' => 10,
		);
		$border_widths = array
		(
			'none'     => 0,
		    'thin'     => 1,
		    'thick'    => 2,
		    'thicker'  => 3,
		    'thickest' => 4,
		);
		$box_settings['headline_text_style'] = $text_styles[$box_settings['headline_style']];
		$box_settings['bio_text_style']      = $text_styles[$box_settings['bio_style']];
		$box_settings['headline_text_size']  = $font_sizes[$box_settings['headline_size']];
		$box_settings['name_text_size']      = $font_sizes[$box_settings['name_size']];
		$box_settings['meta_text_size']      = $font_sizes[$box_settings['meta_size']];
		$box_settings['bio_text_size']       = $font_sizes[$box_settings['bio_size']];
		$box_settings['icons_size']          = $font_sizes[$box_settings['icons_size']];
		$box_settings['box_border_width']    = $border_widths[$box_settings['box_border']];
		$box_settings['bottom_border_width'] = $border_widths[$box_settings['bottom_border']];
		$box_settings['avatar_border_width'] = $border_widths[$box_settings['img_border']];

		// Rename some settings keys.
		$box_settings['avatar_style']        = $box_settings['img_style'];
		$box_settings['avatar_border_color'] = $box_settings['img_border_color'];
		$box_settings['avatar_default_img']  = $box_settings['img_default'];

		// Remove renamed settings.
		unset( $box_settings['headline_style'] );
		unset( $box_settings['bio_style'] );
		unset( $box_settings['headline_size'] );
		unset( $box_settings['name_size'] );
		unset( $box_settings['meta_size'] );
		unset( $box_settings['bio_size'] );
		unset( $box_settings['box_border'] );
		unset( $box_settings['bottom_border'] );
		unset( $box_settings['img_border'] );
		unset( $box_settings['img_style'] );
		unset( $box_settings['img_border_color'] );
		unset( $box_settings['img_default'] );

		// Convert "name_link" configuration.
		$name_link = array
		(
			'link'   => 'keep',
			'nolink' => 'remove',
		);
		if ( $box_settings['name_link'] == 'link' ) $box_settings['name_text_style'] = 'underline';
		$box_settings['name_inherited_underline'] = $name_link[$box_settings['name_link']];

		// Remove renamed settings.
		unset( $box_settings['name_link'] );

		// Move settings from 'main' to 'advanced' key.
		$advanced_settings['add_opengraph_meta'] = $main_settings['add_opengraph_meta'];
		$advanced_settings['add_google_meta']    = $main_settings['add_google_meta'];
		$advanced_settings['add_facebook_meta']  = $main_settings['add_facebook_meta'];

		// Remove moved settings from 'main'.
		unset ( $main_settings['add_opengraph_meta'] );
		unset ( $main_settings['add_google_meta'] );
		unset ( $main_settings['add_facebook_meta'] );

		// Move settings from 'main' to 'guest' key.
		$guest_settings = array();
		$guest_settings['include_guests_in_search'] = $main_settings['include_guests_in_search'];
		$guest_settings['guest_menu_item_level']    = ( $main_settings['admin_menu_level'] == 'true' ? 'top' : $main_settings['admin_menu_level'] );

		// Remove moved settings from 'main'.
		unset ( $main_settings['include_guests_in_search'] );
		unset ( $main_settings['admin_menu_level'] );

		// Move settings from 'main' to 'guest' key.
		$archives_settings = array();
		$archives_settings['guest_archive_enabled']   = $main_settings['enable_guest_archive'];
		$archives_settings['guest_archive_permalink'] = $main_settings['guest_archive_permalink'];
		$archives_settings['guest_archive_base']      = $main_settings['guest_archive_slug'];
		$archives_settings['guest_archive_tmpl']      = $main_settings['guest_archive_tmpl'];

		// Remove moved settings from 'main'.
		unset ( $main_settings['enable_guest_archive'] );
		unset ( $main_settings['guest_archive_permalink'] );
		unset ( $main_settings['guest_archive_slug'] );
		unset ( $main_settings['guest_archive_tmpl'] );

		// Move settings from 'main' to 'byline' key.
		$byline_settings = array();
		$byline_settings['byline_automatic_integration'] = $main_settings['byline_auto_update'];
		$byline_settings['byline_multiauthor_display']   = $main_settings['by_line'];
		$byline_settings['byline_multiauthor_link']      = $main_settings['by_line_link'];

		// Remove moved settings from 'main'.
		unset ( $main_settings['by_line'] );
		unset ( $main_settings['by_line_link'] );
		unset ( $main_settings['byline_auto_update'] );

		// Move settings from 'main' to 'box' key.
		$box_settings['multiauthor_box_layout'] = $main_settings['multiauthor_layout'];

		// Remove moved settings from 'main'.
		unset ( $main_settings['multiauthor_layout'] );

		// Rename some settings keys.
		$box_settings['bottom_background_color'] = $box_settings['bottom_bg'];
		$box_settings['headline_text_align']     = $box_settings['headline_align'];
		$box_settings['headline_text_color']     = $box_settings['headline_color'];
		$box_settings['name_text_color']         = $box_settings['name_color'];
		$box_settings['meta_text_style']         = $box_settings['meta_style'];
		$box_settings['meta_text_color']         = $box_settings['meta_color'];
		$box_settings['bio_text_align']          = $box_settings['bio_align'];
		$box_settings['bio_text_color']          = $box_settings['bio_color'];

		// Remove renamed settings.
		unset( $box_settings['bottom_bg'] );
		unset( $box_settings['headline_align'] );
		unset( $box_settings['headline_color'] );
		unset( $box_settings['name_color'] );
		unset( $box_settings['meta_style'] );
		unset( $box_settings['meta_color'] );
		unset( $box_settings['bio_align'] );
		unset( $box_settings['bio_color'] );

		// Convert saved layout template value.
		$layout = array
		(
			'default'     => 'layout-1',
			'default-rtl' => 'layout-2',
			'tabbed'      => 'layout-3',
			'ribbon'      => 'layout-4',
			'ribbon-rtl'  => 'layout-5',
		);
		$box_settings['layout'] = $layout[$box_settings['layout']];

		// Move settings from 'main' to 'box' key (by now, 'main' should contain just 'related' and 'social' settings).
		$box_settings = array_merge( $box_settings, $main_settings );

		// Update settings into db.
		update_option( MOLONGUI_AUTHORSHIP_BYLINE_SETTINGS, $byline_settings );
		update_option( MOLONGUI_AUTHORSHIP_BOX_SETTINGS, $box_settings );
		update_option( MOLONGUI_AUTHORSHIP_GUEST_SETTINGS, $guest_settings );
		update_option( MOLONGUI_AUTHORSHIP_ARCHIVES_SETTINGS, $archives_settings );
		update_option( MOLONGUI_AUTHORSHIP_ADVANCED_SETTINGS, $advanced_settings );

		// Remove main key from db.
		delete_option( 'molongui_authorship_main' );
	}

	/**
	 * Empty function declaration to avoid any inconsistency when updating from older versions.
	 *
	 * Version 2.2.2 introduced an error for premium installations desynchronizing plugin and db versions with the ones
	 * on the free installer. Versions will be in sync on version 3.0.3
	 *
	 * @access  public
	 * @since   3.0.0
	 * @version 3.0.0
	 */
	public function db_update_7() {}

	/**
	 * Updates data to be compatible with version 3.0.0 and higher.
	 *
	 * Changes made on version 3.0.0:
	 *
	 *   + added 'box_border' setting.
	 *   ~ renamed options for 'layout' setting.
	 *
	 * @access  public
	 * @since   3.0.0
	 * @version 3.0.0
	 */
	public function db_update_8()
	{
		// Get current settings.
		$box_settings      = get_option(MOLONGUI_AUTHORSHIP_BOX_SETTINGS);
		$byline_settings   = get_option(MOLONGUI_AUTHORSHIP_BYLINE_SETTINGS);
		$guest_settings    = get_option(MOLONGUI_AUTHORSHIP_GUEST_SETTINGS);
		$archives_settings = get_option(MOLONGUI_AUTHORSHIP_ARCHIVES_SETTINGS);
		$advanced_settings = get_option(MOLONGUI_AUTHORSHIP_ADVANCED_SETTINGS);

		// Convert box border settings.
		if ( isset( $box_settings['box_border_style'] ) and $box_settings['box_border_style'] == 'none' )
		{
			$box_settings['box_border']       = 'none';
			$box_settings['box_border_style'] = 'solid';
		}
		elseif ( isset( $box_settings['box_border_style'] ) )
		{
			$box_settings['box_border']       = 'all';
		}
		else
		{
			$box_settings['box_border']       = 'none';
			$box_settings['box_border_style'] = 'solid';
		}

		// Initialize profile layout option based on old layout setting.
		$profile = array
		(
			'layout-1' => 'layout-1',
			'layout-2' => 'layout-2',
			'layout-3' => 'layout-1',
			'layout-4' => 'layout-7',
			'layout-5' => 'layout-8',
		);
		$box_settings['profile_layout'] = $profile[$box_settings['layout']];

		// Convert old box layout option to the equivalent new one.
		$layout = array
		(
			'layout-1' => 'slim',
			'layout-2' => 'slim',
			'layout-3' => 'tabbed',
			'layout-4' => 'slim',
			'layout-5' => 'slim',
		);
		$box_settings['layout'] = $layout[$box_settings['layout']];
	}

}