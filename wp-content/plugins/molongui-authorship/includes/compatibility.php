<?php

// Deny direct access to this file.
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Compatibility with third plugins and themes.
 *
 * @author     Amitzy
 * @package    Authorship
 * @subpackage /includes
 * @since      2.0.2
 * @version    2.1.0
 */

/**
 * wpDiscuz
 *
 * @see     wpdiscuz/templates/comment/class.WpdiscuzWalker.php
 *
 * @since   2.0.2
 * @version 2.1.0
 */
//if ( class_exists( 'WpdiscuzWalker' ) )
if ( is_plugin_active( 'wpdiscuz/class.WpdiscuzCore.php' ) )
{
    /**
     * Commenter display name.
     */
    add_filter( 'wpdiscuz_comment_author', function( $authorName, $comment )
    {
        return ( $comment->comment_author ? $comment->comment_author : __( 'Anonymous', 'wpdiscuz' ) );
    }, 99, 2 );

    /**
     * Commenter url for display name link.
     */
    add_filter( 'get_comment_author_url', function( $commentAuthorUrl, $comment_id, $comment )
    {
        $email = $comment->comment_author_email;

        // If empty $email, leave.
        if ( !$email ) return $commentAuthorUrl;

        // Instantiate 'Author' class.
        if ( !class_exists( 'Molongui\Authorship\Includes\Author' ) ) require_once( MOLONGUI_AUTHORSHIP_DIR . '/includes/class-author.php' );
        $author = new Molongui\Authorship\Includes\Author();

        // If registered wp user, check for local avatar.
        if ( $user = $author->get_author_by( 'user_email', $email, 'user' ) )
        {
            // Do nothing.
            //$commentAuthorUrl = get_author_posts_url( $user->ID );
        }
        // If guest author, check for local avatar.
        elseif ( $guest = $author->get_author_by( '_molongui_guest_author_mail', $email, 'guest' ) )
        {
            // Get plugin settings.
            $settings = get_option( MOLONGUI_AUTHORSHIP_ARCHIVES_SETTINGS );

            if ( molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ) and $settings['guest_archive_enabled'] )
            {
                // Get link modifiers.
                $permalink = ( ( isset( $settings['guest_archive_permalink'] ) and !empty( $settings['guest_archive_permalink'] ) ) ? $settings['guest_archive_permalink'] : '' );
                $slug      = ( ( isset( $settings['guest_archive_base'] ) and !empty( $settings['guest_archive_base'] ) ) ? $settings['guest_archive_base'] : 'author' );

                // Get guest author archive page link.
                $commentAuthorUrl = home_url( ( !empty( $permalink ) ? $permalink.'/' : '' ) . $slug . '/' . $guest->post_name );
            }
            else
            {
                $commentAuthorUrl = '#molongui-disabled-link';
            }
        }

        // Return data.
        return $commentAuthorUrl;

    }, 10, 3 );

    /**
     * Commenter archive page url to use as avatar link.
     */
    add_filter( 'wpdiscuz_profile_url', function( $profileUrl, $user )
    {
        return '';
    }, 10, 2 );

    /**
     * Commenter email address to retrieve avatar.
     */
    add_filter( 'wpdiscuz_author_avatar_field', function( $authorAvatarField, $comment, $user, $profileUrl )
    {
        return $comment->comment_author_email;
    }, 10, 4 );
}
