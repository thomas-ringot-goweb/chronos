<?php

namespace Molongui\Authorship\Includes;

/**
 * As this is a namespaced class, calls to external PHP or WP classes require the full namespace.
 * To avoid having to call a function or class by its full namespace every time, 'use' keywords are set.
 *
 * @see https://roots.io/upping-php-requirements-in-your-wordpress-themes-and-plugins/
 */
use WP_Query;
use WP_User_Query;

// Deny direct access to this file.
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * The Guest Author Class.
 *
 * @author     Amitzy
 * @package    Authorship
 * @subpackage /includes
 * @since      1.0.0
 * @version    3.1.2
 */
class Author
{
    /*******************************************************************************************************************
     * GLOBAL METHODS
     ******************************************************************************************************************/

	/**
	 * Hooks into the appropriate actions when the class is constructed.
	 *
	 * @access  public
	 * @since   1.0.0
	 * @version 2.0.0
	 */
	public function __construct() {}

    /**
     * Returns assigned main author for a given post id.
     *
     * Each post should have one main author defined under the key '_molongui_main_author' whose value is composed of a
     * string specifying the author type {user | guest} and the user/post id: "guest-18" or "user-2".
     *
     * This function parses and formats that data to return an object consisting of the author id and the author type.
     *
     * @access  public
     * @return  object
     * @since   2.0.0
     * @version 2.0.0
     */
    public function get_main_author( $post_id )
    {
        // Init vars.
        $data = new \stdClass();

        // Get main author.
        $main_author = get_post_meta( $post_id, '_molongui_main_author', true );

        // Parse and prepare data. If no "main author" defined, go get post_author.
        if ( !empty( $main_author ) )
        {
            $split      = explode( '-', $main_author );
            $data->id   = $split[1];
            $data->type = $split[0];
            $data->ref  = $main_author;
        }
        else
        {
            $data->id   = get_post_field( 'post_author', $post_id );
            $data->type = 'user';
            $data->ref  = $data->type.'-'.$data->id;
        }

        // Return author(s).
        return $data;
    }

    /**
     * Returns assigned author(s) for a given post id.
     *
     * Each post may have several db entries under the key '_molongui_author'. Each key has a value composed of a string
     * specifying the author type {user | guest} and the user/post id: "guest-18" or "user-2".
     *
     * This function collects all that data, parses it and formats it to return an array of objects consisting of the
     * author id and the author type.
     *
     * @access  public
     * @param   int     $post_id    Post id.
     * @param   string  $key        Key to retrieve {'' | id | type | ref}. Defaults to '' (all).
     * @return  array
     * @since   2.0.0
     * @version 2.0.0
     */
    public function get_authors( $post_id, $key = '' )
    {
        // Init vars.
        $data = array();

        // Get main author.
        $main_author = $this->get_main_author( $post_id );

        // Get author(s).
        $authors = get_post_meta( $post_id, '_molongui_author', false );

        // Parse and prepare data skipping main author.
        if ( !empty( $authors) )
        {
            foreach ( $authors as $author )
            {
                $split  = explode( '-', $author );
                if ( $split[1] == $main_author->id ) continue;
                $data[] = (object) array( 'id' => $split[1], 'type' => $split[0], 'ref' => $author );
            }
        }

        // Insert main author as the first item in the array.
        array_unshift( $data, $main_author );

        // Return author(s) as an array of objects.
        if ( !$key ) return $data;

        // Select values from selected key to return.
        $values = array();
        foreach( $data as $author ) $values[] = $author->$key;

        // Return author(s).
        return $values;
    }

    /**
     * Returns whether the author is a guest author.
     *
     * @access  public
     * @param   mixed   $author    Author object (id, type, ref) or db stored author's reference (i.e.: guest-34, user-2...).
     * @return  bool
     * @since   2.0.0
     * @version 2.0.0
     */
    public function is_guest( $author )
    {
        if ( is_object( $author ) ) return ( $author->type == 'guest' ? true : false );

        $split = explode( '-', $author );
        return ( $split[0] == 'guest' ? true : false );
    }

    /**
     * Returns whether the post has a guest author.
     *
     * @access  public
     * @param   int     $post_id    Post id.
     * @return  bool
     * @since   2.0.0
     * @version 2.0.0
     */
    public function is_guest_post( $post_id )
    {
        // Get author(s).
        $authors = get_post_meta( $post_id, '_molongui_author', false );

        // Look for any guest author.
        foreach ( $authors as $author )
        {
            $split = explode( '-', $author );
            if ( $split[0] == 'guest' ) return true;
        }

        // No guest author found.
        return false;
    }

    /**
     * Returns whether the post has more than one author.
     *
     * @access  public
     * @param   mixed   $data   Authors object or post id.
     * @return  bool
     * @since   2.0.0
     * @version 2.0.0
     */
    public function is_multiauthor( $data )
    {
        // If gotten parameter is an array, just count number of elements.
        if ( is_array( $data ) ) return ( count( $data ) > 1 ? true : false );

        // If gotten parameter is post id, get its authors and count number of elements.
        return ( count( $this->get_authors( $data ) ) > 1 ? true : false );
    }

    /**
     * Get author data.
     *
     * @param   int     $author_id      The ID of the author to get data from.
     * @param   string  $author_type    The type of author: {user | guest}.
     * @param   array   $settings       The plugin settings.
     * @return  array   $author         The author data.
     * @access  public
     * @since   1.2.14
     * @version 2.1.0
     */
    public function get_data ( $author_id, $author_type, $settings = array() )
    {
        // Load settings if none given.
        if ( !isset( $settings ) or empty( $settings ) )
        {
	        $box_settings      = get_option( MOLONGUI_AUTHORSHIP_BOX_SETTINGS );
            $byline_settings   = get_option( MOLONGUI_AUTHORSHIP_BYLINE_SETTINGS );
            $guest_settings    = get_option( MOLONGUI_AUTHORSHIP_GUEST_SETTINGS );
            $archives_settings = get_option( MOLONGUI_AUTHORSHIP_ARCHIVES_SETTINGS );
            $advanced_settings = get_option( MOLONGUI_AUTHORSHIP_ADVANCED_SETTINGS );
            $string_settings   = get_option( MOLONGUI_AUTHORSHIP_STRING_SETTINGS );
	        $settings          = array_merge( $box_settings, $byline_settings, $guest_settings, $archives_settings, $advanced_settings, $string_settings );
        }

        // Load available social networks.
        $social_networks = include( MOLONGUI_AUTHORSHIP_DIR . "/config/social.php" );

        // Get author data.
        if ( $author_type == 'guest' )
        {
            // Prepare archive page URI base.
            $uri_slug = ( ( isset( $settings['guest_archive_permalink']) and !empty( $settings['guest_archive_permalink'] ) ) ? '/' . $settings['guest_archive_permalink'] : '' ) .
                ( ( isset( $settings['guest_archive_base']) and !empty( $settings['guest_archive_base'] ) ) ? '/' . $settings['guest_archive_base'] : '/author' );

            /**
             * Action hook to run before getting guest author data.
             *
             * @since   2.0.0
             * @version 2.0.0
             */
            do_action( 'molongui_authorship_before_get_guest_author_data', $author_id, $settings );

            // Guest author.
            $author_post            = get_post( $author_id );
            $author['id']           = $author_id;
            $author['name']         = $author_post->post_title;
            $author['slug']         = $author_post->post_name;
            $author['mail']         = get_post_meta( $author_id, '_molongui_guest_author_mail', true );
            $author['show_mail']    = get_post_meta( $author_id, '_molongui_guest_author_show_mail', true );
            $author['phone']        = get_post_meta( $author_id, '_molongui_guest_author_phone', true );
            $author['show_phone']   = get_post_meta( $author_id, '_molongui_guest_author_show_phone', true );
            $author['link']         = get_post_meta( $author_id, '_molongui_guest_author_link', true );
            $author['url']          = ( $settings['guest_archive_enabled'] ? home_url( $uri_slug.'/'.$author['slug'] ) : '#molongui-disabled-link' );
            $author['img']          = ( has_post_thumbnail( $author_id ) ? get_the_post_thumbnail( $author_id, "thumbnail", array( 'class' => 'mabt-radius-'.$settings['avatar_style'] . ' molongui-border-style-'.$settings['avatar_border_style'] . ' molongui-border-width-'.$settings['avatar_border_width'].'-px', 'style' => 'border-color:'.$settings['avatar_border_color'].';', 'itemprop' => 'image') ) : '' );
            $author['job']          = get_post_meta( $author_id, '_molongui_guest_author_job', true ) ;
            $author['company']      = get_post_meta( $author_id, '_molongui_guest_author_company', true );
            $author['company_link'] = get_post_meta( $author_id, '_molongui_guest_author_company_link', true );
            $author['bio']          = $author_post->post_content;
            $author['type']         = 'guest';
            foreach ( $social_networks as $id => $social_network )
            {
                $author[$id] = get_post_meta( $author_id, '_molongui_guest_author_'.$id, true );
            }

            /**
             * Action hook to run after getting guest author data.
             *
             * @since   2.0.0
             * @version 2.0.0
             */
            do_action( 'molongui_authorship_after_get_guest_author_data', $author_id, $settings );
        }
        else
        {
            // Registered wp user author.
            $author_post            = get_user_by( 'id', $author_id );
            $author['id']           = $author_id;
            $author['name']         = $author_post->display_name;
            $author['slug']         = $author_post->user_nicename;
            $author['mail']         = $author_post->user_email;
            $author['show_mail']    = get_user_meta( $author_id, 'molongui_author_show_mail', true );
            $author['phone']        = get_user_meta( $author_id, 'molongui_author_phone', true );
            $author['show_phone']   = get_user_meta( $author_id, 'molongui_author_show_phone', true );
            $author['link']         = get_user_meta( $author_id, 'molongui_author_link', true );
            $author['url']          = get_author_posts_url( $author_id, $author_post->user_nicename );
            $author['img']          = ( get_the_author_meta( 'molongui_author_image_id', $author_id ) ? wp_get_attachment_image( get_user_meta( $author_id, 'molongui_author_image_id', true ), "thumbnail", false, array( 'class' => 'mabt-radius-'.$settings['avatar_style'] . ' molongui-border-style-'.$settings['avatar_border_style'] . ' molongui-border-width-'.$settings['avatar_border_width'].'-px', 'style' => 'border-color:'.$settings['avatar_border_color'].';', 'itemprop' => 'image' ) ) : "" );
            $author['job']          = get_user_meta( $author_id, 'molongui_author_job', true );
            $author['company']      = get_user_meta( $author_id, 'molongui_author_company', true );
            $author['company_link'] = get_user_meta( $author_id, 'molongui_author_company_link', true );
            $author['bio']          = $author_post->description;
            $author['type']         = 'user';
            foreach ( $social_networks as $id => $social_network )
            {
                $author[$id] = get_user_meta( $author_id, 'molongui_author_'.$id, true );
            }
        }

        // Handle author profile image if none set.
        if( empty( $author['img'] ) and $settings['avatar_default_img'] != 'none' )
        {
            // Show acronym as default image if configured so.
            if ( $settings['avatar_default_img'] == 'acronym' )
            {
                // Generate "initials" image.
                $author['img'] = $this->get_acronym($author['name'], $settings );
            }
            // Try to load Blank, Mistery Man or Gravatar if configured so.
            elseif( $settings['avatar_default_img'] == 'mm' or $settings['avatar_default_img'] == 'blank' or !empty( $author['mail'] ) )
            {
                // Try to load the associated Gravatar.
                // @see https://codex.wordpress.org/Function_Reference/get_avatar
                $author['img'] = get_avatar( $author['mail'], '150', $settings['avatar_default_img'], false, array( 'class' => 'mabt-radius-'.$settings['avatar_style'] . ' molongui-border-style-'.$settings['avatar_border_style'] . ' molongui-border-width-'.$settings['avatar_border_width'].'-px', 'style' => 'border-color:'.$settings['avatar_border_color'].';', 'itemprop' => 'image', 'force_display' => true ) );
            }
        }

        // Return data.
        return $author;
    }

    /**
     * Get author acronym.
     *
     * @param   string  $name       The author name.
     * @param   array   $settings   Plugin settings.
     * @return  string  $acronym    The author acronym.
     * @access  public
     * @since   1.3.0
     * @version 2.1.0
     */
    public function get_acronym ( $name, $settings )
    {
        // Return styled acronym.
        $html  = '';
        $html .= '<div data-avatar-type="acronym" class="mabt-radius-'.$settings['avatar_style'] . ' molongui-border-style-'.$settings['avatar_border_style'] . ' molongui-border-width-'.$settings['avatar_border_width'].'-px' . ' acronym-container" style="background:'.$settings['acronym_bg_color'].'; color:'.$settings['acronym_text_color'].';">';
        $html .= '<div class="molongui-vertical-aligned">';
        $html .= molongui_get_acronym( $name );
        $html .= '</div>';
        $html .= '</div>';

        return $html;
    }

    /**
     * Get posts authored by a given author.
     *
     * @see     http://codex.wordpress.org/Class_Reference/WP_Query
     *
     * @param   int     $author_id      The ID of the author to get data from.
     * @param   string  $author_type    The type of author: {user | guest}.
     * @param   array   $settings       The plugin settings.
     * @param   boolean $get_all        Whether to limit the query or not.
     * @param   array   $exclude        List of post IDs not to retrieve (usually used to avoid current post from being retrieved).
     * @param   string  $entry          Post type: {post | page | ...}. Defaults to post.
     * @param   array   $meta_query     Meta query to include.
     * @return  array   $posts          The author posts.
     * @access  public
     * @since   1.2.17
     * @version 3.1.2
     */
    public function get_posts( $author_id, $author_type, $settings = array(), $get_all = false, $exclude = array(), $entry = 'post', $meta_query = array() )
    {
	    // Load settings if none given.
	    if ( !isset( $settings ) or empty( $settings ) ) $settings = get_option( MOLONGUI_AUTHORSHIP_BOX_SETTINGS );

	    // Handle post types to query.
        switch ( $entry )
        {
            case 'all':
	            $entries = molongui_get_post_types( 'all', 'names', false );
	        break;

	        case 'selected':
		        $entries = molongui_supported_post_types( MOLONGUI_AUTHORSHIP_ID, 'all', false );
		    break;

	        case 'related':
		        foreach( molongui_get_post_types( 'all', 'names', false ) as $post_type_name )
		        {
			        if ( isset( $settings['related_post_type_'.$post_type_name] ) and $settings['related_post_type_'.$post_type_name] ) $entries[] = $post_type_name;
		        }
		    break;

	        default:
		        $entries = $entry;
		    break;
        }

        // Get posts.
        if ( $author_type == 'guest' )
        {
            // Prepare meta-query accepting the one from the arguments.
	        if ( isset( $meta_query ) and !empty( $meta_query ) )
            {
	            $mq = array
                (
		            'relation'  => 'AND',
                    array
                    (
                        'key'   => $meta_query['key'],
                        'value' => $meta_query['value'],
                    ),
                    array
                    (
	                    'key'   => '_molongui_author',
	                    'value' => 'guest-'.$author_id,
                    ),
	            );
            }
            else
            {
	            $mq = array
	            (
		            array
		            (
			            'key'   => '_molongui_author',
			            'value' => 'guest-'.$author_id,
		            ),
	            );
            }

            // Guest author posts query.
            $args = array
            (
                'post_type'      => $entries,
                'post__not_in'   => $exclude,
                'post_status'    => 'publish',
                'meta_query'     => $mq,
                'orderby'        => ( isset( $settings['related_order_by'] ) and $settings['related_order_by'] ? $settings['related_order_by'] : 'ASC' ),
                'order'          => ( isset( $settings['related_order'] ) and $settings['related_order'] ? $settings['related_order'] : 'date' ),
                'posts_per_page' => ( $get_all ? '-1' : $settings['related_items'] ),
                'no_found_rows'  => true,
            );

	        // Get data.
	        $data = new WP_Query( $args );

	        // Prepare data.
	        foreach ( $data->posts as $post ) $posts[] = $post;
        }
        else
        {
            // Registered wp user author posts queries.

	        // Get posts authored by $author_id with no _molongui_author assigned (normally, posts which already existed before installing the plugin and have not been edited).

	        // Prepare meta-query accepting the one from the arguments.
	        if ( isset( $meta_query ) and !empty( $meta_query ) )
	        {
		        $mq = array
		        (
			        'relation'  => 'AND',
			        array
			        (
				        'key'     => $meta_query['key'],
				        'value'   => $meta_query['value'],
			        ),
			        array
                    (
				        'key'     => '_molongui_author',
				        'compare' => 'NOT EXISTS',
			        ),
		        );
	        }
	        else
	        {
		        $mq = array
		        (
			        array
			        (
				        'key'     => '_molongui_author',
				        'compare' => 'NOT EXISTS',
			        ),
		        );
	        }
	        $args1 = array
            (
		        'post_type'      => $entries,
		        'post__not_in'   => $exclude,
		        'post_status'    => 'publish',
		        'meta_query'     => $mq,
		        'author'         => $author_id,
		        'orderby'        => ( isset( $settings['related_order_by'] ) and $settings['related_order_by'] ? $settings['related_order_by'] : 'ASC' ),
		        'order'          => ( isset( $settings['related_order'] ) and $settings['related_order'] ? $settings['related_order'] : 'date' ),
		        'posts_per_page' => ( $get_all ? '-1' : $settings['related_items'] ),
		        'no_found_rows'  => true,
	        );
	        $data1 = get_posts( $args1 );

	        // Get posts which _molongui_author value equals to 'user-'.$author_id.

	        // Prepare meta-query accepting the one from the arguments.
	        if ( isset( $meta_query ) and !empty( $meta_query ) )
	        {
		        $mq = array
		        (
			        'relation'  => 'AND',
			        array
			        (
				        'key'    => $meta_query['key'],
				        'value'  => $meta_query['value'],
			        ),
                    array
                    (
				        'key'     => '_molongui_author',
				        'compare' => 'EXISTS',
			        ),
			        array(
				        'key'     => '_molongui_author',
				        'value'   => 'user-'.$author_id,
				        'compare' => '==',
			        ),
		        );
	        }
	        else
	        {
		        $mq = array
		        (
			        array
                    (
				        'key'     => '_molongui_author',
				        'compare' => 'EXISTS',
			        ),
			        array
                    (
				        'key'     => '_molongui_author',
				        'value'   => 'user-'.$author_id,
				        'compare' => '==',
			        ),
		        );
	        }
	        $args2 = array
            (
		        'post_type'      => $entries,
		        'post__not_in'   => $exclude,
		        'post_status'    => 'publish',
		        'meta_query'     => $mq,
		        'orderby'        => ( isset( $settings['related_order_by'] ) and $settings['related_order_by'] ? $settings['related_order_by'] : 'ASC' ),
		        'order'          => ( isset( $settings['related_order'] ) and $settings['related_order'] ? $settings['related_order'] : 'date' ),
		        'posts_per_page' => ( $get_all ? '-1' : $settings['related_items'] ),
		        'no_found_rows'  => true,
	        );
	        $data2 = get_posts( $args2 );

	        // Merge both data sets.
	        $data = array_merge( $data1, $data2 );

	        // Get just the IDs of all the posts found, while also dropping any duplicates.
	        $postIDs = array_unique( wp_list_pluck( $data, 'ID' ) );

	        // Do a new query with these IDs to get a properly sorted array of post objects.
	        $args3 = array
            (
		        'post_type'      => $entries,
		        'post__in'       => $postIDs,
		        'post_status'    => 'publish',
		        'orderby'        => ( ( isset( $settings['related_order_by'] ) and $settings['related_order_by'] ) ? $settings['related_order_by'] : 'ASC' ),
		        'order'          => ( ( isset( $settings['related_order'] ) and $settings['related_order'] ) ? $settings['related_order'] : 'date' ),
		        'posts_per_page' => ( $get_all ? '-1' : $settings['related_items'] ),
		        'no_found_rows'  => true,
	        );

	        // Get data.
	        $posts = get_posts( $args3 );
        }

        // Return data.
        return ( !empty( $posts ) ? $posts : array() );
    }

    /**
     * Returns a list of all authors.
     *
     * @see     https://codex.wordpress.org/Function_Reference/get_users
     *          https://codex.wordpress.org/Roles_and_Capabilities
     *
     * @access  public
     * @param   string  $type           Type of author to retrieve {authors | users | guests}. Defaults to 'authors'.
     * @param   array   $exclude_users  Array of users ID to exclude from being returned.
     * @param   array   $exclude_guests Array of guests ID to exclude from being returned.
     * @param   string  $order          Designates the ascending or descending order of the 'orderby' parameter {asc | desc}. Defaults to 'asc'.
     * @param   string  $orderby        Sort retrieved authors by parameter: {name | mail | job | company}. Defaults to 'name'.
     * @param   string  $output         How the returned value is formatted {array | object | dropdown}. Defaults to 'array'.
     * @return  mixed   $authors        Authors list.
     * @since   1.3.5
     * @version 2.1.0
     */
    public function get_all( $type = 'authors', $exclude_users = array(), $exclude_guests = array(), $order = 'asc', $orderby = 'name', $output = 'array' )
    {
        // Variables.
        $authors = array();

        // Load plugin settings.
	    $box_settings      = get_option( MOLONGUI_AUTHORSHIP_BOX_SETTINGS );
	    $byline_settings   = get_option( MOLONGUI_AUTHORSHIP_BYLINE_SETTINGS );
	    $guest_settings    = get_option( MOLONGUI_AUTHORSHIP_GUEST_SETTINGS );
	    $archives_settings = get_option( MOLONGUI_AUTHORSHIP_ARCHIVES_SETTINGS );
	    $advanced_settings = get_option( MOLONGUI_AUTHORSHIP_ADVANCED_SETTINGS );
	    $string_settings   = get_option( MOLONGUI_AUTHORSHIP_STRING_SETTINGS );
	    $settings          = array_merge( $box_settings, $byline_settings, $guest_settings, $archives_settings, $advanced_settings, $string_settings );

        // Get registered wp users with post edition capabilities.
        if ( $type == 'authors' or $type == 'users' )
        {
            $args = array(
                'role__in' => array( 'administrator', 'editor', 'author', 'contributor' ),
                'exclude'  => $exclude_users,
                'order'    => $order,
                'orderby'  => $orderby,
            );
            $wp_users = get_users( $args ); // Array of WP_User objects.

            // Parse data from registered wp users.
            foreach ( $wp_users as $wp_user )
            {
                $authors[] = $this->get_data( $wp_user->ID, 'user', $settings );
            }
        }

        // Get guest authors.
        if ( $type == 'authors' or $type == 'guests' )
        {
            $guests = $this->get_guests( $exclude_guests, false ); // Array of stdClass objects.

            // Parse data from registered wp users.
            foreach ( $guests->posts as $guest )
            {
                $authors[] = $this->get_data( $guest->ID, 'guest', $settings );
            }
        }

        // Sort data.
        // @see http://stackoverflow.com/a/2477524
        // @see http://stackoverflow.com/a/2477532
        // @see http://php.net/manual/es/function.strcasecmp.php
        usort( $authors, function ( $a, $b ) use ( $orderby )
        {
            return strcasecmp( $a[$orderby], $b[$orderby] );
        });

        // Reverse data order.
        if ( $order == 'desc' ) $authors = array_reverse( $authors );

        // Check output format.
        if ( $output != 'dropdown' ) return $authors;

        // Author select html markup.
        $output = '';
        if( !empty( $authors ) )
        {
            $output .= '<select name="_molongui_author" class="searchable" data-placeholder="'.__( 'Add another author...', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ).'">';

            foreach( $authors as $author )
            {
                $output .= '<option value="' . $author['type'].'-'.$author['id'] . '">' . $author['name'] . '</option>';
            }
            $output .= '</select>';
        }
        else
        {
            $output .= '<div><p>'.__( 'No authors found.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ).'</p></div>';
        }

        $output .= '<div class="block__list block__list_words"><ul id="molongui_authors">';

        // List current post authors (if any).
        global $post;
        $post_authors = $this->get_authors( $post->ID );

        if( !empty( $post_authors ) )
        {
            foreach ( $post_authors as $author )
            {
                // Get display name.
                if ( $author->type == 'guest' )
                {
                    $author_name = get_post_field( 'post_title', $author->id );
                }
                else
                {
                    $user = get_userdata( $author->id );
                    $author_name = $user->display_name;
                }

                // List existing post authors.
                $output .= '<li data-value="'.$author->ref.'">'.$author_name.'<input type="hidden" name="molongui_authors[]" value="'.$author->ref.'" /><span class="dashicons dashicons-trash molongui-tip js-remove" data-tip="'.__( 'Remove author from selection', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ).'"></span></li>';
            }
        }

        $output .= '</ul></div>';

        // Return dropdown html markup.
        return $output;
    }

    /**
     * Returns a list of registered wp users.
     *
     * @see     https://codex.wordpress.org/Class_Reference/WP_Query#Pagination_Parameters
     *
     * @access  public
     * @param   array       $exclude    Guest authors to exclude from the returned list.
     * @param   boolean     $dropdown   Whether to get an object or an html dropdown list.
     * @return  mixed
     * @since   2.0.0
     * @version 2.0.0
     */
    public function get_users( $exclude = array(), $dropdown = true )
    {

    }

    /**
     * Returns a list of guest authors.
     *
     * @see     https://codex.wordpress.org/Class_Reference/WP_Query#Pagination_Parameters
     *
     * @access  public
     * @param   array       $exclude    Guest authors to exclude from the returned list.
     * @param   boolean     $dropdown   Whether to get an object or an html dropdown list.
     * @return  mixed
     * @since   1.0.0
     * @version 2.0.0
     */
    public function get_guests( $exclude = array(), $dropdown = true )
    {
        // Get post.
        global $post;

        // Query guest authors.
        $args   = array( 'post_type' => 'molongui_guestauthor', 'posts_per_page' => -1, 'post__not_in' => $exclude, 'order' => 'ASC', 'orderby' => 'title' );
        $guests = new WP_Query( $args );

        // Check output format.
        if ( !$dropdown ) return $guests;

        // Get post authors ids (if any).
        $post_authors = $this->get_authors( $post->ID, 'id' );

        // Mount html markup.
        $output = '';
        if ( $guests->have_posts() )
        {
            $output .= '<select name="_molongui_author" class="multiple">';
            foreach( $guests->posts as $guest )
            {
                $output .= '<option value="' . $guest->ID . '" ' . ( in_array( $guest->ID, $post_authors ) ? 'selected' : '' ) . '>' . $guest->post_title . '</option>';
            }
            $output .= '</select>';
            $output .= '<div><ul id="molongui-authors" class="sortable"></ul></div>';
        }

        // Return data.
        return $output;
    }

    /**
     * .
     *
     * @param   string  $field  .
     * @param   mixed   $value  .
     * @param   string  $type   .
     * @return  bool
     * @since   2.0.0
     * @version 2.0.0
     */
    public function get_author_by( $field, $value, $type = 'user' )
    {
        if ( $type == 'user' )
        {
            $user_query = new WP_User_Query(
                array(
                    'search'        => $value,
                    'search_fields' => array( $field ),
                )
            );
            $user = $user_query->get_results();

            return ( !empty( $user['0'] ) ? $user['0'] : false );
        }
        elseif ( $type == 'guest' )
        {
            $guest = new WP_Query(
                array(
                    'post_type'  => 'molongui_guestauthor',
                    'meta_query' => array(
                        array(
                            'key'     => $field,
                            'value'   => $value,
                            'compare' => '=',
                        ),
                    ),
                )
            );

            if ( $guest->have_posts() ) return ( !empty( $guest->posts['0'] ) ? $guest->posts['0'] : false );
        }

        return false;
    }

    /*******************************************************************************************************************
     * CUSTOM POST TYPE
     ******************************************************************************************************************/

	/**
	 * Registers "Guest Author" custom post-type.
	 *
	 * This functions registers a new post-type called "molongui_guestauthor".
	 * This post-type holds guest authors specific data.
	 *
	 * CPT menu item is placed as per user preference. Default is as a new top level menu.
	 *
	 * @see     https://codex.wordpress.org/Function_Reference/register_post_type
	 *          https://tommcfarlin.com/add-custom-post-type-to-menu/
     *
	 * @access  public
	 * @since   1.0.0
	 * @version 2.1.0
	 */
	public function register_custom_post_type()
	{
		// Get plugin settings.
		$settings = get_option( MOLONGUI_AUTHORSHIP_GUEST_SETTINGS );

		// Define custom post type properties.
		$labels = array(
			'name'					=> _x( 'Guest authors', 'post type general name', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'singular_name'			=> _x( 'Guest author', 'post type singular name', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'menu_name'				=> __( 'Guest authors', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'name_admin_bar'		=> __( 'Guest authors', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'all_items'				=> ( ( isset( $settings['guest_menu_item_level'] ) and $settings['guest_menu_item_level'] != 'top' ) ? __( 'Guest authors', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) : __( 'All Guest authors', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) ),
			'add_new'				=> _x( 'Add New', 'molongui_guestauthor', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'add_new_item'			=> __( 'Add New Guest author', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'edit_item'				=> __( 'Edit Guest author', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'new_item'				=> __( 'New Guest author', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'view_item'				=> __( 'View Guest author', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'search_items'			=> __( 'Search Guest authors', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'not_found'				=> __( 'No guest authors found', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'not_found_in_trash'	=> __( 'No guest authors found in the Trash', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'parent_item_colon'		=> '',
		);

		$args = array(
			'labels'				=> $labels,
			'description'			=> 'Holds our guest author and guest authors specific data',
			'public'				=> false,
			'exclude_from_search'	=> false,
			'publicly_queryable'	=> false,
			'show_ui'				=> true,
			'show_in_nav_menus'		=> true,
			'show_in_menu'          => ( ( isset( $settings['guest_menu_item_level'] ) and $settings['guest_menu_item_level'] != 'top' ) ? $settings['guest_menu_item_level'] : true ),
			'show_in_admin_bar '	=> true,
			'menu_position'			=> 5, // 5 = Below posts
			'menu_icon'				=> 'dashicons-id',
			'supports'		 		=> array( 'title', 'editor', 'thumbnail' ),
			'register_meta_box_cb'	=> '',
			'has_archive'			=> true,
			'rewrite'				=> array( 'slug' => 'guest-author' ),
		);

		// Register custom post type.
		register_post_type( 'molongui_guestauthor', $args );
	}

	/**
	 * Removes "Guest authors" menu item for users without capabilities to modify others posts or pages.
     *
     * @see     https://codex.wordpress.org/Roles_and_Capabilities
     *
	 * @access  public
	 * @since   2.0.2
	 * @version 2.1.0
	 */
	public function custom_remove_menu_pages()
    {
        $slug = 'edit.php?post_type=molongui_guestauthor';

		if ( !current_user_can( 'edit_others_pages' ) and !current_user_can( 'edit_others_posts' ) )
        {
	        // Get plugin settings.
	        $settings = get_option( MOLONGUI_AUTHORSHIP_GUEST_SETTINGS );

	        if ( isset( $settings['guest_menu_item_level'] ) and $settings['guest_menu_item_level'] != 'top' )
            {
                // 'users.php' top level menu is 'profile.php' for users without the 'edit_users' capability.
                if ( $settings['guest_menu_item_level'] == 'users.php' ) $settings['guest_menu_item_level'] = 'profile.php';

                remove_submenu_page( $settings['guest_menu_item_level'], $slug );
            }
            else
            {
	            remove_menu_page( $slug );
            }
		}
	}

    /**
     * Changes title placeholder for "guest author" custom post-type.
     *
     * @access  public
     * @param   string  $title  The title to change.
     * @return  string  $title  The modified title.
     * @since   1.0.0
     * @version 2.0.0
     */
    public function change_default_title( $title )
    {
        global $current_screen;

        if ( 'molongui_guestauthor' == $current_screen->post_type ) $title = __( 'Enter guest author name here', MOLONGUI_AUTHORSHIP_TEXTDOMAIN );

        return $title;
    }

    /**
     * Removes media buttons from "guest author" custom post-type.
     *
     * @access  public
     * @since   1.0.0
     * @version 1.0.0
     */
    public function remove_media_buttons()
    {
        global $current_screen;

        if( 'molongui_guestauthor' == $current_screen->post_type ) remove_action( 'media_buttons', 'media_buttons' );
    }

    /**
     * Removes the author dropdown from the post quick edit.
     *
     * @access  public
     * @since   2.0.0
     * @version 2.0.0
     */
    public function remove_quick_edit_author()
    {
        global $pagenow;

        if ( 'edit.php' == $pagenow && molongui_is_post_type_enabled( MOLONGUI_AUTHORSHIP_ID, get_post_type() ) )
        {
            remove_post_type_support( get_post_type(), 'author' );
        }
    }

    /**
     * Customizes the columns of the list shown on the Manage {molongui_guestauthor} Posts screen.
     *
     * @see     https://codex.wordpress.org/Plugin_API/Filter_Reference/manage_$post_type_posts_columns
     *
     * @param   array      $columns    An array of column name => label.
     * @return  array      $columns    Modified array of column name => label.
     * @access  public
     * @since   1.0.0
     * @version 3.1.2
     */
    public function add_list_columns( $columns )
    {
        // Unset some default columns to display them in a different position.
        unset( $columns['title'] );
        unset( $columns['date'] );
        unset( $columns['thumbnail'] );

        // Return customized columns array.
        return array_merge( $columns,
                            array('guestAuthorPic'   => __( 'Photo', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
                                  'title'		     => __( 'Name', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
                                  'guestAuthorMail'  => __( 'e-mail', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
                                  'guestAuthorUrl'   => __( 'Url', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
                                  'guestAuthorJob'   => __( 'Job position', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
                                  'guestAuthorCia'   => __( 'Company', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
                                  'guestAuthorEntries' => __( 'Authored entries', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
                                  'guestAuthorId'    => __( 'ID', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
                            )
        );
    }

    /**
     * Fills out added columns shown on the Manage {molongui_guestauthor} Posts screen.
     *
     * @see     https://codex.wordpress.org/Plugin_API/Action_Reference/manage_$post_type_posts_custom_column
     *
     * @param   array      $column     An array of column name => label.
     * @param   int        $ID         Post ID.
     * @access  public
     * @since   1.0.0
     * @version 3.1.2
     */
    public function fill_list_columns( $column, $ID )
    {
        if ( $column == 'guestAuthorId' )    echo $ID;
        if ( $column == 'guestAuthorPic' )   echo get_the_post_thumbnail( $ID, array( 60, 60) );
        if ( $column == 'guestAuthorJob' )   echo get_post_meta( $ID, '_molongui_guest_author_job', true );
        if ( $column == 'guestAuthorCia' )   echo get_post_meta( $ID, '_molongui_guest_author_company', true );
        if ( $column == 'guestAuthorMail' )  echo get_post_meta( $ID, '_molongui_guest_author_mail', true );
        if ( $column == 'guestAuthorUrl' )   echo '<a href="' . get_post_meta( $ID, '_molongui_guest_author_link', true ) . '">' . get_post_meta( $ID, '_molongui_guest_author_link', true ) . '</a>';
        if ( $column == 'guestAuthorEntries' )
        {
            foreach ( molongui_supported_post_types( MOLONGUI_AUTHORSHIP_ID, 'all', true ) as $post_type )
            {
                echo '<div>'.count( $this->get_posts( $ID, 'guest', false, false, array(), $post_type['id'] ) ).' '.$post_type['label'].'</div>';
            }
        }
    }

    /**
     * Modifies author column shown on the Manage Posts/Pages screen.
     *
     * @see     https://codex.wordpress.org/Plugin_API/Filter_Reference/manage_posts_columns
     * @see     https://codex.wordpress.org/Plugin_API/Filter_Reference/manage_pages_columns
     *
     * @param   array      $columns    An array of column name => label.
     * @return  array      $columns    Modified array of column name => label.
     * @access  public
     * @since   1.0.0
     * @version 2.0.0
     */
    public function change_author_column( $columns )
    {
        global $post;
        $post_type = get_post_type( $post );

        // Limit action to extended post types.
        $post_types = molongui_supported_post_types(MOLONGUI_AUTHORSHIP_ID, 'all' );

        // Modify author column only at Manage 'post-type' screen.
        if ( in_array( $post_type, $post_types ) )
        {
            // Remove default author column from the columns list.
            unset( $columns['author'] );

            // Add new author column in the same place where default was (after title).
            $new_columns = array();
            foreach ( $columns as $key => $column )
            {
                $new_columns[$key] = $column;
                if ( $key == 'title' ) $new_columns['realAuthor'] = __( 'Authors', MOLONGUI_AUTHORSHIP_TEXTDOMAIN );
            }

            return $new_columns;
        }
        else
        {
            return $columns;
        }
    }

    /**
     * Fills out edited author column shown on the Manage Posts/Pages screen.
     *
     * @see      https://codex.wordpress.org/Plugin_API/Action_Reference/manage_posts_custom_column
     *
     * @param   string     $column     Column name.
     * @param   int        $ID         Post ID.
     * @access  public
     * @since   1.0.0
     * @version 2.0.0
     */
    function fill_author_column( $column, $ID )
    {
        if ( $column == 'realAuthor' )
        {
            // Get author(s).
            $authors = $this->get_authors($ID );

            // Display author(s) link.
            foreach ( $authors as $author )
            {
                if ( $author->type == 'guest' )
                {
                    echo '<p style="margin: 0 0 2px;"><a href="' . admin_url() . 'post.php?post=' . $author->id . '&action=edit">' . get_the_title( $author->id ) . '</a> <span style="font-family: \'Courier New\', Courier, monospace; font-size: 81%; color: #a2a2a2;" >['.$author->type.']</span></p>';
                }
                else
                {
                    $user = get_userdata( $author->id );
                    echo '<p style="margin: 0 0 2px;"><a href="' . admin_url() . 'user-edit.php?user_id=' . $author->id . '">' . $user->display_name . '</a> <span style="font-family: \'Courier New\', Courier, monospace; font-size: 81%; color: #a2a2a2;" >['.$author->type.']</span></p>';
                }
            }
        }
    }

    /*******************************************************************************************************************
     * BACKEND METHODS
     ******************************************************************************************************************/

    /**
     * Removes default "Author" meta box.
     *
     * Removes the "Author" meta box from post's and page's edit page.
     *
     * @see     https://codex.wordpress.org/Function_Reference/remove_meta_box
     *
     * @access  public
     * @since   1.0.0
     * @version 2.0.0
     */
    public function remove_author_metabox()
    {
        // Get post types where extend plugin functionality to.
        $post_types = molongui_supported_post_types(MOLONGUI_AUTHORSHIP_ID, 'all' );

        // Remove default author meta box.
        foreach ( $post_types as $post_type ) remove_meta_box( 'authordiv', $post_type, 'normal' );
    }

	/**
	 * Adds the meta box container.
	 *
	 * @see     https://codex.wordpress.org/Function_Reference/add_meta_box
	 * @access  public
	 * @since   1.0.0
	 * @version 2.0.3
	 */
	public function add_meta_boxes( $post_type )
	{
	    // Run only if logged in user has editor capabilities.
		if ( !current_user_can( 'edit_others_pages' ) and !current_user_can( 'edit_others_posts' ) ) return;

		// Limit meta box to certain post types.
        $post_types = molongui_supported_post_types(MOLONGUI_AUTHORSHIP_ID, 'all' );

		// Add author meta box to configured post-types.
		if ( in_array( $post_type, $post_types ) )
		{
			add_meta_box(
				'authorboxdiv'
				,__( 'Authors', MOLONGUI_AUTHORSHIP_TEXTDOMAIN )
				,array( $this, 'render_author_metabox' )
				,$post_type
				,'side'
				,'high'
			);

			// Add selector to choose whether to show authorship box or not.
			add_meta_box(
				'showboxdiv'
				,__( 'Authorship box', MOLONGUI_AUTHORSHIP_TEXTDOMAIN )
				,array( $this, 'render_display_metabox' )
				,$post_type
				,'side'
				,'high'
			);
		}

		// Add custom meta boxes to "Guest Author" custom post-type.
		if ( in_array( $post_type, array('molongui_guestauthor') ))
		{
			// Add job profile meta box.
			add_meta_box(
				'authorjobdiv'
				,__( 'Professional profile', MOLONGUI_AUTHORSHIP_TEXTDOMAIN )
				,array( $this, 'render_job_metabox' )
				,$post_type
				,'side'
				,'core'
			);

			// Add social media meta box.
			add_meta_box(
				'authorsocialdiv'
				,__( 'Social Media', MOLONGUI_AUTHORSHIP_TEXTDOMAIN )
				,array( $this, 'render_social_metabox' )
				,$post_type
				,'normal'
				,'high'
			);
		}
	}

	/**
	 * Render Author Meta Box content.
	 *
	 * @param   WP_Post    $post  The post object.
	 * @access  public
	 * @since   1.0.0
	 * @version 2.0.0
	 */
	public function render_author_metabox( $post )
	{
		// Add an nonce field so we can check for it later.
		wp_nonce_field( 'molongui_authorship', 'molongui_authorship_nonce' );

		// Display the form, loading stored values if available.
		?>
		<div class="molongui-metabox">
            <p class="molongui-description"><?php _e( 'Add as many authors as needed by selecting them from the dropdown below. Drag to change their order and click on trash icon to remove them. First listed author will be the main author.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ); ?></p>
            <div class="molongui-field">
                <label for="_molongui_author">
                    <?php echo $this->get_all( 'authors', array(), array(), 'asc', 'name', 'dropdown' ); ?>
                </label>
            </div>


		</div>
		<?php
	}

	/**
	 * Render selector to choose to show the authorship box or not.
	 *
	 * @param   WP_Post    $post  The post object.
	 * @access  public
	 * @since   1.1.0
	 * @version 3.0.0
	 */
	public function render_display_metabox( $post )
	{
		// Add an nonce field so we can check for it later.
		wp_nonce_field( 'molongui_authorship', 'molongui_authorship_nonce' );

		// Get plugin options.
		$settings = get_option( MOLONGUI_AUTHORSHIP_BOX_SETTINGS );

		// Get current screen.
		$screen = get_current_screen();

		// Get stored values from the database.
		$author_box_display  = get_post_meta( $post->ID, '_molongui_author_box_display', true );
		$author_box_position = get_post_meta( $post->ID, '_molongui_author_box_position', true );

		// If no existing values, set global setting value.
		if ( empty( $author_box_display ) )
		{
			switch( $settings['display'] )
			{
				case '0':

					$author_box_display = 'hide';

				break;

				case '1':

					$author_box_display = 'show';

				break;

				case 'posts':

					if ( $screen->post_type == 'post' ) $author_box_display = 'show';
					else $author_box_display = 'hide';

				break;

				case 'pages':

					if ( $screen->post_type == 'page' ) $author_box_display = 'show';
					else $author_box_display = 'hide';

				break;
			}
		}
		if ( empty( $author_box_position ) ) $author_box_position = $settings['position'];

		// Display the form, loading stored values if available.
		?>
		<div class="molongui-metabox">

            <!-- Author box display -->
			<p class="molongui-description"><?php _e( 'Show the author box in this post?', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ); ?></p>
			<div class="molongui-field">
				<select name="_molongui_author_box_display">
					<option value="show" <?php selected( $author_box_display, 'show' ); ?>><?php _e( 'Show', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ); ?></option>
					<option value="hide" <?php selected( $author_box_display, 'hide' ); ?>><?php _e( 'Hide', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ); ?></option>
				</select>
			</div>

            <!-- Author box position -->
            <p class="molongui-description <?php echo ( $author_box_display == 'hide' ? 'molongui-description-disabled' : '' ); ?>"><?php _e( 'Where to display the author box?', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ); ?></p>
            <div class="molongui-field">
                <select name="_molongui_author_box_position" <?php echo ( $author_box_display == 'hide' ? 'disabled' : '' ); ?>>
                    <option value="above" <?php selected( $author_box_position, 'above' ); ?>><?php _e( 'Above', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ); ?></option>
                    <option value="below" <?php selected( $author_box_position, 'below' ); ?>><?php _e( 'Below', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ); ?></option>
                    <option value="both"  <?php selected( $author_box_position, 'both' );  ?>><?php _e( 'Both', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ); ?></option>
                </select>
            </div>

		</div>
		<?php
	}

	/**
	 * Render job profile meta box content.
	 *
	 * @param   WP_Post    $post  The post object.
	 * @access  public
	 * @since   1.0.0
	 * @version 2.0.3
	 */
	public function render_job_metabox( $post )
	{
		// Add an nonce field so we can check for it later.
		wp_nonce_field( 'molongui_authorship', 'molongui_authorship_nonce' );

		// Use get_post_meta to retrieve an existing value from the database.
		$guest_author_mail         = get_post_meta( $post->ID, '_molongui_guest_author_mail', true );
		$guest_author_show_mail    = get_post_meta( $post->ID, '_molongui_guest_author_show_mail', true );
		$guest_author_phone        = get_post_meta( $post->ID, '_molongui_guest_author_phone', true );
		$guest_author_show_phone   = get_post_meta( $post->ID, '_molongui_guest_author_show_phone', true );
		$guest_author_link         = get_post_meta( $post->ID, '_molongui_guest_author_link', true );
		$guest_author_job          = get_post_meta( $post->ID, '_molongui_guest_author_job', true );
		$guest_author_company      = get_post_meta( $post->ID, '_molongui_guest_author_company', true );
		$guest_author_company_link = get_post_meta( $post->ID, '_molongui_guest_author_company_link', true );

		// Display the form, loading stored values if available.
		echo '<div class="molongui-metabox">';
			//echo '<div class="molongui-note">' . __( 'Empty fields are not displayed in the front-end.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) . '</div>';
			echo '<div class="molongui-field">';
				echo '<label class="title" for="_molongui_guest_author_link">' . __( 'Profile link', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) . '</label>';
				echo '<i class="tip molongui-authorship-icon-tip molongui-help-tip" data-tip="' . __( 'URL the author name will link to. Leave blank to disable link feature.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) . '"></i>';
				echo '<div class="input-wrap"><input type="text" placeholder="' . __( '//www.example.com/', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) . '" id="_molongui_guest_author_link" name="_molongui_guest_author_link" value="' . ( $guest_author_link ? $guest_author_link : '' ) . '" class="text"></div>';
			echo '</div>';
			echo '<div class="molongui-field">';
				echo '<label class="title" for="_molongui_guest_author_mail">' . __( 'Email address', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) . '</label>';
				echo '<i class="tip molongui-authorship-icon-tip molongui-help-tip" data-tip="' . __( 'Used to retrieve author\'s Gravatar if it exists. This field is not displayed in the front-end unless checkbox below is checked.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) . '"></i>';
				echo '<div class="input-wrap"><input type="text" id="_molongui_guest_author_mail" name="_molongui_guest_author_mail" value="' . ( $guest_author_mail ? $guest_author_mail : '' ) . '" class="text" placeholder="' . __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) . '"></div>';
				echo '<div class="input-wrap"><input type="checkbox" id="_molongui_guest_author_show_mail" name="_molongui_guest_author_show_mail" value="yes"' . ( $guest_author_show_mail == 'yes' ? 'checked=checked' : '' ) . '><label class="checkbox-label" for="_molongui_guest_author_show_mail">' . __( 'Display email in the author box.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) . '</label></div>';
			echo '</div>';
            echo '<div class="molongui-field">';
                echo '<label class="title" for="_molongui_guest_author_phone">' . __( 'Phone', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) . '</label>';
                echo '<i class="tip molongui-authorship-icon-tip molongui-help-tip" data-tip="' . __( 'This field is not displayed in the front-end unless checkbox below is checked.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) . '"></i>';
                echo '<div class="input-wrap"><input type="text" id="_molongui_guest_author_phone" name="_molongui_guest_author_phone" value="' . ( $guest_author_phone ? $guest_author_phone : '' ) . '" class="text" placeholder="' . __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) . '"></div>';
                echo '<div class="input-wrap"><input type="checkbox" id="_molongui_guest_author_show_phone" name="_molongui_guest_author_show_phone" value="yes"' . ( $guest_author_show_phone == 'yes' ? 'checked=checked' : '' ) . '><label class="checkbox-label" for="_molongui_guest_author_show_phone">' . __( 'Display phone in the author box.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) . '</label></div>';
            echo '</div>';
			echo '<div class="molongui-field">';
				echo '<label class="title" for="_molongui_guest_author_job">' . __( 'Job title', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) . '</label>';
				echo '<i class="tip molongui-authorship-icon-tip molongui-help-tip" data-tip="' . __( 'Name used to describe what the author does for a business or another enterprise. Leave blank to prevent to display this field in the front-end.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) . '"></i>';
				echo '<div class="input-wrap"><input type="text" id="_molongui_guest_author_job" name="_molongui_guest_author_job" value="' . ( $guest_author_job ? $guest_author_job : '' ) . '" class="text" placeholder="' . __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) . '"></div>';
			echo '</div>';
			echo '<div class="molongui-field">';
				echo '<label class="title" for="_molongui_guest_author_company">' . __( 'Company', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) . '</label>';
				echo '<i class="tip molongui-authorship-icon-tip molongui-help-tip" data-tip="' . __( 'The name of the company the author works for. Leave blank to prevent to display this field in the front-end.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) . '"></i>';
				echo '<div class="input-wrap"><input type="text" id="_molongui_guest_author_company" name="_molongui_guest_author_company" value="' . ( $guest_author_company ? $guest_author_company : '' ) . '" class="text" placeholder="' . __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) . '"></div>';
			echo '</div>';
			echo '<div class="molongui-field">';
				echo '<label class="title" for="_molongui_guest_author_company_link">' . __( 'Company link', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) . '</label>';
				echo '<i class="tip molongui-authorship-icon-tip molongui-help-tip" data-tip="' . __( 'URL the company name will link to. Leave blank to disable link feature.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) . '"></i>';
				echo '<div class="input-wrap"><input type="text" id="_molongui_guest_author_company_link" name="_molongui_guest_author_company_link" value="' . ( $guest_author_company_link ? $guest_author_company_link : '' ) . '" class="text" placeholder="' . __( '//www.example.com/', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) . '"></div>';
			echo '</div>';
		echo '</div>';
	}

	/**
	 * Render social media meta box content.
	 *
	 * @param   WP_Post    $post  The post object.
	 * @access  public
	 * @since   1.0.0
	 * @version 2.1.0
	 */
	public function render_social_metabox( $post )
	{
		// Add an nonce field so we can check for it later.
		wp_nonce_field( 'molongui_authorship', 'molongui_authorship_nonce' );

		// Get plugin config settings.
		$byline_settings   = get_option( MOLONGUI_AUTHORSHIP_BYLINE_SETTINGS );
		$box_settings      = get_option( MOLONGUI_AUTHORSHIP_BOX_SETTINGS );
		$guest_settings    = get_option( MOLONGUI_AUTHORSHIP_GUEST_SETTINGS );
		$advanced_settings = get_option( MOLONGUI_AUTHORSHIP_ADVANCED_SETTINGS );
		$string_settings   = get_option( MOLONGUI_AUTHORSHIP_STRING_SETTINGS );
		$settings          = array_merge( $byline_settings, $box_settings, $guest_settings, $advanced_settings, $string_settings );
        $social_networks = include( MOLONGUI_AUTHORSHIP_DIR . "/config/social.php" );

        // Display the form, loading stored values if available.
        echo '<div class="molongui molongui-metabox">';

            foreach ( $social_networks as $id => $social_network )
            {
                $social_networks[$id]['value'] = get_post_meta( $post->ID, '_molongui_guest_author_'.$id, true );

                if ( isset( $settings['show_'.$id] ) && $settings['show_'.$id] == 1 )
                {
                    echo '<div class="molongui-field">';
                        echo '<label class="title" for="_molongui_guest_author_'.$id.'">' . $social_networks[$id]['name'];
                            if ( !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ) and $social_network['premium'] ) echo '<a href="' . MOLONGUI_AUTHORSHIP_WEB . '" target="_blank">' . '<i class="molongui-authorship-icon-star molongui-help-tip molongui-premium-setting" data-tip="' . $this->premium_option_tip() . '"></i>' . '</a>';
                        echo '</label>';
                        if ( !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ) and $social_network['premium'] )
                        {
                            echo '<div class="input-wrap"><input type="text" disabled placeholder="' . __( 'Premium feature', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) . '" id="_molongui_guest_author_'.$id.'" name="_molongui_guest_author_'.$id.'" value="" class="text"></div>';
                        }
                        else
                        {
                            echo '<div class="input-wrap"><input type="text" placeholder="' . $social_networks[$id]['url'] . '" id="_molongui_guest_author_'.$id.'" name="_molongui_guest_author_'.$id.'" value="' . ( $social_networks[$id]['value'] ? $social_networks[$id]['value'] : '' ) . '" class="text"></div>';
                        }
                    echo '</div>';
                }
            }

        echo '</div>';
	}

    /**
     * Displays a message alerting of premium setting.
     *
     * @access  public
     * @since   1.0.0
     * @version 1.0.0
     */
    public function premium_option_tip()
    {
        return sprintf( __( '%sPremium feature%s. You are using the free version of this plugin. Consider purchasing the Premium Version to enable this feature.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ), '<strong>', '</strong>' );
    }

	/**
	 * Save the meta when the post is saved.
	 *
     * @access  public
	 * @param   int    $post_id  The ID of the post being saved.
	 * @return  mixed
	 * @since   1.0.0
	 * @version 3.0.0
	 */
	public function save( $post_id )
	{
		/*
		 * We need to verify this came from the our screen and with proper authorization,
		 * because save_post can be triggered at other times.
		 */

		// Check if our nonce is set.
		if ( !isset( $_POST['molongui_authorship_nonce'] ) ) return $post_id;
		$nonce = $_POST['molongui_authorship_nonce'];

		// Verify that the nonce is valid.
		if ( !wp_verify_nonce( $nonce, 'molongui_authorship' ) ) return $post_id;

		// If this is an autosave, our form has not been submitted,
		// so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return $post_id;

		// Check the user's permissions.
		if ( 'page' == $_POST['post_type'] )
		{
			if ( !current_user_can( 'edit_page', $post_id ) ) return $post_id;
		}
		else
		{
			if ( !current_user_can( 'edit_post', $post_id ) ) return $post_id;
		}

		// OK, its safe for us to save the data now.

		global $current_screen;
        $social_networks = include( MOLONGUI_AUTHORSHIP_DIR . "/config/social.php" );

        // Saving a 'molongui_guestauthor' cpt.
		if( 'molongui_guestauthor' == $current_screen->post_type )
		{
			// Save data.
			update_post_meta( $post_id, '_molongui_guest_author_mail', sanitize_text_field( $_POST['_molongui_guest_author_mail'] ) );
			update_post_meta( $post_id, '_molongui_guest_author_show_mail', sanitize_text_field( $_POST['_molongui_guest_author_show_mail'] ) );
			update_post_meta( $post_id, '_molongui_guest_author_phone', sanitize_text_field( $_POST['_molongui_guest_author_phone'] ) );
			update_post_meta( $post_id, '_molongui_guest_author_show_phone', sanitize_text_field( $_POST['_molongui_guest_author_show_phone'] ) );
			update_post_meta( $post_id, '_molongui_guest_author_link', sanitize_text_field( $_POST['_molongui_guest_author_link'] ) );
			update_post_meta( $post_id, '_molongui_guest_author_job', sanitize_text_field( $_POST['_molongui_guest_author_job'] ) );
			update_post_meta( $post_id, '_molongui_guest_author_company', sanitize_text_field( $_POST['_molongui_guest_author_company'] ) );
			update_post_meta( $post_id, '_molongui_guest_author_company_link', sanitize_text_field( $_POST['_molongui_guest_author_company_link'] ) );

            foreach ( $social_networks as $id => $social_network )
            {
                if ( isset( $_POST['_molongui_guest_author_'.$id] ) ) update_post_meta( $post_id, '_molongui_guest_author_'.$id, sanitize_text_field( $_POST['_molongui_guest_author_'.$id] ) );
            }
        }
        // Saving any other cpt/post authors selection.
		else
		{
            // If no author selected, include current user as author.
            if ( empty( $_POST['molongui_authors'] ) )
            {
                $current_user = wp_get_current_user();
                $_POST['molongui_authors'][0] = 'user-'.$current_user->ID;
            }

            // Save author(s).
            // @see https://codex.wordpress.org/Function_Reference/add_post_meta
            delete_post_meta( $post_id, '_molongui_author' );
            foreach( $_POST['molongui_authors'] as $author ) add_post_meta( $post_id, '_molongui_author', $author, false );

            // Save main author.
            update_post_meta( $post_id, '_molongui_main_author', $_POST['molongui_authors'][0] );

            // Change "post_author" id from "post" table if selected main author is a registered wp user.
            if ( !$this->is_guest( $_POST['molongui_authors'][0] ) )
            {
                $split   = explode( '-', $_POST['molongui_authors'][0] );
                $my_post = array(
                    'ID'          => $post_id,
                    'post_author' => $split[1],
                );

                // Unhook this function so it doesn't loop infinitely.
                // @see https://codex.wordpress.org/Plugin_API/Action_Reference/save_post#Avoiding_infinite_loops
                remove_action( 'save_post', array( $this, 'save' ) );

                // Update the post author, which calls 'save_post' again.
                wp_update_post( $my_post );

                // Re-hook this function.
                add_action( 'save_post', array( $this, 'save' ) );
            };

            // Save whether to display the author box.
            update_post_meta( $post_id, '_molongui_author_box_display', $_POST['_molongui_author_box_display'] );

            // Save where to display the author box.
            update_post_meta( $post_id, '_molongui_author_box_position', $_POST['_molongui_author_box_position'] );
		}

	}

    /*******************************************************************************************************************
     * FRONTEND METHODS
     * @since 2.0.0
     ******************************************************************************************************************/

    /**
     * Returns author name(s) to be displayed.
     *
     * Hooked into the_author() function overrides default author name so multiple and guest authors option is taken
     * into account.
     *
     * @access  public
     * @param   object      $author     Post author name.
     * @return  string
     * @since   1.0.0
     * @version 2.2.2
     */
    public function filter_author_name( $author )
    {
        global $post;

        // Avoid php notices when $post is empty.
        if ( empty( $post ) ) return $author;

	    /**
	     * Generate a PHP backtrace.
	     *
	     * 'wp_debug_backtrace_summary()' WP function could be used, but it does not allow to limit the number of stack
	     * frames returned, so that results in more memory usage. PHP's base 'debug_backtrace()' is used instead.
	     *
	     * @see http://php.net/manual/en/function.debug-backtrace.php
	     *      https://developer.wordpress.org/reference/functions/wp_debug_backtrace_summary/
	     *
	     * If an empty array is returned, exit (return) to avoid code failure.
	     *
	     * @since   2.2.2
	     * @version 2.2.2
	     */
	    $dbt = debug_backtrace( DEBUG_BACKTRACE_IGNORE_ARGS, 5 );
	    if ( empty( $dbt ) ) return $author;

	    // Debug vars.
	    //molongui_debug( array( $author, $dbt ), false, false );

	    /**
	     * Do not modify $author when retrieving it for "Revisions" meta-box (backend).
	     *
	     * Avoid author name being replaced by assigned author(s) name(s) on revision history meta-box.
	     *
	     * @since   2.2.2
	     * @version 2.2.2
	     */
	    if ( is_admin() and isset( $dbt[4]['function'] ) and $dbt[4]['function'] == "wp_post_revision_title_expanded" )
	    {
		    return $author;
	    }

        // Handle name on author archive pages title.
        if ( is_author() and !in_the_loop() )
        {
            global $wp_query;

            // Filter guest-author name on virtual archive pages.
            if ( $wp_query->is_virtual and isset( $wp_query->guest_author_id ) ) return get_post_field( 'post_title', $wp_query->guest_author_id );

	        /**
	         * Fixes author archive pages' title (for registered wp users).
             *
	         * Modifying 'where' clause from main query to include co-authored posts into author archive pages (see
	         * 'molongui_include_coauthored_posts' function in 'functions.php' file) makes displaying the registered
	         * wp user name stop working fine, displaying the name of the main author of the last listed post.
             *
	         * @since 2.0.2
	         */
            if ( $wp_query->query_vars['author'] )
            {
                $user = get_userdata( $wp_query->query_vars['author'] );

	            return $user->display_name;
            }

            // Leave name untouched.
            return $author;
        }

        // Filter author name(s).
        return $this->get_by_line();
    }

	/**
	 * Filters author display name.
	 *
	 * @param   string  $value              The value of the metadata.
	 * @param   int     $user_id            The user ID.
	 * @param   int     $original_user_id   The original user ID..
	 * @return  string
	 * @since   2.0.7
	 * @version 2.2.2
	 */
	public function filter_the_author_display_name( $value, $user_id, $original_user_id )
	{
		global $post;

		// Avoid php notices when $post is empty.
		if ( empty( $post ) ) return $value;

		/**
		 * Generate a PHP backtrace.
		 *
		 * 'wp_debug_backtrace_summary()' WP function could be used, but it does not allow to limit the number of stack
		 * frames returned, so that results in more memory usage. PHP's base 'debug_backtrace()' is used instead.
		 *
		 * @see http://php.net/manual/en/function.debug-backtrace.php
		 *      https://developer.wordpress.org/reference/functions/wp_debug_backtrace_summary/
		 *
		 * If an empty array is returned, exit (return) to avoid code failure.
		 *
		 * @since   2.2.2
		 * @version 2.2.2
		 */
		$dbt = debug_backtrace( DEBUG_BACKTRACE_IGNORE_ARGS, 5 );
		if ( empty( $dbt ) ) return $value;

		// Debug vars.
		//molongui_debug( array( $value, $dbt ), false, false );

		/**
		 * Do not modify $author when retrieving it for "Revisions" meta-box (backend).
		 *
		 * Avoid author name being replaced by assigned author(s) name(s) on revision history meta-box.
		 *
		 * @since   2.2.2
		 * @version 2.2.2
		 */
		if ( is_admin() and isset( $dbt[4]['function'] ) and $dbt[4]['function'] == "wp_post_revision_title_expanded" )
		{
			return $value;
		}

		/**
		 * BuddyPress retrieves $post correctly but ID is set to '0', which causes "get_by_line" function to fail.
		 *
		 * The code below, which gets the post ID from post_title to pass it to "get_by_line" function, doesn't work
		 * because it takes that post ID for each user changing all user names displayed on the page with the name of
		 * the author of the current page.
		 *
		 *      if ( $post->ID == 0 ) {
		 *          $pid = get_page_by_title( $post->post_title ); )
		 *          return $this->get_by_line( $pid->ID );
		 *      }
		 *
		 * Being so, solution taken consists on returning original value untouched.
		 *
		 * @since   2.1.0
		 * @version 2.1.0
		 */
		if ( $post->ID == 0 ) return $value;

		return $this->get_by_line( $post->ID );
	}

    /**
     * Retrieves the string to be displayed in the "byline".
     *
     * @access  public
     * @param   int     $pid            Post id. Defaults to null.
     * @param   string  $separator      Delimiter that should appear between the authors.
     * @param   string  $last_separator Delimiter that should appear between the last two authors.
     * @param   bool    $linked         Whether to add a link to each author name to its posts archive page. Defaults to true.
     * @return  string
     * @since   2.0.0
     * @version 2.1.0
     */
    public function get_by_line( $pid = null, $separator = '', $last_separator = '', $linked = false )
    {
        if ( is_null( $pid ) or !is_integer( $pid ) )
        {
	        global $post;

	        // Avoid php notices when $post is empty.
	        if ( empty( $post ) ) return '';

	        // Get post ID.
	        $pid = $post->ID;
        }

        // Filter author name on guest or multiple authored posts.
        if ( $authors = $this->get_authors( $pid ) )
        {
            // Get plugin settings.
            $settings = get_option( MOLONGUI_AUTHORSHIP_BYLINE_SETTINGS );

            // Display author(s) name(s) as per settings configuration.
            switch ( $settings['byline_multiauthor_display'] )
            {
                // Display all authors names separated by commas.
                case 'all':

                    $byline = $this->prepare_name( $authors, count( $authors ), true, $separator, $last_separator, $linked );

                break;

                // Display only main author name.
                case 'main':

                    $byline = $this->prepare_name( $authors, '1', false, '', '', $linked );

                break;

                // Display main author name, one more and the count of remaining authors.
                case '2':

                    $byline = $this->prepare_name( $authors, '2', true, $separator, $last_separator, $linked );

                break;

                // Display main author name, two more and the count of remaining authors.
                case '3':

                    $byline = $this->prepare_name( $authors, '3', true, $separator, $last_separator, $linked );

                break;

                // Display main author name and the count of remaining authors.
                case '1':
                default:

                    $byline = $this->prepare_name( $authors, '1', true, '', '', $linked );

                break;
            }
        }

        return ( isset( $byline ) ? $byline : '' );
    }

    /**
     * Prepares author name to be displayed in the "byline".
     *
     * @access  private
     * @param   object  $authors        Post authors.
     * @param   int     $qty            Number of authors to show.
     * @param   bool    $count          Whether to show count of remaining authors to show.
     * @param   string  $separator      Delimiter that should appear between the authors.
     * @param   string  $last_separator Delimiter that should appear between the last two authors.
     * @param   bool    $linked         Whether to add a link to each author name to its posts archive page. Defaults to true.
     * @return  string
     * @since   2.0.0
     * @version 2.1.0
     */
    private function prepare_name( $authors, $qty, $count = true, $separator = '', $last_separator = '', $linked = false )
    {
        // Init vars.
        $string = '';
        $total  = count( $authors );
        $i = 0;

        // Load plugin settings.
	    $settings = get_option( MOLONGUI_AUTHORSHIP_BYLINE_SETTINGS );

        // Handle name separators.
	    $separator      = ( !empty( $separator ) ? $separator : ( ( isset( $settings['byline_multiauthor_separator'] ) and !empty( $settings['byline_multiauthor_separator'] ) ) ? $settings['byline_multiauthor_separator'] : ', ' ) );
	    $last_separator = ( !empty( $last_separator ) ? $last_separator : ( ( isset( $settings['byline_multiauthor_last_separator'] ) and !empty( $settings['byline_multiauthor_last_separator'] ) ) ? $settings['byline_multiauthor_last_separator'] : __( 'and', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) ) );

        foreach ( $authors as $author )
        {
            $divider = ( $i == 0 ? '' : ( $i == ( $total - 1 ) ? ' '.$last_separator.' ' : $separator.' ' ) );

            // This author is a guest.
            if ( $author->type == 'guest' )
            {
                if ( $linked ) $author = $this->prepare_link( $author, get_the_title( $author->id ) );
                else $author = get_the_title( $author->id );
                $string .= $divider . $author;
            }
            // This author is a registered wp user.
            else
            {
                $user = get_userdata( $author->id );
                if ( $linked ) $author = $this->prepare_link( $author, $user->display_name );
                else $author = $user->display_name;
                $string .= $divider . $author;
            }

            // Leave if there are $qty authors already.
            if ( ++$i == $qty ) break;
        }

        // Add remaining authors count as number.
        if ( $count and ( $total > $qty ) ) $string .= ' '.sprintf( __( 'and %d more', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ), $total - $qty );

        return $string;
    }

    /**
     * .
     *
     * @access  public
     * @param   int     $author .
     * @param   string  $name   .
     * @return  string
     * @since   1.0.0
     * @version 2.1.0
     */
    public function prepare_link( $author, $name )
    {
        // Get plugin settings.
        $settings = get_option( MOLONGUI_AUTHORSHIP_ARCHIVES_SETTINGS );

        if ( $author->type == 'guest' )
        {
            // Prepare archive page URI base.
            $uri_slug = ( ( isset( $settings['guest_archive_permalink']) and !empty( $settings['guest_archive_permalink'] ) ) ? '/' . $settings['guest_archive_permalink'] : '' ) .
                ( ( isset( $settings['guest_archive_base']) and !empty( $settings['guest_archive_base'] ) ) ? '/' . $settings['guest_archive_base'] : '/author' );

            $slug = get_post_field( 'post_name', $author->id );

            $url = ( $settings['guest_archive_enabled'] ? home_url( $uri_slug.'/'.$slug ) : '#molongui-disabled-link' );
        }
        else
        {
            $usr = get_user_by( 'id', $author->id );
            $url = get_author_posts_url( $author->id, $usr->user_nicename );
        }

        return '<a href="'.$url.'">'.$name.'</a>';//return '#molongui-disabled-link';
    }

    /**
     * Modify guest author link.
     *
     * Hook into the_author_link() function to override author link if there is a guest author set.
     *
     * @access  public
     * @param   object      $link       The author link.
     * @return  string
     * @since   1.0.0
     * @version 2.1.0
     */
    public function filter_author_link( $link )
    {
        /**
         * Generate a PHP backtrace.
         *
         * 'wp_debug_backtrace_summary()' WP function could be used, but it does not allow to limit the number of stack
         * frames returned, so that results in more memory usage. PHP's base 'debug_backtrace()' is used instead.
         *
         * @see http://php.net/manual/en/function.debug-backtrace.php
         *      https://developer.wordpress.org/reference/functions/wp_debug_backtrace_summary/
         *
         * If an empty array is returned, exit (return) to avoid code failure.
         *
         * @since   2.0.2
         * @version 2.0.2
         */
        $dbt = debug_backtrace( DEBUG_BACKTRACE_IGNORE_ARGS, 8 );
        if ( empty( $dbt ) ) return $link;

        // Debug vars.
        //molongui_debug( $dbt, false, false );

        /**
         * NO $link alteration wanted.
         *
         * [molongui_author_list] shortcode runs on the frontend to show a list of all authors (registered wp users
         * and guests) so when 'get_author_posts_url()' function is called from there, nothing should be altered.
         *
         * [molongui_author_list] shortcode calls 'get_all()' function from the 'Author' class, which
         * calls 'get_data()' function which is the one that finally calls the 'get_author_posts_url()' function
         * (there are intermediate calls, though).
         *
         * @since   1.3.5
         * @version 2.0.0
         */
        if ( isset( $dbt[6]['function'] ) and $dbt[6]['function'] == 'molongui_shortcode_display_author_list'
             and
             isset( $dbt[5]['function'] ) and $dbt[5]['function'] == 'get_all'  and isset( $dbt[5]['class'] ) and $dbt[5]['class'] == 'Molongui\Authorship\Includes\Author'
             and
             isset( $dbt[4]['function'] ) and $dbt[4]['function'] == 'get_data' and isset( $dbt[4]['class'] ) and $dbt[4]['class'] == 'Molongui\Authorship\Includes\Author'
             and
             isset( $dbt[3]['function'] ) and $dbt[3]['function'] == 'get_author_posts_url'
        ) return $link;

        /**
         * Do not modify $link when being called from 'prepare_link()' which had being called by 'prepare_name()'.
         *
         * @since   2.0.2
         * @version 2.0.2
         */
        if ( isset( $dbt[5]['function'] ) and $dbt[5]['function'] == "prepare_link" and isset( $dbt[5]['class'] ) and $dbt[5]['class'] == 'Molongui\Authorship\Includes\Author'
             and
             isset( $dbt[6]['function'] ) and $dbt[6]['function'] == "prepare_name" and isset( $dbt[6]['class'] ) and $dbt[6]['class'] == 'Molongui\Authorship\Includes\Author'
             and
             isset( $dbt[7]['function'] ) and $dbt[7]['function'] == "get_by_line"  and isset( $dbt[7]['class'] ) and $dbt[7]['class'] == 'Molongui\Authorship\Includes\Author'
        ) return $link;

        /**
         * WordPress does not contemplate the option of having more than one author per post, so it only allows returning
         * a single URL. This behavior is a problem when the post has more than one author assigned.
         *
         * Our plugin solves this fact by offering specific functions that replace those from WordPress (the_author() or
         * the_author_posts_link()). See 'includes/template-tags.php' file for more detail.
         *
         * However, this solution is only valid for developers or users who want to create a child theme that uses those
         * functions. For the rest of users, we offer a partial solution: unlink authors' names or link them to the main
         * author's archive page.
         *
         * @since 2.0.0
         */

        // Avoid php notices when $post is empty.
        global $post;
        if ( empty( $post ) ) return $link;

        // Get plugin settings.
	    $box_settings      = get_option( MOLONGUI_AUTHORSHIP_BOX_SETTINGS );
	    $byline_settings   = get_option( MOLONGUI_AUTHORSHIP_BYLINE_SETTINGS );
	    $guest_settings    = get_option( MOLONGUI_AUTHORSHIP_GUEST_SETTINGS );
	    $archives_settings = get_option( MOLONGUI_AUTHORSHIP_ARCHIVES_SETTINGS );
	    $advanced_settings = get_option( MOLONGUI_AUTHORSHIP_ADVANCED_SETTINGS );
	    $string_settings   = get_option( MOLONGUI_AUTHORSHIP_STRING_SETTINGS );
	    $settings          = array_merge( $box_settings, $byline_settings, $guest_settings, $archives_settings, $advanced_settings, $string_settings );

        // Get post authors.
        $authors = $this->get_authors( $post->ID );

        // If multiauthor, take into account configured settings.
        if ( $this->is_multiauthor( $authors ) )
        {
            // Disable link if configured so.
            // This setting is taken into account only when multiauthor.
            if ( $settings['byline_multiauthor_link'] == 'disabled' ) return '#molongui-disabled-link';
            else
            {
                // just continue down the code...
            }
        }

        // Filter link if main author is a guest, plugin version is premium and enabled guest author archives.
        if ( $authors[0]->type == 'guest' )
        {
            if ( molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ) and $settings['guest_archive_enabled'] )
            {
                // Get guest author.
                $guest = get_post( $authors[0]->id );

                // Get link modifiers.
                $permalink = ( ( isset( $settings['guest_archive_permalink'] ) and !empty( $settings['guest_archive_permalink'] ) ) ? $settings['guest_archive_permalink'] : '' );
                $slug      = ( ( isset( $settings['guest_archive_base'] ) and !empty( $settings['guest_archive_base'] ) ) ? $settings['guest_archive_base'] : 'author' );

                // Get guest author archive page link.
                $guest_link = home_url( ( !empty( $permalink ) ? $permalink.'/' : '' ) . $slug . '/' . $guest->post_name );

                // Return data.
                return $guest_link;
            }
            else
            {
                return '#molongui-disabled-link';
            }
        }
        // If registered wp user, return link untouched.
        else return $link;
    }

    /**
     * Filters guest author avatar.
     *
     * Hook into get_avatar() function to override author profile image if there is one locally defined.
     *
     * @see     https://codex.wordpress.org/Plugin_API/Filter_Reference/get_avatar
     *          https://developer.wordpress.org/reference/functions/get_avatar/
     *
     * @access  public
     * @param   string  $avatar         Image tag for the user's avatar.
     * @param   mixed   $id_or_email    The Gravatar to retrieve. Accepts a user_id, gravatar md5 hash, user email, WP_User object, WP_Post object, or WP_Comment object.
     * @param   int     $size           Height and width of the avatar image file in pixels. Defaults to 96.
     * @param   string  $default        URL for the default image or a default type.
     * @param   string  $alt            Alternative text to use in <img> tag.
     * @param   array   $args           Extra arguments to retrieve the avatar.
     * @return  string
     * @since   2.0.0
     * @version 2.2.2
     */
    public function filter_author_avatar( $avatar = '', $id_or_email, $size = 96, $default = '', $alt = '', $args )
    {
        // Init vars.
        $local_avatar_url = false;

        /**
         * Generate a PHP backtrace.
         *
         * 'wp_debug_backtrace_summary()' WP function could be used, but it does not allow to limit the number of stack
         * frames returned, so that results in more memory usage. PHP's base 'debug_backtrace()' is used instead.
         *
         * @see http://php.net/manual/en/function.debug-backtrace.php
         *      https://developer.wordpress.org/reference/functions/wp_debug_backtrace_summary/
         *
         * If an empty array is returned, exit (return) to avoid code failure.
         *
         * @since   2.0.1
         * @version 2.0.2
         */
        $dbt = debug_backtrace( DEBUG_BACKTRACE_IGNORE_ARGS, 5 );
        if ( empty( $dbt ) ) return $avatar;

        // Debug vars.
        //molongui_debug( array( $id_or_email, $dbt ), false, false );

        /**
         * DON'T FILTER
         * WordPress (backend)
         * Do not modify $avatar markup when retrieving it from 'wp_admin_bar' to show avatars on '#wp-admin-bar-my-account'.
         *
         * @see     wp-includes/admin-bar.php
         *          https://developer.wordpress.org/reference/functions/wp_admin_bar_my_account_menu/
         *          https://developer.wordpress.org/reference/functions/wp_admin_bar_my_account_item/
         *
         * @since   2.0.1
         * @version 2.0.1
         */
        if ( ( isset( $dbt[4]['function'] ) and ( $dbt[4]['function'] == "wp_admin_bar_my_account_menu" or $dbt[4]['function'] == "wp_admin_bar_my_account_item" ) ) )
        {
            if ( $local_avatar_url = get_user_meta( get_current_user_id(), 'molongui_author_image_url', true ) )
            {
                return $author_avatar = sprintf(
                    "<img alt='%s' src='%s' srcset='%s' class='%s' height='%d' width='%d' %s/>",
                    esc_attr( $args['alt'] ),
                    esc_url( $local_avatar_url ),
                    esc_attr( "$local_avatar_url 2x" ),
                    esc_attr( join( ' ', array( 'avatar', 'avatar-'.(int)$args['size'], 'photo' ) ) ),
                    (int) $args['height'],
                    (int) $args['width'],
                    $args['extra_attr']
                );
            }
            else
            {
                return $avatar;
            }
        }
        /**
         * DON'T FILTER
         * WordPress (backend)
         * Do not modify $avatar markup when retrieving it from the wp posts list.
         *
         * @since   2.2.2
         * @version 2.2.2
         */
        elseif ( ( isset( $dbt[3]['function'] ) and ( $dbt[3]['function'] == "get_avatar" ) and isset( $dbt[4]['class'] ) and ( $dbt[4]['class'] == "WP_Posts_List_Table" ) ) )
        {
            if ( $local_avatar_url = get_user_meta( $id_or_email, 'molongui_author_image_url', true ) )
            {
                return $author_avatar = sprintf(
                    "<img alt='%s' src='%s' srcset='%s' class='%s' height='%d' width='%d' %s/>",
                    esc_attr( $args['alt'] ),
                    esc_url( $local_avatar_url ),
                    esc_attr( "$local_avatar_url 2x" ),
                    esc_attr( join( ' ', array( 'avatar', 'avatar-'.(int)$args['size'], 'photo' ) ) ),
                    (int) $args['height'],
                    (int) $args['width'],
                    $args['extra_attr']
                );
            }
            else
            {
                return $avatar;
            }
        }
        /**
	     * DON'T FILTER
	     * WordPress (backend)
	     * Do not modify $avatar markup when retrieving it for "Revisions" meta-box.
         *
         * Avoid user avatar being replaced by assigned author avatar on revision history meta-box.
	     *
	     * @since   2.0.4
	     * @version 2.2.2
	     */
	    elseif ( is_admin() and isset( $dbt[4]['function'] ) and $dbt[4]['function'] == "wp_post_revision_title_expanded" )
        {
            global $post;
	        if ( $local_avatar_url = get_user_meta( $post->post_author, 'molongui_author_image_url', true ) )
	        {
		        return $author_avatar = sprintf(
			        "<img alt='%s' src='%s' srcset='%s' class='%s' height='%d' width='%d' %s/>",
			        esc_attr( $args['alt'] ),
			        esc_url( $local_avatar_url ),
			        esc_attr( "$local_avatar_url 2x" ),
			        esc_attr( join( ' ', array( 'avatar', 'avatar-'.(int)$args['size'], 'photo' ) ) ),
			        (int) $args['height'],
			        (int) $args['width'],
			        $args['extra_attr']
		        );
	        }
	        else
	        {
		        return $avatar;
	        }
        }
	    /**
	     * DON'T FILTER
	     * Molongui Authorship plugin (frontend)
	     * Do not modify $avatar markup when retrieving it from "get_data" function.
	     *
	     * @since   2.0.9
	     * @version 2.2.2
	     */
        elseif ( ( isset( $dbt[4]['function'] ) and $dbt[4]['function'] == "get_data" and isset( $dbt[4]['class'] ) and $dbt[4]['class'] == 'Molongui\Authorship\Includes\Author' ) )
	    {
		    return $avatar;
	    }
        /**
         * DON'T FILTER
         * Extra theme (frontend)
         * Do not modify $avatar markup when retrieving it from "authors template" by Extra theme.
         *
         * @since   3.0.0
         * @version 3.0.0
         */
        elseif ( ( isset( $dbt[3]['function'] ) and ( $dbt[3]['function'] == "get_avatar" ) and
	        isset( $dbt[3]['file'] ) and substr_compare( $dbt[3]['file'], '/themes/Extra/page-template-authors.php', strlen( $dbt[3]['file'] )-strlen( '/themes/Extra/page-template-authors.php' ), strlen( '/themes/Extra/page-template-authors.php' ) ) === 0 ) )
        {
	        if ( $local_avatar_url = get_user_meta( $id_or_email, 'molongui_author_image_url', true ) )
	        {
		        return $author_avatar = sprintf(
			        "<img alt='%s' src='%s' srcset='%s' class='%s' height='%d' width='%d' %s/>",
			        esc_attr( $args['alt'] ),
			        esc_url( $local_avatar_url ),
			        esc_attr( "$local_avatar_url 2x" ),
			        esc_attr( join( ' ', array( 'avatar', 'avatar-'.(int)$args['size'], 'photo' ) ) ),
			        (int) $args['height'],
			        (int) $args['width'],
			        $args['extra_attr']
		        );
	        }
	        else
	        {
		        return $avatar;
	        }
        }
        /**
         * WordPress (backend and frontend)
         * Filter avatar on comments.
         *
         * Comments can be made by registered wp users, guest authors or third party commenters.
         *
         * For those comments made by registered wp users who have a local uploaded avatar, override WP retrieved avatar
         * to use the local one.
         *
         * For those comments made by a guest author who have a local uploaded avatar, override WP retrieved avatar
         * to use the local one. If no local uploaded avatar, override WP retrieved avatar (which might be wrong because
         * it is taking post author id) getting associated Gravatar or default if none associated to the given email.
         *
         * On any other case (wp user without local avatar or comment made by a third commenter), just do nothing, let's
         * use the avatar retrieved by WP.
		 *
		 * @version 2.2.2
         */
        //if ( $id_or_email instanceof WP_Comment )
        elseif ( is_object( $id_or_email ) and isset( $id_or_email->comment_ID ) )
        {
            $email = $id_or_email->comment_author_email;

            // If empty $email, leave.
            if ( !$email ) return $avatar;

            // If registered wp user, check for local avatar.
            if ( $user = $this->get_author_by( 'user_email', $email, 'user' ) )
            {
                $user_local_url   = get_user_meta( $user->ID, 'molongui_author_image_url', true );
                $local_avatar_url = ( $user_local_url ? $user_local_url : '' );
            }
            // If guest-author, check for local avatar.
            elseif ( $guest = $this->get_author_by( '_molongui_guest_author_mail', $email, 'guest' ) )
            {
                $local_avatar_url = ( has_post_thumbnail( $guest->ID ) ? get_the_post_thumbnail_url( $guest->ID, $size ) : '' );

                // If no local avatar, try to retrieve one from Gravatar.
                // Do not pass $args to 'get_avatar_url' because it contains $args['url'], which makes the function leave early.
                if ( !$local_avatar_url ) $local_avatar_url = get_avatar_url( $email );
            }
        }
        /**
         * WordPress (backend)
         * Filters avatar on wp users list.
         *
         * If there is a local uploaded avatar for a registered wp user, this retrieves it. Else, nothing is filtered so
         * avatar gotten from Gravatar is used.
         *
         * @since   2.0.2
         * @version 2.0.2
         */
        elseif ( is_admin() and ( isset( $dbt[4]['function'] ) and $dbt[4]['function'] == "single_row" and isset( $dbt[4]['class'] ) and $dbt[4]['class'] == 'WP_Users_List_Table' ) )
        {
            // If empty $id_or_email or it is not the user id (is_numeric), leave.
            $user_id = $id_or_email;
            if ( !$user_id or !is_numeric( $user_id ) ) return $avatar;

            if ( $local_avatar_url = get_user_meta( $user_id, 'molongui_author_image_url', true ) )
            {
                return $author_avatar = sprintf(
                    "<img alt='%s' src='%s' srcset='%s' class='%s' height='%d' width='%d' %s/>",
                    esc_attr( $args['alt'] ),
                    esc_url( $local_avatar_url ),
                    esc_attr( "$local_avatar_url 2x" ),
                    esc_attr( join( ' ', array( 'avatar', 'avatar-'.(int)$args['size'], 'photo' ) ) ),
                    (int) $args['height'],
                    (int) $args['width'],
                    $args['extra_attr']
                );
            }
            else
            {
                return $avatar;
            }
        }
        /**
         * Molongui Authorship plugin (frontend)
         * Filters avatar on guest-author archive (virtual) pages.
         *
         * @since   2.1.0
         * @version 3.0.0
         */
	    elseif ( is_author() and !in_the_loop() )
        {
            global $wp_query;

	        // Filter guest-author avatar on virtual archive pages.
	        if ( $wp_query->is_virtual and isset( $wp_query->guest_author_id ) )
            {
	            // Look for an uploaded local avatar.
	            $local_avatar_url = ( has_post_thumbnail( $wp_query->guest_author_id ) ? get_the_post_thumbnail_url( $wp_query->guest_author_id, $size ) : '' );

	            // If no local avatar found, try to retrieve one from Gravatar.
	            // Do not pass $args to 'get_avatar_url' because it contains $args['url'], which makes the function leave early.
	         //   if ( !$local_avatar_url ) $local_avatar_url = get_avatar_url( $email );
             //   return get_post_field( 'post_title', $wp_query->guest_author_id );
            }
            else
            {
	            // If empty $id_or_email or it is not the user id (is_numeric), leave.
	            $user_id = $id_or_email;
	            if ( !$user_id or !is_numeric( $user_id ) ) return $avatar;

	            if ( $local_avatar_url = get_user_meta( $user_id, 'molongui_author_image_url', true ) )
	            {
		            return $author_avatar = sprintf(
			            "<img alt='%s' src='%s' srcset='%s' class='%s' height='%d' width='%d' %s/>",
			            esc_attr( $args['alt'] ),
			            esc_url( $local_avatar_url ),
			            esc_attr( "$local_avatar_url 2x" ),
			            esc_attr( join( ' ', array( 'avatar', 'avatar-'.(int)$args['size'], 'photo' ) ) ),
			            (int) $args['height'],
			            (int) $args['width'],
			            $args['extra_attr']
		            );
	            }
	            else
	            {
		            return $avatar;
	            }
            }
        }
        /**
         * WPdiscuz plugin (frontend)
         * Filter avatar on WPdiscuz comments.
         *
         * Comments can be made by registered wp users, guest authors or third party commenters.
         *
         * For those comments made by registered wp users who have a local uploaded avatar, override WP retrieved avatar
         * to use the local one.
         *
         * For those comments made by a guest author who have a local uploaded avatar, override WP retrieved avatar
         * to use the local one. If no local uploaded avatar, override WP retrieved avatar (which might be wrong because
         * it is taking post author id) getting associated Gravatar or default if none associated to the given email.
         *
         * On any other case (wp user without local avatar or comment made by a third commenter), just do nothing, let's
         * use the avatar retrieved by WP.
         *
         * @since   2.0.2
         * @version 2.0.2
         */
        elseif ( ( isset( $dbt[4]['function'] ) and $dbt[4]['function'] == "start_el" and isset( $dbt[4]['class'] ) and $dbt[4]['class'] == 'WpdiscuzWalker' ) )
        {
            // If empty $email, leave.
            $email = $id_or_email;
            if ( !$email ) return $avatar;

            // If registered wp user, check for local avatar.
            if ( $user = $this->get_author_by( 'user_email', $email, 'user' ) )
            {
                $user_local_url   = get_user_meta( $user->ID, 'molongui_author_image_url', true );
                $local_avatar_url = ( $user_local_url ? $user_local_url : '' );
            }
            // If guest-author, check for local avatar.
            elseif ( $guest = $this->get_author_by( '_molongui_guest_author_mail', $email, 'guest' ) )
            {
                $local_avatar_url = ( has_post_thumbnail( $guest->ID ) ? get_the_post_thumbnail_url( $guest->ID, $size ) : '' );

                // If no local avatar, try to retrieve one from Gravatar.
                // Do not pass $args to 'get_avatar_url' because it contains $args['url'], which makes the function leave early.
                if ( !$local_avatar_url ) $local_avatar_url = get_avatar_url( $email );
            }
        }
        /**
         * WordPress (frontend)
         * Filter avatar on posts.
         *
         * Some themes show author avatar on byline. On those cases, avatar retrieve by WP must be filtered to get the
         * local uploaded one or the one that might be associated with a guest author.
         */
        else
        {
            global $post;

            // Avoid php notices when $post is empty.
            if ( empty( $post ) ) return $avatar;

            // Get post authors.
            $authors = $this->get_authors( $post->ID );

            // Get local post author avatar.
            if ( $authors[0]->type == 'guest' )
            {
                // Get guest author avatar (uploaded locally as featured image).
                $local_avatar_url = ( has_post_thumbnail( $authors[0]->id ) ? get_the_post_thumbnail_url( $authors[0]->id, $size ) : '' );
            }
            else
            {
                // Check if registered wp user has a local avatar and get it.
                $user_local_url   = get_user_meta( $post->post_author, 'molongui_author_image_url', true );
                $local_avatar_url = ( $user_local_url ? $user_local_url : '' );
            }
        }

        /**
         * If local avatar found, return customized markup.
         *
         * This markup is gotten from 'get_avatar()' function from 'wp-includes/pluggable.php' file and just the avatar
         * url is changed.
         */
        if ( $local_avatar_url )
        {
            return $author_avatar = sprintf(
                                        "<img alt='%s' src='%s' srcset='%s' class='%s' height='%d' width='%d' %s/>",
                                        esc_attr( $args['alt'] ),
                                        esc_url( $local_avatar_url ),
                                        esc_attr( "$local_avatar_url 2x" ),
                                        esc_attr( join( ' ', array( 'avatar', 'avatar-'.(int)$args['size'], 'photo' ) ) ),
                                        (int) $args['height'],
                                        (int) $args['width'],
                                        $args['extra_attr']
            );
        }

        /**
         * If no local avatar found, do not filter anything, stick to the one retrieved by WP.
         */
        return $avatar;
    }

	/**
     * Filters guest author avatar.
	 *
	 * Hook into 'get_the_author_description' filter to override guest-author biography.
	 *
	 * @see     https://developer.wordpress.org/reference/functions/get_the_author_meta/
	 *          https://developer.wordpress.org/reference/functions/the_archive_description/
     *          https://codex.wordpress.org/Function_Reference/get_post_field
     *
	 * @access  public
     * @param   string      $value              The value of the description.
	 * @param   int         $user_id            The user ID for the value.
	 * @param   int|bool    $original_user_id   The original user ID, as passed to the function.
	 * @return  string
	 * @since   2.1.1
	 * @version 2.1.1
	 */
	public function filter_author_bio( $value, $user_id, $original_user_id )
	{

		if ( is_author() and !in_the_loop() )
		{
			global $wp_query;

			/**
			 * Filters guest-author bio on guest-author archive (virtual) pages.
			 *
			 * @since   2.1.1
			 * @version 2.1.1
			 */
			if ( $wp_query->is_virtual and isset( $wp_query->guest_author_id ) ) return get_post_field( 'post_content', $wp_query->guest_author_id );

			/**
			 * Fixes wp-user bio on archive pages.
			 *
			 * Modifying 'where' clause from main query to include co-authored posts into author archive pages (see
			 * 'molongui_include_coauthored_posts' function in 'functions.php' file) makes $post value to point to the
			 * first post on the list of posts retrieved, so if that post's main-author is not the one being retrieved,
			 * the bio displayed will be wrong, because it will be the bio of that main-author.
			 *
			 * @since   2.1.1
			 * @version 2.1.1
			 */
			if ( $wp_query->query_vars['author'] )
			{
				$user = get_userdata( $wp_query->query_vars['author'] );

				return $user->description;
			}
		}

		// No filtering required. Return original value untouched.
		return $value;
	}

	/**
     * Adds (main) author metadata into html head.
     *
     * If it is a multiauthor post, only main's author data is added into the head of the HTML document.
     *
     * @access  public
     * @return  void
     * @since   1.0.0
     * @version 2.1.0
     */
    public function add_author_meta()
    {
        global $post;

        // Avoid PHP notices when $post is empty or $post->ID is 0.
        if ( empty( $post ) or empty( $post->ID ) ) return;

        // Get plugin options.
        $settings = get_option( MOLONGUI_AUTHORSHIP_ADVANCED_SETTINGS );

        // Leave if configured not to add any author meta.
        if ( !$settings['add_opengraph_meta'] and !$settings['add_google_meta'] and !$settings['add_facebook_meta'] ) return;

        // Get (main) author data.
        if ( $this->is_guest( $main_author = $this->get_main_author( $post->ID ) ) )
        {
            // Get guest author data.
            $author['name'] = get_post_field( 'post_title', $main_author->id );
            $author['link'] = get_post_meta( $main_author->id, '_molongui_guest_author_link', true );
            $author['fb']   = get_post_meta( $main_author->id, '_molongui_guest_author_facebook', true );
            $author['gp']   = get_post_meta( $main_author->id, '_molongui_guest_author_googleplus', true );
        }
        else
        {
            // Get registered wp user author data.
            $user = get_userdata( $main_author->id );
            $author['name'] = $user->display_name;
            $author['link'] = get_the_author_meta( 'molongui_author_link' );
            $author['fb']   = get_the_author_meta( 'molongui_author_facebook' );
            $author['gp']   = get_the_author_meta( 'molongui_author_googleplus' );
        }

        // Add author meta tags.
        if ( molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ) ) $meta = "\n<!-- Molongui Authorship " . MOLONGUI_AUTHORSHIP_VERSION . ", visit: https://wordpress.org/plugins/molongui-authorship/ -->\n";
        else $meta = "\n<!-- Molongui Authorship Premium " . MOLONGUI_AUTHORSHIP_VERSION . ", visit: " . MOLONGUI_AUTHORSHIP_WEB . " -->\n";

        // Add HTML author meta.
        $authors = $this->get_authors( $post->ID );
        $meta .= '<meta name="author" content="'.$this->prepare_name( $authors, count( $authors ) ).'">';

        // Add Google author meta.
        if ( $settings['add_google_meta'] == 1 && isset( $author['gp'] ) && $author['gp'] <> '' ) $meta .= $this->add_google_author_meta( $author['gp'] );

        // Add Facebook author meta.
        if ( $settings['add_facebook_meta'] == 1 && isset( $author['fb'] ) && $author['fb'] <> '' ) $meta .= $this->add_facebook_author_meta( $author['fb'] );

        // Add OpenGraph metadata only on "author archive" page.
        if ( $settings['add_opengraph_meta'] == 1 && is_author() )
        {
            $meta .= $this->add_opengraph_meta( $author['name'], $author['link'] );
        }

        $meta .= "<!-- /Molongui Authorship -->\n\n";

        echo $meta;
    }

    /**
     * Get the Open Graph for the current (guest) author.
     *
     * @access  public
     * @param   string  $author_name    Author name.
     * @param   string  $author_link    Author link.
     * @return  string  $meta           The meta tags to include into the html head.
     * @since   1.0.0
     * @version 1.0.0
     */
    public function add_opengraph_meta( $author_name, $author_link )
    {
        $og  = '';
        $og .= sprintf( '<meta property="og:type" content="%s" />', 'profile' ) . "\n";
        $og .= ( $author_link ? sprintf( '<meta property="og:url" content="%s" />', $author_link )."\n" : '' );
        $og .= ( $author_name ? sprintf( '<meta property="profile:username" content="%s" />', $author_name )."\n" : '' );

        return $og;
    }

    /**
     * Get the Google author Meta.
     *
     * @access  public
     * @param   string  $author_gp  Google id.
     * @return  string  $meta       The meta tags to include into the html head.
     * @since   1.0.0
     * @version 1.0.0
     */
    public function add_google_author_meta( $author_gp )
    {
        return '<link rel="author" href="' . ( ( strpos( $author_gp, 'http' ) === false ) ? 'https://plus.google.com/' : '' ) . $author_gp . '" />' . "\n";
    }

    /**
     * Get the Facebook author Meta.
     *
     * @access  public
     * @param   string  $author_fb  Facebook id.
     * @return  string  $meta       The meta tags to include into the html head.
     * @since   1.0.0
     * @version 1.0.0
     */
    public function add_facebook_author_meta( $author_fb )
    {
        return '<meta property="article:author" content="' . ( ( strpos( $author_fb, 'http' ) === false ) ? 'https://www.facebook.com/' : '' ) . $author_fb . '" />' . "\n";
    }

    /**
     * Renders the author box on those posts and pages configured to do so.
     *
     * Author box is not added to virtual pages.
     * Author box is not added to author nor archive pages since version 1.2.17.
     * Author box is not added on paged posts/pages unless being the first/last page since version 2.0.0.
     *
     * @see     https://pippinsplugins.com/playing-nice-with-the-content-filter/
     *
     * @access  public
     * @param   string      $content        The post content.
     * @return  string      $content        The modified post content.
     * @since   1.0.0
     * @version 3.0.0
     */
    public function render_author_box( $content )
    {
        // Leave if not on a post/cpt or page, on a virtualpage or if it is not the main query or outside the loop.
        if ( ( !is_single() and !is_page() ) or is_virtualpage() or !is_main_query() or !in_the_loop() ) return $content;

        global $post, $multipage, $numpages, $page;

        // Move forward only on post types where plugin functionality has been extended to.
        if ( !in_array( get_post_type( $post ), molongui_supported_post_types( MOLONGUI_AUTHORSHIP_ID, 'all' ) ) ) return $content;

	    // Get plugin settings.
	    $box_settings      = get_option( MOLONGUI_AUTHORSHIP_BOX_SETTINGS );
	    $byline_settings   = get_option( MOLONGUI_AUTHORSHIP_BYLINE_SETTINGS );
	    $guest_settings    = get_option( MOLONGUI_AUTHORSHIP_GUEST_SETTINGS );
	    $advanced_settings = get_option( MOLONGUI_AUTHORSHIP_ADVANCED_SETTINGS );
	    $archives_settings = get_option( MOLONGUI_AUTHORSHIP_ARCHIVES_SETTINGS );
	    $string_settings   = get_option( MOLONGUI_AUTHORSHIP_STRING_SETTINGS );
	    $settings          = array_merge( $box_settings, $byline_settings, $guest_settings, $archives_settings, $advanced_settings, $string_settings );

	    // Get author box post's settings.
        $author_box_display  = get_post_meta( $post->ID, '_molongui_author_box_display', true );
        $author_box_position = get_post_meta( $post->ID, '_molongui_author_box_position', true );

        // Post configuration prevails over global settings.
        $author_box_position = ( !empty( $author_box_position ) ? $author_box_position : $settings['position'] );
        if ( empty( $author_box_display ) )                                 // If no post configuration, consider global setting.
        {
            if ( ( $settings['display'] == '0' ) or                         // Global setting set to not display the authorship box, exit.
	             ( $settings['display'] == 'posts' and !is_single() ) or    // Global setting set to display the author box only on posts, exit if not displaying a post.
		         ( $settings['display'] == 'pages' and !is_page() ) )       // Global setting set to display the author box only on pages, exit if not displaying a page.
            {
	            return $content;
            }

            // If displaying a post and global setting to display the author box on all posts, check post categories are not any of those in exclusion list.
	        if ( ( $settings['display'] == 'posts' or $settings['display'] == '1' ) and is_single() )
            {
	            foreach ( molongui_post_categories( true ) as $category )
	            {
		            if ( isset( $box_settings['hide_on_category_'.$category['id']] ) and $box_settings['hide_on_category_'.$category['id']] )
		            {
			            if ( in_array( $category['id'], wp_get_post_categories( $post->ID ) ) ) return $content;
		            }
	            }
            }
        }

        // If paged post/page and author box is configured to be displayed at the top of the content, display it only if first page.
        if ( $multipage and $page != 1 and $author_box_position == 'above' ) return $content;

        // If paged post/page and author box is configured to be displayed at the bottom of the content, display it only if last page.
        if ( $multipage and $page != $numpages and $author_box_position == 'below' ) return $content;

        // If paged post/page and author box is configured to be displayed above and below the content, display it only on first and last pages.
        if ( $multipage and ( $page != 1 and $page != $numpages ) and $author_box_position == 'both' ) return $content;

        // If post configured to not display the authorship box, exit and do not display it.
        if ( $author_box_display == 'hide' ) return $content;

        // Get authors.
        $post_authors = $this->get_authors( $post->ID );

        // If no authors or author id is 0, leave.
        if ( empty( $post_authors ) or $post_authors[0]->id == 0 ) return $content;

        // Get author(s) box markup.
        $html = $this->get_box_markup( $post, $post_authors, $settings );

        // Add the author(s) box(es) to the post content.
        switch ( $author_box_position )
        {
            case "both":
	            if ( !$multipage )
                {
	                // Generate a second box markup to avoid issues with tabbed layout and box's id.
	                $html_2  = $this->get_box_markup( $post, $post_authors, $settings );
	                $content = $html.$content.$html_2;
                }
                elseif ( $page == 1 )
                {
	                $content = $html.$content;
                }
                elseif ( $page == $numpages )
                {
	                $content .= $html;
                }
            break;

            case "above":
                $content = $html.$content;
            break;

            case "below":
            case "default":
                $content .= $html;
            break;
        }

        // Return markup to render.
        return $content;
    }

	/**
	 * Returns the author box markup.
	 *
	 * @access  public
	 * @param   object      $post           The post.
	 * @param   array       $post_authors   The post authors.
	 * @param   array       $settings       The plugin settings.
	 * @return  string      $html           The author(s) box markup.
	 * @since   2.0.7
	 * @version 3.0.0
	 */
	public function get_box_markup( $post, $post_authors, $settings )
    {
	    // Init vars.
	    $html = '';
	    $is_multiauthor = $this->is_multiauthor( $post_authors );
	    $show_headline  = true;

	    // Get author(s) data.
	    foreach ( $post_authors as $post_author )
	    {
		    $authors[$post_author->ref] = $this->get_data( $post_author->id, $post_author->type, $settings );
		    if ( $settings['show_related'] or $settings['layout'] == 'tabbed' )
		    {
			    // Get author's posts.
			    $authors[$post_author->ref]['posts'] = $this->get_posts( $post_author->id, $post_author->type, $settings, false, ( ( is_object( $post ) and !empty( $post->ID ) ) ? array( $post->ID ) : array() ), 'related' );
		    }

		    // If there is no significant info to show about this author, do not display him/her on the author box.
		    if ( !$authors[$post_author->ref]['bio'] and $settings['hide_if_no_bio'] )
		    {
			    $authors[$post_author->ref]['hide'] = true;
			    continue;
		    }

		    // Generate markup to add to content if single author or multi-author with 'individual' layout selected.
		    if ( !$is_multiauthor or ( $is_multiauthor and $settings['multiauthor_box_layout'] == 'individual' ) )
		    {
			    // Save data into the variable used by template files ($author).
			    $author = $authors[$post_author->ref];

			    // Make up a random id that uniquely identifies this box.
			    $random_id = substr( number_format( time() * mt_rand(), 0, '', '' ), 0, 10 );

			    // The author box markup.
			    ob_start();
			    include( MOLONGUI_AUTHORSHIP_DIR . 'public/views/html-author-box-layout.php' );
                $html .= ob_get_clean();

                // Flag to not show headline again when multiauthored post and 'individual' layout selected.
			    $show_headline = false;
		    }
	    }

	    // Generate markup to add to content if multi-author and selected layout is not the 'individual' one.
	    if ( $is_multiauthor and $settings['multiauthor_box_layout'] != 'individual' )
	    {
		    // Make up a random id that uniquely identifies this box.
		    $random_id = substr( number_format( time() * mt_rand(), 0, '', '' ), 0, 10 );

		    // The multi-author box markup.
		    ob_start();
		    include( MOLONGUI_AUTHORSHIP_DIR . 'public/views/html-multiauthor-box-layout.php' );
		    $html .= ob_get_clean();
	    }

	    // Return author(s) markup.
	    return $html;
    }

	/**
	 * Generates CSS rules based on set plugin configuration.
	 *
	 * @access  public
	 * @param   string      Box border setting value.
	 * @return  string      Box border classes to add to the author box markup.
	 * @since   3.0.0
	 * @version 3.0.0
	 */
	public function get_box_border( $box_border )
	{
		switch ( $box_border )
		{
			case 'none':
				return 'molongui-border-none';
				break;

			case 'horizontals':
				return 'molongui-border-right-none molongui-border-left-none';
				break;

			case 'verticals':
				return 'molongui-border-top-none molongui-border-bottom-none';
				break;

			case 'top':
				return 'molongui-border-right-none molongui-border-bottom-none molongui-border-left-none';
				break;

			case 'right':
				return 'molongui-border-top-none molongui-border-bottom-none molongui-border-left-none';
				break;

			case 'bottom':
				return 'molongui-border-top-none molongui-border-right-none molongui-border-left-none';
				break;

			case 'left':
				return 'molongui-border-top-none molongui-border-right-none molongui-border-bottom-none';
				break;

			case 'all':
			default:
				return '';
				break;
		}
	}

	/**
	 * Returns author type for the author given nicename.
	 *
	 * @access  public
	 * @param   string  $nicename   Author nicename.
	 * @return  string  $type       Author type ( user, guest or not_found).
	 * @since   2.0.15
	 * @version 2.0.15
	 */
    public function get_type_by_nicename( $nicename )
    {
	    // Search whether there is a guest author with the given nicename.
	    $my_query = new WP_Query(
		    array(
			    'name'      => $nicename,
			    'post_type' => 'molongui_guestauthor',
		    )
	    );
	    if ( $my_query->have_posts() )
	    {
		    return 'guest';
	    }
	    else
	    {
		    // If no matching guest author found, search for a matching registered user.
		    if ( $author = molongui_get_user_by( 'user_nicename', $nicename ) )
		    {
			    return 'user';
		    }
	    }

	    // No match was found for the given nicename.
	    return 'not_found';
    }

    /**
     * Returns posts co-authored by, at least, the given authors.
     *
	 * @see     http://codex.wordpress.org/Class_Reference/WP_Query
     *          https://stackoverflow.com/a/44311064
     *
     * @param   array   $authors        The authors ref (user-<id> or guest-<id>) to get posts by.
     * @param   array   $settings       The plugin settings.
     * @param   boolean $get_all        Whether to limit the query or not.
     * @param   array   $exclude        List of post IDs not to retrieve (usually used to avoid current post from being retrieved).
     * @param   string  $entry          Post type: {post | page | ...}. Defaults to post.
     * @param   array   $meta_query     Meta query to include.
     * @return  array   $posts          The author posts.
     * @access  public
     * @since   3.0.0
	 * @version 3.0.0
	*/
    public function get_coauthored_posts( $authors, $settings = array(), $get_all = false, $exclude = array(), $entry = 'post', $meta_query = array() )
	{
		// Load settings if none given.
		if ( !isset( $settings ) or empty( $settings ) ) $settings = get_option( MOLONGUI_AUTHORSHIP_BOX_SETTINGS );

		// Handle post types to query.
		switch ( $entry )
		{
			case 'all':
				$entries = molongui_get_post_types( 'all', 'names', false );
			break;

			case 'selected':
				$entries = molongui_supported_post_types( MOLONGUI_AUTHORSHIP_ID, 'all', false );
			break;

			case 'related':
				foreach( molongui_get_post_types( 'all', 'names', false ) as $post_type_name )
				{
					if ( isset( $settings['related_post_type_'.$post_type_name] ) and $settings['related_post_type_'.$post_type_name] ) $entries[] = $post_type_name;
				}
			break;

			default:
				$entries = $entry;
			break;
		}

		// Prepare meta-query.
		if ( count( $authors ) > 1 )
		{
			$mq['authors']['relation'] = 'AND';

			// Create a comparison for every single author.
			foreach( $authors as $author )
			{
				$mq['authors'][] = array( 'key' => '_molongui_author', 'value' => $author->ref, 'compare' => '=' );
			}
		}
		// If only one author then proceed with simple query.
		else
		{
			$mq['authors'] = array( 'key' => '_molongui_author', 'value' => $authors, 'compare' => '=' );
		}

		// Add given meta-query arguments to the prepared meta-query.
		if ( isset( $meta_query ) and !empty( $meta_query ) )
		{
			$mq['authors']['relation'] = 'AND';
			$mq['authors'] = array
            (
                'key'   => $meta_query['key'],
                'value' => $meta_query['value'],
            );
		}

        // Prepare query args.
        $args = array(
            'post_type'      => $entries,
            'orderby'        => ( isset( $settings['related_order_by'] ) and $settings['related_order_by'] ? $settings['related_order_by'] : 'ASC' ),
            'order'          => ( isset( $settings['related_order'] ) and $settings['related_order'] ? $settings['related_order'] : 'date' ),
            'posts_per_page' => ( $get_all ? '-1' : $settings['related_items'] ),
            'post__not_in'   => $exclude,
            'meta_query'     => $mq,
        );

        // Get data.
        $data = new WP_Query( $args );

        // Prepare data.
        foreach ( $data->posts as $post ) $posts[] = $post;

		// Return data.
		return ( !empty( $posts ) ? $posts : array() );
	}

}