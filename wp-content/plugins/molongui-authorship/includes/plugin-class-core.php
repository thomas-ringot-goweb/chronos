<?php

namespace Molongui\Authorship\Includes;

use Molongui\Fw\Includes\Loader;
use Molongui\Fw\Includes\i18n;
use Molongui\Fw\Includes\DB_Update;
use Molongui\Fw\Includes\License;
use Molongui\Fw\Admin\Admin as CommonAdmin;
use Molongui\Fw\FrontEnd\FrontEnd as CommonFrontEnd;
use Molongui\Authorship\Admin\Admin;
use Molongui\Authorship\FrontEnd\FrontEnd;
use Molongui\Authorship\Includes\Author as Plugin_Aim;
use Molongui\Authorship\Includes\Custom_Profile;

// Deny direct access to this file.
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @author     Amitzy
 * @package    Authorship
 * @subpackage /includes
 * @since      1.0.0
 * @version    3.0.0
 */
class Core
{
    /**
     * The unique identifier of this plugin.
     *
     * @access  protected
     * @var     string      $plugin_name        The string used to uniquely identify this plugin.
     * @since   1.0.0
     * @version 1.0.0
     */
    protected $plugin_name;

    /**
     * The current version of the plugin.
     *
     * @access  protected
     * @var     string      $plugin_version     The current version of the plugin.
     * @since   1.0.0
     * @version 2.0.0
     */
    protected $plugin_version;

    /**
     * The loader that's responsible for maintaining and registering all hooks that power
     * the plugin.
     *
     * @access  protected
     * @var     Loader      $loader             Maintains and registers all hooks for the plugin.
     * @since   1.0.0
     * @version 2.0.0
     */
    protected $loader;

    /**
     * Plugin's admin.
     *
     * @access  protected
     * @since   2.0.0
     * @version 2.0.0
     */
    protected $admin;

    /**
     * Plugin's aim.
     *
     * @access  protected
     * @since   2.0.0
     * @version 2.0.0
     */
    protected $aim;

    /**
     * Define the core functionality of the plugin.
     *
     * Set the plugin name and the plugin version that can be used throughout the plugin.
     * Load the dependencies, define the locale, and set the hooks for the admin area and
     * the public-facing side of the site.
     *
     * @access  public
     * @since   1.0.0
     * @version 2.0.0
     */
    public function __construct()
    {
        // Initialize variables.
        $this->plugin_name    = MOLONGUI_AUTHORSHIP_ID;
        $this->plugin_version = MOLONGUI_AUTHORSHIP_VERSION;

        // Load code dependencies.
        $this->load_dependencies();

        // Update database schema if needed.
        $this->update_db();

        // Define the locale for internationalization.
        $this->set_locale();

        // Define admin hooks.
        if ( is_admin() ) $this->define_admin_hooks();

        // Define public hooks.
        if ( $this->check_license() ) $this->define_frontend_hooks();
    }

    /**
     * Updates database schema if required.
     *
     * @access  private
     * @since   1.3.0
     * @version 2.0.0
     */
    private function update_db()
    {
        $update_db = new DB_Update( MOLONGUI_AUTHORSHIP_ID, MOLONGUI_AUTHORSHIP_DB_VERSION );
        if ( $update_db->db_update_needed() ) $update_db->run_update();
    }

    /**
     * Define the locale for this plugin for internationalization.
     *
     * Uses the i18n class in order to set the domain and to register the hook
     * with WordPress.
     *
     * @access  private
     * @since   1.0.0
     * @version 2.0.0
     */
    private function set_locale()
    {
        $plugin_i18n = new i18n();
        $plugin_i18n->set_domain( MOLONGUI_AUTHORSHIP_TEXTDOMAIN );

        $this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );
    }

    /**
     * Check license status.
     *
     * This function checks whether a required license is set or not. Free plugins are not checked.
     *
     * @access  private
     * @return  bool
     * @since   1.0.0
     * @version 2.0.0
     */
    private function check_license()
    {
        // Check activation for premium plugins.
        if ( molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ) ) return ( molongui_is_active( MOLONGUI_AUTHORSHIP_DIR ) );

        // Default.
        return true;
    }

    /**
     * Loads the required dependencies for this plugin.
     *
     * @access  private
     * @since   1.0.0
     * @version 2.0.0
     */
    private function load_dependencies()
    {
        // Load common dependencies.
        $this->load_common_dependencies();

        // Load plugin dependencies.
        $this->load_plugin_dependencies();
    }

    /**
     * Register all of the hooks related to the admin area functionality
     * of the plugin.
     *
     * @access  private
     * @since   1.0.0
     * @version 2.0.0
     */
    private function define_admin_hooks()
    {
        // Define common admin hooks.
        $this->define_common_admin_hooks();

        // Define plugin specific admin hooks.
        $this->define_plugin_admin_hooks();
    }

    /**
     * Register all of the hooks related to the public-facing functionality
     * of the plugin.
     *
     * @access  private
     * @since   1.0.0
     * @version 2.0.0
     */
    private function define_frontend_hooks()
    {
        // Define common frontend hooks.
        $this->define_common_frontend_hooks();

        // Define plugin specific frontend hooks.
        $this->define_plugin_frontend_hooks();
    }

    /**
     * Loads the common dependencies for this plugin.
     *
     * Includes the following files that make up the plugin:
     *
     * - Loader.    Orchestrates the hooks of the plugin.
     * - i18n.      Defines internationalization functionality.
     * - Admin.     Defines all hooks for the admin area.
     * - FrontEnd.  Defines all hooks for the public side of the site.
     *
     * Creates an instance of the loader which will be used to register the hooks with WP.
     * Also creates an instance of admin.
     *
     * @access  private
     * @since   2.0.0
     * @version 2.1.0
     */
    private function load_common_dependencies()
    {
        /**
         * The class responsible for handling database schema update to keep backwards compatibility.
         */
        if ( !class_exists( 'Molongui\Fw\Includes\DB_Update' ) ) require_once MOLONGUI_AUTHORSHIP_FW_DIR . 'includes/fw-class-db-update.php';

        /**
         * The class responsible for orchestrating the actions and filters of the
         * core plugin.
         */
        if ( !class_exists( 'Molongui\Fw\Includes\Loader' ) ) require_once MOLONGUI_AUTHORSHIP_FW_DIR . 'includes/fw-class-loader.php';
        $this->loader = new Loader();

        /**
         * The class responsible for defining internationalization functionality
         * of the plugin.
         */
        if ( !class_exists( 'Molongui\Fw\Includes\i18n' ) ) require_once MOLONGUI_AUTHORSHIP_FW_DIR . 'includes/fw-class-i18n.php';

        /**
         * The file responsible for defining common helper functionalities.
         */
        require_once MOLONGUI_AUTHORSHIP_FW_DIR . 'includes/fw-helper-functions.php';

        /**
         * The class responsible for defining common actions that occur in the admin area.
         */
        if ( !class_exists( 'Molongui\Fw\Admin\Admin' ) ) require_once MOLONGUI_AUTHORSHIP_FW_DIR . 'admin/fw-class-admin.php';

        /**
         * The class responsible for defining common actions that occur in the frontend.
         *
         * @since 2.1.0
         */
        if ( !class_exists( 'Molongui\Fw\FrontEnd\FrontEnd' ) ) require_once MOLONGUI_AUTHORSHIP_FW_DIR . 'public/fw-class-public.php';

	    /**
	     * The library containing customizer functions.
	     *
	     * @since 2.1.0
	     */
	    if ( !class_exists( 'Molongui\Fw\Customizer\Customizer' ) ) require_once( MOLONGUI_AUTHORSHIP_FW_DIR . 'customizer/fw-class-customizer.php' );
    }

    /**
     * Loads this plugin specific dependencies.
     *
     * @access  private
     * @since   2.0.0
     * @version 2.0.0
     */
    private function load_plugin_dependencies()
    {
	    /**
	     * The class responsible for defining plugin specific actions that occur in the admin area.
	     */
	    require_once MOLONGUI_AUTHORSHIP_DIR . 'admin/plugin-class-admin.php';
	    $this->admin = new Admin();

	    /**
	     * The class responsible for defining all actions that occur in the public-facing
	     * side of the site.
	     */
	    require_once MOLONGUI_AUTHORSHIP_DIR . 'public/plugin-class-public.php';


        /**
         * The class responsible for defining all Author stuff.
         */
        require_once MOLONGUI_AUTHORSHIP_DIR . 'includes/class-author.php';
        $this->aim = new Plugin_Aim();

        /**
         * The class responsible for customizing WordPress user profile.
         */
        require_once MOLONGUI_AUTHORSHIP_DIR . 'includes/class-custom-profile.php';
    }

    /**
     * Registers all the common hooks related to the admin area functionality
     * of the plugin.
     *
     * @access  private
     * @since   2.0.0
     * @version 2.0.0
     */
    private function define_common_admin_hooks()
    {
        // Instantiate the fw admin class.
        $common_admin = new CommonAdmin( MOLONGUI_AUTHORSHIP_ID, $this->loader );

        // Common admin hooks.
        // TODO: Define here any required common admin hook.

        // Load plugin settings.
        $this->loader->add_action( 'init', $this->admin, 'init_plugin_settings' );

        // Load admin CSS and JS scripts.
        $this->loader->add_action( 'admin_enqueue_scripts', $this->admin, 'enqueue_styles' );
        $this->loader->add_action( 'admin_enqueue_scripts', $this->admin, 'enqueue_scripts' );
    }

    /**
     * Registers all the plugin specific hooks related to the admin area functionality of the plugin.
     *
     * @access  private
     * @since   2.0.0
     * @version 2.2.0
     */
    private function define_plugin_admin_hooks()
    {
    	// Load configured settings.
	    $guest_settings = get_option( MOLONGUI_AUTHORSHIP_GUEST_SETTINGS );

	    // Register "guest-author" custom post-type if functionality enabled.
    	if ( $guest_settings['enable_guest_authors_feature'] )
	    {
		    $this->loader->add_action( 'init', $this->aim, 'register_custom_post_type' );
		    $this->loader->add_filter( 'enter_title_here', $this->aim, 'change_default_title' );
		    $this->loader->add_action( 'admin_head', $this->aim, 'remove_media_buttons' );
		    $this->loader->add_action( 'admin_menu', $this->aim, 'custom_remove_menu_pages' );
	    }

        // Removes the 'author' dropdown from the post quick edit.
        $this->loader->add_action( 'admin_head', $this->aim, 'remove_quick_edit_author' );

        // Customize columns shown on the Manage Guest Authors screen.
        $this->loader->add_filter( 'manage_molongui_guestauthor_posts_columns', $this->aim, 'add_list_columns' );
        $this->loader->add_action( 'manage_molongui_guestauthor_posts_custom_column', $this->aim, 'fill_list_columns', 5, 2 );

        // Modify author column shown on the Manage Posts screen.
        $this->loader->add_filter( 'manage_posts_columns', $this->aim, 'change_author_column', 5, 2 );
        $this->loader->add_action( 'manage_posts_custom_column', $this->aim, 'fill_author_column', 5, 2 );

        // Modify author column shown on the Manage Pages screen.
        $this->loader->add_filter( 'manage_pages_columns', $this->aim, 'change_author_column', 5, 2 );
        $this->loader->add_action( 'manage_pages_custom_column', $this->aim, 'fill_author_column', 5, 2 );

        // Replace default WP author meta box.
        $this->loader->add_action( 'admin_menu', $this->aim, 'remove_author_metabox' );
        $this->loader->add_action( 'add_meta_boxes', $this->aim, 'add_meta_boxes' );
        $this->loader->add_action( 'save_post', $this->aim, 'save' );

        // Instantiate 'Custom_Profile' class.
        $custom_profile = new Custom_Profile();

        // Add "id" column to the "All users" screen.
        $this->loader->add_filter( 'manage_users_columns', $custom_profile, 'add_id_column' );
        $this->loader->add_action( 'manage_users_custom_column', $custom_profile, 'fill_id_column', 10, 3 );

        // Add custom profile fields to WordPress user form.
        $this->loader->add_action( 'show_user_profile', $custom_profile, 'add_authorship_fields' );
        $this->loader->add_action( 'edit_user_profile', $custom_profile, 'add_authorship_fields' );
        $this->loader->add_action( 'personal_options_update', $custom_profile, 'save_authorship_fields' );
        $this->loader->add_action( 'edit_user_profile_update', $custom_profile, 'save_authorship_fields' );
    }

    /**
     * Registers all the common hooks related to the frontend area functionality
     * of the plugin.
     *
     * @access  private
     * @since   2.0.0
     * @version 2.1.0
     */
    private function define_common_frontend_hooks()
    {
	    // Instantiate the fw frontend class.
	    $common_frontend = new CommonFrontEnd( $this->plugin_name, $this->loader );

        // Load frontend CSS and JS scripts.
        $this->loader->add_action( 'wp_enqueue_scripts', $common_frontend, 'enqueue_styles' );
        $this->loader->add_action( 'wp_enqueue_scripts', $common_frontend, 'enqueue_scripts' );
    }

    /**
     * Registers all the plugin specific hooks related to the frontend area functionality of the plugin.
     *
     * @access  private
     * @since   2.0.0
     * @version 3.0.0
     */
    private function define_plugin_frontend_hooks()
    {
    	// Get plugin settings.
	    $box_settings = (array) get_option( MOLONGUI_AUTHORSHIP_BOX_SETTINGS );

    	// Instantiate Frontend class.
	    $plugin_frontend = new FrontEnd( $this->plugin_name, $this->plugin_version );

	    // Load frontend CSS and JS scripts.
	    $this->loader->add_action( 'wp_enqueue_scripts', $plugin_frontend, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_frontend, 'enqueue_scripts' );

        // OpenGraph, Google and Facebook authorship head meta tags.
        $this->loader->add_action( 'wp_head', $this->aim, 'add_author_meta' );

        // Display guest author credits if set.
        // High priority numbers (99) are set to ensure filters are run last so they take effect and are not overridden.
        $this->loader->add_filter( 'the_author', $this->aim, 'filter_author_name', 10, 1 );
        $this->loader->add_filter( 'author_link', $this->aim, 'filter_author_link', 99, 1 );
        $this->loader->add_filter( 'get_avatar', $this->aim, 'filter_author_avatar', 99, 6 );
		$this->loader->add_filter( 'get_the_author_display_name', $this->aim, 'filter_the_author_display_name', 10, 3 );
	    $this->loader->add_filter( 'get_the_author_description', $this->aim, 'filter_author_bio', 10, 3 );

        // Display author box on single posts.
        $this->loader->add_filter( 'the_content', $this->aim, 'render_author_box', $box_settings['order'], 1 );     // Make it run after default priority to avoid collisions (i.e: SiteOrigin builder).
    }

    /**
     * Run the loader to execute all of the hooks with WordPress.
     *
     * @access  public
     * @since   1.0.0
     * @version 1.0.0
     */
    public function run()
    {
        $this->loader->run();
    }

    /**
     * The name of the plugin used to uniquely identify it within the context of
     * WordPress and to define internationalization functionality.
     *
     * @access  public
     * @return  string     The name of the plugin.
     * @since   1.0.0
     * @version 1.0.0
     */
    public function get_plugin_name()
    {
        return $this->plugin_name;
    }

    /**
     * Retrieve the version number of the plugin.
     *
     * @access  public
     * @return  string      The version number of the plugin.
     * @since   1.0.0
     * @version 1.0.0
     */
    public function get_version()
    {
        return $this->plugin_version;
    }

    /**
     * The reference to the class that orchestrates the hooks with the plugin.
     *
     * @access  public
     * @return  Loader    Orchestrates the hooks of the plugin.
     * @since   1.0.0
     * @version 1.0.0
     */
    public function get_loader()
    {
        return $this->loader;
    }
}