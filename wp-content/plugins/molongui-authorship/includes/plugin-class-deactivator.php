<?php

namespace Molongui\Authorship\Includes;

// Deny direct access to this file.
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @author     Amitzy
 * @package    Authorship
 * @subpackage /includes
 * @since      1.0.0
 * @version    2.1.0
 */
class Deactivator
{
    /**
     * Fires all required actions during plugin deactivation.
     *
     * @see     https://shibashake.com/wordpress-theme/write-a-plugin-for-wordpress-multi-site
     *          https://leaves-and-love.net/blog/making-plugin-multisite-compatible/
     *          https://codex.wordpress.org/Class_Reference/wpdb#Multi-Site_Variables
     *
     * @access  public
     * @param   bool    $network_wide   Whether to disable the plugin for all sites in the network or just the current site. Multisite only. Default is false.
     * @since   1.0.0
     * @version 3.0.0
     */
    public static function deactivate( $network_wide )
    {
	    // Check if the plugin is being network-deactivated on a multisite installation.
	    if ( function_exists('is_multisite') and is_multisite() and $network_wide )
	    {
		    // Check whether current user is really a super admin.
		    if ( false == is_super_admin() ) return;

		    // Deactivate the plugin for all these sites in this network.
		    foreach ( molongui_get_sites() as $site_id )
		    {
			    switch_to_blog( $site_id );
				self::deactivate_single_blog();
			    restore_current_blog();
		    }
	    }
	    // Deactivation on a single site installation.
	    else
	    {
		    // Check whether current user can deactivate plugins.
		    if ( false == current_user_can( 'activate_plugins' ) ) return;

			self::deactivate_single_blog();
	    }
    }

	/**
	 * Deactivation tasks that need to be executed on every site.
	 *
	 * @access  public
	 * @since   2.0.7
	 * @version 3.0.0
	 */
	private static function deactivate_single_blog()
	{
		// Remove existing rewrite rules and recreate them.
		flush_rewrite_rules();

		// If premium plugin, remove license key if configured so.
		if( molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ) )
		{
			if ( !class_exists( 'Molongui\Fw\Includes\License' ) ) require_once( MOLONGUI_AUTHORSHIP_DIR . 'fw/update/includes/fw-class-license.php' );
			$license = new \Molongui\Fw\Includes\License( MOLONGUI_AUTHORSHIP_ID );
			$license->remove( false );
		}

		// Delete notice transients.
		delete_transient( MOLONGUI_AUTHORSHIP_SLUG.'-activated' );
		delete_transient( MOLONGUI_AUTHORSHIP_SLUG.'-updated' );
	}

}