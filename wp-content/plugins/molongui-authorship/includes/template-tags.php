<?php

use Molongui\Authorship\Includes\Author;

// Deny direct access to this file.
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Template tags functions.
 *
 * @author     Amitzy
 * @package    Authorship
 * @subpackage /includes
 * @since      2.1.0

/**
 * Returns the authors display names, without links to their archive pages. This tag must be used within 'The Loop'.
 *
 * This function is the one to use as replacement for 'get_the_author()' template tag.
 *
 * @param   int     $pid            Post id. Defaults to null.
 * @param   string  $separator      Delimiter that should appear between the authors.
 * @param   string  $last_separator Delimiter that should appear between the last two authors.
 * @param   string  $before         String to be prepended before the author name(s). Defaults to an empty string.
 * @param   string  $after          String to be appended after the author name(s). Defaults to an empty string.
 * @return  string                  Byline as a string.
 * @since   2.0.0
 * @version 2.1.0
 */
function get_the_molongui_author( $pid = null, $separator = '', $last_separator = '', $before = '', $after = '' )
{
    // Check this tag is used within 'The Loop'.
    if ( !in_the_loop() ) return '';

	// Get plugin settings.
	$settings = get_option( MOLONGUI_AUTHORSHIP_BYLINE_SETTINGS );

    // Prepend given string.
    $output  = '';
    $output .= apply_filters( 'molongui_author_byline_before', ( !empty( $before ) ? $before : $settings['byline_modifier_before'] ) );

    // Get byline.
    $author  = new Author();
    $output .= $author->get_by_line( $pid, $separator, $last_separator, false );

    // Append given string.
    $output .= apply_filters( 'molongui_author_byline_after', ( !empty( $after ) ? $after : $settings['byline_modifier_after'] ) );

    // Return byline.
    return $output;
}

/**
 * Outputs the authors display names, without links to their archive pages. This tag must be used within 'The Loop'.
 *
 * This function is the one to use as replacement for 'the_author()' template tag.
 *
 * @param   int     $pid            Post id. Defaults to null.
 * @param   string  $separator      Delimiter that should appear between the authors.
 * @param   string  $last_separator Delimiter that should appear between the last two authors.
 * @param   string  $before         String to be prepended before the author name(s). Defaults to an empty string.
 * @param   string  $after          String to be appended after the author name(s). Defaults to an empty string.
 * @return  void                    Displays the byline.
 * @since   2.0.0
 * @version 2.1.0
 */
function the_molongui_author( $pid = null, $separator = '', $last_separator = '', $before = '', $after = '' )
{
    echo get_the_molongui_author( $pid, $separator, $last_separator, $before, $after );
}

/**
 * Returns the authors display names, with links to their archive pages. This tag must be used within 'The Loop'.
 *
 * This function is the one to use as replacement for 'get_the_author_posts_link()' template tag.
 *
 * @param   int     $pid            Post id. Defaults to null.
 * @param   string  $separator      Delimiter that should appear between the authors.
 * @param   string  $last_separator Delimiter that should appear between the last two authors.
 * @param   string  $before         String to be prepended before the author name(s). Defaults to an empty string.
 * @param   string  $after          String to be appended after the author name(s). Defaults to an empty string.
 * @return  string                  Displays the byline with linked names.
 * @since   2.0.0
 * @version 2.1.0
 */
function get_the_molongui_author_posts_link( $pid = null, $separator = '', $last_separator = '', $before = '', $after = '' )
{
    // Check this tag is used within 'The Loop'.
    if ( !in_the_loop() ) return '';

	// Get plugin settings.
	$settings = get_option( MOLONGUI_AUTHORSHIP_BYLINE_SETTINGS );

    // Prepend given string.
    $output  = '';
    $output .= apply_filters( 'molongui_author_byline_before', ( !empty( $before ) ? $before : $settings['byline_modifier_before'] ) );

    // Get byline.
    $author  = new Author();
    $output .= $author->get_by_line( $pid, $separator, $last_separator, true );

    // Append given string.
    $output .= apply_filters( 'molongui_author_byline_after', ( !empty( $after ) ? $after : $settings['byline_modifier_after'] ) );

    // Return byline.
    return $output;
}

/**
 * Outputs the authors display names, with links to their archive pages. This tag must be used within 'The Loop'.
 *
 * This function is the one to use as replacement for 'the_author_posts_link()' template tag.
 *
 * @param   int     $pid            Post id. Defaults to null.
 * @param   string  $separator      Delimiter that should appear between the authors.
 * @param   string  $last_separator Delimiter that should appear between the last two authors.
 * @param   string  $before         String to be prepended before the author name(s). Defaults to an empty string.
 * @param   string  $after          String to be appended after the author name(s). Defaults to an empty string.
 * @return  void                    Displays the byline with linked names.
 * @since   2.0.0
 * @version 2.1.0
 */
function the_molongui_author_posts_link( $pid = null, $separator = '', $last_separator = '', $before = '', $after = '' )
{
    echo get_the_molongui_author_posts_link( $pid, $separator, $last_separator, $before, $after );
}