<?php

use Molongui\Authorship\Includes\Author;

// Deny direct access to this file.
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Plugin functions.
 *
 * @author     Amitzy
 * @package    Authorship
 * @subpackage /includes
 * @since      1.2.12
 * @version    2.2.0
 */

// Load plugin settings.
$settings = get_option( MOLONGUI_AUTHORSHIP_BYLINE_SETTINGS );


if ( isset( $settings['byline_automatic_integration'] ) and $settings['byline_automatic_integration'] == 1 )
{
	/**
	 * Overwrite of the WP core function 'get_user_by' located in 'wp-includes/pluggable.php' in order
	 * to enhance themes compatibility.
	 *
	 * There are many themes not using 'the_author' template tag to display the author name. They use
	 * the 'get_user_by' WP core function to get author data. As "guest authors" are not WP users, the
	 * function would collect data about the user who posted the guest post, not data about the guest
	 * author. To display guest author information, 'get_user_by' function is overridden (only on the
	 * frontend).
	 *
	 * @see		https://codex.wordpress.org/Function_Reference/get_user_by
	 * @see		https://core.trac.wordpress.org/browser/tags/4.4.1/src/wp-includes/pluggable.php#L149
	 * @see     https://codex.wordpress.org/Function_Reference/is_admin
	 * @see     http://wordpress.stackexchange.com/a/70677
	 * @see     http://wp-time.com/wordpress-if-login-page/
	 * @see     http://wordpress.stackexchange.com/questions/70676/how-to-check-if-i-am-in-admin-ajax-php
	 *
	 * @param	string          $field The field to retrieve the user with. id | ID | slug | email | login.
	 * @param	int|string      $value A value for $field. A user ID, slug, email address, or login name.
	 * @return  WP_User|false	WP_User object on success, false on failure.
	 * @since	1.2.12
	 * @version 2.0.3
	 */
	if ( !function_exists( 'get_user_by' ) )
	{
		function get_user_by( $field, $value )
		{
	        /**
	         * Mimic the 'get_user_by()' function behaviour.
	         *
	         * If necessary, $user data will be altered later on.
	         *
	         * @since   1.2.12
	         * @version 1.2.12
	         */
			$userdata = WP_User::get_data_by( $field, $value );

			if ( !$userdata ) return false;

			$user = new WP_User;
			$user->init( $userdata );

	        /**
	         * Do not modify $user object on backend pages.
	         *
	         *  is_admin()      Checks if the administration panel is being displayed. Returns false when trying to access 'wp-login.php'.
	         *  $pagenow        Checks if the login page is being displayed.
	         *  DOING_AJAX      Checks if it is an Ajax request.
	         *  wp_doing_ajax() Checks if it is an Ajax request for WP 4.7 and higher.
	         *
	         * @since   2.0.0
	         * @version 2.0.0
	         */
	        global $pagenow;
	        if ( is_admin() or $pagenow == 'wp-login.php' or defined( 'DOING_AJAX' ) /*or wp_doing_ajax()*/ ) return $user;
	//if ( !in_the_loop() ) return $user;
	        /**
	         * Generate a PHP backtrace.
	         *
	         * 'wp_debug_backtrace_summary()' WP function could be used, but it does not allow to limit the number of stack
	         * frames returned, so that results in more memory usage. PHP's base 'debug_backtrace()' is used instead.
	         *
	         * @see http://php.net/manual/en/function.debug-backtrace.php
	         *      https://developer.wordpress.org/reference/functions/wp_debug_backtrace_summary/
	         *
	         * If an empty array is returned, exit (return) to avoid code failure.
	         *
	         * @since   2.0.0
	         * @version 2.0.2
	         */
	        $dbt = debug_backtrace( DEBUG_BACKTRACE_IGNORE_ARGS, 4 );
	        if ( empty( $dbt ) ) return $user;

	        // Debug vars.
	        //molongui_debug( wp_debug_backtrace_summary( null, 0, false ) );
	        //molongui_debug( debug_backtrace( DEBUG_BACKTRACE_IGNORE_ARGS, 5 ) );
	        //molongui_debug( $dbt );

	        /**
	         * Do not modify $user object when running 'molongui_author_list' shortcode (1/2).
	         *
	         * [molongui_author_list] shortcode runs on the frontend to show a list of all authors (registered
	         * and guest) so when this function is called from there, nothing should be altered.
	         *
	         * [molongui_author_list] shortcode calls 'get_all()' function from the 'Author' class, which
	         * calls 'get_data()' function which is the one that finally calls the 'get_user_by()' function.
	         *
	         * @since   1.3.5
	         * @version 2.0.0
	         */
	        if ( isset( $dbt[1]['function'] ) and $dbt[1]['function'] == "get_data" and isset( $dbt[1]['class'] ) and $dbt[1]['class'] == 'Molongui\Authorship\Includes\Author' ) return $user;

	        /**
	         * Do not modify $user object when running 'molongui_author_list' shortcode (2/2).
	         *
	         * [molongui_author_list] shortcode runs on the frontend to show a list of all authors (registered
	         * and guest) so when 'get_author_posts_url()' function is called from there, nothing should be altered.
	         *
	         * [molongui_author_list] shortcode calls 'get_all()' function from the 'Author' class, which
	         * calls 'get_data()' function which is the one that finally calls the 'get_author_posts_url()' function
	         * (there are intermediate calls, though).
	         *
	         * @since   1.3.5
	         * @version 2.0.0
	         */
	        if ( isset( $dbt[3]['function'] ) and $dbt[3]['function'] == "get_data" and isset( $dbt[3]['class'] ) and $dbt[3]['class'] == 'Molongui\Authorship\Includes\Author' ) return $user;

	        /**
	         * Do not modify $user object when being called from 'prepare_name()' which had being called by 'get_by_line()'.
	         *
	         * @since   2.0.2
	         * @version 2.0.2
	         */
	        if ( isset( $dbt[2]['function'] ) and $dbt[2]['function'] == "prepare_name" and isset( $dbt[2]['class'] ) and $dbt[2]['class'] == 'Molongui\Authorship\Includes\Author'
	             and
	             isset( $dbt[3]['function'] ) and $dbt[3]['function'] == "get_by_line" and isset( $dbt[3]['class'] ) and $dbt[3]['class'] == 'Molongui\Authorship\Includes\Author'
	        ) return $user;

	        /**
	         * Do not modify $user object when being called from 'prepare_link()' which had being called by 'prepare_name()' which had being called by 'get_by_line()'.
	         *
	         * @since   2.0.2
	         * @version 2.0.2
	         */
	        if ( isset( $dbt[1]['function'] ) and $dbt[1]['function'] == "prepare_link" and isset( $dbt[1]['class'] ) and $dbt[1]['class'] == 'Molongui\Authorship\Includes\Author'
	             and
	             isset( $dbt[2]['function'] ) and $dbt[2]['function'] == "prepare_name" and isset( $dbt[2]['class'] ) and $dbt[2]['class'] == 'Molongui\Authorship\Includes\Author'
	             and
	             isset( $dbt[3]['function'] ) and $dbt[3]['function'] == "get_by_line"  and isset( $dbt[3]['class'] ) and $dbt[3]['class'] == 'Molongui\Authorship\Includes\Author'
	        ) return $user;

	        /**
	         * wpDiscuz
	         *
	         * @since   2.0.2
	         * @version 2.0.2
	         */
	        if ( ( isset( $dbt[1]['function'] ) and $dbt[1]['function'] == "start_el" and isset( $dbt[1]['class'] ) and $dbt[1]['class'] == 'WpdiscuzWalker' ) ) return $user;

	        /**
	         * $user data alteration wanted.
	         *
	         * If a guest post is being displayed on the frontend, override guest author display information.
	         *
	         * 'is_admin()' function returns false when trying to access wp-login.php so that have to be also
	         * taken into account (thanks to @Trevor).
	         *
	         * @since   1.2.12
	         * @version 2.0.0
	         */
	        if ( !class_exists( 'Molongui\Authorship\Includes\Author' ) ) require_once( MOLONGUI_AUTHORSHIP_DIR . '/includes/class-author.php' );
	        $author = new Author();

	        if ( $author->is_guest( $main_author = $author->get_main_author( get_the_ID() ) ) )
	        {
	            $guest_data          = get_post( $main_author->id );

	            $user->ID            = $main_author->id;
	            $user->display_name  = $guest_data->post_title;
	            $user->user_nicename = $guest_data->post_title;
	            $user->user_url      = ( get_post_meta( $main_author->id, '_molongui_guest_author_link', true ) ? get_post_meta( $main_author->id, '_molongui_guest_author_link', true ) : '#molongui-disabled-link' );

	            /**
	             * Comments' gravatar is gotten by the 'get_avatar_data' function, which uses an email address to retrieve the profile image (gravatar).
	             * That function checks if the comment was made by a registered user, if so, tries to get the User Object and retrieve the associated email
	             * address from there. If an error occurs or the comment was not written by a registered user, then the email field from the Comment Object is
	             * retrieved.
	             *
	             * Being so, we must left untouched the 'user_email' field if the calling function is 'get_avatar_data', so, on guest posts, comments made by
	             * registered authors load the proper gravatar. If not, it would load the gravatar associated with the email address of the guest author.
	             *
	             * See https://developer.wordpress.org/reference/functions/get_avatar_data/
	             *     http://stackoverflow.com/questions/2110732/how-to-get-name-of-calling-function-method-in-php
	             *     http://php.net/manual/es/function.debug-backtrace.php
	             *
	             * @since   1.2.19
	             * @version 2.0.0
	             */
	            if ( isset( $dbt[1]['function'] ) and $dbt[1]['function'] != "get_avatar_data" )
	            {
	                $user->user_email = ( get_post_meta( $main_author->id, '_molongui_guest_author_mail', true ) ? get_post_meta( $main_author->id, '_molongui_guest_author_mail', true ) : '' );
	            }
	        }

			return $user;
		}
	}
}

/**
 * Checks if a Virtual Page is being displayed.
 *
 * @return  boolean
 * @since   1.2.17
 * @version 2.0.0
 */
if ( !function_exists( 'is_virtualpage' ) )
{
	function is_virtualpage()
	{
		global $wp_query;

		return ( ( isset( $wp_query->is_virtual ) ? $wp_query->is_virtual : false ) );
	}
}

// TODO: Move this section to user-archive.

/***********************************************************************************************************************
 * AUTHOR ARCHIVE PAGES
 **********************************************************************************************************************/

/**
 * Modifies main query on author archive pages to retrieve all posts authored and co-authored by the queried wp-user.
 *
 * IMPORTANT: This function works in conjunction with the 'molongui_include_coauthored_posts' function below. That
 *            function is required because WHERE clause is altered after 'pre_get_posts' hook is run.
 *
 * 'posts' table has a 'post_author' field. That field is the one WP uses to know the author of a post, but it is not
 * used nor updated/modified by Molongui Authorship. So, regardless a post might have been assigned to (a) different
 * author(s) with Molongui Authorship, that field doesn't get changed.
 *
 * Molongui Authorship makes use of meta fields called '_molongui_author' to keep track of the authors assigned to a
 * given post.
 *
 * When retrieving posts assigned to a given author, two scenarios must be considered:
 *
 * EXISTING POSTS PRIOR TO PLUGIN INSTALLATION
 * When a post was added before installing Molongui Authorship and has not updated ever since, it doesn't have the meta
 * field used by the plugin. Being that the case, the author of the post must be considered to be the one stored in
 * the 'post_author' field at 'posts' table. So, the condition for the query would be:
 *
 *      '_molongui_author' meta field does not exist
 *      AND
 *      wp_posts.post_author = $given_author_id
 *
 * ADDED/UPDATED POSTS AFTER PLUGIN INSTALLATION
 * When a post has been added/updated after installing Molongui Authorship, it has the required meta fields added by the
 * plugin, so authorship is handled by those fields, not by the 'post_author' field, which must be ignored. If author ID
 * is present in any of the existing '_molongui_author' fields for a given post, then that post is authored or
 * co-authored by the given author. So, the condition for the query would be:
 *
 *      '_molongui_author' meta field exists
 *      AND
 *      '_molongui_author' meta field contains $given_author_id
 *
 * With that condition we are able to include co-authored posts and remove posts created by one user and then assigned
 * to others.
 *
 * @see     http://wordpress.stackexchange.com/a/72126
 *          https://codex.wordpress.org/Plugin_API/Action_Reference/pre_get_posts
 *          http://www.emenia.es/pre-get-posts-en-vez-de-query-posts-wordpress/
 *
 * @param   WP_Query    $query      The WP_Query object passed by reference.
 * @since   1.2.17
 * @version 2.2.0
 */
add_action( 'pre_get_posts', 'molongui_filter_user_posts' );
function molongui_filter_user_posts( $wp_query )
{
	// Do not affect admin screen queries or other than main query.
	if ( is_admin() or !$wp_query->is_main_query() ) return;

	// Modify main query to affect only "author" pages at the frontend.
	if ( $wp_query->is_author() )
	{
		// Get original meta query.
		$meta_query = $wp_query->get( 'meta_query' );

        // Avoid php errors (appearing on php 7.1 and up).
        if ( !is_array( $meta_query ) and empty( $meta_query ) ) $meta_query = array();

        // Get author id being queried.
        $author = get_users( array( 'nicename' => $wp_query->query_vars['author_name'] ) );

		// Add our meta query to the original meta queries.
		$meta_query[] = array
		(
			'relation' => 'OR',
            // Include existing posts prior to plugin installation which have not been updated ever since.
			// This condition is incomplete at this point. It will be finished at 'posts_where' hook.
            array
            (
                'key'     => '_molongui_author',
                'compare' => 'NOT EXISTS',
            ),
            // Include any post with '_molongui_author' meta key assigned to the given author.
            array
			(
                'relation' => 'AND',
                array
                (
                    'key'     => '_molongui_author',
                    'compare' => 'EXISTS',
                ),
                array
                (
                    'key'     => '_molongui_author',
                    'value'   => 'user-'.$author[0]->ID,
                    'compare' => '==',
                ),
            ),
		);
        $wp_query->set( 'meta_query', $meta_query );
	}
}

/**
 * Modifies 'WHERE' clause in WP_QUERY on author archive pages so the all the posts by the queried wp user are retrieved.
 *
 * IMPORTANT: This function works in conjunction with the 'molongui_filter_user_posts' function above.
 *            This function is required because modifying WHERE clause is the only way to get those posts existing
 *            before the plugin was installed that do not have the '_molongui_author' meta key.
 *
 * After modifying the main query on 'pre_get_posts' with 'molongui_filter_guest_posts' function above to include
 * co-authored posts on the results, WP also modifies the main query adding some conditions to it. On author archive
 * pages it adds the condition " AND (wp_posts.post_author = <user_id>)". That prevents co-authored posts by the user
 * from being displayed in the page. Thus, we need to remove that condition added by WP. So we use 'posts_where' hook
 * to be able to modify the query after WP changing it.
 *
 * The WHERE clause, when fetching user = 1, looks like this before being filtered here:
 *
 *      AND (wp_posts.post_author = 1) AND (
 *      (
 *          wp_postmeta.post_id IS NULL
 *          OR
 *          (
 *              mt1.meta_key = '_molongui_author'
 *              AND
 *              ( mt2.meta_key = '_molongui_author' AND mt2.meta_value = 'user-1' )
 *          )
 *      )
 *      ) AND wp_posts.post_type = 'post' AND (wp_posts.post_status = 'publish' OR wp_posts.post_status = 'private')
 *
 * After being filtered, it results in:
 *
 *      AND (
 *      (
 *          ( wp_postmeta.post_id IS NULL AND wp_posts.post_author = 1 )
 *          OR
 *          (
 *              mt1.meta_key = '_molongui_author'
 *              AND
 *              ( mt2.meta_key = '_molongui_author' AND mt2.meta_value = 'user-1' )
 *          )
 *      )
 *      ) AND wp_posts.post_type = 'post' AND (wp_posts.post_status = 'publish' OR wp_posts.post_status = 'private')
 *
 * Code from 'molongui_filter_user_posts' function could be merged here, adding here the meta_queries, but it is safer
 * and recommended to do that on 'pre_get_posts'.
 *
 * @see     https://presscustomizr.com/snippet/three-techniques-to-alter-the-query-in-wordpress/
 *          https://developer.wordpress.org/reference/hooks/posts_where/
 *          https://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
 *
 * @param   string      $where      The WHERE clause of the query.
 * @param   WP_Query    $wp_query   The WP_Query instance (passed by reference).
 * @since   2.0.2
 * @version 2.2.0
 */
add_filter( 'posts_where', 'molongui_remove_author_from_where_clause', 10, 2 );
function molongui_remove_author_from_where_clause( $where, $wp_query )
{
    // Do not affect admin screen queries or other than main query.
    if ( is_admin() or !$wp_query->is_main_query() ) return $where;

    // Modify where clause only on registered wp-user archive pages (frontend).
    if ( $wp_query->is_author() )
    {
        if ( isset( $wp_query->query_vars['author'] ) and !empty( $wp_query->query_vars['author'] ) )
        {
            global $wpdb;

            // Remove the 'post_author' condition that WP adds to the $wp_query by default.
            $where = str_replace( ' AND ('.$wpdb->posts.'.post_author = '.$wp_query->query_vars['author'].')', '', $where );

            // Add the 'post_author' condition to retrieve those posts without '_molongui_author' metakey.
            $where = str_replace( $wpdb->postmeta.'.post_id IS NULL ', '( '.$wpdb->postmeta.'.post_id IS NULL AND '.$wpdb->posts.'.post_author = '.$wp_query->query_vars['author'].' )', $where );
        }
    }

	// Return WHERE clause.
    return $where;
}

/**
 * Makes the archive page retrieve only those (custom) post-types where the plugin is enabled to.
 *
 * @since   3.0.0
 * @version 3.0.0
 */
add_action( 'pre_get_posts', 'molongui_add_custom_post_types' );
function molongui_add_custom_post_types( $wp_query )
{
	// Do not affect admin screen queries or other than main query.
	if ( is_admin() or !$wp_query->is_main_query() ) return;

	// Modify main query only on author (wp-user or guest) archive pages at the frontend.
	if ( $wp_query->is_author() or array_key_exists( 'ma-guest-author', $wp_query->query_vars ) )
	{
		// Add those (custom) post-types where the plugin is enabled on.
		$post_types = molongui_supported_post_types( MOLONGUI_AUTHORSHIP_ID, 'all', false );

		// Update query parameter.
		$wp_query->set( 'post_type', $post_types );
	}
}


		/***********************************************************************************************************************
 * SEARCH PAGE
 **********************************************************************************************************************/

/**
 * Adds 'molongui_guestauthor' CPT to the search results.
 *
 * @param   WP_Query  $query  The WP_Query object passed by reference.
 * @since   1.3.6
 * @version 2.1.0
 */
add_action( 'pre_get_posts', 'molongui_add_guest_authors_to_search' );
function molongui_add_guest_authors_to_search( $wp_query )
{
    // Do not affect admin screen queries or other than main query.
    if ( is_admin() or !$wp_query->is_main_query() ) return;

    // Load plugin settings.
    $settings = get_option( MOLONGUI_AUTHORSHIP_GUEST_SETTINGS );

    // Modify main query to affect only "search" pages at the frontend.
    if ( $wp_query->is_search and
         isset( $settings['include_guests_in_search'] ) and $settings['include_guests_in_search'] )
    {
        // Get searchable post types.
        $post_types = get_post_types( array( 'public' => true, 'exclude_from_search' => false), 'names' );

        // Add our CPT to the original set.
        $post_types[] = 'molongui_guestauthor';

        // Update query parameter.
        $wp_query->set( 'post_type', $post_types );
    }
}

/**
 * Filters the guest author archive page link on search results page.
 *
 * @param   string  $url
 * @param   object  $post
 *
 * @since   2.0.0
 * @version 2.1.0
 */
add_filter( 'post_link', function( $url, $post )
{
    // Get plugin settings.
	$guest_settings    = get_option( MOLONGUI_AUTHORSHIP_GUEST_SETTINGS );
	$archives_settings = get_option( MOLONGUI_AUTHORSHIP_ARCHIVES_SETTINGS );
	$settings          = array_merge( $guest_settings, $archives_settings );

    // Filter link for 'molongui_guestauthor' post types.
    if ( is_search() and $settings['include_guests_in_search'] and $post->post_type == 'molongui_guestauthor' )
    {
        // If premium plugin version and guest archive enabled, modify link.
        if ( molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ) and $settings['guest_archive_enabled'] )
        {
            // Get link modifiers.
            $permalink = ( ( isset( $settings['guest_archive_permalink'] ) and !empty( $settings['guest_archive_permalink'] ) ) ? $settings['guest_archive_permalink'] : '' );
            $slug      = ( ( isset( $settings['guest_archive_base'] ) and !empty( $settings['guest_archive_base'] ) ) ? $settings['guest_archive_base'] : 'author' );

            // Get guest author archive page link.
            $url = home_url( ( !empty( $permalink ) ? $permalink.'/' : '' ) . $slug . '/' . $post->post_name );
        }
        // If no premium version or guest archive disabled, remove link.
        else
        {
            $url = '#molongui-disabled-link';
        }
    }

    // Return data.
    return $url;

}, 10, 2 );

/**
 * Returns user based on given field and value.
 *
 * @param   string  $field  Field to retrieve the user by.
 * @param   string  $value  Value the field must have.
 * @param   string  $type   User type {user|guest}. Defaults to 'user'.
 * @return  WP_User|false	WP_User object on success, false on failure.
 * @since	2.0.0
 * @version 2.0.0
 */
if ( !function_exists( 'molongui_get_user_by' ) )
{
    function molongui_get_user_by( $field, $value, $type = 'user' )
    {
        if ( $type == 'user' )
        {
            $user_query = new WP_User_Query(
                array(
                    'search'        => $value,
                    'search_fields' => array( $field ),
                )
            );
            $user = $user_query->get_results();

            return ( !empty( $user['0'] ) ? $user['0'] : false );
        }

        return false;
    }
}