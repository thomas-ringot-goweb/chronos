<?php

// Deny direct access to this file.
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Plugin configuration.
 *
 * Contains the plugin main configuration parameters and declares them as global constants.
 *
 * This file is loaded like:
 *      require_once( plugin_dir_path( __FILE__ ) . 'config/plugin.php' );
 *
 * @author     Amitzy
 * @package    Authorship
 * @subpackage /config
 * @since      1.0.0
 * @version    3.1.2
 */

/**
 * Constants.
 *
 * Add as many as needed by the plugin.
 *
 *  NAME                // The human readable plugin name without any license nor version reference.
 *  ID                  // The plugin ID.
 *  VERSION             // The plugin version.
 *  DB_VERSION          // The plugin database schema version.
 *  UPGRADABLE          // Whether the plugin has a premium version or not {true | false}.
 *  WEB                 // The plugin web.
 *  DB_SETTINGS         // DB key used to store db version.
 *  BYLINE_SETTINGS     // DB key used to store byline plugin settings.
 *  BOX_SETTINGS        // DB key used to store box plugin settings.
 *  GUEST_SETTINGS      // DB key used to store guest plugin settings.
 *  ARCHIVES_SETTINGS   // DB key used to store archive pages settings.
 *  STRING_SETTINGS     // DB key used to store frontend label customizations.
 *  ADVANCED_SETTINGS   // DB key used to store advanced plugin settings.
 *
 * @var     array
 * @since   1.0.0
 * @version 3.1.2
 */
$config = array(
    'NAME'              => 'Molongui Authorship',
    'ID'                => 'Authorship',
    'VERSION'           => '3.1.2',
    'DB_VERSION'        => '8',
    'UPGRADABLE'        => true,
    'WEB'               => 'https://molongui.amitzy.com/product/authorship',
    'DB_SETTINGS'       => 'molongui_authorship_db_version',
    'BYLINE_SETTINGS'   => 'molongui_authorship_byline',
    'BOX_SETTINGS'      => 'molongui_authorship_box',
    'GUEST_SETTINGS'    => 'molongui_authorship_guest',
    'ARCHIVES_SETTINGS' => 'molongui_authorship_archives',
    'STRING_SETTINGS'   => 'molongui_authorship_strings',
    'ADVANCED_SETTINGS' => 'molongui_authorship_advanced',
    'CONTRIBUTORS_PAGE' => 'molongui_authorship_contributors_page',
);

// DO NOT EDIT FROM HERE ON...

/**
 * Leave without declaring global constants.
 *
 * Useful when we just need to load the $config array.
 *
 * @since   2.0.0
 * @version 2.0.0
 */
if ( isset( $dont_load_constants ) and $dont_load_constants )
{
    unset( $dont_load_constants );
    return;
}

/**
 * Global constant namespace.
 *
 * String added before each constant to avoid collisions in the global PHP namespace.
 *
 * @var     string
 * @since   1.0.0
 * @version 2.0.0
 */
$constant_prefix = 'MOLONGUI_' . strtoupper( str_replace( ' ', '_', $config['ID'] ) ) . '_';

/**
 * Define each constant if not already set
 *
 * @since   1.0.0
 * @version 1.0.0
 */
foreach( $config as $param => $value )
{
    $param = $constant_prefix . $param;
    if( !defined( $param ) ) define( $param, $value );
}

/**
 * Define other required constants.
 *
 * Defines plugin paths and other settings.
 *
 *  DIR             The plugin local path. Something like: /var/www/hmtl/wp-content/plugins/molongui-boilerplate/
 *  URL             The plugin public path. Something like: http://domain.com/wp-content/plugins/molongui-boilerplate
 *  SLUG            The plugin URI slug. Something like: molongui-boilerplate
 *  BASENAME        The plugin basename. Something like: molongui-boilerplate/molongui-boilerplate.php
 *  NAMESPACE       The plugin namespace key. Something like: Boilerplate
 *  TEXTDOMAIN      The plugin text domain used for localisation. If changed, change it also at "/molongui-boilerplate.php".
 *  DB_PREFIX       The plugin DB prefix key. Something like: molongui_boilerplate
 *
 * @since   1.0.0
 * @version 3.1.0
 */

if( !defined( $constant_prefix . 'DIR' ) )        define( $constant_prefix . 'DIR'       , plugin_dir_path( plugin_dir_path( __FILE__ ) ) );
if( !defined( $constant_prefix . 'URL' ) )        define( $constant_prefix . 'URL'       , plugins_url( '/', plugin_dir_path( __FILE__ ) ) );
if( !defined( $constant_prefix . 'SLUG' ) )       define( $constant_prefix . 'SLUG'      , 'molongui-' . strtolower( str_replace( ' ', '-', $config['ID'] ) ) );
if( !defined( $constant_prefix . 'BASENAME' ) )   define( $constant_prefix . 'BASENAME'  , plugin_basename( dirname( plugin_dir_path( __FILE__ ) ) ) . '/molongui-' . strtolower( str_replace( ' ', '-', $config['ID'] ) ) . '.php' );
if( !defined( $constant_prefix . 'NAMESPACE' ) )  define( $constant_prefix . 'NAMESPACE' , str_replace( ' ', '', ucwords( $config['ID'] ) ) );
if( !defined( $constant_prefix . 'TEXTDOMAIN' ) ) define( $constant_prefix . 'TEXTDOMAIN', constant( $constant_prefix . 'SLUG' ) );
if( !defined( $constant_prefix . 'DB_PREFIX' ) )  define( $constant_prefix . 'DB_PREFIX' , 'molongui_' . strtolower( str_replace( ' ', '_', $config['ID'] ) ) );