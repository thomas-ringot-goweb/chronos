<?php

/**
 * Plugin customizer settings.
 *
 * This file contains visual-related plugin settings that can be configured and previewed in the WP Customizer.
 *
 * @author     Amitzy
 * @package    Authorship
 * @subpackage /config
 * @since      2.1.0
 * @version    3.1.0
 */

return array
(
	'add_panel'       => true,
	'id'              => MOLONGUI_AUTHORSHIP_SLUG,
	'title'           => __( 'Molongui Author Box', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
	'description'     => sprintf( '%s%s%s', '<p>', __( 'Customize visual settings to your like.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ), '</p>' ),
	'priority'        => 121,
	'capability'      => 'manage_options',
	'active_callback' => '',
	'sections'        => array
	(
		//--------------------------------------------------------------------------------------------------------------
		// PRE-DEFINED APPEARANCE SCHEMES
		//--------------------------------------------------------------------------------------------------------------
		array
		(
			'id'                 => 'molongui_authorship_scheme',
			'title'              => __( 'Pre-defined appearance schemes', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'description'        => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'display'            => false,
			'priority'           => '',
			'type'               => '',
			'capability'         => 'manage_options',
			'active_callback'    => '',
			'description_hidden' => true,
			'fields'             => array
			(
				// Pre-defined schemes [PREMIUM]
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[scheme]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'scheme-1',
						'transport'            => 'refresh',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Pre-defined schemes', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( 'Choose the pre-defined appearance scheme you would like to use. Once loaded, you can customize any setting you need.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Select_Control',
						'type'            => 'molongui-select',
						'choices'         => array
						(
							'scheme-1' => array
							(
								'label'    => __( 'Scheme 1', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'disabled' => false,
								'premium'  => false,
							),
							'scheme-2' => array
							(
								'label'    => __( 'Scheme 2', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'disabled' => false,
								'premium'  => false,
							),
							'scheme-3' => array
							(
								'label'    => __( 'Scheme 3', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'disabled' => false,
								'premium'  => false,
							),
						),
						'input_attrs'     => array(),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
			),
		),
		//--------------------------------------------------------------------------------------------------------------
		// BOX
		//--------------------------------------------------------------------------------------------------------------
		array
		(
			'id'                 => 'molongui_authorship_box',
			'title'              => __( 'Box', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'description'        => __( 'Customize to your likings the author box that will be displayed on your posts by selecting which layout to use, the position where to display it, and many more color, size and styling options. Make it fit the best with your site. You can preview Premium settings. Upgrade to Premium to unlock them all :)', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'display'            => true,
			'priority'           => '',
			'type'               => '',
			'capability'         => 'manage_options',
			'active_callback'    => '',
			'description_hidden' => true,
			'fields'             => array
			(
/*				// Premium notice.
				array
				(
					'id'      => 'molongui_premium_notice',
					'display' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
					'setting' => array
					(
						'sanitize_callback' => '',
					),
					'control' => array
					(
						'label'           => __( 'Upgrade to unlock all features', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => sprintf( __( 'You can preview premium settings but default values will be saved. Consider upgrading to %sPremium version%s to unlock all features and have premium support.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ), '<a href="https://molongui.amitzy.com/product/authorship" target="_blank">', '</a>' ),
						'priority'        => 1,
						'class'           => 'Molongui_Customize_Notice_Control',
						'type'            => 'notice',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'bg'    => 'orange',
							'color' => 'white',
						),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
				// Heading: Position.
				array
				(
					'id'      => 'heading_box_position',
					'display' => true,
					'setting' => array
					(
						'sanitize_callback' => 'esc_html',
					),
					'control' => array
					(
						'label'           => __( 'Display', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Heading_Control',
						'type'            => 'heading',
						'choices'         => array(),
						'input_attrs'     => array(),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),*/
				// Box layout
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[layout]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'slim',
						'transport'            => 'refresh',
						'validate_callback'    => '',
						'sanitize_callback'    => '',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Layout', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( 'The template used to render the author box. The first two displays the author profile or the related posts in the same space while the third one shows related entries below author profile.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Image_Radio_Button_Control',
						'type'            => 'molongui-image-radio',
						'choices'         => array
						(
							'slim' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/box-layout/box-layout-slim.png',
								'label'   => __( 'Slim', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'tabbed' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/box-layout/box-layout-tabbed.png',
								'label'   => __( 'Tabbed', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'stacked' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/box-layout/box-layout-stacked.png',
								'label'   => __( 'Stacked', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
						),
						'input_attrs'     => array(),
						'allow_addition'  => false,
						'active_callback' => '',
					),
								),
				// Box position
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[position]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'below',
						'transport'            => 'refresh',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Position', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( 'Whether to show the author box above the post content, below or on both places.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Image_Radio_Button_Control',
						'type'            => 'molongui-image-radio',
						'choices'         => array
						(
							'above' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/box-position/author-box-position-above.png',
								'label'   => __( 'Above content', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'below' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/box-position/author-box-position-below.png',
								'label'   => __( 'Below content', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'both' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/box-position/author-box-position-both.png',
								'label'   => __( 'Above and below content', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
						),
						'input_attrs'     => array(),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
				// Box order (addition hook priority) >>> HIDDEN because setting is not taken on 'refresh' unless changes are saved and another refresh is made.
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[order]',
					'display' => false,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 11,
						'transport'            => 'refresh',
						'validate_callback'    => '',
						'sanitize_callback'    => '',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Order', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( 'Author box is added to post content in the configured position. Nonetheless, other plugins may also add their stuff, making the author box appear above/below it. Reduce the number below until the box goes up there where you like or increase it to make it go down.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Number_Control',
						'type'            => 'molongui-number',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'min'     => '1',
							'max'     => '',
							'step'    => '1',
							'premium' => false,
						),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
				// Box margin
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[box_margin]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 0,
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Margin', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( 'Space in pixels to add above and below the author box.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Range_Control',
						'type'            => 'flat',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium' => false,
							'min'     => 0,
							'max'     => 200,
							'step'    => 1,
							'suffix'  => 'px',
						),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
				// Box width
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[box_width]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 100,
						'transport'            => 'postMessage',
						'validate_callback'    => 'molongui_authorship_validate_box_width',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Width', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( 'Amount of space in percentage the author box can take.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Range_Control',
						'type'            => 'flat',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium' => false,
							'min'     => 0,
							'max'     => 100,
							'step'    => 1,
							'suffix'  => '%',
						),
						'allow_addition'  => false,
						'active_callback' => '',
					),
								),
				// Box border
				// @since 3.0.0
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[box_border]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'solid',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Border', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Image_Radio_Button_Control',
						'type'            => 'molongui-image-radio',
						'choices'         => array
						(
							'none' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/box-border/box-border-none.png',
								'label'   => __( 'None', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'all' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/box-border/box-border-all.png',
								'label'   => __( 'All', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'horizontals' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/box-border/box-border-horizontal.png',
								'label'   => __( 'Horizontals', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'verticals' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/box-border/box-border-vertical.png',
								'label'   => __( 'Verticals', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'left' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/box-border/box-border-left.png',
								'label'   => __( 'Left', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'top' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/box-border/box-border-top.png',
								'label'   => __( 'Top', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'right' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/box-border/box-border-right.png',
								'label'   => __( 'Right', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'bottom' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/box-border/box-border-bottom.png',
								'label'   => __( 'Bottom', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
						),
						'input_attrs'     => array(),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
				// Box border style [PREMIUM]
				// @since 2.1.0
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[box_border_style]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'solid',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',//array( 'Molongui\Fw\Customizer\Customizer', 'molongui_sanitize_to_default' ),//'molongui_sanitize_to_default',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Border style', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Image_Radio_Button_Control',
						'type'            => 'molongui-image-radio',
						'choices'         => array
						(
							'solid' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/border-style/border-style-solid.png',
								'label'   => __( 'Solid', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'double' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/border-style/border-style-double.png',
								'label'   => __( 'Double', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'dotted' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/border-style/border-style-dotted.png',
								'label'   => __( 'Dotted', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'dashed' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/border-style/border-style-dashed.png',
								'label'   => __( 'Dashed', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
						),
						'input_attrs'     => array(),
						'allow_addition'  => false,
						'active_callback' => 'molongui_active_box_border_setting',
					),
				),
				// Box border width
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[box_border_width]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 1,
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Border width', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Range_Control',
						'type'            => 'flat',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium' => false,
							'min'     => 1,
							'max'     => 10,
							'step'    => 1,
							'suffix'  => 'px',
						),
						'allow_addition'  => false,
						'active_callback' => 'molongui_active_box_border_setting',
					),
				),
				// Box border color
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[box_border_color]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'inherit',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Border color', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Color_Control',
						'type'            => 'molongui-color',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium' => false,
						),
						'allow_addition'  => false,
						'active_callback' => 'molongui_active_box_border_setting',
					),
				),
				// Box background color
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[box_background]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'inherit',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Background color', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Color_Control',
						'type'            => 'molongui-color',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium' => false,
						),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
				// Box shadow
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[box_shadow]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'none',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Shadow', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Image_Radio_Button_Control',
						'type'            => 'molongui-image-radio',
						'choices'         => array
						(
							'left' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/box-shadow/box-shadow-left.png',
								'label'   => __( 'Left', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'none' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/box-shadow/box-shadow-none.png',
								'label'   => __( 'None', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'right' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/box-shadow/box-shadow-right.png',
								'label'   => __( 'Right', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
						),
						'input_attrs'     => array(),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
			),
		),
		//--------------------------------------------------------------------------------------------------------------
		// HEADLINE
		//--------------------------------------------------------------------------------------------------------------
		array
		(
			'id'                 => 'molongui_authorship_headline',
			'title'              => __( 'Headline', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'description'        => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'display'            => true,
			'priority'           => '',
			'type'               => '',
			'capability'         => 'manage_options',
			'active_callback'    => '',
			'description_hidden' => true,
			'fields'             => array
			(
				// Show headline
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[show_headline]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => '0',
						'transport'            => 'refresh',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Display', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( 'Whether to show a headline above the author box.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Image_Radio_Button_Control',
						'type'            => 'molongui-image-radio',
						'choices'         => array
						(
							'1' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/box-headline/author-box-headline-on.png',
								'label'   => __( 'Show headline', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'0' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/box-headline/author-box-headline-off.png',
								'label'   => __( 'Hide headline', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
						),
						'input_attrs'     => array(),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
				// Headline text
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_strings[headline]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => __( 'About the author', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_string',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Text', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'WP_Customize_Control',
						'type'            => 'text',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium'     => false,
							'placeholder' => __( 'About the author', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						),
						'allow_addition'  => false,
						'active_callback' => 'molongui_active_headline_setting',
					),
				),
/*array
(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[looo]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'inherit',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => '',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Typography', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Typography_Control',
						'type'            => 'molongui-typography',
						'choices'         => array
						(
							'family' => array
							(
								'display' => false,
								'label'   => __( 'Family', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'choices' => array
								(
									'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/box-position/author-box-position-above.png',
									'label'   => __( 'Above content', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
									'premium' => false,
								),
							),
							'size' => array
							(
								'display' => true,
								'label'   => __( 'Size', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'min'     => 8,
								'max'     => '',
								'premium' => false,
							),
							'style' => array
							(
								'display' => true,
								'label'   => __( 'Style', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'choices' => array
								(
									'normal' => array
									(
										'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-normal.png',
										'label'   => __( 'Normal', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
										'premium' => false,
									),
									'bold' => array
									(
										'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-bold.png',
										'label'   => __( 'Bold', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
										'premium' => false,
									),
									'italic' => array
									(
										'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-italic.png',
										'label'   => __( 'Italic', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
										'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
									),
									'underline' => array
									(
										'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-underline.png',
										'label'   => __( 'Underline', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
										'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
									),
									'overline' => array
									(
										'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-overline.png',
										'label'   => __( 'Overline', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
										'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
									),
									'overunderline' => array
									(
										'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-overunderline.png',
										'label'   => __( 'Overline and underline', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
										'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
									),
									'capitalize' => array
									(
										'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-capitalize.png',
										'label'   => __( 'Capitalize', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
										'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
									),
									'uppercase' => array
									(
										'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-uppercase.png',
										'label'   => __( 'Uppercase', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
										'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
									),
									'lowercase' => array
									(
										'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-lowercase.png',
										'label'   => __( 'Lowercase', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
										'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
									),
								),
							),
							'align' => array
							(
								'display' => true,
								'label'   => __( 'Align', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'choices' => array
								(
									'left' => array
									(
										'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/text-align/text-align-left.png',
										'label'   => __( 'Left', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
										'premium' => false,
									),
									'center' => array
									(
										'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/text-align/text-align-center.png',
										'label'   => __( 'Center', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
										'premium' => false,
									),
									'right' => array
									(
										'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/text-align/text-align-right.png',
										'label'   => __( 'Right', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
										'premium' => false,
									),
									'justify' => array
									(
										'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/text-align/text-align-justify.png',
										'label'   => __( 'Justify', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
										'premium' => false,
									),
								),
							),
							'color' => array
							(
								'display' => true,
								'label'   => __( 'Color', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
						),
						'input_attrs'     => array
						(
							'premium' => false,
						),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),*/
				// Typography label (group of compact settings)
				array
				(
					'id'      => 'molongui_headline_typography_settings',
					'display' => true,
					'setting' => array
					(
						'sanitize_callback' => 'esc_html',
					),
					'control' => array
					(
						'label'           => __( 'Typography', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Group_Label_Control',
						'type'            => 'molongui-compact-group-label',
						'active_callback' => 'molongui_active_headline_setting',
						'input_attrs'     => array(),
						'choices'         => array(),
					),
				),
				// Headline text size
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[headline_text_size]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'normal',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Size', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Range_Control',
						'type'            => 'molongui-compact-range-flat',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium' => false,
							'min'     => 8,
							'max'     => 100,
							'step'    => 1,
							'suffix'  => 'px',
						),
						'allow_addition'  => false,
						'active_callback' => 'molongui_active_headline_setting',
					),
				),
				// Headline text style [PREMIUM]
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[headline_text_style]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => '',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Style', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Image_Checkbox_Button_Control',
						'type'            => 'molongui-compact-image-checkbox',
						'choices'         => array
						(
							'normal' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-normal.png',
								'label'   => __( 'Normal', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'bold' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-bold.png',
								'label'   => __( 'Bold', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'italic' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-italic.png',
								'label'   => __( 'Italic', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'underline' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-underline.png',
								'label'   => __( 'Underline', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'overline' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-overline.png',
								'label'   => __( 'Overline', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'overunderline' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-overunderline.png',
								'label'   => __( 'Overline and underline', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
						),
						'input_attrs'     => array( 'compact' => true ),
						'allow_addition'  => true,
						'active_callback' => 'molongui_active_headline_setting',
					),
				),
				// Headline text case
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[headline_text_case]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'none',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Case', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Image_Radio_Button_Control',
						'type'            => 'molongui-compact-image-radio',
						'choices'         => array
						(
							'none' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-case/font-case-none.png',
								'label'   => __( 'Leave as is', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'capitalize' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-case/font-case-capitalize.png',
								'label'   => __( 'Capitalize', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'uppercase' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-case/font-case-uppercase.png',
								'label'   => __( 'Uppercase', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'lowercase' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-case/font-case-lowercase.png',
								'label'   => __( 'Lowercase', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
						),
						'input_attrs'     => array(),
						'allow_addition'  => false,
						'active_callback' => 'molongui_active_headline_setting',
					),
				),
				// Headline text align
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[headline_text_align]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'left',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Align', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Image_Radio_Button_Control',
						'type'            => 'molongui-compact-image-radio',
						'choices'         => array
						(
							'left' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/text-align/text-align-left.png',
								'label'   => __( 'Left', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'center' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/text-align/text-align-center.png',
								'label'   => __( 'Center', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'right' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/text-align/text-align-right.png',
								'label'   => __( 'Right', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'justify' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/text-align/text-align-justify.png',
								'label'   => __( 'Justify', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
						),
						'input_attrs'     => array(),
						'allow_addition'  => false,
						'active_callback' => 'molongui_active_headline_setting',
					),
				),
				// Headline font color [PREMIUM]
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[headline_text_color]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'inherit',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Color', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Color_Control',
						'type'            => 'molongui-compact-color',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
						),
						'allow_addition'  => false,
						'active_callback' => 'molongui_active_headline_setting',
					),
				),
			),
		),
		//--------------------------------------------------------------------------------------------------------------
		// TABS
		//--------------------------------------------------------------------------------------------------------------
		array
		(
			'id'                 => 'molongui_authorship_tabs',
			'title'              => __( 'Tabs', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'description'        => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'display'            => true,
			'priority'           => '',
			'type'               => '',
			'capability'         => 'manage_options',
			'active_callback'    => 'molongui_active_box_tabbed_layout_setting',
			'description_hidden' => true,
			'fields'             => array
			(
				// Tabs position [PREMIUM]
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[tabs_position]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'top-full',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Position', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( 'Where to display the tabs.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Image_Radio_Button_Control',
						'type'            => 'molongui-image-radio',
						'choices'         => array
						(
							'top-full' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/box-tabs/box-tabs-position-top-full.png',
								'label'   => __( 'Top full width', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'top-left' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/box-tabs/box-tabs-position-top-left.png',
								'label'   => __( 'Top left', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'top-center' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/box-tabs/box-tabs-position-top-center.png',
								'label'   => __( 'Top center', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'top-right' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/box-tabs/box-tabs-position-top-right.png',
								'label'   => __( 'Top right', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
/*							'bottom-full' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/box-headline/author-box-headline-off.png',
								'label'   => __( 'Bottom full width', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'bottom-left' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/box-headline/author-box-headline-on.png',
								'label'   => __( 'Bottom left', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'bottom-center' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/box-headline/author-box-headline-off.png',
								'label'   => __( 'Bottom center', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'bottom-right' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/box-headline/author-box-headline-off.png',
								'label'   => __( 'Bottom right', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),*/
						),
						'input_attrs'     => array(),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
				// Tabs color [PREMIUM]
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[tabs_color]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'inherit',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Tabs color', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Color_Control',
						'type'            => 'molongui-color',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
						),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
				// Tabs active tab color [PREMIUM]
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[tabs_active_color]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'inherit',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Active tab color', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Color_Control',
						'type'            => 'molongui-color',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
						),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
				// Tabs background [PREMIUM]
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[tabs_background]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'inherit',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Background color', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Color_Control',
						'type'            => 'molongui-color',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
						),
						'allow_addition'  => false,
						'active_callback' => 'molongui_active_tabs_background_setting',
					),
				),
				// Tabs border [PREMIUM]
/*				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[tabs_border]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'around',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Border', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Image_Radio_Button_Control',
						'type'            => 'molongui-image-radio',
						'choices'         => array
						(
							'none' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/border-style/border-style-none.png',
								'label'   => __( 'None', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'around' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/border-style/border-style-none.png',
								'label'   => __( 'Around', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'top' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/border-style/border-style-solid.png',
								'label'   => __( 'Top', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'topline' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/border-style/border-style-double.png',
								'label'   => __( 'Top + line', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'bottom' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/border-style/border-style-dotted.png',
								'label'   => __( 'Bottom', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
						),
						'input_attrs'     => array(),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),*/
				// Tabs border style [PREMIUM]
/*				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[tabs_border_style]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'solid',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Border style', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Image_Radio_Button_Control',
						'type'            => 'molongui-image-radio',
						'choices'         => array
						(
							'solid' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/border-style/border-style-solid.png',
								'label'   => __( 'Solid', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'double' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/border-style/border-style-double.png',
								'label'   => __( 'Double', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'dotted' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/border-style/border-style-dotted.png',
								'label'   => __( 'Dotted', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'dashed' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/border-style/border-style-dashed.png',
								'label'   => __( 'Dashed', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
						),
						'input_attrs'     => array(),
						'allow_addition'  => false,
						'active_callback' => 'molongui_active_tabs_border_style_width_setting',
					),
				),*/
				// Tabs border width
/*				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[tabs_border_width]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 1,
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Border width', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Range_Control',
						'type'            => 'flat',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium' => false,
							'min'     => 1,
							'max'     => 10,
							'step'    => 1,
							'suffix'  => 'px',
						),
						'allow_addition'  => false,
						'active_callback' => 'molongui_active_tabs_border_style_width_setting',
					),
				),*/
				// Tabs border color
/*				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[tabs_border_color]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'inherit',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Border color', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Color_Control',
						'type'            => 'molongui-color',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium' => false,
						),
						'allow_addition'  => false,
						'active_callback' => 'molongui_active_tabs_border_color_setting',
					),
				),*/
				// Tabs text color [PREMIUM]
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[tabs_text_color]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'inherit',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Text color', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Color_Control',
						'type'            => 'molongui-color',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
						),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
				// "About the author" label
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_strings[about_the_author]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => __( 'About the author', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_string',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( '"About the author" label', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( 'Text to show as author bio tab label.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'WP_Customize_Control',
						'type'            => 'text',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium'     => false,
							'placeholder' => __( 'About the author', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
				// "Related posts" label
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_strings[related_posts]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => __( 'Related posts', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_string',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( '"Related posts" label', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( 'Text to show as author related posts tab label', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'WP_Customize_Control',
						'type'            => 'text',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium'     => false,
							'placeholder' => __( 'Related posts', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
			),
		),
		//--------------------------------------------------------------------------------------------------------------
		// PROFILE TEMPLATE
		//--------------------------------------------------------------------------------------------------------------
		array
		(
			'id'                 => 'molongui_authorship_profile',
			'title'              => __( 'Template', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'description'        => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'display'            => true,
			'priority'           => '',
			'type'               => '',
			'capability'         => 'manage_options',
			'active_callback'    => '',
			'description_hidden' => true,
			'fields'             => array
			(
				// Profile layout [PREMIUM]
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[profile_layout]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'layout-1',
						'transport'            => 'refresh',
						'validate_callback'    => '',
						'sanitize_callback'    => '',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Profile template', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( 'The template to be used to render the author profile section.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Image_Radio_Button_Control',
						'type'            => 'molongui-image-radio',
						'choices'         => array
						(
							'layout-1' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/profile-template/profile-template-1.png',
								'label'   => __( 'Template 1', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'layout-2' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/profile-template/profile-template-2.png',
								'label'   => __( 'Template 2', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'layout-3' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/profile-template/profile-template-3.png',
								'label'   => __( 'Template 3', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'layout-4' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/profile-template/profile-template-4.png',
								'label'   => __( 'Template 4', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'layout-5' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/profile-template/profile-template-5.png',
								'label'   => __( 'Template 5', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'layout-6' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/profile-template/profile-template-6.png',
								'label'   => __( 'Template 6', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'layout-7' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/profile-template/profile-template-7.png',
								'label'   => __( 'Template 7', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'layout-8' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/profile-template/profile-template-8.png',
								'label'   => __( 'Template 8', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
						),
						'input_attrs'     => array(),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
				// Profile title
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_strings[profile_title]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => __( 'Author profile', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_string',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Section label', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'WP_Customize_Control',
						'type'            => 'text',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium'     => false,
							'placeholder' => __( 'Author profile', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						),
						'allow_addition'  => false,
						'active_callback' => 'molongui_active_box_stacked_layout_setting',
					),
				),
/*				// Heading: Bottom bar.
								array
								(
									'id'      => 'heading_bottom_bar',
									'display' => true,
									'setting' => array
									(
										'sanitize_callback'    => 'esc_html',
									),
									'control' => array
									(
										'label'           => __( 'Bottom bar', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
										'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
										'priority'        => 10,
										'class'           => 'Molongui_Customize_Heading_Control',
										'type'            => 'heading',
										'input_attrs'     => array(),
										'active_callback' => 'molongui_active_ribbon_layout_setting',
									),
								),*/
				// Bottom background color [PREMIUM]
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[bottom_background_color]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'inherit',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Bottom background color', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( 'The color used to fill the background of the bottom section on a "ribbon" layout.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Color_Control',
						'type'            => 'molongui-color',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
						),
						'allow_addition'  => false,
						'active_callback' => 'molongui_active_ribbon_layout_setting',
					),
				),
				// Bottom border style [PREMIUM]
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[bottom_border_style]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'solid',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Bottom border style', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Image_Radio_Button_Control',
						'type'            => 'molongui-image-radio',
						'choices'         => array
						(
							'none' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/border-style/border-style-none.png',
								'label'   => __( 'None', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'solid' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/border-style/border-style-solid.png',
								'label'   => __( 'Solid', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'double' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/border-style/border-style-double.png',
								'label'   => __( 'Double', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'dotted' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/border-style/border-style-dotted.png',
								'label'   => __( 'Dotted', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'dashed' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/border-style/border-style-dashed.png',
								'label'   => __( 'Dashed', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
						),
						'input_attrs'     => array(),
						'allow_addition'  => false,
						'active_callback' => 'molongui_active_ribbon_layout_setting',
					),
				),
				// Bottom border width [PREMIUM]
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[bottom_border_width]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 1,
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Bottom border width', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( 'Width of the border to add at the top of bottom section on a "ribbon" layout.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Range_Control',
						'type'            => 'flat',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							'min'     => 1,
							'max'     => 10,
							'step'    => 1,
							'suffix'  => 'px',
						),
						'allow_addition'  => false,
						'active_callback' => function( $control ) { return ( molongui_active_ribbon_layout_setting( $control ) and molongui_active_ribbon_border_setting( $control ) ? true : false ); },
					),
				),
				// Bottom border color [PREMIUM]
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[bottom_border_color]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'transparent',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Bottom border color', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Color_Control',
						'type'            => 'molongui-color',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
						),
						'allow_addition'  => false,
						'active_callback' => function( $control ) { return ( molongui_active_ribbon_layout_setting( $control ) and molongui_active_ribbon_border_setting( $control ) ? true : false ); },
					),
				),
			),
		),
		//--------------------------------------------------------------------------------------------------------------
		// AVATAR
		//--------------------------------------------------------------------------------------------------------------
		array
		(
			'id'                 => 'molongui_authorship_avatar',
			'title'              => __( 'Avatar', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'description'        => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'display'            => true,
			'priority'           => '',
			'type'               => '',
			'capability'         => 'manage_options',
			'active_callback'    => '',
			'description_hidden' => true,
			'fields'             => array
			(
				// Avatar style
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[avatar_style]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'none',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Shape', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( 'Whether and how to shape the author avatar.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Image_Radio_Button_Control',
						'type'            => 'molongui-image-radio',
						'choices'         => array
						(
							'none' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/avatar-style/avatar-style-none.png',
								'label'   => __( 'None', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'rounded' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/avatar-style/avatar-style-rounded.png',
								'label'   => __( 'Rounded', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'circled' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/avatar-style/avatar-style-circled.png',
								'label'   => __( 'Circled', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
						),
						'input_attrs'     => array(),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
				// Avatar border style [PREMIUM]
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[avatar_border_style]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'solid',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Border style', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Image_Radio_Button_Control',
						'type'            => 'molongui-image-radio',
						'choices'         => array
						(
							'none' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/border-style/border-style-none.png',
								'label'   => __( 'None', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'solid' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/border-style/border-style-solid.png',
								'label'   => __( 'Solid', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'double' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/border-style/border-style-double.png',
								'label'   => __( 'Double', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'dotted' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/border-style/border-style-dotted.png',
								'label'   => __( 'Dotted', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'dashed' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/border-style/border-style-dashed.png',
								'label'   => __( 'Dashed', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
						),
						'input_attrs'     => array(),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
				// Avatar border width
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[avatar_border_width]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 1,
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Border width', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Range_Control',
						'type'            => 'flat',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium' => false,
							'min'     => 1,
							'max'     => 10,
							'step'    => 1,
							'suffix'  => 'px',
						),
						'allow_addition'  => false,
						'active_callback' => 'molongui_active_avatar_border_width_setting',
					),
				),
				// Avatar border color
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[avatar_border_color]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'inherit',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Border color', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Color_Control',
						'type'            => 'molongui-color',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium' => false,
						),
						'allow_addition'  => false,
						'active_callback' => 'molongui_active_avatar_border_color_setting',
					),
				),
				// Avatar default image [PREMIUM]
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[avatar_default_img]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'blank',
						'transport'            => 'refresh',
						'validate_callback'    => '',
						'sanitize_callback'    => '',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Default image', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( 'Avatar to show if none uploaded and no gravatar configured.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Image_Radio_Button_Control',
						'type'            => 'molongui-image-radio',
						'choices'         => array
						(
							'none' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/avatar-default/avatar-default-image-none.png',
								'label'   => __( 'None', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'blank' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/avatar-default/avatar-default-image-blank.png',
								'label'   => __( 'Blank', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'mm' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/avatar-default/avatar-default-image-mm.png',
								'label'   => __( 'Mistery man', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'acronym' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/avatar-default/avatar-default-image-acronym.png',
								'label'   => __( 'Author acronym', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
						),
						'input_attrs'     => array(),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
				// Avatar acronym font color [PREMIUM]
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[acronym_text_color]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'inherit',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( ' Acronym font color', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Color_Control',
						'type'            => 'molongui-color',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
						),
						'allow_addition'  => false,
						'active_callback' => 'molongui_active_acronym_setting',
					),
				),
				// Avatar acronym background color [PREMIUM]
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[acronym_bg_color]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'inherit',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Acronym background color', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Color_Control',
						'type'            => 'molongui-color',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
						),
						'allow_addition'  => false,
						'active_callback' => 'molongui_active_acronym_setting',
					),
				),
			),
		),
		//--------------------------------------------------------------------------------------------------------------
		// NAME
		//--------------------------------------------------------------------------------------------------------------
		array
		(
			'id'                 => 'molongui_authorship_name',
			'title'              => __( 'Name', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'description'        => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'display'            => true,
			'priority'           => '',
			'type'               => '',
			'capability'         => 'manage_options',
			'active_callback'    => '',
			'description_hidden' => true,
			'fields'             => array
			(
				// Typography label (group of compact settings)
				array
				(
					'id'      => 'molongui_name_typography_settings',
					'display' => true,
					'setting' => array
					(
						'sanitize_callback' => 'esc_html',
					),
					'control' => array
					(
						'label'           => __( 'Typography', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Group_Label_Control',
						'type'            => 'molongui-compact-group-label',
						'active_callback' => '',
						'input_attrs'     => array(),
						'choices'         => array(),
					),
				),
				// Author name font size
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[name_text_size]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'normal',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Size', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Range_Control',
						'type'            => 'molongui-compact-range-flat',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium' => false,
							'min'     => 8,
							'max'     => 100,
							'step'    => 1,
							'suffix'  => 'px',
						),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
				// Author name text style [PREMIUM]
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[name_text_style]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => '',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Style', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Image_Checkbox_Button_Control',
						'type'            => 'molongui-compact-image-checkbox',
						'choices'         => array
						(
							'normal' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-normal.png',
								'label'   => __( 'Normal', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'bold' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-bold.png',
								'label'   => __( 'Bold', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'italic' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-italic.png',
								'label'   => __( 'Italic', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'underline' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-underline.png',
								'label'   => __( 'Underline', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'overline' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-overline.png',
								'label'   => __( 'Overline', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'overunderline' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-overunderline.png',
								'label'   => __( 'Overline and underline', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
						),
						'input_attrs'     => array(),
						'allow_addition'  => true,
						'active_callback' => '',
					),
				),
				// Author name text case [PREMIUM]
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[name_text_case]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'none',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Case', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Image_Radio_Button_Control',
						'type'            => 'molongui-compact-image-radio',
						'choices'         => array
						(
							'none' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-case/font-case-none.png',
								'label'   => __( 'Leave as is', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'capitalize' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-case/font-case-capitalize.png',
								'label'   => __( 'Capitalize', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'uppercase' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-case/font-case-uppercase.png',
								'label'   => __( 'Uppercase', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'lowercase' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-case/font-case-lowercase.png',
								'label'   => __( 'Lowercase', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
						),
						'input_attrs'     => array(),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
				// Author name text align
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[name_text_align]',
					'display' => false,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'left',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Align', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Image_Radio_Button_Control',
						'type'            => 'molongui-compact-image-radio',
						'choices'         => array
						(
							'left' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/text-align/text-align-left.png',
								'label'   => __( 'Left', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'center' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/text-align/text-align-center.png',
								'label'   => __( 'Center', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'right' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/text-align/text-align-right.png',
								'label'   => __( 'Right', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'justify' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/text-align/text-align-justify.png',
								'label'   => __( 'Justify', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
						),
						'input_attrs'     => array(),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
				// Author name font color
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[name_text_color]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'inherit',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Color', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Color_Control',
						'type'            => 'molongui-compact-color',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium' => false,
						),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
				// Link to author archive page
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[name_link_to_archive]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'link',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Link to archive', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => ( !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ) ? __( "Whether to make the author name link to the author's archive page.", MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) : __( "Whether to make the author name link to the author's archive page. Regardless of this setting being enabled, the author name might not become a link. i.e. When author archive pages are disabled.", MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Text_Radio_Button_Control',
						'type'            => 'molongui-text-radio',
						'allow_addition'  => false,
						'active_callback' => '',
						'input_attrs'     => array(),
						'choices'         => array
						(
							'1' => array
							(
								'label'   => __( 'Link', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'0' => array
							(
								'label'   => __( 'No link',  MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
						),
					),
				),
				// Remove inherited underline [PREMIUM]
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[name_inherited_underline]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'keep',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Remove inherited underline', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( 'Whether to (try to) remove the underline added by your theme or any other third plugin.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Text_Radio_Button_Control',
						'type'            => 'molongui-text-radio',
						'allow_addition'  => false,
						'active_callback' => '',
						'input_attrs'     => array(),
						'choices'         => array
						(
							'keep' => array
							(
								'label'   => __( 'Keep as is', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'remove' => array
							(
								'label'   => __( 'Remove it',  MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
						),
					),
				),
			),
		),
		//--------------------------------------------------------------------------------------------------------------
		// META
		//--------------------------------------------------------------------------------------------------------------
		array
		(
			'id'                 => 'molongui_authorship_meta',
			'title'              => __( 'Metadata', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'description'        => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'display'            => true,
			'priority'           => '',
			'type'               => '',
			'capability'         => 'manage_options',
			'active_callback'    => '',
			'description_hidden' => true,
			'fields'             => array
			(
				// Typography label (group of compact settings)
				array
				(
					 'id'      => 'molongui_meta_typography_settings',
					 'display' => true,
					 'setting' => array
					 (
						 'sanitize_callback' => 'esc_html',
					 ),
					 'control' => array
					 (
						 'label'           => __( 'Typography', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						 'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						 'priority'        => 10,
						 'class'           => 'Molongui_Customize_Group_Label_Control',
						 'type'            => 'molongui-compact-group-label',
						 'active_callback' => '',
						 'input_attrs'     => array(),
						 'choices'         => array(),
					 ),
				 ),
				// Meta font size
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[meta_text_size]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'normal',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Size', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Range_Control',
						'type'            => 'molongui-compact-range-flat',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium' => false,
							'min'     => 8,
							'max'     => 100,
							'step'    => 1,
							'suffix'  => 'px',
						),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
				// Meta text style [PREMIUM]
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[meta_text_style]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => '',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Style', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Image_Checkbox_Button_Control',
						'type'            => 'molongui-compact-image-checkbox',
						'choices'         => array
						(
							'normal' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-normal.png',
								'label'   => __( 'Normal', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'bold' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-bold.png',
								'label'   => __( 'Bold', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'italic' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-italic.png',
								'label'   => __( 'Italic', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'underline' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-underline.png',
								'label'   => __( 'Underline', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'overline' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-overline.png',
								'label'   => __( 'Overline', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'overunderline' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-overunderline.png',
								'label'   => __( 'Overline and underline', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
						),
						'input_attrs'     => array(),
						'allow_addition'  => true,
						'active_callback' => '',
					),
				),
				// Meta text case [PREMIUM]
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[meta_text_case]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'none',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Case', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Image_Radio_Button_Control',
						'type'            => 'molongui-compact-image-radio',
						'choices'         => array
						(
							'none' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-case/font-case-none.png',
								'label'   => __( 'Leave as is', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'capitalize' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-case/font-case-capitalize.png',
								'label'   => __( 'Capitalize', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'uppercase' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-case/font-case-uppercase.png',
								'label'   => __( 'Uppercase', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'lowercase' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-case/font-case-lowercase.png',
								'label'   => __( 'Lowercase', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
						),
						'input_attrs'     => array(),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
				// Meta text align
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[meta_text_align]',
					'display' => false,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'left',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Align', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Image_Radio_Button_Control',
						'type'            => 'molongui-compact-image-radio',
						'choices'         => array
						(
							'left' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/text-align/text-align-left.png',
								'label'   => __( 'Left', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'center' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/text-align/text-align-center.png',
								'label'   => __( 'Center', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'right' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/text-align/text-align-right.png',
								'label'   => __( 'Right', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'justify' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/text-align/text-align-justify.png',
								'label'   => __( 'Justify', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
						),
						'input_attrs'     => array(),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
				// Meta font color [PREMIUM]
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[meta_text_color]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'inherit',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Color', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Color_Control',
						'type'            => 'molongui-compact-color',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
						),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
				// "at" text
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_strings[at]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => __( 'at', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_string',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( '"at" label', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( 'Text to show between author\'s job position and company.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'WP_Customize_Control',
						'type'            => 'text',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium'     => false,
							'placeholder' => __( 'at', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
				// "Web" text
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_strings[web]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => __( 'Website', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_string',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( '"Website" label', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( 'Text to show as link name to author\'s personal website.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'WP_Customize_Control',
						'type'            => 'text',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium'     => false,
							'placeholder' => __( 'Website', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
				// "More posts" text
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_strings[more_posts]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => __( '+ posts', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_string',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( '"+ posts" label', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( 'Text to show as toggle button to display author\'s related posts when displaying author bio.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'WP_Customize_Control',
						'type'            => 'text',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium'     => false,
							'placeholder' => __( '+ posts', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						),
						'allow_addition'  => false,
						'active_callback' => 'molongui_active_box_slim_layout_setting',
					),
				),
				// "Biography" text
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_strings[bio]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => __( 'Bio', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_string',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( '"Bio" label', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( 'Text to show as toggle button to display author\'s bio when displaying related posts.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'WP_Customize_Control',
						'type'            => 'text',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium'     => false,
							'placeholder' => __( 'Bio', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						),
						'allow_addition'  => false,
						'active_callback' => 'molongui_active_box_slim_layout_setting',
					),
				),
				// Meta separator character [PREMIUM]
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[meta_separator]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => '|',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Separator', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( 'Character used to separate author meta data information', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Select_Control',
						'type'            => 'select',
						'choices'         => array
						(
							// https://www.w3schools.com/charsets/ref_utf_punctuation.asp
							// pipe
							'|' => array
							(
								'label'    => __( '|' ),
								'disabled' => false,
								'premium'  => false,
							),
							// 2xpipe
							'||' => array
							(
								'label'    => __( '||' ),
								'disabled' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
								'premium'  => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							// slash
							'/' => array
							(
								'label'    => __( '/' ),
								'disabled' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
								'premium'  => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							// 2xslash
							'//' => array
							(
								'label'    => __( '//' ),
								'disabled' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
								'premium'  => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							// hyphen
							'-' => array
							(
								'label'    => __( '–' ),
								'disabled' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
								'premium'  => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							// 2xhyphen
							'--' => array
							(
								'label'    => __( '--' ),
								'disabled' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
								'premium'  => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							// ndash
							'–' => array
							(
								'label'    => __( '–' ),
								'disabled' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
								'premium'  => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							// sdot
							'·' => array
							(
								'label'    => __( '·' ),
								'disabled' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
								'premium'  => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							// bullet
							'•' => array
							(
								'label'    => __( '•' ),
								'disabled' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
								'premium'  => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							// rsaquo
							'>' => array
							(
								'label'    => __( '>' ),
								'disabled' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
								'premium'  => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							// 2xrsaquo
							'>>' => array
							(
								'label'    => __( '>>' ),
								'disabled' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
								'premium'  => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							// tilde
							'~' => array
							(
								'label'    => __( '~' ),
								'disabled' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
								'premium'  => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							// asterisk
							'*' => array
							(
								'label'    => __( '*' ),
								'disabled' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
								'premium'  => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							// arrows
							'⇄' => array
							(
								'label'    => __( '⇄' ),
								'disabled' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
								'premium'  => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							// uptack
							'⊥' => array
							(
								'label'    => __( '⊥' ),
								'disabled' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
								'premium'  => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							// diamond
							'⋄' => array
							(
								'label'    => __( '⋄' ),
								'disabled' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
								'premium'  => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							// Line break
							'<br>' => array
							(
								'label'    => __( 'Line break', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'disabled' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
								'premium'  => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
						),
						'input_attrs'     => array
						(
							'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
						),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
			),
		),
		//--------------------------------------------------------------------------------------------------------------
		// BIOGRAPHY
		//--------------------------------------------------------------------------------------------------------------
		array
		(
			'id'                 => 'molongui_authorship_bio',
			'title'              => __( 'Biography', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'description'        => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'display'            => true,
			'priority'           => '',
			'type'               => '',
			'capability'         => 'manage_options',
			'active_callback'    => '',
			'description_hidden' => true,
			'fields'             => array
			(
				// Typography label (group of compact settings)
				array
				(
					'id'      => 'molongui_bio_typography_settings',
					'display' => true,
					'setting' => array
					(
						'sanitize_callback' => 'esc_html',
					),
					'control' => array
					(
						'label'           => __( 'Typography', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Group_Label_Control',
						'type'            => 'molongui-compact-group-label',
						'active_callback' => '',
						'input_attrs'     => array(),
						'choices'         => array(),
					),
				),
				// Bio font size
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[bio_text_size]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'normal',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Size', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Range_Control',
						'type'            => 'molongui-compact-range-flat',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium' => false,
							'min'     => 8,
							'max'     => 100,
							'step'    => 1,
							'suffix'  => 'px',
						),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
				// Bio text style [PREMIUM]
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[bio_text_style]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => '',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Style', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Image_Checkbox_Button_Control',
						'type'            => 'molongui-compact-image-checkbox',
						'choices'         => array
						(
							'normal' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-normal.png',
								'label'   => __( 'Normal', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'bold' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-bold.png',
								'label'   => __( 'Bold', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'italic' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-italic.png',
								'label'   => __( 'Italic', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'underline' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-underline.png',
								'label'   => __( 'Underline', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
						),
						'input_attrs'     => array(),
						'allow_addition'  => true,
						'active_callback' => '',
					),
				),
				// Bio text case
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[bio_text_case]',
					'display' => false,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'none',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Case', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Image_Radio_Button_Control',
						'type'            => 'molongui-compact-image-radio',
						'choices'         => array
						(
							'none' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-case/font-case-none.png',
								'label'   => __( 'Leave as is', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'capitalize' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-case/font-case-capitalize.png',
								'label'   => __( 'Capitalize', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'uppercase' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-case/font-case-uppercase.png',
								'label'   => __( 'Uppercase', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'lowercase' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-case/font-case-lowercase.png',
								'label'   => __( 'Lowercase', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
						),
						'input_attrs'     => array(),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
				// Bio text align [PREMIUM]
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[bio_text_align]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'justify',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Align', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Image_Radio_Button_Control',
						'type'            => 'molongui-compact-image-radio',
						'choices'         => array
						(
							'left' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/text-align/text-align-left.png',
								'label'   => __( 'Left', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'center' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/text-align/text-align-center.png',
								'label'   => __( 'Center', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'right' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/text-align/text-align-right.png',
								'label'   => __( 'Right', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'justify' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/text-align/text-align-justify.png',
								'label'   => __( 'Justify', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
						),
						'input_attrs'     => array(),
						'allow_addition'  => true,
						'active_callback' => '',
					),
				),
				// Bio font color [PREMIUM]
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[bio_text_color]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'inherit',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Color', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Color_Control',
						'type'            => 'molongui-compact-color',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
						),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
			),
		),
		//--------------------------------------------------------------------------------------------------------------
		// SOCIAL ICONS
		//--------------------------------------------------------------------------------------------------------------
		array
		(
			'id'                 => 'molongui_authorship_icons',
			'title'              => __( 'Social icons', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'description'        => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'display'            => true,
			'priority'           => '',
			'type'               => '',
			'capability'         => 'manage_options',
			'active_callback'    => '',
			'description_hidden' => true,
			'fields'             => array
			(
				// Show social icons
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[show_icons]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'default',
						'transport'            => 'refresh',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Social icons', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( 'Whether to show social media icons.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Text_Radio_Button_Control',
						'type'            => 'molongui-text-radio',
						'choices'         => array
						(
							'1' => array
							(
								'label'   => __( 'Show', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'0' => array
							(
								'label'   => __( 'Hide', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
						),
						'input_attrs'     => array
						(
							'premium' => false,
						),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
				// Social media icons style [PREMIUM]
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[icons_style]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'default',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Style', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Image_Radio_Button_Control',
						'type'            => 'molongui-image-radio',
						'choices'         => array
						(
							'default' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/icons-style/icons-style-default.png',
								'label'   => __( 'Default', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'squared' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/icons-style/icons-style-squared.png',
								'label'   => __( 'Squared', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'circled' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/icons-style/icons-style-circled.png',
								'label'   => __( 'Circled', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'boxed' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/icons-style/icons-style-boxed.png',
								'label'   => __( 'Boxed', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'branded' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/icons-style/icons-style-branded.png',
								'label'   => __( 'Branded', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'branded-squared' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/icons-style/icons-style-branded-squared.png',
								'label'   => __( 'Branded squared', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'branded-squared-reverse' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/icons-style/icons-style-branded-squared-reverse.png',
								'label'   => __( 'Branded squared reverse', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'branded-circled' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/icons-style/icons-style-branded-circled.png',
								'label'   => __( 'Branded circled', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'branded-circled-reverse' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/icons-style/icons-style-branded-circled-reverse.png',
								'label'   => __( 'Branded circled reverse', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'branded-boxed' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/icons-style/icons-style-branded-boxed.png',
								'label'   => __( 'Branded boxed', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
						),
						'input_attrs'     => array(),
						'allow_addition'  => false,
						'active_callback' => 'molongui_active_icons_setting',
					),
				),
				// Social media icons color
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[icons_color]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'inherit',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Color', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Color_Control',
						'type'            => 'molongui-color',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium' => false,
						),
						'allow_addition'  => false,
						'active_callback' => 'molongui_active_icons_color_setting',
					),
				),
				// Social media icons size
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[icons_size]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'normal',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Font size', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Range_Control',
						'type'            => 'flat',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium' => false,
							'min'     => 8,
							'max'     => 100,
							'step'    => 1,
							'suffix'  => 'px',
						),
						'allow_addition'  => false,
						'active_callback' => 'molongui_active_icons_setting',
					),
				),
			),
		),
		//--------------------------------------------------------------------------------------------------------------
		// RELATED ENTRIES TEMPLATE
		//--------------------------------------------------------------------------------------------------------------
		array
		(
			'id'                 => 'molongui_authorship_related',
			'title'              => __( 'Template', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'description'        => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'display'            => true,
			'priority'           => '',
			'type'               => '',
			'capability'         => 'manage_options',
			'active_callback'    => '',
			'description_hidden' => true,
			'fields'             => array
			(
				// Related layout [PREMIUM]
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[related_layout]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'layout-1',
						'transport'            => 'refresh',
						'validate_callback'    => '',
						'sanitize_callback'    => '',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Related entries template', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( 'The template to be used to render related entries section.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Image_Radio_Button_Control',
						'type'            => 'molongui-image-radio',
						'choices'         => array
						(
							'layout-1' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/related-template/related-template-1.png',
								'label'   => __( 'Template 1', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'layout-2' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/related-template/related-template-2.png',
								'label'   => __( 'Template 2', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'layout-3' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'customizer/img/related-template/related-template-3.png',
								'label'   => __( 'Template 3', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
						),
						'input_attrs'     => array(),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
				// Related title ('stacked' layout setting)
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_strings[related_title]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => __( 'Related posts', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_string',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Section label', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'WP_Customize_Control',
						'type'            => 'text',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium'     => false,
							'placeholder' => __( 'Related posts', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						),
						'allow_addition'  => false,
						'active_callback' => 'molongui_active_box_stacked_layout_setting',
					),
				),
				// "No related posts found" label
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_strings[no_related_posts]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => __( 'This author does not have any more posts.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_string',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( '"No related posts found" label', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( 'Text to display when no related entries are found.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'WP_Customize_Control',
						'type'            => 'text',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium'     => false,
							'placeholder' => __( 'This author does not have any more posts.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
			),
		),
		//--------------------------------------------------------------------------------------------------------------
		// RELATED ENTRIES CONFIG
		//--------------------------------------------------------------------------------------------------------------
		array
		(
			'id'                 => 'molongui_authorship_related_config',
			'title'              => __( 'Entries', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'description'        => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'display'            => true,
			'priority'           => '',
			'type'               => '',
			'capability'         => 'manage_options',
			'active_callback'    => '',
			'description_hidden' => true,
			'fields'             => array
			(
				// Typography label (group of compact settings)
				array
				(
					'id'      => 'molongui_related_typography_settings',
					'display' => true,
					'setting' => array
					(
						'sanitize_callback' => 'esc_html',
					),
					'control' => array
					(
						'label'           => __( 'Typography', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Group_Label_Control',
						'type'            => 'molongui-compact-group-label',
						'active_callback' => '',
						'input_attrs'     => array(),
						'choices'         => array(),
					),
				),
				// Related font size
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[related_text_size]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'normal',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Size', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Range_Control',
						'type'            => 'molongui-compact-range-flat',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium' => false,
							'min'     => 8,
							'max'     => 100,
							'step'    => 1,
							'suffix'  => 'px',
						),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
				// Related text style [PREMIUM]
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[related_text_style]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => '',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Style', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Image_Checkbox_Button_Control',
						'type'            => 'molongui-compact-image-checkbox',
						'choices'         => array
						(
							'normal' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-normal.png',
								'label'   => __( 'Normal', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'bold' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-bold.png',
								'label'   => __( 'Bold', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'italic' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-italic.png',
								'label'   => __( 'Italic', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'underline' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-style/font-style-underline.png',
								'label'   => __( 'Underline', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
						),
						'input_attrs'     => array(),
						'allow_addition'  => true,
						'active_callback' => '',
					),
				),
				// Related text case
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[related_text_case]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'none',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Case', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Image_Radio_Button_Control',
						'type'            => 'molongui-compact-image-radio',
						'choices'         => array
						(
							'none' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-case/font-case-none.png',
								'label'   => __( 'Leave as is', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => false,
							),
							'capitalize' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-case/font-case-capitalize.png',
								'label'   => __( 'Capitalize', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'uppercase' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-case/font-case-uppercase.png',
								'label'   => __( 'Uppercase', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							'lowercase' => array
							(
								'image'   => MOLONGUI_AUTHORSHIP_URL.'fw/customizer/img/font-case/font-case-lowercase.png',
								'label'   => __( 'Lowercase', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
						),
						'input_attrs'     => array(),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
				// Related font color [PREMIUM]
				array
				(
					'id'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_box[related_text_color]',
					'display' => true,
					'setting' => array
					(
						'type'                 => 'option',
						'capability'           => 'manage_options',
						'default'              => 'inherit',
						'transport'            => 'postMessage',
						'validate_callback'    => '',
						'sanitize_callback'    => 'molongui_authorship_sanitize_setting',
						'sanitize_js_callback' => '',
						'dirty'                => false,
					),
					'control' => array
					(
						'label'           => __( 'Color', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'description'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'priority'        => 10,
						'class'           => 'Molongui_Customize_Color_Control',
						'type'            => 'molongui-compact-color',
						'choices'         => array(),
						'input_attrs'     => array
						(
							'premium' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
						),
						'allow_addition'  => false,
						'active_callback' => '',
					),
				),
			),
		),
	),
);