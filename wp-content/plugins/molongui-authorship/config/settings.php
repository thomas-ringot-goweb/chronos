<?php

/**
 * Admin settings.
 *
 * This file contains specific plugin settings.
 *
 * This file MUST be loaded after WordPress has initialized its translation routines. Try putting the declaration in an init action hook:
 *
 *      function settings_setup() { $settings = include( MOLONGUI_BOILERPLATE_DIR . 'config/settings.php' ); ]
 *      add_action( 'admin_init', 'settings_setup' );
 *
 * @see http://stackoverflow.com/a/19160452
 *
 * Returned array must have the following structure:
 *
 * array(
 *  'key'      => 'db_key',
 *  'slug'     => 'tab_uri_slug',
 *  'label'    => 'Tab label',
 *  'callback' => function,
 *  'sections' => array(
 *          array(
 *              'id'       => 'section_id',
 *              'display'  => 'true',
 *              'label'    => 'Section label',
 *              'desc'     => 'Section description',
 *              'callback' => 'render_description',
 *              'fields'   => array(
 *                  array(
 *                      'id'          => 'field_name_id',
 *                      'display'     => 'true',
 *                      'label'       => 'Field label',
 *                      'desc'        => 'Field description',
 *                      'tip'         => 'Field tip',
 *                      'premium'     => 'Field premium notice tip,
 *                      'type'        => 'text',
 *                      'placeholder' => 'Field placeholder',
 *                  ),
 *              ),
 *          ),
 *          array(
 *              'id'       => 'section_id',
 *              'display'  => 'true',
 *              'label'    => 'Section label',
 *              'callback' => 'render_page',
 *              'cb_page'  => 'path_to_the_view_to_render',
 *              'cb_args'  => array(),
 *          ),
 *   )
 * )
 *
 * where the 'type' key of a field can be: {text | textarea | select | radio | checkbox | checkboxes | colorpicker | button | link | number | }:
 *
 *  array(
 *      'id'          => 'field_name_id',
 *      'display'     => 'true',
 *      'label'       => __( 'Field label', 'text_domain' ),
 *      'desc'        => __( 'Field description', 'text_domain' ),
 *      'tip'         => __( 'Helping tip', text_domain ),
 *      'premium'     => $this->premium_setting_tip(),
 *      'type'        => 'text',
 *      'placeholder' => 'Field placeholder',
 *      'icon'        => array(
 *          'position' => 'right',
 *          'type'     => 'status',
 *      ),
 *  ),
 *
 *  array(
 *      'id'      => 'field_name_id',
 *      'display' => 'true',
 *      'label'   => __( 'Field label', 'text_domain' ),
 *      'desc'    => __( 'Field description', 'text_domain' ),
 *      'tip'     => 'Field tip',
 *      'premium' => $this->premium_setting_tip(),
 *      'type'    => 'textarea',
 *  ),
 *
 *  array(
 *      'id'      => 'field_name_id',
 *      'display' => 'true',
 *      'label'   => __( 'Field label', 'text_domain' ),
 *      'desc'    => __( 'Field description', 'text_domain' ),
 *      'tip'     => __( 'Helping tip', text_domain ),
 *      'premium' => $this->premium_setting_tip(),
 *      'type'    => 'select',
 *      'options' => array(
 *          array(
 *              'id'    => 'select_option_1_id',
 *              'label' => __( 'Select option 1 label', 'text_domain' ),
 *              'value' => 'select_option_1_value',
 *          ),
 *          array(
 *              'id'    => 'select_option_2_id',
 *              'label' => __( 'Select option 2 label', 'text_domain' ),
 *              'value' => 'select_option_2_value',
 *          ),
 *      ),
 *  ),
 *
 *  array(
 *      'id'      => 'field_name_id',
 *      'display' => 'true',
 *      'label'   => __( 'Field label', 'text_domain' ),
 *      'desc'    => __( 'Field description', 'text_domain' ),
 *      'tip'     => __( 'Helping tip', text_domain ),
 *      'premium' => $this->premium_setting_tip(),
 *      'type'    => 'radio',
 *      'options' => array(
 *          array(
 *              'id'    => 'radio_option_1_id',
 *              'label' => __( 'Radio option 1 label', 'text_domain' ),
 *              'value' => 'radio_option_1_value',
 *          ),
 *          array(
 *              'id'    => 'radio_option_2_id',
 *              'label' => __( 'Radio option 2 label', 'text_domain' ),
 *              'value' => 'radio_option_2_value',
 *          ),
 *      ),
 *  ),
 *
 *  array(
 *      'id'      => 'field_name_id',
 *      'display' => 'true',
 *      'label'   => __( 'Field label', 'text_domain' ),
 *      'desc'    => __( 'Field description', 'text_domain' ),
 *      'tip'     => __( 'Helping tip', text_domain ),
 *      'premium' => $this->premium_setting_tip(),
 *      'type'    => 'checkbox',
 *  ),
 *
 *  array(
 *      'id'      => 'field_name_id',
 *      'display' => 'true',
 *      'label'   => __( 'Field label', 'text_domain' ),
 *      'desc'    => __( 'Field description', 'text_domain' ),
 *      'tip'     => __( 'Helping tip', text_domain ),
 *      'type'    => 'checkboxes',
 *      'options' => array(
 *          array(
 *              'id'    => 'checkbox_option_1_id',
 *              'label' => __( 'Checkbox option 1 label', 'text_domain' ),
 *          ),
 *          array(
 *              'id'    => 'checkbox_option_2_id',
 *              'label' => __( 'Checkbox option 2 label', 'text_domain' ),
 *          ),
 *      ),
 *  ),
 *
 *  array(
 *      'id'      => 'field_name_id',
 *      'display' => 'true',
 *      'label'   => __( 'Field label', 'text_domain' ),
 *      'desc'    => __( 'Field description', 'text_domain' ),
 *      'tip'     => __( 'Helping tip', text_domain ),
 *      'premium' => $this->premium_setting_tip(),
 *      'type'    => 'colorpicker',
 *  ),
 *
 *  array(
 *      'id'      => 'field_name_id',
 *      'display' => 'true',
 *      'label'   => __( 'Field label', text_domain ),
 *      'desc'    => __( 'Field description', text_domain ),
 *      'tip'     => __( 'Helping tip', text_domain ),
 *      'premium' => $this->premium_setting_tip(),
 *      'type'    => 'button',
 *      'args'    => array(
 *          'id'    => 'button_id',
 *          'label' => __( 'Button label', text_domain ),
 *          'class' => 'button_class1 button_class2 ...',
 *       ),
 *  ),
 *
 *  array(
 *      'id'      => 'field_name_id',
 *      'display' => true,
 *      'label'   => __( 'Field label', text_domain ),
 *      'desc'    => __( 'Field description', text_domain ),
 *      'tip'     => __( 'Helping tip', text_domain ),
 *      'premium' => $this->premium_setting_tip(),
 *      'class'   => 'class_name',
 *      'type'    => 'link',
 *      'args'    => array(
 *                      'id'     => 'link_id',
 *                      'url'    => '#',
 *                      'target' => '_self',
 *                      'text' => __( 'Link label', text_domain ),
 *                      'class' => 'button button-primary text_class1 text_class2 ...',
 *                  ),
 *  ),
 *
 * @author     Amitzy
 * @package    Authorship
 * @subpackage /config
 * @since      1.3.0
 * @version    3.1.0
 */
return array
(
	'box' => array
	(
		'key'      => MOLONGUI_AUTHORSHIP_DB_PREFIX . '_' . 'box',
		'slug'     => strtolower( MOLONGUI_AUTHORSHIP_ID ) . '_' . 'box',
		'label'    => __( 'Author box', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
		'callback' => array( new Molongui\Authorship\Admin\Admin(), 'validate_box_tab' ),
		'sections' => array
		(
			array
			(
				'id'       => 'styling',
				'display'  => 'true',
				'label'    => __( 'Styling', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				'desc'     => '',
				'callback' => 'render_description',
				'fields'  => array
				(
					array
					(
						'id'      => 'open_customizer',
						'display' => true,
						'label'   => __( 'Author box styles', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'desc'    => sprintf( __( 'All author box styling settings are available in the WordPress Customizer. Go there and customize your author box to your likings with %sfull live preview%s.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ), '<span style="font-weight:bold; text-decoration:underline;">', '</span>' ),
						'tip'     => __( 'From plugin version 2.1.0 on, all styling settings are available in the WordPress Customizer. Click the button on the right to open it.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'type'    => 'link',
						'args'    => array
						(
							'id'     => 'open_customizer_button',
							'url'    => Molongui\Authorship\Admin\Admin::get_customizer_link(),
							'target' => '_self',
							'class'  => 'button button-primary',
							'text'   => __( 'Open customizer', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						),
					),
				),
			),
			array
			(
				'id'       => 'display',
				'display'  => 'true',
				'label'    => __( 'Display', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
//				'desc'     => __( 'Set display options that will apply by default to all posts. They can be changed for each post on its edit page.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				'desc'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				'callback' => 'render_description',
				'fields'  => array
				(
					array
					(
						'id'      => 'display',
						'display' => 'true',
						'label'   => __( 'Show by default', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'desc'    => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'tip'     => __( 'Whether to show the authorship box by default. This setting is overridden by post configuration.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'premium' => '',
						'class'   => '',
						'type'    => 'select',
						'options' => array
						(
							array
							(
								'id'    => 'all',
								'label' => __( 'Yes, on all posts and pages', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => '1',
							),
							array
							(
								'id'    => 'posts',
								'label' => __( 'Only on posts', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => 'posts',
							),
							array
							(
								'id'    => 'pages',
								'label' => __( 'Only on pages', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => 'pages',
							),
							array
							(
								'id'    => 'none',
								'label' => __( 'No, let me choose where to display it', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => '0',
							),
						),
					),
					array
					(
						'id'      => 'hide_on_category',
						'display' => 'true',
						'label'   => __( 'Hide on these post categories', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'desc'    => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'tip'     => __( 'Select the post categories on which the author box won\'t be displayed. This setting is overridden by post configuration.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'premium' => $this->premium_setting_tip(),
						'class'   => '',
						'type'    => 'checkboxes',
						'options' => molongui_post_categories( true ),
					),
					array
					(
						'id'      => 'hide_if_no_bio',
						'display' => 'true',
						'label'   => __( 'Hide if no author biography', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'desc'    => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'tip'     => __( 'Whether to hide the author box if there is not biographical information to show.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'premium' => '',
						'class'   => '',
						'type'    => 'select',
						'options' => array
						(
							array
							(
								'id'    => 'yes',
								'label' => __( 'Yes', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => '1',
							),
							array
							(
								'id'    => 'no',
								'label' => __( 'No', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => '0',
							),
						),
					),
					array
					(
						'id'      => 'order',
						'display' => 'true',
						'label'   => __( 'Order', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'desc'    => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'tip'     => __( 'Author box is added to post content in the configured position. Nonetheless, other plugins may also add their stuff, making the author box appear above/below it. Reduce the number below until the box goes up there where you like or increase it to make it go down.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'premium' => '',
						'class'   => '',
						'type'    => 'number',
						'min'     => 1,
						'max'     => '',
						'step'    => 1,
					),
				),
			),
			array
			(
				'id'       => 'multiauthor_box',
				'display'  => 'true',
				'label'    => __( 'Multiauthor', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				'desc'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				'callback' => 'render_description',
				'fields'  => array
				(
					array
					(
						'id'      => 'multiauthor_box_layout',
						'display' => 'true',
						'label'   => __( 'Author box layout', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'desc'    => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'tip'     => __( 'How to show multiple author boxes.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'premium' => '',
						'class'   => '',
						'type'    => 'select',
						'options' => array
						(
							array
							(
								'id'    => 'individual',
								'label' => __( 'As many author boxes as authors', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => 'individual',
							),
							array
							(
								'id'    => 'default',
								'label' => __( 'All authors in one single box', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => 'default',
							),
						),
					),
				),
			),
			array
			(
				'id'       => 'related_posts',
				'display'  => 'true',
				'label'    => __( 'Related posts', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				'desc'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				'callback' => 'render_description',
				'fields'   => array
				(
					array
					(
						'id'      => 'show_related',
						'display' => 'true',
						'label'   => __( 'Show related posts', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'desc'    => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'tip'     => __( 'Whether to show "related posts" link on the author box.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'premium' => '',
						'class'   => '',
						'type'    => 'select',
						'options' => array
						(
							array
							(
								'id'    => 'yes',
								'label' => __( 'Yes', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => '1',
							),
							array
							(
								'id'    => 'no',
								'label' => __( 'No', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => '0',
							),
						),
					),
					array
					(
						'id'      => 'related_post_type',
						'display' => 'true',
						'label'   => __( 'Post types', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'desc'    => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'tip'     => __( 'Type of posts to show as related.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'premium' => $this->premium_setting_tip(),
						'class'   => '',
						'type'    => 'checkboxes',
						'options' => molongui_supported_post_types( $this->plugin->id, 'all', true ),
					),
					array
					(
						'id'      => 'related_items',
						'display' => 'true',
						'label'   => __( 'Number of posts', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'desc'    => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'tip'     => __( 'Amount of related posts to show in the author box.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'premium' => '',
						'class'   => '',
						'type'    => 'number',
						'min'     => 1,
						'max'     => '',
						'step'    => 1,
					),
					array
					(
						'id'      => 'related_orderby',
						'display' => 'true',
						'label'   => __( 'Order by', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'desc'    => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'tip'     => __( 'The criteria to sort related posts.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'premium' => $this->premium_setting_tip(),
						'class'   => '',
						'type'    => 'select',
						'options' => array
						(
							array
							(
								'id'    => 'title',
								'label' => __( 'Title', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => 'title',
							),
							array
							(
								'id'    => 'date',
								'label' => __( 'Date', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => 'date',
							),
							array
							(
								'id'    => 'modified',
								'label' => __( 'Modified', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => 'modified',
							),
							array
							(
								'id'    => 'comment_count',
								'label' => __( 'Comment count', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => 'comment_count',
							),
							array
							(
								'id'    => 'rand',
								'label' => __( 'Random order', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => 'rand',
							),
						),
					),
					array
					(
						'id'      => 'related_order',
						'display' => 'true',
						'label'   => __( 'Order', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'desc'    => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'tip'     => __( 'The order to sort related posts.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'premium' => $this->premium_setting_tip(),
						'class'   => '',
						'type'    => 'select',
						'options' => array
						(
							array
							(
								'id'    => 'asc',
								'label' => __( 'Ascending order', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => 'asc',
							),
							array
							(
								'id'    => 'desc',
								'label' => __( 'Descending order', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => 'desc',
							),
						),
					),
				),
			),
			array
			(
				'id'       => 'social_networks',
				'display'  => 'true',
				'label'    => __( 'Social networks', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				'desc'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				'callback' => 'render_description',
				'fields'   => array
				(
					array
					(
						'id'      => 'show',
						'display' => 'true',
						'label'   => __( 'Social networks', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'desc'    => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'tip'     => __( 'There are many social networking websites out there and this plugin allows you to link to the most popular. Not to glut the edit page, choose the ones you want to be able to configure.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'premium' => '',
						'class'   => '',
						'type'    => 'checkboxes',
						'options' => array
						(
							array
							(
								'id'    => 'facebook',
								'label' => 'Facebook',
							),
							array
							(
								'id'    => 'twitter',
								'label' => 'Twitter',
							),
							array
							(
								'id'    => 'linkedin',
								'label' => 'Linkedin',
							),
							array
							(
								'id'    => 'googleplus',
								'label' => 'Google+',
							),
							array
							(
								'id'    => 'youtube',
								'label' => 'Youtube',
							),
							array
							(
								'id'    => 'pinterest',
								'label' => 'Pinterest',
							),
							array
							(
								'id'    => 'tumblr',
								'label' => 'Tumblr',
							),
							array
							(
								'id'    => 'instagram',
								'label' => 'Instagram',
							),
							array
							(
								'id'    => 'slideshare',
								'label' => 'Slideshare',
							),
							array
							(
								'id'    => 'xing',
								'label' => 'Xing',
							),
							array
							(
								'id'    => 'renren',
								'label' => 'Renren',
							),
							array
							(
								'id'    => 'vk',
								'label' => 'Vk',
							),
							array
							(
								'id'    => 'flickr',
								'label' => 'Flickr',
							),
							array
							(
								'id'    => 'vine',
								'label' => 'Vine',
							),
							array
							(
								'id'    => 'meetup',
								'label' => 'Meetup',
							),
							array
							(
								'id'    => 'weibo',
								'label' => 'Sina Weibo',
							),
							array
							(
								'id'    => 'deviantart',
								'label' => 'Deviantart',
							),
							array
							(
								'id'    => 'stumbleupon',
								'label' => 'Stumbleupon',
							),
							array
							(
								'id'    => 'myspace',
								'label' => 'MySpace',
							),
							array
							(
								'id'    => 'yelp',
								'label' => 'Yelp',
							),
							array
							(
								'id'    => 'mixi',
								'label' => 'Mixi',
							),
							array
							(
								'id'    => 'soundcloud',
								'label' => 'SoundCloud',
							),
							array
							(
								'id'    => 'lastfm',
								'label' => 'Last.fm',
							),
							array
							(
								'id'    => 'foursquare',
								'label' => 'Foursquare',
							),
							array
							(
								'id'    => 'spotify',
								'label' => 'Spotify',
							),
							array
							(
								'id'    => 'vimeo',
								'label' => 'Vimeo',
							),
							array
							(
								'id'    => 'dailymotion',
								'label' => 'Dailymotion',
							),
							array
							(
								'id'    => 'reddit',
								'label' => 'Reddit',
							),
							array
							(
								'id'    => 'skype',
								'label' => 'Skype',
							),
							array
							(
								'id'    => 'livejournal',
								'label' => 'Live Journal',
							),
							array
							(
								'id'    => 'taringa',
								'label' => 'Taringa!',
							),
							array
							(
								'id'    => 'odnoklassniki',
								'label' => 'OK.ru',
							),
							array
							(
								'id'    => 'askfm',
								'label' => 'ASKfm',
							),
							array
							(
								'id'    => 'bebee',
								'label' => 'beBee',
							),
							array
							(
								'id'    => 'goodreads',
								'label' => 'Goodreads',
							),
							array
							(
								'id'    => 'periscope',
								'label' => 'Periscope',
							),
							array
							(
								'id'    => 'telegram',
								'label' => 'Telegram',
							),
							array
							(
								'id'    => 'livestream',
								'label' => 'Livestream',
							),
							array
							(
								'id'    => 'styleshare',
								'label' => 'StyleShare',
							),
							array
							(
								'id'    => 'reverbnation',
								'label' => 'Reverbnation',
							),
							array
							(
								'id'    => 'everplaces',
								'label' => 'Everplaces',
							),
							array
							(
								'id'    => 'eventbrite',
								'label' => 'Eventbrite',
							),
							array
							(
								'id'    => 'draugiemlv',
								'label' => 'Draugiem.lv',
							),
							array
							(
								'id'    => 'blogger',
								'label' => 'Blogger',
							),
							array
							(
								'id'    => 'patreon',
								'label' => 'Patreon',
							),
							array
							(
								'id'    => 'plurk',
								'label' => 'Plurk',
							),
							array
							(
								'id'    => 'buzzfeed',
								'label' => 'BuzzFeed',
							),
							array
							(
								'id'    => 'slides',
								'label' => 'Slides',
							),
							array
							(
								'id'    => 'deezer',
								'label' => 'Deezer',
							),
							array
							(
								'id'    => 'spreaker',
								'label' => 'Spreaker',
							),
							array
							(
								'id'    => 'runkeeper',
								'label' => 'Runkeeper',
							),
							array
							(
								'id'    => 'medium',
								'label' => 'Medium',
							),
							array
							(
								'id'    => 'speakerdeck',
								'label' => 'Speaker Deck',
							),
							array
							(
								'id'    => 'teespring',
								'label' => 'Teespring',
							),
							array
							(
								'id'    => 'kaggle',
								'label' => 'Kaggle',
							),
							array
							(
								'id'    => 'houzz',
								'label' => 'Houzz',
							),
							array
							(
								'id'    => 'gumtree',
								'label' => 'Gumtree',
							),
							array
							(
								'id'    => 'upwork',
								'label' => 'Upwork',
							),
							array
							(
								'id'    => 'superuser',
								'label' => 'SuperUser',
							),
							array
							(
								'id'    => 'bandcamp',
								'label' => 'Bandcamp',
							),
							array
							(
								'id'    => 'glassdoor',
								'label' => 'Glassdoor',
							),
							array
							(
								'id'    => 'toptal',
								'label' => 'TopTal',
							),
							array
							(
								'id'    => 'ifixit',
								'label' => 'I fix it',
							),
							array
							(
								'id'    => 'stitcher',
								'label' => 'Stitcher',
							),
							array
							(
								'id'    => 'storify',
								'label' => 'Storify',
							),
							array
							(
								'id'    => 'readthedocs',
								'label' => 'Read the docs',
							),
							array
							(
								'id'    => 'ello',
								'label' => 'Ello',
							),
							array
							(
								'id'    => 'tinder',
								'label' => 'Tinder',
							),
							array
							(
								'id'    => 'github',
								'label' => 'GitHub',
							),
							array
							(
								'id'    => 'stackoverflow',
								'label' => 'Stack Overflow',
							),
							array
							(
								'id'    => 'jsfiddle',
								'label' => 'JSFiddle',
							),
							array
							(
								'id'    => 'twitch',
								'label' => 'Twitch',
							),
							array
							(
								'id'    => 'whatsapp',
								'label' => 'WhatsApp',
							),
							array
							(
								'id'    => 'tripadvisor',
								'label' => 'Tripadvisor',
							),
							array
							(
								'id'    => 'wikipedia',
								'label' => 'Wikipedia',
							),
							array
							(
								'id'    => '500px',
								'label' => '500px',
							),
							array
							(
								'id'    => 'mixcloud',
								'label' => 'Mixcloud',
							),
							array
							(
								'id'    => 'viadeo',
								'label' => 'Viadeo',
							),
							array
							(
								'id'    => 'quora',
								'label' => 'Quora',
							),
							array
							(
								'id'    => 'etsy',
								'label' => 'Etsy',
							),
							array
							(
								'id'    => 'codepen',
								'label' => 'CodePen',
							),
							array
							(
								'id'    => 'coderwall',
								'label' => 'Coderwall',
							),
							array
							(
								'id'    => 'behance',
								'label' => 'Behance',
							),
							array
							(
								'id'    => 'coursera',
								'label' => 'Coursera',
							),
							array
							(
								'id'    => 'googleplay',
								'label' => 'Google Play',
							),
							array
							(
								'id'    => 'itunes',
								'label' => 'iTunes',
							),
							array
							(
								'id'    => 'angellist',
								'label' => 'AngelList',
							),
							array
							(
								'id'    => 'amazon',
								'label' => 'Amazon',
							),
							array
							(
								'id'    => 'ebay',
								'label' => 'eBay',
							),
							array
							(
								'id'    => 'paypal',
								'label' => 'Paypal',
							),
							array
							(
								'id'    => 'digg',
								'label' => 'Digg',
							),
							array
							(
								'id'    => 'dribbble',
								'label' => 'Dribbble',
							),
							array
							(
								'id'    => 'dropbox',
								'label' => 'Dropbox',
							),
							array
							(
								'id'    => 'scribd',
								'label' => 'Scribd',
							),
						),
					),
				),
			),
		),
	),
	'byline' => array
    (
        'key'      => MOLONGUI_AUTHORSHIP_DB_PREFIX.'_'.'byline',
        'slug'     => strtolower( MOLONGUI_AUTHORSHIP_ID ).'_'.'byline',
        'label'    => __( 'Byline', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
        'callback' => array( new Molongui\Authorship\Admin\Admin(), 'validate_byline_tab' ),
        'sections' => array
        (
	        array
	        (
		        'id'       => 'byline_partial_integration',
		        'display'  => 'true',
		        'label'    => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
		        //'desc'     => __( "The byline on a post gives the name of the writer. Bylines are commonly placed between the headline and the text of the post, although some themes place them at the bottom of the page or anywhere else. With Molongui Authorship you can easily assign one or more authors to a post. It even allows those authors to be guest authors, so they get no access to your installation. Molongui Authorship can automatically update bylines everywhere to keep consistency, but that may cause issues with some themes or other installed plugins. That's why it is recommended to use provided template tags instead. Using the provided template tags is currently the only way to make each author name link to that author's archive page, so a full integration is achieved.", MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
		        'desc'     => sprintf( __( "Molongui Authorship can automatically update bylines everywhere to keep consistency, but that may cause issues with some themes or other installed plugins. That's why %sit is recommended to use the provided template tags instead%s. Using the provided template tags is currently the only way to make each author name link to that author's archive page, so a full integration is achieved.", MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),  '<strong>', '</strong>' ),
		        'd_class'  => 'molongui-settings-section-notice molongui-settings-section-notice-warning w3-hide',
		        'callback' => 'render_description',
		        'fields'   => array(),
	        ),
	        array
	        (
		        'id'       => 'byline_full_integration',
		        'display'  => 'true',
		        'label'    => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
		        'desc'     => __( "Displaying fully customized and functional bylines can be achieved using the provided template tags. It requires some basic coding skills, but using template tags is the best way to go in order to avoid issues with other plugins and make each author name in the byline to link to each author archive page. Some parameters can be provided to the template tags so byline can be fully localized and customized with custom HTML code and CSS styles.", MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
		        'd_class'  => 'molongui-settings-section-notice molongui-settings-section-notice-info w3-hide',
		        'callback' => 'render_description',
		        'fields'   => array(),
	        ),
	        array
	        (
		        'id'       => 'byline_integration',
		        'display'  => 'true',
		        'label'    => __( 'Integration', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
		        'desc'     => '',//__( "Molongui Authorship makes it easy to assign one or more authors to a post. Those authors can even be guest authors, so they get no access to wp-admin. Bylines are automatically updated everywhere to keep consistency, but they can only link to an author archive page. That's a WordPress limitation that we are trying to overcome. Nonetheles,  In order for those authors to appear in the byline on the frontend, you may need to make some small modifications to your theme.", MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
		        'callback' => 'render_description',
		        'fields'   => array
		        (
			        array
			        (
				        'id'      => 'byline_automatic_integration',
				        'display' => 'true',
				        'label'   => __( 'Integration', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				        'desc'    => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				        'tip'     => __( 'Whether to automatically update byline everywhere on your site by default. Activating this option may cause issues with other plugins. It is recommended to use provided template tags instead. Check out the documentation.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				        'premium' => '',
				        'class'   => 'molongui-width-500',
				        'type'    => 'select',
				        'options' => array
				        (
					        array
					        (
						        'id'    => 'automatic',
						        'label' => __( 'I want the plugin to implement an automatic partial integration', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						        'value' => '1',
					        ),
					        array
					        (
						        'id'    => 'template_tags',
						        'label' => __( 'I will integrate template tags to get a full integration', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						        'value' => '0',
					        ),
				        ),
			        ),
			        array
			        (
				        'id'      => 'byline_full_integration_documentation',
				        'display' => true,
				        'label'   => __( 'Template tags documentation', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				        'desc'    => sprintf( __( 'Using template tags requires you to make some small modifications to your theme. %sIf you are a skilled coder, you may want to take a look at "%stemplate-tags.php%s" file.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ), '<br>', '<strong>', '</strong>' ),
				        'tip'     => __( 'Check out the documentation to know how to integrate the provided template tags into your child theme.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				        'type'    => 'link',
				        'args'    => array
				        (
					        'id'     => 'open_template_tags_documentation_button',
					        'url'    => MOLONGUI_AUTHORSHIP_WEB.'kb/incorporate-molongui-authorship-template-tags-into-your-theme/',
					        'target' => '_blank',
					        'class'  => 'button',
					        'text'   => __( 'Read documentation', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				        ),
			        ),
		        ),
	        ),
	        array
	        (
		        'id'       => 'byline_modifiers',
		        'display'  => 'true',
		        'label'    => __( 'Display modifiers', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
		        'desc'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
		        'callback' => 'render_description',
		        'fields'   => array
		        (
			        array
			        (
				        'id'          => 'byline_modifier_before',
				        'display'     => 'true',
				        'label'       => __( 'Code to prepend to byline', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				        'desc'        => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				        'tip'         => __( 'String to be prepended before the author name(s). Defaults to an empty string.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				        'premium'     => '',
				        'class'       => 'molongui-width-500',
				        'type'        => 'text',
				        'placeholder' => 'Written by ',
			        ),
			        array
			        (
				        'id'          => 'byline_modifier_after',
				        'display'     => 'true',
				        'label'       => __( 'Code to append to byline', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				        'desc'        => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				        'tip'         => __( 'String to be appended after the author name(s). Defaults to an empty string.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				        'premium'     => '',
				        'class'       => 'molongui-width-500',
				        'type'        => 'text',
				        'placeholder' => '',
			        ),
		        ),
	        ),
	        array
	        (
		        'id'       => 'byline_multiauthor',
		        'display'  => 'true',
		        'label'    => __( 'Multiauthor', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
		        'desc'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
		        'callback' => 'render_description',
		        'fields'   => array
		        (
			        array
			        (
				        'id'      => 'byline_multiauthor_display',
				        'display' => 'true',
				        'label'   => __( 'How to handle multiple author names', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				        'desc'    => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				        'tip'     => __( 'How to display author name when there is more than one.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				        'premium' => '',
				        'class'   => 'molongui-width-500',
				        'type'    => 'select',
				        'options' => array
				        (
					        array
					        (
						        'id'    => 'byline_all',
						        'label' => __( 'Show all authors names', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						        'value' => 'all',
					        ),
					        array
					        (
						        'id'    => 'byline_main',
						        'label' => __( 'Show just the name of the main author', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						        'value' => 'main',
					        ),
					        array
					        (
						        'id'    => 'byline_1',
						        'label' => __( 'Show the name of main author and remaining authors count as number', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						        'value' => '1',
					        ),
					        array
					        (
						        'id'    => 'byline_2',
						        'label' => __( 'Show the name of 2 authors and remaining authors count as number', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						        'value' => '2',
					        ),
					        array
					        (
						        'id'    => 'byline_3',
						        'label' => __( 'Show the name of 3 authors and remaining authors count as number', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						        'value' => '3',
					        ),
				        ),
			        ),
			        array
			        (
				        'id'          => 'byline_multiauthor_separator',
				        'display'     => 'true',
				        'label'       => __( 'Delimiter between author names', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				        'desc'        => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				        'tip'         => __( 'Delimiter that should appear between the authors. Defaults to a comma.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				        'premium'     => '',
				        'class'       => 'molongui-width-500',
				        'type'        => 'text',
				        'placeholder' => ',',
			        ),
			        array
			        (
				        'id'          => 'byline_multiauthor_last_separator',
				        'display'     => 'true',
				        'label'       => __( 'Delimiter between last two names', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				        'desc'        => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				        'tip'         => __( 'Delimiter that should appear between the last two authors. Defaults to "and"', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				        'premium'     => '',
				        'class'       => 'molongui-width-500',
				        'type'        => 'text',
				        'placeholder' => 'and',
			        ),
			        array
			        (
				        'id'      => 'byline_multiauthor_link',
				        'display' => 'true',
				        'label'   => __( 'Byline link behavior', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				        'desc'    => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				        'tip'     => __( 'Default WordPress behaviour makes the byline a link to the author page. Despite showing more than one author, there can only be a link, so choose whether to disable that link when multiauthor or make it link to the main author page. Check documentation to know how to avoid this and make each author name a link to their page.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				        'premium' => '',
				        'class'   => 'molongui-width-500',
				        'type'    => 'select',
				        'options' => array
				        (
					        array
					        (
						        'id'    => 'disabled',
						        'label' => __( 'Disable byline link when multiple authors', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						        'value' => 'disabled',
					        ),
					        array
					        (
						        'id'    => 'main',
						        'label' => __( 'Make the link point to the main author page', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						        'value' => 'main',
					        ),
				        ),
			        ),
		        ),
	        ),
        ),
    ),
	'guest' => array
	(
		'key'      => MOLONGUI_AUTHORSHIP_DB_PREFIX . '_' . 'guest',
		'slug'     => strtolower( MOLONGUI_AUTHORSHIP_ID ) . '_' . 'guest',
		'label'    => __( 'Guest authors', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
		'callback' => array( new Molongui\Authorship\Admin\Admin(), 'validate_guest_tab' ),
		'sections' => array
		(
			array
			(
				'id'       => 'guest_feature',
				'display'  => 'true',
				'label'    => __( 'Functionality', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				'desc'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				'callback' => 'render_description',
				'fields'   => array
				(
					array
					(
						'id'      => 'enable_guest_authors_feature',
						'display' => 'true',
						'label'   => __( 'Enable "guest authors" feature', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'desc'    => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'tip'     => __( 'Whether to enable guest authors feature.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'premium' => '',
						'class'   => '',
						'type'    => 'select',
						'options' => array
						(
							array
							(
								'id'    => 'yes',
								'label' => __( 'Yes', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => '1',
							),
							array
							(
								'id'    => 'no',
								'label' => __( 'No', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => '0',
							),
						),
					),
				),
			),
			array
			(
				'id'       => 'search',
				'display'  => 'true',
				'label'    => __( 'Search results', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				'desc'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				'callback' => 'render_description',
				'fields'   => array
				(
					array
					(
						'id'      => 'include_guests_in_search',
						'display' => 'true',
						'label'   => __( 'Include guest authors in search', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'desc'    => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'tip'     => __( 'Whether to make guest authors searchable.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'premium' => '',
						'class'   => '',
						'type'    => 'select',
						'options' => array
						(
							array
							(
								'id'    => 'yes',
								'label' => __( 'Yes', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => '1',
							),
							array
							(
								'id'    => 'no',
								'label' => __( 'No', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => '0',
							),
						),
					),
				),
			),
			array
			(
				'id'       => 'admin_menu',
				'display'  => 'true',
				'label'    => __( 'Admin menu item', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				'desc'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				'callback' => 'render_description',
				'fields'   => array
				(
					array
					(
						'id'      => 'guest_menu_item_level',
						'display' => 'true',
						'label'   => __( 'Where to add "Guest authors" menu', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'desc'    => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'tip'     => __( 'Choose where to add the "Guest authors" link in the admin menu.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'premium' => $this->premium_setting_tip(),
						'class'   => '',
						'type'    => 'select',
						'options' => array
						(
							array
							(
								'id'       => 'top_level',
								'label'    => __( 'Top level', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value'    => 'top',
								'disabled' => false,
							),
							array
							(
								'id'       => 'users',
								'label'    => __( 'Users', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value'    => 'users.php',
								'disabled' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							array
							(
								'id'       => 'posts',
								'label'    => __( 'Posts', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value'    => 'edit.php',
								'disabled' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
							array
							(
								'id'       => 'pages',
								'label'    => __( 'Pages', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value'    => 'edit.php?post_type=page',
								'disabled' => !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ),
							),
						),
					),
				),
			),
		),
	),
	'archives' => array
	(
		'key'      => MOLONGUI_AUTHORSHIP_DB_PREFIX . '_' . 'archives',
		'slug'     => strtolower( MOLONGUI_AUTHORSHIP_ID ) . '_' . 'archives',
		'label'    => __( 'Author archives', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
		'callback' => array( new Molongui\Authorship\Admin\Admin(), 'validate_archives_tab' ),
		'sections' => array
		(
			array
			(
				'id'       => 'guest_archives',
				'display'  => Molongui\Authorship\Admin\Admin::display_guest_archives_section(),
				'label'    => __( 'Guest authors', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				'desc'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				'callback' => 'render_description',
				'fields'   => array
				(
					array
					(
						'id'      => 'guest_archive_enabled',
						'display' => 'true',
						'label'   => ( !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ) ? __( 'Enable "guest author" archives', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) : __( 'Disable "guest author" archives', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) ),
						'desc'    => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'tip'     => __( 'Whether to enable the use of guest author archive pages. Disabling this option, author name link is disabled.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'premium' => $this->premium_setting_tip(),
						'class'   => '',
						'type'    => 'select',
						'options' => array
						(
							array
							(
								'id'    => 'yes',
								'label' => __( 'Yes', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => ( !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ) ? '1' : '0' ),
							),
							array
							(
								'id'    => 'no',
								'label' => __( 'No', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => ( !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ) ? '0' : '1' ),
							),
						),
					),
					array
					(
						'id'          => 'guest_archive_tmpl',
						'display'     => 'true',
						'label'       => __( 'Template', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'desc'        => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'tip'         => __( 'Template to be used on guest author archive pages.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'premium'     => $this->premium_setting_tip(),
						'class'       => '',
						'placeholder' => 'template_name.php',
						'type'        => 'text',
					),
					array
					(
						'id'          => 'guest_archive_permalink',
						'display'     => 'true',
						'label'       => __( 'Permalink', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'desc'        => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'tip'         => __( 'Permastruct to add after your installation URI and before the slug set below.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'premium'     => $this->premium_setting_tip(),
						'class'       => '',
						'placeholder' => '',
						'type'        => 'text',
					),
					array
					(
						'id'          => 'guest_archive_base',
						'display'     => 'true',
						'label'       => __( 'Author base', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'desc'        => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'tip'         => __( 'Part of the permalink that identifies your author archive page.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'premium'     => $this->premium_setting_tip(),
						'class'       => '',
						'placeholder' => 'author',
						'type'        => 'text',
					),
				),
			),
			array
			(
				'id'       => 'user_archives',
				'display'  => 'true',
				'label'    => __( 'Registered users', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				'desc'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				'callback' => 'render_description',
				'fields'   => array
				(
					array
					(
						'id'      => 'user_archive_enabled',
						'display' => 'true',
						'label'   => __( 'Disable user archives', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'desc'    => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'tip'     => __( 'Whether to disable author archive pages. Disabling this option, author name links are disabled too.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'premium' => $this->premium_setting_tip(),
						'class'   => '',
						'type'    => 'select',
						'options' => array
						(
							array
							(
								'id'    => 'yes',
								'label' => __( 'Yes', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => '0',
							),
							array
							(
								'id'    => 'no',
								'label' => __( 'No', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => '1',
							),
						),
					),
					array
					(
						'id'          => 'user_archive_tmpl',
						'display'     => 'true',
						'label'       => __( 'Template', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'desc'        => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'tip'         => __( 'Template to be used on author archive pages.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'premium'     => $this->premium_setting_tip(),
						'class'       => '',
						'placeholder' => 'template_name.php',
						'type'        => 'text',
					),
					array
					(
						'id'          => 'user_archive_base',
						'display'     => 'true',
						'label'       => __( 'Author base', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'desc'        => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'tip'         => __( 'Part of the permalink that identifies your author archive page.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'premium'     => $this->premium_setting_tip(),
						'class'       => '',
						'placeholder' => 'author',
						'type'        => 'text',
					),
					array
					(
						'id'      => 'user_archive_redirect',
						'display' => 'true',
						'label'   => __( 'Redirect URL access to', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'desc'    => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'tip'     => __( 'Select the page the user will be redirected to if author archive page is introduced manually.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'premium' => $this->premium_setting_tip(),
						'class'   => '',
						'type'    => 'dropdown_pages',
					),
					array
					(
						'id'      => 'user_archive_status',
						'display' => 'true',
						'label'   => __( 'Redirection status code', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'desc'    => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'tip'     => __( 'Select the status code the web server will return upon redirection.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'premium' => $this->premium_setting_tip(),
						'class'   => '',
						'type'    => 'select',
						'options' => array
						(
							array
							(
								'id'    => '301',
								'label' => __( '301 - Moved Permanently', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => '301',
							),
							array
							(
								'id'    => '307',
								'label' => __( '307 - Temporary Redirect', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => '307',
							),
							array
							(
								'id'    => '308',
								'label' => __( '308 - Permanent Redirect', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => '308',
							),
							array
							(
								'id'    => '403',
								'label' => __( '403 - Forbidden', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => '403',
							),
							array
							(
								'id'    => '404',
								'label' => __( '404 - Not Found', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => '404',
							),
						),
					),
				),
			),
		),
	),
	'advanced' => array
	(
		/**
		 * 'Advanced' tab is created and handled by the FW. In order to be able to insert plugin-specific settings into
		 * the 'Advanced' tab without issues, only 'sections' array can be defined. Do not define 'key', 'slug', 'label',
		 * 'callback' or any other section than 'sections'.
		 *
		 * NOTE that 'callback' function will be defined by the FW, so any data sanitation/validation must be done there.
		 */
		'sections' => array
		(
			array
			(
				'id'       => 'display_manipulation',
				'display'  => true,
				'label'    => __( 'Compatibility', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				'desc'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				'callback' => 'render_description',
				'fields'   => array
				(
					array
					(
						'id'          => 'hide_elements',
						'display'     => true,
						'label'       => __( 'Elements to hide', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'desc'        => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'tip'         => __( 'Comma-separated DOM elements IDs or Classes to hide from being displayed on the front-end.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'premium'     => '',
						'class'       => '',
						'placeholder' => '',
						'type'        => 'text',
					),
				),
			),
			array
			(
				'id'       => 'nofollow',
				'display'  => 'true',
				'label'    => __( 'Search engines', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				'desc'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				'callback' => 'render_description',
				'fields'   => array
				(
					array
					(
						'id'      => 'add_nofollow',
						'display' => 'true',
						'label'   => __( 'Add \'nofollow\' attribute', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'desc'    => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'tip'     => __( 'Whether to add \'nofollow\' attribute to social networks links.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'premium' => '',
						'class'   => '',
						'type'    => 'select',
						'options' => array
						(
							array
							(
								'id'    => 'yes',
								'label' => __( 'Yes', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => '1',
							),
							array
							(
								'id'    => 'no',
								'label' => __( 'No', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => '0',
							),
						),
					),
				),
			),
			array
			(
				'id'       => 'metadata',
				'display'  => 'true',
				'label'    => __( 'Metadata', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				'desc'     => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
				'callback' => 'render_description',
				'fields'   => array
				(
					array
					(
						'id'      => 'add_opengraph_meta',
						'display' => 'true',
						'label'   => __( 'Open Graph', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'desc'    => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'tip'     => __( 'Whether to add Open Graph metadata for author details (useful for rich snippets).', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'premium' => '',
						'class'   => '',
						'type'    => 'select',
						'options' => array
						(
							array
							(
								'id'    => 'yes',
								'label' => __( 'Yes', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => '1',
							),
							array
							(
								'id'    => 'no',
								'label' => __( 'No', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => '0',
							),
						),
					),
					array
					(
						'id'      => 'add_google_meta',
						'display' => 'true',
						'label'   => __( 'Google Authorship', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'desc'    => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'tip'     => __( 'Whether to add Google Authorship metadata for author details (useful for rich snippets).', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'premium' => '',
						'class'   => '',
						'type'    => 'select',
						'options' => array
						(
							array
							(
								'id'    => 'yes',
								'label' => __( 'Yes', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => '1',
							),
							array
							(
								'id'    => 'no',
								'label' => __( 'No', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => '0',
							),
						),
					),
					array
					(
						'id'      => 'add_facebook_meta',
						'display' => 'true',
						'label'   => __( 'Facebook Authorship', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'desc'    => __( '', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'tip'     => __( 'Whether to add Facebook Authorship metadata for author details (useful for rich snippets).', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
						'premium' => '',
						'class'   => '',
						'type'    => 'select',
						'options' => array
						(
							array
							(
								'id'    => 'yes',
								'label' => __( 'Yes', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => '1',
							),
							array
							(
								'id'    => 'no',
								'label' => __( 'No', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
								'value' => '0',
							),
						),
					),
				),
			),
		),
	),
);