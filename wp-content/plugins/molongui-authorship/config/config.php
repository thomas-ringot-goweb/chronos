<?php

// Deny direct access to this file.
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Plugin configuration.
 *
 * Defines whether to load packaged sources and the behavior of other aspects.
 *
 *  scripts         JS scripts packaged within the FW.
 *  styles          CSS stylesheets packaged within the FW.
 *  fonts           Fonts packaged within the FW.
 *  plugins         WP plugins packaged within the FW.
 *  admin_tabs      Plugin settings page tabs behavior.
 *  extra_settings  .
 *  notice          Backend notices properties.
 *
 * This file is loaded like:
 *      $src = include dirname( plugin_dir_path( __FILE__ ) ) . "/config/config.php";
 *
 * @author     Amitzy
 * @package    Authorship
 * @subpackage /config
 * @since      2.0.0
 * @version    3.0.0
 */

return array
(
    'scripts' => array
    (
        'back' => array
        (
	        'sweetalert'      => true,
            'element_queries' => true,
            'select2'         => false,
            'choices'         => true,
            'sortable'        => true,
        ),
        'front' => array
        (
	        'sweetalert'      => false,
            'element_queries' => true,
            'select2'         => false,
            'choices'         => false,
            'sortable'        => false,
        ),
    ),
    'styles' => array
    (
        'back' => array
        (
            'select2' => false,
            'choices' => true,
        ),
        'front' => array
        (
            'select2' => false,
            'choices' => false,
        ),
    ),
    'fonts' => array
    (
        'back' => array
        (
			//'fontfamily_name' => false,
        ),
        'front' => array
        (
			//'fontfamily_name' => false,
        ),
    ),
    'plugins' => array
    (
    	//'plugin_name' => false,
    ),
    'admin_tabs' => array
    (
        'hide' => array
        (
            'box'      => false,
            'byline'   => false,
            'guest '   => false,
            'advanced' => false,
            'license'  => false,
            'support'  => false,
            'more'     => true,
        ),
        'no_save' => array
        (
	        'box'      => false,
	        'byline'   => false,
	        'guest '   => false,
	        'advanced' => false,
            'license'  => false,
            'support'  => true,
            'more'     => true,
        ),
        'no_sidebar' => array
        (
	        'box'      => true,
	        'byline'   => true,
	        'guest '   => true,
	        'advanced' => true,
            'license'  => true,
            'support'  => true,
	        'more'     => true,
        ),
    ),
    'notices' => array
    (
    	'install' => array
	    (
		    'dismissible' => true,
		    'dismissal'   => 'forever',
	    ),
    	'whatsnew' => array
	    (
		    'dismissible' => true,
		    'dismissal'   => 'forever',
	    ),
    	'upgrade' => array
	    (
		    'dismissible' => true,
		    'dismissal'   => 60,
	    ),
    	'update' => array
	    (
		    'dismissible' => false,
		    'dismissal'   => 0,
	    ),
    	'inactive-license' => array
	    (
		    'dismissible' => false,
		    'dismissal'   => 0,
	    ),
    	'renew-license' => array
	    (
		    'dismissible' => true,
		    'dismissal'   => 7,
	    ),
    	'expired-license' => array
	    (
		    'dismissible' => false,
		    'dismissal'   => 0,
	    ),
    ),
);