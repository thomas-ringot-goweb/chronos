<?php

/**
 * Fired when the plugin is uninstalled.
 *
 * When populating this file, consider the following flow
 * of control:
 *
 * - This method should be static
 * - Check if the $_REQUEST content actually is the plugin name
 * - Run an admin referrer check to make sure it goes through authentication
 * - Verify the output of $_GET makes sense
 * - Repeat with other user roles. Best directly by using the links/query string parameters.
 * - Repeat things for multisite. Once for a single site in the network, once sitewide.
 *
 * @author     Amitzy
 * @package    Authorship
 * @subpackage uninstall
 * @since      1.0.0
 * @version    3.0.0
 */

// If uninstall not called from WordPress, then exit.
if ( !defined( 'WP_UNINSTALL_PLUGIN' ) ) exit;

/**
 * Recreates plugin constants.
 *
 * @since   2.0.7
 * @version 2.1.0
 */
require_once( plugin_dir_path( __FILE__ ) . 'config/plugin.php' );

/**
 * Loads helper functions.
 *
 * @since   2.0.2
 * @version 2.0.2
 */
require_once( plugin_dir_path( __FILE__ ) . 'fw/includes/fw-helper-functions.php' );

/**
 * Uninstalls the plugin with multi-site support.
 *
 * @since   2.0.7
 * @version 2.0.7
 */
if ( function_exists('is_multisite') and is_multisite() )
{
	// Uninstall the plugin on all the sites defined in this network.
	foreach ( molongui_get_sites() as $site_id )
	{
		switch_to_blog( $site_id );
		molongui_authorship_uninstall_single_site();
		restore_current_blog();
	}
}
else
{
	// Uninstall the plugin on a single site.
	molongui_authorship_uninstall_single_site();
}

/**
 * Runs required tasks to uninstall the plugin on a single site.
 *
 * @since   2.0.7
 * @version 3.0.0
 */
function molongui_authorship_uninstall_single_site()
{
	global $wpdb;

	// Get plugin settings.
	$settings = get_option( MOLONGUI_AUTHORSHIP_ADVANCED_SETTINGS );

	// Deactivate and remove premium license (if any).
	if ( file_exists( MOLONGUI_AUTHORSHIP_DIR.'premium/' ) and file_exists( MOLONGUI_AUTHORSHIP_DIR.'fw/update/includes/fw-class-license.php' ) )
	{
		if ( !class_exists( 'Molongui\Fw\Includes\License' ) ) require_once( MOLONGUI_AUTHORSHIP_DIR . 'fw/update/includes/fw-class-license.php' );
		$license = new \Molongui\Fw\Includes\License( MOLONGUI_AUTHORSHIP_ID );
		$license->remove( true );
	}

	// Delete plugin settings if not configured otherwise.
	if ( $settings['keep_config'] == 0 )
	{
		$like = MOLONGUI_AUTHORSHIP_DB_PREFIX.'_%';

		$wpdb->query( "DELETE FROM {$wpdb->prefix}options WHERE option_name LIKE '{$like}';");
	}

	// Delete plugin data if not configured otherwise.
	if ( $settings['keep_data'] == 0 )
	{
		$like = MOLONGUI_AUTHORSHIP_DB_PREFIX.'_%';

		// Get all "molongui_guestauthor" custom-posts.
		$ids = $wpdb->get_results(
			"
					SELECT ID
					FROM {$wpdb->prefix}posts
					WHERE post_type LIKE 'molongui_guestauthor'
					",
						ARRAY_A
					);

		// Convert numerically indexed array of associative arrays (ARRAY_A) to comma separated string.
		foreach ( $ids as $key => $id )
		{
			if ( $key == 0 ) $postids = $id['ID'];
			else $postids = $postids . ', ' . $id['ID'];
		}

		// Delete all "postmeta" entries related with those custom-posts.
		$wpdb->query( "DELETE FROM {$wpdb->prefix}postmeta WHERE post_id IN ( $postids );" );

		// Delete all "molongui_guestauthor" custom-posts.
		$wpdb->query( "DELETE FROM {$wpdb->prefix}posts WHERE ID IN ( $postids );" );

		// Delete all "postmeta" entries created by this plugin.
		$wpdb->query( "DELETE FROM {$wpdb->prefix}postmeta WHERE meta_key = '_molongui_author';" );
		$wpdb->query( "DELETE FROM {$wpdb->prefix}postmeta WHERE meta_key = '_molongui_main_author';" );
		$wpdb->query( "DELETE FROM {$wpdb->prefix}postmeta WHERE meta_key LIKE '%_molongui_guest_author%';" );
		$wpdb->query( "DELETE FROM {$wpdb->prefix}postmeta WHERE meta_key = '_molongui_author_box_display';" );
		$wpdb->query( "DELETE FROM {$wpdb->prefix}postmeta WHERE meta_key = '_molongui_author_box_position';" );
	}

	// Delete plugin transients.
	$like = '_site_transient_'.MOLONGUI_AUTHORSHIP_SLUG.'%';
	$wpdb->query( "DELETE FROM {$wpdb->prefix}options WHERE option_name LIKE '{$like}';");
}