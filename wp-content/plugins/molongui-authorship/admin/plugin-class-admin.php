<?php

namespace Molongui\Authorship\Admin;

// Deny direct access to this file.
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version and all the needed hooks.
 *
 * @author     Amitzy
 * @package    Authorship
 * @subpackage /admin
 * @since      1.0.0
 * @version    3.1.2
 */
class Admin
{
    /**
     * The name of this plugin.
     *
     * @access  private
     * @var     string
     * @since   1.0.0
     * @version 1.0.0
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @access  private
     * @var     string
     * @since   1.0.0
     * @version 2.0.0
     */
    private $plugin_version;

    /**
     * Whether this plugin is premium.
     *
     * @access  private
     * @var     boolean
     * @since   2.0.0
     * @version 2.0.0
     */
    private $is_premium;

    /**
     * Initialize the class and set its properties.
     *
     * @access  public
     * @since   1.0.0
     * @version 1.0.0
     */
    public function __construct()
    {
        $this->plugin_name    = MOLONGUI_AUTHORSHIP_NAME;
        $this->plugin_version = MOLONGUI_AUTHORSHIP_VERSION;
        $this->is_premium     = molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR );
    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @access  public
     * @since   1.0.0
     * @version 1.0.0
     */
    public function enqueue_styles()
    {
        // Enqueue plugin styles.
        if( !$this->is_premium )
        {
            $fpath = 'admin/css/molongui-authorship-admin.2da5.min.css';
            if ( file_exists( MOLONGUI_AUTHORSHIP_DIR . $fpath ) )
                wp_enqueue_style( $this->plugin_name, MOLONGUI_AUTHORSHIP_URL . $fpath, array(), $this->plugin_version, 'all' );
        }
        else
        {
            $fpath = 'premium/admin/css/molongui-authorship-premium-admin.2da5.min.css';
            if ( file_exists( MOLONGUI_AUTHORSHIP_DIR . $fpath ) )
                wp_enqueue_style( $this->plugin_name, MOLONGUI_AUTHORSHIP_URL . $fpath, array(), $this->plugin_version, 'all' );
        }
    }

    /**
     * Register the JavaScript for the admin area.
     *
     * Ajax variables are localized by the Molongui Framework.
     *
     * @access  public
     * @param   string  $hook
     * @since   1.0.0
     * @version 2.0.7
     */
    public function enqueue_scripts( $hook )
    {
	    global $pagenow;

	    // Enqueue scripts that only apply to the plugin settings page.
	    if ( ( $pagenow == 'admin.php' ) and ( $_GET['page'] == 'molongui-authorship' ) )
	    {
		    $fpath = 'admin/js/settings.643d.min.js';
		    if ( file_exists( MOLONGUI_AUTHORSHIP_DIR.$fpath ) )
		    {
			    // Enqueue script.
			    wp_enqueue_script( $this->plugin_name.'-settings', MOLONGUI_AUTHORSHIP_URL.$fpath, array( 'jquery', 'wp-color-picker' ), $this->plugin_version , true );
		    }
	    }

	    // Enqueue scripts that only apply to wp user pages.
	    if ( ( $pagenow == 'profile.php' ) or ( $pagenow == 'user-new.php' ) or ( $pagenow == 'user-edit.php' ) )
	    {
		    $fpath = 'admin/js/user.af06.min.js';
		    if ( file_exists( MOLONGUI_AUTHORSHIP_DIR.$fpath ) )
		    {
			    // Define script handle.
			    $handle = $this->plugin_name.'-user';

			    // Enqueue script.
			    wp_enqueue_script( $handle, MOLONGUI_AUTHORSHIP_URL.$fpath, array( 'jquery', 'wp-color-picker' ), $this->plugin_version , true );
		    }
	    }

	    // Enqueue scripts that only apply to "Edit-post" pages for those post-types where plugin functionality is extended to.
	    if ( ( $pagenow == 'post.php' and $_GET['action'] == 'edit' ) or ( $pagenow == 'post-new.php' ) )
	    {
		    $fpath = 'admin/js/edit-post.3ad6.min.js';
		    if ( file_exists( MOLONGUI_AUTHORSHIP_DIR.$fpath ) )
		    {
		    	// Define script handle.
		    	$handle = $this->plugin_name.'-edit-post';

		    	// Enqueue script.
			    wp_enqueue_script( $handle, MOLONGUI_AUTHORSHIP_URL.$fpath, array( 'jquery' ), $this->plugin_version , true );

			    // Add server-side data to previously enqueued script so it can used as variables.
			    wp_localize_script( $handle, 'authorship', array
			    (
				    // String to show as tooltip on remove author button.
				    'remove_author_tip' => esc_html__( 'Remove author from selection', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			    ));
		    }
	    }

        // Enqueue plugin scripts that are used everywhere on wp-admin.
        if( !$this->is_premium )
        {
            $fpath = 'admin/js/admin.xxxx.min.js';
            if ( file_exists( MOLONGUI_AUTHORSHIP_DIR . $fpath ) )
                wp_enqueue_script( $this->plugin_name, MOLONGUI_AUTHORSHIP_URL . $fpath, array( 'jquery', 'wp-color-picker' ), $this->plugin_version , true );
        }
        else
        {
            $fpath = 'premium/admin/js/premium-admin.xxxx.min.js';
            if ( file_exists( MOLONGUI_AUTHORSHIP_DIR . $fpath ) )
                wp_enqueue_script( $this->plugin_name, MOLONGUI_AUTHORSHIP_URL . $fpath, array( 'jquery', 'wp-color-picker' ), $this->plugin_version, true );
        }
    }

    /**
     * Initialize all the plugin settings.
     *
     * This function loads the plugin settings from the database and then merges them with some
     * default values when they are not set.
     *
     * As settings page is divided into tabs, each tab has its own options group.
     *
     * @see     http://stackoverflow.com/a/19160452
     *
     * @access  public
     * @since   1.0.0
     * @version 3.1.0
     */
    function init_plugin_settings()
    {
        // Load saved settings.
	    $tab_box      = (array) get_option( MOLONGUI_AUTHORSHIP_BOX_SETTINGS );
        $tab_byline   = (array) get_option( MOLONGUI_AUTHORSHIP_BYLINE_SETTINGS );
	    $tab_guest    = (array) get_option( MOLONGUI_AUTHORSHIP_GUEST_SETTINGS );
	    $tab_archives = (array) get_option( MOLONGUI_AUTHORSHIP_ARCHIVES_SETTINGS );
	    $tab_advanced = (array) get_option( MOLONGUI_AUTHORSHIP_ADVANCED_SETTINGS );
        $tab_strings  = (array) get_option( MOLONGUI_AUTHORSHIP_STRING_SETTINGS );

        // Default box values.
        $default_box_settings = array
        (
	        // Settings page.
	        'display'                => '1',
	        'hide_if_no_bio'         => 'no',
	        'multiauthor_box_layout' => 'default',
	        'show_related'           => '0',
	        'related_order_by'       => 'date',
	        'related_order'          => 'asc',
	        'related_items'          => '4',
	        'related_post_type_post' => '1',
	        'related_post_type_page' => '1',
	        'show_facebook'          => '1',
	        'show_twitter'           => '1',
	        'show_linkedin'          => '1',
	        'show_googleplus'        => '1',
	        'show_youtube'           => '1',
	        'show_pinterest'         => '1',
	        'show_tumblr'            => '0',
	        'show_instagram'         => '1',
	        'show_slideshare'        => '1',
	        'show_xing'              => '0',
	        'show_renren'            => '0',
	        'show_vk'                => '0',
	        'show_flickr'            => '0',
	        'show_vine'              => '0',
	        'show_meetup'            => '0',
	        'show_weibo'             => '0',
	        'show_deviantart'        => '0',
	        'show_stumbleupon'       => '0',
	        'show_myspace'           => '0',
	        'show_yelp'              => '0',
	        'show_mixi'              => '0',
	        'show_soundcloud'        => '0',
	        'show_lastfm'            => '0',
	        'show_foursquare'        => '0',
	        'show_spotify'           => '0',
	        'show_vimeo'             => '0',
	        'show_dailymotion'       => '0',
	        'show_reddit'            => '0',
	        'show_skype'             => '0',
	        'show_livejournal'       => '0',
	        'show_taringa'           => '0',
	        'show_odnoklassniki'     => '0',
	        'show_askfm'             => '0',
	        'show_bebee'             => '0',
	        'show_goodreads'         => '0',
	        'show_periscope'         => '0',
	        'show_telegram'          => '0',
	        'show_livestream'        => '0',
	        'show_styleshare'        => '0',
	        'show_reverbnation'      => '0',
	        'show_everplaces'        => '0',
	        'show_eventbrite'        => '0',
	        'show_draugiemlv'        => '0',
	        'show_blogger'           => '0',
	        'show_patreon'           => '0',
	        'show_plurk'             => '0',
	        'show_buzzfeed'          => '0',
	        'show_slides'            => '0',
	        'show_deezer'            => '0',
	        'show_spreaker'          => '0',
	        'show_runkeeper'         => '0',
	        'show_medium'            => '0',
	        'show_speakerdeck'       => '0',
	        'show_teespring'         => '0',
	        'show_kaggle'            => '0',
	        'show_houzz'             => '0',
	        'show_gumtree'           => '0',
	        'show_upwork'            => '0',
	        'show_superuser'         => '0',
	        'show_bandcamp'          => '0',
	        'show_glassdoor'         => '0',
	        'show_toptal'            => '0',
	        'show_ifixit'            => '0',
	        'show_stitcher'          => '0',
	        'show_storify'           => '0',
	        'show_readthedocs'       => '0',
	        'show_ello'              => '0',
	        'show_tinder'            => '0',
	        'show_github'            => '0',
	        'show_stackoverflow'     => '0',
	        'show_jsfiddle'          => '0',
	        'show_twitch'            => '0',
	        'show_whatsapp'          => '0',
	        'show_tripadvisor'       => '0',
	        'show_wikipedia'         => '0',
	        'show_500px'             => '0',
	        'show_mixcloud'          => '0',
	        'show_viadeo'            => '0',
	        'show_quora'             => '0',
	        'show_etsy'              => '0',
	        'show_codepen'           => '0',
	        'show_coderwall'         => '0',
	        'show_behance'           => '0',
	        'show_coursera'          => '0',
	        'show_googleplay'        => '0',
	        'show_itunes'            => '0',
	        'show_angellist'         => '0',
	        'show_amazon'            => '0',
	        'show_ebay'              => '0',
	        'show_paypal'            => '0',
	        'show_digg'              => '0',
	        'show_dribbble'          => '0',
	        'show_dropbox'           => '0',
	        'show_scribd'            => '0',
            // WP Customizer.
            'layout'                   => 'slim',
	        'position'                 => 'below',
            'order'                    => 11,
            'box_width'                => '100',
            'box_margin'               => '20',
            'box_shadow'               => 'right',
            'box_border'               => 'all',
            'box_border_style'         => 'solid',
            'box_border_width'         => '2',
            'box_border_color'         => '#a5a5a5',
            'box_background'           => '#e6e6e6',
            'show_headline'            => '0',
            'headline_text_size'       => '18',
            'headline_text_color'      => 'inherit',
            'headline_text_align'      => 'left',
            'headline_text_style'      => '',
            'headline_text_case'       => 'none',
	        'tabs_position'            => 'full',
	        'tabs_background'          => '#000000',
	        'tabs_color'               => '#f4f4f4',
	        'tabs_active_color'        => '#e6e6e6',
	        'tabs_border'              => 'top',
	        'tabs_border_style'        => 'solid',
	        'tabs_border_width'        => '4',
	        'tabs_border_color'        => 'orange',
	        'tabs_text_color'          => 'inherit',
	        'profile_layout'           => 'layout-1',
	        'bottom_background_color'  => 'inherit',
	        'bottom_border_style'      => 'solid',
	        'bottom_border_width'      => '1',
	        'bottom_border_color'      => '#B6B6B6',
	        'avatar_style'             => 'none',
            'avatar_border_style'      => 'solid',
            'avatar_border_width'      => '1',
            'avatar_border_color'      => 'inherit',
            'avatar_default_img'       => 'mm',
            'acronym_text_color'       => '#dd9933',
	        'acronym_bg_color'         => '#000000',
            'name_link_to_archive'     => '1',
            'name_text_size'           => '16',
            'name_text_style'          => '',
            'name_text_case'           => 'none',
            'name_text_color'          => 'inherit',
            'name_inherited_underline' => 'keep',
            'meta_text_size'           => '11',
	        'meta_text_style'          => '',
	        'meta_text_case'           => 'none',
            'meta_text_color'          => 'inherit',
	        'meta_separator'           => '|',
            'bio_text_size'            => '14',
            'bio_text_color'           => 'inherit',
            'bio_text_align'           => 'justify',
            'bio_text_style'           => '',
            'show_icons'               => '1',
            'icons_size'               => '20',
            'icons_color'              => '#999999',
            'icons_style'              => 'default',
	        'related_layout'           => 'layout-1',
	        'related_text_size'        => '14',
	        'related_text_style'       => '',
	        'related_text_color'       => 'inherit',
        );

	    // Default byline values.
	    $default_byline_settings = array
	    (
		    'byline_automatic_integration'      => '1',
		    'byline_multiauthor_display'        => '1',
		    'byline_multiauthor_separator'      => '',
		    'byline_multiauthor_last_separator' => '',
		    'byline_multiauthor_link'           => 'disable',
		    'byline_modifier_before'            => '',
		    'byline_modifier_after'             => '',
	    );

	    // Default guest values.
	    $default_guest_settings = array
	    (
		    'enable_guest_authors_feature' => '1',
		    'include_guests_in_search'     => '0',
		    'guest_menu_item_level'        => 'top',
	    );

	    // Default guest values.
	    $default_archives_settings = array
	    (
		    'guest_archive_enabled'   => ( molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ) ? '1' : '0' ),
		    'guest_archive_tmpl'      => '',
		    'guest_archive_permalink' => '',
		    'guest_archive_base'      => 'author',
		    'user_archive_enabled'    => '1',
		    'user_archive_tmpl'       => '',
		    'user_archive_base'       => 'author',
	    );

	    // Default string values.
	    $default_string_settings = array
	    (
	    	// Headline
		    'headline'         => __( 'About the author', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
		    // Meta
		    'at'               => __( 'at', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
		    'web'              => __( 'Website', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
		    'more_posts'       => __( '+ posts', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
		    'bio'              => __( 'Bio', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
		    // Tabbed layout
		    'about_the_author' => __( 'About the author', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
		    'related_posts'    => __( 'Related posts', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
		    // Stacked layout
		    'profile_title'    => __( 'Author profile', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
		    'related_title'    => __( 'Related entries', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
		    // Related
		    'no_related_posts' => __( 'This author does not have any more posts.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
	    );

	    // Default advanced values.
	    $default_advanced_settings = array
	    (
		    'extend_to_post'         => '1',
		    'extend_to_page'         => '1',
		    'enable_sc_text_widgets' => '0',
		    'keep_config'            => '1',
		    'keep_data'              => '1',
		    'hide_elements'          => '',
		    'add_nofollow'           => '0',
		    'add_opengraph_meta'     => '1',
		    'add_google_meta'        => '1',
		    'add_facebook_meta'      => '1',
		    // TODO: Any setting added here, have to be also added into 'fw/config/settings.php' and 'fw/admin/fw-class-admin.php' files.
	    );

	    // Merge settings and update DB.
	    $update = array_merge( $default_box_settings, $tab_box );
	    update_option( MOLONGUI_AUTHORSHIP_BOX_SETTINGS, $update );

        $update = array_merge( $default_byline_settings, $tab_byline );
        update_option( MOLONGUI_AUTHORSHIP_BYLINE_SETTINGS, $update );

	    $update = array_merge( $default_guest_settings, $tab_guest );
	    update_option( MOLONGUI_AUTHORSHIP_GUEST_SETTINGS, $update );

	    $update = array_merge( $default_archives_settings, $tab_archives );
	    update_option( MOLONGUI_AUTHORSHIP_ARCHIVES_SETTINGS, $update );

        $update = array_merge( $default_advanced_settings, $tab_advanced );
        update_option( MOLONGUI_AUTHORSHIP_ADVANCED_SETTINGS, $update );

	    $update = array_merge( $default_string_settings, $tab_strings );
	    update_option( MOLONGUI_AUTHORSHIP_STRING_SETTINGS, $update );
    }

	/**
	 * Box tab data validation.
	 *
	 * Sanitizes and validates data submitted using box tab form.
	 *
	 * @access   public
	 * @since    1.3.0
	 * @version  3.0.0
	 */
	function validate_box_tab( $input )
	{
		// DEBUG: Check involved data.
		// molongui_debug( array( $_REQUEST, $input ), true, false, true );

		// Load saved settings.
		$box_settings = (array) get_option( MOLONGUI_AUTHORSHIP_BOX_SETTINGS );

		// Save plugin version (useful on plugin updates).
		$input['plugin_version'] = MOLONGUI_AUTHORSHIP_VERSION;

		/**
		 * Handle unchecked checkboxes.
		 *
		 * Unchecked checkboxes doesn't have any value, so it must be equaled to 0 in order to override default values.
		 */
		// Related post-types.
		if ( !isset( $input['related_post_type_post'] ) ) $input['related_post_type_post'] = '0';
		if ( !isset( $input['related_post_type_page'] ) ) $input['related_post_type_page'] = '0';
		// Social media checkboxes.
		if ( !isset( $input['show_tw'] ) ) $input['show_tw'] = '0';
		if ( !isset( $input['show_fb'] ) ) $input['show_fb'] = '0';
		if ( !isset( $input['show_in'] ) ) $input['show_in'] = '0';
		if ( !isset( $input['show_gp'] ) ) $input['show_gp'] = '0';
		if ( !isset( $input['show_yt'] ) ) $input['show_yt'] = '0';
		if ( !isset( $input['show_pi'] ) ) $input['show_pi'] = '0';
		if ( !isset( $input['show_tu'] ) ) $input['show_tu'] = '0';
		if ( !isset( $input['show_ig'] ) ) $input['show_ig'] = '0';
		if ( !isset( $input['show_ss'] ) ) $input['show_ss'] = '0';
		if ( !isset( $input['show_xi'] ) ) $input['show_xi'] = '0';
		if ( !isset( $input['show_re'] ) ) $input['show_re'] = '0';
		if ( !isset( $input['show_vk'] ) ) $input['show_vk'] = '0';
		if ( !isset( $input['show_fl'] ) ) $input['show_fl'] = '0';
		if ( !isset( $input['show_vi'] ) ) $input['show_vi'] = '0';
		if ( !isset( $input['show_me'] ) ) $input['show_me'] = '0';
		if ( !isset( $input['show_we'] ) ) $input['show_we'] = '0';
		if ( !isset( $input['show_de'] ) ) $input['show_de'] = '0';
		if ( !isset( $input['show_st'] ) ) $input['show_st'] = '0';
		if ( !isset( $input['show_my'] ) ) $input['show_my'] = '0';
		if ( !isset( $input['show_ye'] ) ) $input['show_ye'] = '0';
		if ( !isset( $input['show_mi'] ) ) $input['show_mi'] = '0';
		if ( !isset( $input['show_so'] ) ) $input['show_so'] = '0';
		if ( !isset( $input['show_la'] ) ) $input['show_la'] = '0';
		if ( !isset( $input['show_fo'] ) ) $input['show_fo'] = '0';
		if ( !isset( $input['show_sp'] ) ) $input['show_sp'] = '0';
		if ( !isset( $input['show_vm'] ) ) $input['show_vm'] = '0';
		if ( !isset( $input['show_dm'] ) ) $input['show_dm'] = '0';
		if ( !isset( $input['show_rd'] ) ) $input['show_rd'] = '0';

		// Force default option for premium settings.
		if( !$this->is_premium )
		{
			/**
			 * Validates Premium settings available from the plugin settings page: Molongui > Authorship settings > Author box.
			 */
			$input['related_order_by']       = 'date';
			$input['related_order']          = 'asc';
			$input['related_post_type_post'] = '1';
			$input['related_post_type_page'] = '1';

			/**
			 * Validates Premium settings available only in WP Customizer.
			 *
			 * WP Customizer has 3 methods to validate and sanitize input data ('validate_callback', 'sanitize_callback' and 'sanitize_js_callback').
			 * Those methods are executed when the "Publish" button is clicked or when the page is refreshed by a setting with 'transport' property
			 * set to 'refresh'. Being so, 'validate_callback' and/or 'sanitize_callback' can be used with Premium 'postMessage' settings but not
			 * with the 'refresh' ones, because validation would prevent the live preview of the premium setting, reverting its value to the previous
			 * stored one or to its default.
			 *
			 * Not sure why, the 'sanitize_callback' function ('validate_tab_xxxx') given to the 'register_setting' function is executed when the
			 * "Publish" button (at the WP Customizer) is clicked, so this function can be used to validate Premium 'refresh' settings available in
			 * the WP Customizer.
			 *
			 * @since   2.1.0
			 * @version 3.0.0
			 */
			$customizer_premium_refresh_settings = array
			(
				'profile_layout' => array
				(
					'accepted' => array( 'layout-1' ),
					'default'  => 'layout-1',
				),
				'related_layout' => array
				(
					'accepted' => array( 'layout-1', 'layout-2' ),
					'default'  => 'layout-1',
				),
				'avatar_default_img' => array
				(
					'accepted' => array( 'blank', 'mm' ),
					'default'  => 'mm',
				),
			);
			foreach ( $customizer_premium_refresh_settings as $setting => $params )
			{
				if ( !in_array( $input[$setting], $params['accepted'] ) )
				{
					// Revert to previous stored value if it exists and it is an accepted one. Revert to the default one otherwise.
					if ( isset( $box_settings[$setting] ) and !empty( $box_settings[$setting] ) and in_array( $box_settings[$setting], $params['accepted'] ) ) $input[$setting] = $box_settings[$setting];
					else $input[$setting] = $params['default'];
				}
			}
		}

		/**
		 * Handles those settings stored into MOLONGUI_AUTHORSHIP_BOX_SETTINGS key that are only available on the WP Customizer.
		 *
		 * Using the same database key to store some settings only accessible from the WP Customizer and some other settings only available
		 * from the plugin settings page, requires to handle the value of those only available on the WP Customizer when saving the settings
		 * from the plugin settings page. If no handle is carried out, then default values would overwrite those set on the WP Customizer.
		 *
		 * @since   2.1.0
		 * @version 3.1.0
		 */
		$customizer_settings = array
		(
			'layout',
			'position',
			'order',
			'show_headline',
			'show_icons',
			'box_margin',
			'box_width',
			'box_border',
			'box_border_style',
			'box_border_width',
			'box_border_color',
			'box_background',
			'box_shadow',
			'headline_text_size',
			'headline_text_style',
			'headline_text_case',
			'headline_text_align',
			'headline_text_color',
			'tabs_position',
			'tabs_background',
			'tabs_color',
			'tabs_active_color',
			'tabs_border',
			'tabs_border_style',
			'tabs_border_width',
			'tabs_border_color',
			'tabs_text_color',
			'profile_layout',
			'bottom_background_color',
			'bottom_border_style',
			'bottom_border_width',
			'bottom_border_color',
			'avatar_style',
			'avatar_border_style',
			'avatar_border_width',
			'avatar_border_color',
			'avatar_default_img',
			'acronym_text_color',
			'acronym_bg_color',
			'name_link_to_archive',
			'name_text_size',
			'name_text_style',
			'name_text_case',
			'name_inherited_underline',
			'name_text_color',
			'meta_text_size',
			'meta_text_style',
			'meta_text_case',
			'meta_text_color',
			'meta_separator',
			'bio_text_size',
			'bio_text_style',
			'bio_text_align',
			'bio_text_color',
			'icons_style',
			'icons_size',
			'icons_color',
			'related_layout',
			'related_text_size',
			'related_text_style',
			'related_text_color',
		);
		foreach ( $customizer_settings as $customizer_setting )
		{
			if ( !isset( $input[$customizer_setting] ) ) $input[$customizer_setting] = $box_settings[$customizer_setting];
		}

		/**
		 * Avoid empty strings being saved as labels.
		 *
		 * @since   3.0.0
		 * @version 3.0.0
		 */
		$customizer_strings = array
		(
			'headline'         => __( 'About the author', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'about_the_author' => __( 'About the author', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'related_posts'    => __( 'Related posts', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'profile_title'    => __( 'Author profile', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'at'               => __( 'at', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'web'              => __( 'Web', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'more_posts'       => __( '+ posts', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'bio'              => __( 'Bio', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'related_title'    => __( 'Related posts', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
			'no_related_posts' => __( 'This author does not have any more posts.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ),
		);
		foreach ( $customizer_strings as $customizer_string => $default )
		{
			if ( !isset( $input[$customizer_string] ) or empty( $input[$customizer_string] ) ) $input[$customizer_string] = $default;
		}

		// Return sanitized and validated data.
		return $input;
	}

    /**
     * Byline tab data validation.
     *
     * Sanitizes and validates data submitted using the byline tab form.
     *
     * @access  public
     * @since   2.1.0
     * @version 2.1.0
     */
    function validate_byline_tab( $input )
    {
        // DEBUG: Check involved data.
        // molongui_debug( array( $_REQUEST, $input ), true, false, true );

        // Save plugin version (useful on plugin updates).
        $input['plugin_version'] = MOLONGUI_AUTHORSHIP_VERSION;

	    // Force default option for premium settings.
        if ( !$this->is_premium )
        {
	        // TODO: Add here any validation for premium settings/options configurable from the plugin settings page: $input['setting_name'] = 'default_value';
        }

        // Return validated data.
        return $input;
    }

    /**
     * Labels tab data validation.
     *
     * Sanitizes and validates data submitted using the labels tab form.
     *
     * @access  public
     * @since   1.3.0
     * @version 1.3.0
     */
/*    function validate_strings_tab( $input )
    {
        // DEBUG: Check involved data.
        // molongui_debug( array( $_REQUEST, $input ), true, false, true );

        // Save plugin version (useful on plugin updates).
        $input['plugin_version'] = MOLONGUI_AUTHORSHIP_VERSION;

        return $input;
    }
*/
    /**
     * Guest tab data validation.
     *
     * Sanitizes and validates data submitted using the guest tab form.
     *
     * @access  public
     * @since   2.1.0
     * @version 2.1.0
     */
   function validate_guest_tab( $input )
    {
        // DEBUG: Check involved data.
        // molongui_debug( array( $_REQUEST, $input ), true, false, true );

        // Save plugin version (useful on plugin updates).
        $input['plugin_version'] = MOLONGUI_AUTHORSHIP_VERSION;

	    // Force default option for premium settings.
	    if ( !$this->is_premium )
	    {
		    $input['guest_menu_item_level'] = 'top';
	    }

	    // Handle mandatory fields.
	    // TODO when required.

        return $input;
    }

    /**
     * Archives tab data validation.
     *
     * Sanitizes and validates data submitted using the authors archives tab form.
     *
     * @access  public
     * @since   2.1.0
     * @version 2.1.0
     */
   function validate_archives_tab( $input )
    {
        // DEBUG: Check involved data.
        // molongui_debug( array( $_REQUEST, $input ), true, false, true );

        // Save plugin version (useful on plugin updates).
        $input['plugin_version'] = MOLONGUI_AUTHORSHIP_VERSION;

	    // Force default option for premium settings.
	    if( !$this->is_premium )
	    {
		    $input['guest_archive_enabled']   = '0';
		    $input['guest_archive_tmpl']      = '';
		    $input['guest_archive_permalink'] = '';
		    $input['guest_archive_base']      = 'author';
		    $input['user_archive_enabled']    = '1';
		    $input['user_archive_tmpl']       = '';
		    $input['user_archive_permalink']  = '';
		    $input['user_archive_base']       = 'author';
	    }

	    // Handle mandatory fields.
	    if ( !isset( $input['guest_archive_base'] ) ) $input['guest_archive_base'] = 'author';

        return $input;
    }

	/**
	 * Returns a link to the customizer opening it on the last created post.
	 *
	 * @see     https://gist.github.com/slushman/6f08885853d4a7ef31ebceafd9e0c180
	 *
	 * @return  string  Link to the customizer.
	 * @since   2.1.0
	 * @version 3.1.2
	 */
	static function get_customizer_link()
	{
		$customizer_panel = MOLONGUI_AUTHORSHIP_SLUG;
		$latest_post_url  = wp_get_recent_posts( array( 'numberposts' => 1, 'meta_key' => '_molongui_author_box_display', 'meta_value' =>'show', ) );

		// If there is no post displaying the default author box, return link to plugin settings panel.
		if ( !$latest_post_url or empty( $latest_post_url ) ) return admin_url( 'customize.php?autofocus[panel]='.$customizer_panel );

		// If there is a post displaying the default author box, return link to plugin settings panel displaying that post.
		return admin_url( 'customize.php?autofocus[panel]='.$customizer_panel.'&url='.get_permalink( $latest_post_url[0]['ID'] ) );
	}

	/**
	 * Returns whether "Guest archives" section should be displayed.
	 *
	 * @return  boolean  True if the section must be displayed. False otherwise.
	 * @since   2.1.0
	 * @version 2.1.0
	 */
	static function display_guest_archives_section()
	{
		// Load saved settings.
		$guest_settings = (array) get_option( MOLONGUI_AUTHORSHIP_GUEST_SETTINGS );

		// Return data.
		return ( ( isset( $guest_settings['enable_guest_authors_feature'] ) and $guest_settings['enable_guest_authors_feature'] ) ? true : false );
	}

}