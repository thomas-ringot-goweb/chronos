<?php

namespace Molongui\Fw\Includes;

// Deny direct access to this file
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Class Sysinfo.
 *
 * @author     Amitzy
 * @package    Molongui Common Framework (/fw)
 * @subpackage /fw/includes
 * @since      1.0.0
 * @version    1.2.2
 */
if ( !class_exists( 'Molongui\Fw\Includes\Sysinfo' ) )
{
	class Sysinfo
	{
		/**
		 * Information of the plugin loading this class.
		 *
		 * @access  private
		 * @var     object
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		private $plugin;

		/**
		 * Initialize the class and set its properties.
		 *
		 * @access  public
		 *
		 * @param   string $plugin_id The id of the plugin loading this class.
		 *
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		public function __construct( $plugin_id )
		{
			// Initialize information of the plugin loading this class.
			molongui_get_plugin( $plugin_id, $this->plugin );
		}

		/**
		 * Renders System Information page.
		 *
		 * @since   1.0.0
		 * @version 1.2.2
		 */
		public function render_output()
		{
			// Get set of functions used to interact with database.
			global $wpdb;

			// Load theme stuff.
			include_once( ABSPATH . 'wp-admin/includes/theme-install.php' );
			$active_theme = wp_get_theme();

			// Load browser class.
			$browser = new \Molongui\Fw\Includes\Browser();

			// Get installed themes.
			$themes = wp_get_themes();

			// Get installed plugins.
			$plugins = get_plugins();

			// Get active plugins.
			$active_plugins = (array) get_option( 'active_plugins', array() );

			if ( is_multisite() )
			{
				$network_activated_plugins = array_keys( get_site_option( 'active_sitewide_plugins', array() ) );
				$active_plugins            = array_merge( $active_plugins, $network_activated_plugins );
			}

			// Render page.
			ob_start();
			include( molongui_get_constant( $this->plugin->id, 'DIR', true ) . 'admin/views/html-admin-tab-support.php' );

			return ob_get_clean();
		}

		/**
		 * Sends plain-text system info report to Molongui.
		 *
		 * @access  public
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		public function send_support_report()
		{
			// Check security (Die if invalid nonce).
			check_ajax_referer( 'molongui-js-nonce', 'security', true );

			// Leave if no data to send.
			if ( !is_admin() and !isset( $_POST['report'] ) ) return;

			// Get sender info.
			$current_user = wp_get_current_user();

			// Prepare headers.
			$subject = sprintf( __( 'MOLONGUI - Support report for %s', 'molongui-common-framework' ), $_POST['domain'] );
			$headers = array(
				'From: ' . $current_user->display_name . ' <' . 'noreply@' . $_POST['domain'] . '>',
				'Reply-To: ' . $current_user->display_name . ' <' . $current_user->user_email . '>',
				'Content-Type: ' . 'text/plain; charset=UTF-8',
			);

			// Sent report.
			$sent = wp_mail( molongui_get_constant( $this->plugin->id, 'SUPPORT_MAIL', true ), $subject, $_POST['report'], $headers );

			// Return result.
			echo( $sent ? 'sent' : 'error' );

			// Avoid 'admin-ajax.php' to append the value outputted with a "0".
			wp_die();
		}

	}
}