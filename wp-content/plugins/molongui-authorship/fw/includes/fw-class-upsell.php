<?php

namespace Molongui\Fw\Includes;

// Deny direct access to this file
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * The Molongui plugins class.
 *
 * This is used to get the list of all plugins developed by Molongui.
 *
 * @author     Amitzy
 * @package    Molongui Common Framework (/fw)
 * @subpackage /fw/includes
 * @since      1.0.0
 * @version    1.0.0
 */
if ( !class_exists( 'Molongui\Fw\Includes\Upsell' ) )
{
	class Upsell
	{
		/**
		 * Information of the plugin loading this class.
		 *
		 * @access  private
		 * @var     object
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		private $plugin;

		/**
		 * Used to enable/disable debug mode.
		 *
		 * @var   boolean
		 * @since 1.0.0
		 */
		private static $debug = false;

		/**
		 * Class constructor.
		 *
		 * @access  public
		 *
		 * @param   string $plugin_id Name of the plugin loading this class.
		 *
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		public function __construct( $plugin_id )
		{
			// Initialize information of the plugin loading this class.
			molongui_get_plugin( $plugin_id, $this->plugin );
		}

		/**
		 * Handles output of the upsells in admin.
		 *
		 * Data to display is gotten from a JSON file on a remote server. JSON data structure is shown below and can be checked at http://jsoneditoronline.org/.
		 *
		 * {
		 *  "all": {
		 *      "plugin-name": {
		 *          "id": "plugin-name",
		 *          "link": "http://molongui.amitzy.com/plugins/plugin-name",
		 *          "price": "123.00",
		 *          "name": "Plugin Name",
		 *          "image": "http://molongui.amitzy.com/plugins/img/banner_en_US.png",
		 *          "excerpt": "Plugin short description in English.",
		 *          "name_es_ES": "Nombre en castellano",
		 *          "image_es_ES": "http://molongui.amitzy.com/plugins/img/banner_es_ES.png",
		 *          "excerpt_es_ES": "Breve descripci&oacute;n del plugin en castellano."
		 *      }
		 *  },
		 *  "featured": {},
		 *  "popular": {},
		 *  "free": {},
		 *  "premium": {}
		 * }
		 *
		 * Images size must be 300x163px.
		 *
		 * @access  public
		 *
		 * @param   string $category  The category to show plugins from.
		 * @param   mixed  $num_items The number of featured plugins to show.
		 * @param   int    $num_words Number of words to use as plugin description.
		 * @param   string $more      Text to add when ellipsing plugin description.
		 *
		 * @since   1.0.0
		 * @version 1.2.2
		 */
		public function output( $category = 'all', $num_items = 'all', $num_words = 36, $more = null )
		{
			// Load configuration file.
			$config = include molongui_get_constant( $this->plugin->id, 'DIR', true ) . "config/upsell.php";

			// Premium plugins download data from Molongui server.
			if ( $this->plugin->is_premium )
			{
				// If cached data, don't download it again.
				if ( false === ( $upsells = get_site_transient( 'molongui_sw_data' ) ) )
				{
					// Get data from remote server.
					$upsell_json = wp_safe_remote_get( $config['server']['url'], array( 'user-agent' => $config['server']['agent'] ) );

					if ( ! is_wp_error( $upsell_json ) )
					{
						// Decode data to a stdClass object.
						$upsells = json_decode( wp_remote_retrieve_body( $upsell_json ) );

						// Store data (cache) for future uses (within this week time).
						if ( $upsells )
							set_site_transient( 'molongui_sw_data', $upsells, WEEK_IN_SECONDS );
					}
					else
					{
						// Get data from local file.
						$upsell_json = file_get_contents( $config['local']['url'] );

						// Set correct local path.
						$upsell_json = str_replace( '%%MOLONGUI_PLUGIN_URL%%', $this->plugin->url, $upsell_json );

						// Decode data to a stdClass object.
						$upsells = json_decode( $upsell_json, false );
					}
				}
			}
			// Free plugins get data from local file. Downloading it would be banned by WP rules.
			else
			{
				// Get data from local file.
				if ( file_exists( $config['local']['url'] ) )
				{
					// Get data.
					$upsell_json = file_get_contents( $config['local']['url'] );

					// Set correct local path.
					$upsell_json = str_replace( '%%MOLONGUI_PLUGIN_URL%%', $this->plugin->url, $upsell_json );

					// Decode data to a stdClass object.
					$upsells = json_decode( $upsell_json, false );
				}
			}

			// Leave if no data to show.
			if ( ! isset( $upsells ) or empty( $upsells ) ) return;

			// Check data before displaying anything.
			/**
			 * Single line typecasting does not work in PHP <= 5.4, so the following
			 * cannot be used to keep backward compatibility:
			 *
			 * if ( !empty( (array)$upsells->{$category} ) )
			 */
			$tmp = (array) $upsells->{$category};
			if ( ! empty( $tmp ) )
			{
				// Avoid current plugin to be displayed.
				if ( isset( $upsells->{$category}->{$this->plugin->id} ) ) unset( $upsells->{$category}->{$this->plugin->id} );

				// Slice array so just $num_items are displayed.
				if ( isset( $num_items ) && ( $num_items != 'all' ) && ( $num_items > 0 ) ) $upsells->{$category} = array_slice( (array) $upsells->{$category}, 0, $num_items );

				// Debug results on development.
				if ( self::$debug ) molongui_debug( $upsells );

				// Display data.
				require_once molongui_get_constant( $this->plugin->id, 'DIR', true ) . 'admin/views/html-admin-part-upsells.php';
			}
		}

		/**
		 * Returns whether there are local upsells to show.
		 *
		 * @access  public
		 * @return  boolean     Whether there are upsells to show.
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		public function empty_upsells()
		{
			$empty_upsells = true;

			// Load configuration file.
			$config = include molongui_get_constant( $this->plugin->id, 'DIR', true ) . "config/upsell.php";

			// Get data from local file.
			if ( file_exists( $config['local']['url'] ) )
			{
				// Get data.
				$upsell_json = file_get_contents( $config['local']['url'] );

				// Set correct local path.
				$upsell_json = str_replace( '%%MOLONGUI_PLUGIN_URL%%', $this->plugin->url, $upsell_json );

				// Decode data to an array.
				$upsells = json_decode( $upsell_json, true );

				// Check if empty subarray.
				$empty_upsells = empty( $upsells['all'] ) ? true : false;
			}

			// Return check result.
			return $empty_upsells;
		}

		/**
		 * Returns an array of installed Molongui plugins.
		 *
		 * @see     https://developer.wordpress.org/reference/functions/get_plugins/
		 *          http://php.net/manual/es/function.array-filter.php
		 *
		 * @access  public
		 * @param   string  $field  Fields to return. Default to 'all'. Options: { Name | PluginURI | Version | Description | Author | AuthorURI | TextDomain | DomainPath | Network | Title | AuthorName }.
		 * @return  array           Installed Molongui plugins.
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		public function get_installed_molongui_plugins( $field = 'all' )
		{
			// Check if get_plugins() function exists. This is required on the front end of the
			// site, since it is in a file that is normally only loaded in the admin.
			if ( ! function_exists( 'get_plugins' ) ) require_once ABSPATH . 'wp-admin/includes/plugin.php';

			// Get all installed plugins.
			$all_plugins = get_plugins();

			// Get only installed Molongui plugins.
			$molongui_plugins = array_filter( $all_plugins, function( $value, $key )
			{
				// Copy $value (plugin info) if 'Author' field equals to 'Amitzy'.
				return ( $value['Author'] == 'Amitzy' );
			}, ARRAY_FILTER_USE_BOTH);

			// Filter data to return.
			if ( $field != 'all' )
			{
				$data = array();
				foreach ( $molongui_plugins as $plugin )
				{
					$data[] = $plugin[$field];
				}
				$molongui_plugins = $data;
			}

			// Return data.
			return $molongui_plugins;
		}
	}
}