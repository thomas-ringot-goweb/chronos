<?php

namespace Molongui\Fw\Includes;

use Molongui\Fw\Includes\Upsell;

// Deny direct access to this file
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Class Settings.
 *
 * Displays a settings page at the admin area.
 *
 * @author     Amitzy
 * @package    Molongui Common Framework (/fw)
 * @subpackage /fw/includes
 * @since      1.0.0
 * @version    1.2.5
 */
if ( !class_exists( 'Molongui\Fw\Includes\Settings' ) )
{
	class Settings
	{
		/**
		 * Information of the plugin loading this class.
		 *
		 * @access  private
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		private $plugin;

		/**
		 * The URI slugs of each tab of the admin settings page.
		 *
		 * @access   private
		 * @var      string
		 * @since    1.0.0
		 * @version  1.2.2
		 */
		private $slug_tab_more = 'more';

		/**
		 * Class constructor.
		 *
		 * @access  public
		 * @param   string  $plugin_id      Name of the plugin loading this class.
		 * @param   array   $tabs           Settings page tabs.
		 * @param   string  $default_tab    Default tab to display.
		 * @since   1.0.0
		 * @version 1.2.2
		 */
		public function __construct( $plugin_id, $tabs, $default_tab = '' )
		{
			// Initialize information of the plugin loading this class.
			molongui_get_plugin( $plugin_id, $this->plugin );

			// Initialize tab information.
            if ( $default_tab == '' )
            {
                reset( $tabs );
	            $default_tab = key( $tabs );
            }
			$this->tabs        = $tabs;
			$this->default_tab = strtolower( $plugin_id ) . '_' . $default_tab;

			// Unset "More" tab if no upsells to show.
			if ( !class_exists( 'Molongui\Fw\Includes\Upsell' ) ) require_once( molongui_get_constant( $this->plugin->id, 'DIR', true ) . 'includes/fw-class-upsell.php' );
			$upsell = new Upsell( $plugin_id );
			if ( $upsell->empty_upsells() ) array_pop( $this->tabs );
		}

		/**
		 * Adds a link to the theme settings page into the 'themes' menu.
		 *
		 * This function registers the menu link to the settings page and the settings page itself.
		 *
		 * @see     https://codex.wordpress.org/Function_Reference/add_menu_page
		 * @see     https://codex.wordpress.org/Function_Reference/add_submenu_page
		 * @see     http://wordpress.stackexchange.com/a/119284
		 * @see     http://wordpress.stackexchange.com/a/66499
		 * @see     http://wordpress.stackexchange.com/a/182739
		 * @see     http://wordpress.stackexchange.com/a/182739
		 * @access  public
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		public function add_menu_item()
		{
			// Add "Molongui" menu item if it does not exist already.
			if ( empty ( $GLOBALS['admin_page_hooks']['molongui'] ) )
			{
				// Add top-level menu item.
				add_menu_page( __( 'Molongui', 'molongui-common-framework' ), __( 'Molongui', 'molongui-common-framework' ), 'manage_options', 'molongui', array( $this, 'render_page_plugins' ), molongui_get_base64_svg( $this->get_admin_menu_icon() ), '59.1' );

				// Rename default submenu item.
				add_submenu_page( 'molongui', __( 'Plugins', 'molongui-common-framework' ), __( 'Plugins', 'molongui-common-framework' ), 'manage_options', 'molongui', array( $this, 'render_page_plugins' ) );

				// Add 'About' item.
				add_submenu_page( 'molongui', __( 'About', 'molongui-common-framework' ), __( 'About', 'molongui-common-framework' ), 'manage_options', 'molongui-about', array( $this, 'render_page_about' ) );

				// Add 'Support' item.
				add_submenu_page( 'molongui', __( 'Support', 'molongui-common-framework' ), __( 'Support', 'molongui-common-framework' ), 'manage_options', 'molongui-support', array( $this, 'render_page_support' ) );

				// Add a separator after added menu item.
				$this->add_admin_menu_separator('59.2');
			}

			// Add plugin submenu item.
			add_submenu_page( 'molongui', ucfirst( $this->plugin->name ).' Settings', ucfirst( $this->plugin->id ).' settings', 'manage_options', $this->plugin->slug, array( $this, 'render_page_plugin_settings' ) );

			// Sort submenu items.
			$this->reorder_submenu_items();
		}

		/**
		 * Returns SVG encoded admin menu item's icon.
		 *
		 * @access  private
		 * @return  string      SVG encoded admin menu item's icon.
		 * @since   1.0.0
		 * @version 1.0.0
		 *
		 */
		private function get_admin_menu_icon()
		{
			return '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					 viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
						<g>
							<path d="M50,0C22.4,0,0,22.4,0,50c0,27.6,22.4,50,50,50s50-22.4,50-50C100,22.4,77.6,0,50,0z M27.8,66.3v0.4
								c-0.1,1.4-0.6,2.5-1.5,3.4c-1,0.9-2.1,1.4-3.5,1.4c-1.3,0-2.5-0.5-3.4-1.4c-1-0.9-1.5-2.1-1.5-3.4v-35c0.1-1.4,0.6-2.5,1.6-3.4
								c0.9-0.9,2.1-1.4,3.5-1.4c1.3,0,2.5,0.5,3.4,1.4c0.9,0.9,1.5,2.1,1.6,3.4V66.3z M81.9,66.5c0,1.4-0.5,2.6-1.5,3.5
								c-1,1-2.2,1.4-3.6,1.4c-1.4,0-2.6-0.5-3.6-1.5c-1-1-1.4-2.2-1.4-3.4v-19c0-1.2-0.1-2.5-0.3-3.8c-0.2-1.3-0.6-2.5-1.1-3.6
								c-0.6-1.1-1.4-2-2.5-2.7c-1.1-0.7-2.5-1.1-4.4-1.1c-1.8,0-3.3,0.3-4.4,1c-1.1,0.7-2,1.5-2.6,2.6c-0.6,1-1.1,2.2-1.3,3.5
								c-0.2,1.3-0.4,2.6-0.4,3.8v19c0,1.4-0.5,2.7-1.4,3.7c-0.9,1-2.1,1.6-3.7,1.6c-1.4,0-2.6-0.5-3.5-1.4c-1-1-1.4-2.1-1.4-3.5V47.2
								c0-1.2-0.1-2.4-0.3-3.7c-0.2-1.3-0.6-2.5-1.2-3.5c-0.6-1-1.4-1.9-2.5-2.6c-1.1-0.7-2.5-1-4.2-1c-1.5,0-2.8,0.3-3.8,0.8
								c-1,0.5-1.9,1.2-2.6,1.9v-9c1.1-0.8,2.4-1.5,3.9-2.2c1.5-0.6,3.3-1,5.4-1c1.1,0,2.2,0.1,3.4,0.4c1.2,0.3,2.4,0.7,3.6,1.3
								c1.1,0.6,2.2,1.4,3.2,2.5s1.9,2.4,2.6,4c0.6-1.1,1.4-2.1,2.3-3.1c0.9-1,1.9-1.9,3-2.6c1.1-0.7,2.4-1.3,3.8-1.8
								c1.4-0.5,3-0.7,4.7-0.7c1.8,0,3.7,0.3,5.7,0.9c2,0.6,3.8,1.7,5.4,3.4c1,1,1.7,2,2.4,3c0.6,1,1.1,2.1,1.4,3.3
								c0.3,1.2,0.5,2.6,0.7,4.2c0.1,1.6,0.2,3.4,0.2,5.6V66.5z"/>
						</g>
					</svg>';
		}

		/**
		 * Adds a separator to the Backend WordPress menu.
		 *
		 * @see     https://tommcfarlin.com/add-a-separator-to-the-wordpress-menu/
		 * @access  private
		 * @param   $position   Where to insert the separator.
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		private function add_admin_menu_separator( $position )
		{
			global $menu;

			$menu[ $position ] = array(
				0	=>	'',
				1	=>	'read',
				2	=>	'separator' . $position,
				3	=>	'',
				4	=>	'wp-menu-separator'
			);
		}

		/**
		 * Sorts Molongui top-level menu items.
		 *
		 * @see     http://stackoverflow.com/a/1598385
		 * @access  private
		 * @since   1.0.0
		 * @version 1.1.0
		 */
		private function reorder_submenu_items()
		{
			global $submenu;

			// Sanity check.
			if ( !isset( $submenu['molongui'] ) or empty( $submenu['molongui'] ) ) return;

			// Sort submenu alphabetically.
			$titles = array();
			foreach ( $submenu['molongui'] as $items )
			{
				$titles[] = $items[0];
			}
			array_multisort( $titles, SORT_ASC, $submenu['molongui'] );

			// Copy common submenus (plguins, support, abouut...).
			foreach ( $submenu['molongui'] as $key => $value )
			{
				if ( $value[2] == 'molongui' )         { $plugins_key = $key; $plugins = $value; }
				if ( $value[2] == 'molongui-support' ) { $support_key = $key; $support = $value; }
				if ( $value[2] == 'molongui-about' )   { $about_key   = $key; $about   = $value; }
			}

			// Unset common submenus.
			unset( $submenu['molongui'][$plugins_key] );
			unset( $submenu['molongui'][$support_key] );
			unset( $submenu['molongui'][$about_key] );

			// Reset common submenus.
			array_unshift( $submenu['molongui'], $plugins );        // Set "plugins" submenu at the top of the list.
			array_push( $submenu['molongui'], $support, $about );   // Set "support" and "about" submenus at the bottom.
		}

		/**
		 * Renders Molongui dashboard page.
		 *
		 * @access  public
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		public function render_page_plugins()
		{
			include molongui_get_constant( $this->plugin->id, 'DIR', true ) . 'admin/views/html-admin-page-plugins.php';
		}

		/**
		 * Renders Molongui support page.
		 *
		 * @access  public
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		public function render_page_support()
		{
			include molongui_get_constant( $this->plugin->id, 'DIR', true ) . 'admin/views/html-admin-page-support.php';
		}

		/**
		 * Renders Molongui about page.
		 *
		 * @access  public
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		public function render_page_about()
		{
			include molongui_get_constant( $this->plugin->id, 'DIR', true ) . 'admin/views/html-admin-page-about.php';
		}

		/**
		 * Renders calling plugin's settings page.
		 *
		 * This function displays a tabbed settings page. In order to customize it, edit
		 * 'views/html-admin-page-settings.php' file. This function should not be modified.
		 *
		 * There are two HOOKS enabled within this function:
		 *
		 *      molongui_${plugin_id}_settings_before_submit_button
		 *      molongui_${plugin_id}_settings_after_submit_button
		 *
		 * @access  public
		 * @since   1.0.0
		 * @version 1.2.2
		 */
		public function render_page_plugin_settings()
		{
			// Get current tab.
			$current_tab = isset( $_GET['tab'] ) ? $_GET['tab'] : $this->default_tab;

			// Get plugin configuration.
			$config = include $this->plugin->dir."config/config.php";

			// Decide whether to show the sidebar.
			$current_tab_id  = str_replace( $this->plugin->id.'_', '', $current_tab );
			$display_sidebar = ( !$this->plugin->is_premium and isset( $config['admin_tabs']['no_sidebar'][$current_tab_id] ) and !$config['admin_tabs']['no_sidebar'][$current_tab_id] ? true : false );

			// Set main content class.
			$main_class = 'main' . ' ' . ( !$display_sidebar ? 'no-sidebar' : '' );

			?>

			<div id="molongui-settings" data-tab="<?php echo $current_tab_id; ?>" data-license="license-<?php echo ( $this->plugin->is_premium ? 'premium' : 'free' ); ?>" class="wrap molongui">

				<!-- Page title -->
				<h2>
					<?php _e( $this->plugin->name, '' ); ?>
					<span class="version"><?php echo $this->plugin->version; ?></span>
					<span class="version"><?php echo ( $this->plugin->is_premium ? __( 'Premium', 'molongui-common-framework' ) : __( 'Free', 'molongui-common-framework' ) ); ?></span>
				</h2>

				<!-- Display tabs -->
				<h2 class="nav-tab-wrapper">
					<?php
						foreach ( $this->tabs as $tab )
						{
							// Hide "License" tab on free version.
							if ( $tab['slug'] == $this->plugin->id.'_'.'license' and !$this->plugin->is_premium ) continue;

							// Hide tabs configured so.
							$tab_id = str_replace( $this->plugin->id.'_', '', $tab['slug'] );
							if ( isset( $config['admin_tabs']['hide'][$tab_id] ) and $config['admin_tabs']['hide'][$tab_id] ) continue;

							$active = $current_tab == $tab['slug'] ? 'nav-tab-active' : '';
							echo '<a class="nav-tab ' . $active . '" href="?page=' . $this->plugin->slug . '&tab=' . $tab['slug'] . '">' . $tab['label'] . '</a>';
						}
					?>
				</h2>

				<?php settings_errors(); ?>

				<?php do_action( 'molongui_'.$this->plugin->id.'_settings_before_submit_button', $current_tab ); ?>

				<!-- Main content -->
				<div class="<?php echo $main_class; ?>">
					<form id="molongui-settings-form" method="post" action="options.php">
						<?php wp_nonce_field( 'update-options' ); ?>
						<?php settings_fields( $current_tab ); ?>
						<?php $this->molongui_do_settings_sections( $current_tab ); ?>
						<?php
							// Show "Save changes" button if no hidden.
							if ( !isset( $config['admin_tabs']['no_save'][$current_tab_id] ) or
							     ( isset( $config['admin_tabs']['no_save'][$current_tab_id] ) and !$config['admin_tabs']['no_save'][$current_tab_id] ) )
							{
								submit_button();
							}
						?>
					</form>
				</div>

				<?php do_action( 'molongui_'.$this->plugin->id.'_settings_after_submit_button', $current_tab ); ?>

				<!-- Sidebar -->
				<?php
				if ( $display_sidebar )
				{
					switch ( $current_tab )
					{
						case $this->plugin->id.'_'.$this->slug_tab_more:

							include molongui_get_constant( $this->plugin->id, 'DIR', true ) . 'admin/views/html-admin-part-sidebar-rate.php';

						break;

						default:

							include molongui_get_constant( $this->plugin->id, 'DIR', true ) . 'admin/views/html-admin-part-sidebar-upsell.php';

						break;
					}
				}
				?>

			</div>
			<?php
		}

		/**
		 * Customized 'do_settings_sections' function to be able to call
		 * the customized 'molongui_do_settings_fields' function in order
		 * to add the tip's icon at the end of field's label.
		 *
		 * @access  private
		 * @param   string  $page   Current settings tab.
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		private function molongui_do_settings_sections( $page )
		{
			global $wp_settings_sections, $wp_settings_fields;

			if ( ! isset( $wp_settings_sections[$page] ) ) return;

			foreach ( (array) $wp_settings_sections[$page] as $section )
			{
				if ( $section['title'] ) echo "<h2>{$section['title']}</h2>\n";

				if ( $section['callback'] ) call_user_func( $section['callback'], $section );

				if ( !isset( $wp_settings_fields ) || !isset( $wp_settings_fields[$page] ) || !isset( $wp_settings_fields[$page][$section['id']] ) ) continue;

				echo '<table class="form-table">';
				$this->molongui_do_settings_fields( $page, $section['id'] );
				echo '</table>';
			}
		}

		/**
		 * Customized 'do_settings_fields' function to be able to add
		 * the tip's icon at the end of field's label.
		 *
		 * @access  private
		 * @param   string  $page       Current settings tab.
		 * @param   string  $section    Section to fill up.
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		private function molongui_do_settings_fields( $page, $section )
		{
			global $wp_settings_fields;

			if ( !isset( $wp_settings_fields[$page][$section] ) ) return;

			foreach ( (array) $wp_settings_fields[$page][$section] as $field )
			{
				$class = '';

				if ( !empty( $field['args']['class'] ) )
				{
					$class = ' class="' . esc_attr( $field['args']['class'] ) . '"';
				}

				echo "<tr{$class}>";

				if ( !empty( $field['args']['label_for'] ) )
				{
					echo '<th scope="row"><label for="' . esc_attr( $field['args']['label_for'] ) . '">' . $field['title'] . '</label></th>';
				}
				else
				{
					echo '<th scope="row">'
					     . $field['title']
					     . ( isset( $field['args']['field']['tip'] ) ? molongui_help_tip( $field['args']['field']['tip'] ) : "" )
					     . ( ( isset( $field['args']['field']['premium'] ) and !empty( $field['args']['field']['premium'] ) ) ? $this->molongui_premium_setting( $field['args']['field']['premium'] ) : "" )
					     . '</th>';
				}

				echo '<td>';
				call_user_func($field['callback'], $field['args']);
				echo '</td>';
				echo '</tr>';
			}
		}

		/**
		 * Renders the star icon meaning a premium setting.
		 *
		 * @access  private
		 * @param   string  $tip        Tip's text to display.
		 * @param   boolean $allow_html Whether to allow HTML.
		 * @return  string              HTML markup to render star icon.
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		private function molongui_premium_setting( $tip, $link = true, $allow_html = false )
		{
			if ( !$this->plugin->is_premium )
			{
				// Handle tip.
				if ( $allow_html )
				{
					$tip = molongui_sanitize_tooltip( $tip );
				}
				else
				{
					$tip = esc_attr( $tip );
				}

				// Return data.
				$html  = '';
				$html .= ( $link ? '<a href="' . $this->plugin->web . '" target="_blank">' : '' );
				$html .= '<i class="molongui-icon-star molongui-help-tip molongui-premium-setting" data-tip="' . $tip . '"></i>';
				$html .= ( $link ? '</a>' : '' );

				return $html;
			}
		}

		/**
		 * Register settings page tabs.
		 *
		 * @access  public
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		public function add_page_tabs()
		{
			foreach ( $this->tabs as $tab )
			{
				$this->register_tab( $tab );
			}
		}

		/**
		 * Register tabs.
		 *
		 * @access  private
		 * @param   string  $tab    Tab to register.
		 * @since   1.0.0
         * @version 1.1.0
         */
        private function register_tab( $tab )
        {
            register_setting( $tab['slug'], $tab['key'], $tab['callback'] );

	        // Sanity check.
	        if ( !isset( $tab['sections'] ) or empty( $tab['sections'] ) ) return;

            // Display sections.
            foreach ( $tab['sections'] as $section )
            {
                // Do not display section if it is configured to not be displayed.
                if ( isset( $section['display'] ) and !$section['display'] ) continue;

                add_settings_section( $section['id'], $section['label'], array( $this, $section['callback'] ), $tab['slug'] );

                // Display section's fields.
                if ( !empty( $section['fields'] ) )
                {
                    foreach ( $section['fields'] as $field )
                    {
                        // Do not display field if configured so.
                        if ( isset( $field['display'] ) and !$field['display'] ) continue;

                        add_settings_field( $field['id'], $field['label'], array( $this, 'render_field' ), $tab['slug'], $section['id'], array( 'field' => $field, 'option_group' => $tab['key'] ) );
                    }
                }
            }
        }

		/**
		 * Function that fills the section with the desired description.
		 *
		 * This function is called as a callback, so no parameters can be passed. Being so, the code is more complicated that
		 * just an echo.
		 *
		 * @see     http://wordpress.stackexchange.com/questions/19156/how-to-pass-variable-to-add-settings-section-callback
		 *
		 * @access  public
		 * @param   array   $args   Sections to render.
		 * @since   1.0.0
		 * @version 1.2.2
		 */
		public function render_description( $args )
		{
			foreach ( $this->tabs as $tab )
			{
				foreach ( $tab['sections'] as $section )
				{
					if ( $section['id'] == $args['id'] ) echo '<p id="'.$section['id'].'-description" class="'.( isset( $section['d_class'] ) ? $section['d_class'] : '' ).'">' . $section['desc'] . '</p>';
				}
			}
		}

        /**
         * Function that fills the section with the desired content returned by a class.
         *
         * This function is called as a callback, so no parameters can be passed. Being so, the code is more complicated that
         * just an echo.
         *
         * @see     http://wordpress.stackexchange.com/questions/19156/how-to-pass-variable-to-add-settings-section-callback
         *
         * @access  public
         * @param   array   $args   Class to render.
         * @since   1.0.0
         * @version 1.0.0
         */
		public function render_class( $args )
		{
			foreach ( $this->tabs as $tab )
			{
				foreach ( $tab['sections'] as $section )
				{
					if ( $section['id'] == $args['id'] )
					{
						// Declare passed arguments as variables so they can be used by the page to be loaded.
						if ( !empty( $section['cb_args'] ))
						{
							foreach ( $section['cb_args'] as $key => $value )
							{
								${$key} = $value;
							}
						}

						// Load the class.
						$cname = 'Molongui\\Fw\\Includes\\'.$section['cb_class'];
						$class = new $cname( $this->plugin->id );
						echo $class->render_output();
					}
				}
			}
		}

		/**
		 * Function that fills the section with the desired content.
		 *
		 * This function is called as a callback, so no parameters can be passed. Being so, the code is more complicated that
		 * just an echo.
		 *
		 * @see     http://wordpress.stackexchange.com/questions/19156/how-to-pass-variable-to-add-settings-section-callback
		 *
		 * @access  public
		 * @param   array   $args   Sections to render.
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		public function render_page( $args )
		{
			foreach ( $this->tabs as $tab )
			{
				foreach ( $tab['sections'] as $section )
				{
					if ( $section['id'] == $args['id'] )
					{
						// Declare passed arguments as variables so they can be used by the page to be loaded.
						if ( !empty( $section['cb_args'] ))
						{
							foreach ( $section['cb_args'] as $key => $value )
							{
								${$key} = $value;
							}
						}

						// Load the page markup.
						include( $section['cb_page'] );
					}
				}
			}
		}

		/**
		 * Renders a field.
		 *
		 * @access  public
		 * @param   array   $args  Field to render and option group where to store its value to.
		 * @since   1.0.0
		 * @version 1.2.5
		 */
		public function render_field( $args )
		{
			// Parse parameters.
			$field        = $args['field'];
			$option_group = $args['option_group'];

			// Avoid PHP warnings/notices.
			if ( ! isset( $field['type'] ) )        return;
			if ( ! isset( $field['icon'] ) )        $field['icon']  = '';
			if ( ! isset( $field['id'] ) )          $field['id']    = '';
			if ( ! isset( $field['label'] ) )       $field['label'] = '';
			if ( ! isset( $field['desc'] ) )        $field['desc']  = '';
			if ( ! isset( $field['tip'] ) )         $field['tip']   = '';
			if ( ! isset( $field['name'] ) )        $field['name']  = '';
			if ( ! isset( $field['placeholder'] ) ) $field['placeholder'] = '';

			// Get saved options.
			$options = get_option( $option_group );

			// Get field value.
			$default = isset( $field['default'] ) ? $field['default'] : '';
			$value   = isset( $options[ $field['id'] ] ) ? $options[ $field['id'] ] : $default;

			// Render field.
			switch ( $field['type'] )
			{
				case 'text':
					if ( $field['icon'] and $field['icon']['position'] == 'left' ) $this->render_icon( $options, $field );
					echo '<input type="text" id="' . $field['id'] . '" name="' . $option_group . '[' . $field['id'] . ']" placeholder="' . $field['placeholder'] . '" value="' . $value . '" class="regular-text ltr ' . $field['class'] . ' ' . ( ( stripos( $field['id'], "activation_" ) !== false and $options[ $field['id'] ] ) ? 'molongui-field-validated' : '' ) . '" />' . ' ' . $field['desc'];
					if ( $field['icon'] and $field['icon']['position'] == 'right' ) $this->render_icon( $options, $field );
				break;

				case 'textarea':
					echo '<textarea id="' . $field['id'] . '" name="' . $option_group . '[' . $field['id'] . ']" rows="5" cols="50">' . $value . '</textarea>';
				break;

				case 'select':
					if ( $field['icon'] and $field['icon']['position'] == 'left' ) $this->render_icon( $options, $field );
					echo '<select id="' . $field['id'] . '" name="' . $option_group . '[' . $field['id'] . ']" class="' . $field['class'] . '">';
						foreach ( $field['options'] as $option )
						{
							echo '<option value="' . $option['value'] . '"' . selected( $options[$field['id']], $option['value'], false ) . ( ( isset( $option['disabled'] ) and $option['disabled'] ) ? 'disabled' : '' ) . '>' . $option['label'] . '</option>';
						}
					echo '</select>';
					if ( $field['icon'] and $field['icon']['position'] == 'right' ) $this->render_icon( $options, $field );
				break;

				case 'radio':
					foreach ( $field['options'] as $option )
					{
						echo '<input type="radio" id="' . $field['id'] . '" name="' . $option_group . '[' . $field['id'] . ']" value="' . $option['value'] . '"' . checked( $option['value'], $options[$field['id']], false ) . '/>';
						echo '&nbsp;';
						echo '<label for="' . $option_group . '[' . $field['id'] . ']">' . $option['label'] . '</label>';
						echo '<br>';
					}
				break;

				case 'checkbox':
					echo '<input type="checkbox" id="' . $field['id'] . '" name="' . $field['name'] . '" value="' . $field['value'] . '"';
					echo checked( get_option( $field['name'] ), 'on' );
					echo '/>';
					echo '<label for="' . $field['name'] . '">' . $field['desc'] . '</label>';
				break;

				case 'checkboxes':

                    if ( isset( $field['options'][0]['id'] ) )
                    {
	                    // Sort options alphabetically.
	                    usort($field['options'] , function ( $item1, $item2 )
	                    {
		                    if ( $item1['id'] == $item2['id'] ) return 0;
		                    return $item1['id'] < $item2['id'] ? -1 : 1;
	                    });
                    }

					echo '<ul id="' . $field['id'] . '">';
					foreach ( $field['options'] as $option )
					{
						echo '<li style="float:left; width:150px;">';
						echo '<input type="checkbox" id="' . $field['id'].'_'.$option['id'] . '" name="' . $option_group . '[' . $field['id'] . '_' . $option['id'] . ']" value="1"' . ( ( isset( $options[$field['id'].'_'.$option['id']] ) && $options[$field['id'].'_'.$option['id']] == 1 ) ? 'checked="checked"' : '')  . '/>';
						echo '<label for="' . $field['id'].'_'.$option['id'] . '">' . $option['label'] . '</label>';
						echo '</li>';
					}
					echo '</ul>';

				break;

				case 'colorpicker':
						echo '<input type="text" class="colorpicker" name="' . $option_group . '[' . $field['id'] . ']" value="' . $value . '">';
				break;

				case 'range':
					?>
					<input  type="range" id="<?php echo $field['id']; ?>" name="<?php echo $option_group.'['.$field['id'].']'; ?>" value="<?php echo $value; ?>" min="<?php echo $field['min']; ?>" max="<?php echo $field['max']; ?>" step="<?php echo ( $field['step'] ? $field['step'] : '1' ); ?>" oninput="<?php echo $field['id'].'_output'; ?>.value = <?php echo $field['id']; ?>.value">
					<output type="range" id="<?php echo $field['id'].'_output'; ?>" name="<?php echo $field['id'].'_output'; ?>"><?php echo $value; ?></output>
					<?php
				break;

				case 'toggle':
					?>
					<div class="switch">
						<input type="checkbox" id="<?php echo $field['id']; ?>" name="<?php echo $option_group.'['.$field['id'].']'; ?>" class="molongui-toggle molongui-toggle-<?php echo $field['style']; ?>" <?php checked( $value, 'on' ); ?>>
						<label for="<?php echo $field['id']; ?>" data-on="Yes" data-off="No"></label>
					</div>
					<?php
				break;

				case 'button':
						echo '<button id="' . $field['args']['id'] . '" class="' . $field['args']['class'] . '">' . $field['args']['label'] . '</button>';
				break;

				case 'link':
                    ?>
                        <a href="<?php echo $field['args']['url']; ?>" target="<?php echo ( ( isset( $field['args']['target'] ) and !empty( $field['args']['target'] ) ) ? $field['args']['target'] : '_self' ); ?>" id="<?php echo $field['args']['id']; ?>" class="<?php echo $field['args']['class']; ?>"><?php echo $field['args']['text']; ?></a>
                        <p class="molongui-settings-link-desc"><?php echo $field['desc']; ?></p>
                    <?php
				break;

				case 'number':
					if ( $field['icon'] and $field['icon']['position'] == 'left' ) $this->render_icon( $options, $field );
					echo '<input type="number" id="' . $field['id'] . '" name="' . $option_group . '[' . $field['id'] . ']" min="' . $field['min'] . '" max="' . $field['max'] . '" step="' . ( $field['step'] ? $field['step'] : '1' ) . '" value="' . $value . '" class="' . ( ( stripos( $field['id'], "activation_" ) !== false and $options[ $field['id'] ] ) ? 'molongui-field-validated' : '' ) . '" />' . ' ' . $field['desc'];
					if ( $field['icon'] and $field['icon']['position'] == 'right' ) $this->render_icon( $options, $field );
				break;

				case 'password':
					if ( $field['icon'] and $field['icon']['position'] == 'left' ) $this->render_icon( $options, $field );
					echo '<input type="password" id="' . $field['id'] . '" name="' . $option_group . '[' . $field['id'] . ']" value="' . $value . '" class="regular-text ltr ' . ( ( stripos( $field['id'], "activation_" ) !== false and $options[ $field['id'] ] ) ? 'molongui-field-validated' : '' ) . '" />' . ' ' . $field['desc'];
					if ( $field['icon'] and $field['icon']['position'] == 'right' ) $this->render_icon( $options, $field );
				break;

				case 'email':
					if ( $field['icon'] and $field['icon']['position'] == 'left' ) $this->render_icon( $options, $field );
					echo '<input type="email" id="' . $field['id'] . '" name="' . $option_group . '[' . $field['id'] . ']" placeholder="' . $field['placeholder'] . '" value="' . $value . '" class="regular-text ltr ' . ( ( stripos( $field['id'], "activation_" ) !== false and $options[ $field['id'] ] ) ? 'molongui-field-validated' : '' ) . '" />' . ' ' . $field['desc'];
					if ( $field['icon'] and $field['icon']['position'] == 'right' ) $this->render_icon( $options, $field );
				break;

				case 'date':
					if ( $field['icon'] and $field['icon']['position'] == 'left' ) $this->render_icon( $options, $field );
					echo '<input type="date" id="' . $field['id'] . '" name="' . $option_group . '[' . $field['id'] . ']" min="' . $field['min'] . '" max="' . $field['max'] . '" value="' . $value . '" class="regular-text ltr ' . ( ( stripos( $field['id'], "activation_" ) !== false and $options[ $field['id'] ] ) ? 'molongui-field-validated' : '' ) . '" />' . ' ' . $field['desc'];
					if ( $field['icon'] and $field['icon']['position'] == 'right' ) $this->render_icon( $options, $field );
				break;

				case 'dropdown_pages':
					$args = array
					(
						'depth'                 => 0,
						'child_of'              => 0,
						'selected'              => $options[$field['id']],
						'echo'                  => 1,
						'name'                  => $option_group.'['.$field['id'].']',
						'id'                    => $field['id'],
						'class'                 => $field['class'],
						'show_option_none'      => null, // string
						'show_option_no_change' => null, // string
						'option_none_value'     => null, // string
					);
					wp_dropdown_pages( $args );
				break;

				case 'dropdown_users':
					$args = array
                    (
						'show_option_all'         => null, // string
						'show_option_none'        => null, // string
						'hide_if_only_one_author' => null, // string
						'orderby'                 => 'display_name',
						'order'                   => 'ASC',
						'include'                 => null, // string
						'exclude'                 => null, // string
						'multi'                   => false,
						'show'                    => 'display_name',
						'echo'                    => true,
						'selected'                => $options[$field['id']],
						'include_selected'        => false,
						'name'                    => $option_group.'['.$field['id'].']',
						'id'                      => $field['id'],
						'class'                   => $field['class'],
						'blog_id'                 => $GLOBALS['blog_id'],
						'who'                     => null, // string,
						'role'                    => null, // string|array,
						'role__in'                => null, // array
						'role__not_in'            => null, // array
					);
					wp_dropdown_users( $args );
				break;

				case 'dropdown_roles':
					echo '<select id="' . $field['id'] . '" name="' . $option_group . '[' . $field['id'] . ']" class="' . $field['class'] . '">';
                        wp_dropdown_roles( $options[$field['id']] );
                    echo '</select>';
				break;
			}
		}

		/**
		 * Renders a field's icon.
		 *
		 * @access  public
		 * @param   array   $options    Saved settings.
		 * @param   array   $field      Field to render.
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		public function render_icon( $options, $field )
		{
			switch ( $field['icon']['type'] )
			{
				case 'status':

					if ( $options[ $field['id'] ] )
					{
						echo '<i class="molongui-icon-check-circled molongui-license-data-ok"></i>';
					}
					else
					{
						echo '<i class="molongui-icon-notice molongui-license-data-ko"></i>';
					}

				break;

				case 'tip':

					echo '<span class="molongui-help-tip" data-tip="' . $field['icon']['tip'] . '">?</span>';

				break;
			}
		}

	} // End of 'Settings_Page' class
}