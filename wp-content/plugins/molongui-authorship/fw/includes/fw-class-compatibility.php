<?php

namespace Molongui\Fw\Includes;

use Molongui\Fw\Includes\Notice;

// Deny direct access to this file.
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Check whether there is another instance of the same plugin installed preventing the plugin to activate if so.
 *
 * This class is loaded before anything else, so no functions, styles or scripts from the plugin can be used here.
 *
 * @see        https://pento.net/2014/02/18/dont-let-your-plugin-be-activated-on-incompatible-sites/
 *             https://www.sitepoint.com/preventing-wordpress-plugin-incompatibilities/
 *
 * @author     Amitzy
 * @package    Molongui Common Framework (/fw)
 * @subpackage /fw/includes
 * @since      1.0.0
 * @version    1.2.0
 */
if ( !class_exists( 'Molongui\Fw\Includes\Compatibility' ) )
{
	class Compatibility
	{
		/**
		 * The basename of the plugin loading this class.
		 *
		 * @access  private
		 * @var     string
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		private $basename;

		/**
		 * Class constructor.
		 *
		 * @access  public
		 * @param   string  $plugin     Filename of the plugin loading this class.
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		public function __construct( $basename )
		{
			$this->basename = plugin_basename( $basename );

			// Backup check.
			add_action( 'admin_init', array( $this, 'check_version' ) );
		}

		/**
		 * The primary sanity check, automatically disables the plugin on activation if it doesn't
		 * meet requirements.
		 *
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		public function activation_check()
		{
			if ( !$this->compatible_version() )
			{
				//deactivate_plugins( $this->basename );
				//wp_die( __( 'There can be only one instance of this plugin installed. Please, uninstall all other instances but the one you want to activate', 'molongui-common-framework' ) );
				if ( is_plugin_active( $this->basename ) )
				{
					deactivate_plugins( $this->basename );
					add_action( 'admin_notices', array( $this, 'disabled_notice' ), 100 );
					if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );
				}
			}
		}

		/**
		 * The backup sanity check, in case the plugin is activated in a weird way.
		 *
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		public function check_version()
		{
			if ( !$this->compatible_version() )
			{
				if ( is_plugin_active( $this->basename ) )
				{
					deactivate_plugins( $this->basename );
					add_action( 'admin_notices', array( $this, 'disabled_notice' ), 100 );
					if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );
				}
			}
		}

		/**
		 * Displays an admin error notice alerting the plugin was not activated because there's another instance of itself already installed.
		 *
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		public function disabled_notice()
		{
			// Get plugin name.
			$plugin = explode( '/', $this->basename );
			$plugin = explode( '.', $plugin[1] );
			$plugin = ucwords( str_replace( '-', ' ', $plugin[0] ) );

			// Define notice parameters.
			$d_style = 'background:red; color:white; padding-top:10px; padding-bottom:10px;';
			$p_style = 'font-size: 1.2em;';
			$class   = 'notice notice-error';
			$message = sprintf( __( '%s%s%s - There can be only one instance of %s plugin installed. Please, uninstall all other instances but the one you want to activate.', 'molongui-common-framework' ), '<strong>', $plugin, '</strong>', $plugin );

			// Display notice.
			printf( '<div class="%1$s" style="%2$s"><p style="%3$s">%4$s</p></div>', esc_attr( $class ), esc_attr( $d_style ), esc_attr( $p_style ), $message );
		}

		/**
		 * Returns whether there is another version (free or premium) of the plugin installed.
		 *
		 * @since   1.0.0
		 * @version 1.2.0
		 */
		public function compatible_version()
		{
			// Variables.
			$count = 0;

			// Get current plugin filename.
			$us = explode( "/", $this->basename );
			$us = $us[1];

			// Get an array of all installed plugins.
			if ( !function_exists( 'get_plugins' ) ) require_once ABSPATH . 'wp-admin/includes/plugin.php';
			$plugins = get_plugins();

			// Check if there is more than one installed version of this plugin.
			foreach ( $plugins as $basename => $plugin )
			{
				$basename = explode( "/", $basename );
				$basename = ( ( isset( $basename[1] ) and !empty( $basename[1] ) ) ? $basename[1] : $basename[0] );
				if ( $basename == $us ) ++$count;
			}

			// Return check.
			if ( $count > 1 ) return false;
			return true;
		}
	}
}