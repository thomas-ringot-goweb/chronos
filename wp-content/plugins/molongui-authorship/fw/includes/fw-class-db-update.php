<?php

namespace Molongui\Fw\Includes;

// Deny direct access to this file.
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * DB_Update class.
 *
 * The class responsible for handling database schema update to keep backwards compatibility.
 *
 * @author     Amitzy
 * @package    Molongui Common Framework (/fw)
 * @subpackage /fw/includes
 * @since      1.0.0
 * @version    1.2.0
 */
if ( !class_exists( 'Molongui\Fw\Includes\DB_Update' ) )
{
	class DB_Update
	{
		/**
		 * Information of the plugin loading this class.
		 *
		 * @access  private
		 * @var     object
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		private $plugin;

		/**
		 * The target version of the plugin's database schema to reach.
		 *
		 * @access  protected
		 * @var     integer
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		protected $target_version;

		/**
		 * Class constructor.
		 *
		 * @access  public
		 * @param   string      $plugin_id      Name of the plugin loading this class.
		 * @param   string      $target_version Current database schema version.
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		public function __construct( $plugin_id, $target_version )
		{
			// Initialize information of the plugin loading this class.
			molongui_get_plugin( $plugin_id, $this->plugin );

			// Initialize variables.
			$this->target_version = $target_version;
		}

		/**
		 * Checks whether a database update is needed.
		 *
		 * @access  private
		 * @return  bool
		 * @since   1.0.0
		 * @version 1.2.0
		 */
		public function db_update_needed()
		{
			// Get current database schema version.
			$current_version = get_option( $this->plugin->db_settings );

			// If no existing version, this is the first install.
			if ( empty( $current_version ) )
			{
				// Set the option in the database for future checks.
				update_option( $this->plugin->db_settings, $this->target_version );

				// No update needed.
				return false;
			}

			// Check update need.
			if ( $current_version >= $this->target_version ) return false;
			return true;
		}

		/**
		 * Runs updates to get database schema to the latest version.
		 *
		 * @access  public
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		public function run_update()
		{
			// Get current database schema version. 1 if none exists on db.
			$current_db_ver = get_option( $this->plugin->db_settings, 1 );

			// Set the target version to reach.
			$target_db_ver = $this->target_version;

			// Run update routines one by one until the current version reaches the target version.
			while ( $current_db_ver < $target_db_ver )
			{
				// Increment the current db_ver by one.
				$current_db_ver ++;

				// Each db version will require a separate update function, for example, for db_ver 3, the function name should be db_update_3.
				$func = "db_update_{$current_db_ver}";

				// Load plugin class DB update.
				if ( !class_exists( '\Molongui\\'.ucfirst($this->plugin->id).'\Includes\DB_Update' ) ) require_once $this->plugin->dir . 'includes/plugin-class-db-update.php';

				// Update if $func exists.
				if ( method_exists( '\Molongui\\'.ucfirst($this->plugin->id).'\Includes\DB_Update', $func ) )
				{
					$class_name   = '\Molongui\\'.ucfirst($this->plugin->id).'\Includes\DB_Update';
					$plugin_class = new $class_name();
					$plugin_class->{$func}();
				}

				// Update the option in the database, so that this process can always pick up where it left off.
				update_option( $this->plugin->db_settings, $current_db_ver );
			}
		}
	}
}