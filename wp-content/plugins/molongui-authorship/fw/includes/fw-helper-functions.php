<?php

// Deny direct access to this file
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Common functions.
 *
 * @author     Amitzy
 * @package    Molongui Common Framework (/fw)
 * @subpackage /fw/includes
 * @since      1.0.0
 * @version    1.2.6
 */

/**
 * Returns the value of the given constant.
 *
 * Three parameters are required:
 *
 *  $plugin     The ID of the calling plugin. Something like: MOLONGUI_BOILERPLATE_ID.
 *  $constant   The name of the constant to get. Something like: version or VERSION.
 *  $fw         Whether the constant is a framework constant or plugin specific.
 *
 * @see     http://php.net/manual/en/function.constant.php
 *
 * @param   string  $plugin     Calling plugin id (used to build up the constant prefix).
 * @param   string  $constant   Constant name.
 * @param   boolean $fw         Whether it is a framework constant or not (because it is a plugin constant).
 * @return  mixed               Constant value if it exists. NULL otherwise.
 * @since   1.0.0
 * @version 1.0.0
 */
if ( !function_exists( 'molongui_get_constant' ) )
{
    function molongui_get_constant( $plugin, $constant, $fw = false )
    {
        return ( constant( "MOLONGUI_" . strtoupper( str_replace( ' ', '_', $plugin ) ) . "_" . ( $fw ? "FW_" : "" ) . strtoupper( $constant ) ) );
    }
}

/**
 * Initializes the object containing information about the plugin loading a class.
 *
 * The object to initialize is passed by reference. If none passed, a new one is created and returned.
 *
 * Object description:
 *
 *  $plugin = stdClass
 *  (
 *      id            = Plugin id in lowercase and spaces (if any) replaced by underscores (e.g. boilerplate).
 *      name          = Plugin name without premium reference (if it was the case) (e.g. Molongui Boilerplate).
 *      version       = Plugin version (e.g. 2.0.2).
 *      dir           = Plugin path with trailing slash (e.g. /var/www/site/wp-content/plugins/molongui-boilerplate/).
 *      url           = Plugin url (e.g. https://domain.com/wp-content/plugins/molongui-boilerplate).
 *      slug          = Plugin slug with hyphen as word separator (e.g. molongui-boilerplate).
 *      basename      = Plugin base name (e.g. molongui-boilerplate-premium/molongui-boilerplate.php).
 *      namespace     = Plugin namespace key. (e.g. Boilerplate)
 *      textdomain    = Plugin text domain. Same as slug (e.g. molongui-boilerplate).
 *      is_premium    = Whether it is a premium plugin/version {true | false}.
 *      is_upgradable = Whether the plugin is upgradable to a premium version {true | false}.
 *      web           = Plugin web (e.g. https://molongui.amitzy.com/product/boilerplate).
 *      db_version    = Plugin db version (e.g. 4).
 *      db_prefix     = Plugin db key prefix (e.g. molongui_boilerplate).
 *      db_settings   = Db key used to store db version (e.g. molongui_boilerplate_db_version).
 *      settings      = Plugin configuration settings.
 *  )
 *
 * @param   string  $id         Loading plugin id as defined in its config file (/config/plugin.php).
 * @param   object  $plugin     Object containing loading plugin info.
 * @since   1.0.0
 * @version 1.2.6
 */
if ( !function_exists( 'molongui_get_plugin' ) )
{
    function molongui_get_plugin( $id, &$plugin )
    {
        if ( !isset( $plugin ) ) $plugin = new stdClass();

        $plugin->id = strtolower( str_replace( ' ', '_', $id ) );

        // Prefix used in plugin constant names. Uppercased.
        $prefix = 'MOLONGUI_' . strtoupper( $plugin->id ) . '_';

        $plugin->name          = str_replace( ' Premium', '', constant( $prefix . "NAME" ) );
        $plugin->version       = constant( $prefix . "VERSION" );
        $plugin->dir           = constant( $prefix . "DIR" );
        $plugin->url           = constant( $prefix . "URL" );
        $plugin->slug          = constant( $prefix . "SLUG" );
        $plugin->basename      = constant( $prefix . "BASENAME" );
	    $plugin->namespace     = constant( $prefix . "NAMESPACE" );
        $plugin->textdomain    = constant( $prefix . "TEXTDOMAIN" );
        $plugin->is_premium    = molongui_is_premium( $plugin->dir );
        $plugin->is_upgradable = molongui_is_premium( $plugin->dir ) ? false : constant( $prefix . "UPGRADABLE" );
        $plugin->web           = constant( $prefix . "WEB" );
        $plugin->db_version    = constant( $prefix . "DB_VERSION" );
        $plugin->db_prefix     = constant( $prefix . "DB_PREFIX" );
        $plugin->db_settings   = constant( $prefix . "DB_SETTINGS" );

	    // Load plugin settings.
	    $plugin->settings = array();
	    global $wpdb;
	    $settings_keys = $wpdb->get_col( "SELECT option_name FROM {$wpdb->options} WHERE option_name LIKE '{$plugin->db_prefix}%'" );
	    foreach ( $settings_keys as $key )
	    {
		    if ( !in_array( substr( $key, strlen( $plugin->db_prefix )+1 ), array( 'db_version', 'activated', 'instance', 'license', 'product_id', 'version' ) ) and is_array( $key ) )
		    {
			    $plugin->settings = array_merge( $plugin->settings, get_option( $key ) );
		    }
	    }
    }
}

/**
 * Whether the calling plugin is premium.
 *
 * This conditional tag checks if the plugin license is premium.
 * This is a boolean function, meaning it returns either TRUE or FALSE.
 *
 * @param   string  $plugin_dir The directory of the calling plugin.
 * @return  boolean             True if premium plugin, false otherwise.
 * @since   1.0.0
 * @version 1.0.0
 */
if ( !function_exists( 'molongui_is_premium' ) )
{
    function molongui_is_premium( $plugin_dir )
    {
        $path = $plugin_dir.'premium';

        if ( file_exists( $path ) ) return true;

        return false;
    }
}

/**
 * Whether the plugin license has been activated.
 *
 * This conditional tag checks if the plugin license is active.
 * This is a boolean function, meaning it returns either TRUE or FALSE.
 *
 * @return  boolean True if license activated, false otherwise.
 * @since   1.0.0
 * @version 1.0.0
 */
if ( !function_exists( 'molongui_is_active' ) )
{
    function molongui_is_active( $plugin_dir )
    {
        // Load configuration data.
        if ( file_exists( $file = $plugin_dir.'premium/config/update.php' ) ) $config = include $file;

        return ( get_option( $config['db']['activated_key'] ) == 'Activated' ? true : false );
    }
}

/**
 * Displays an admin notice.
 *
 * @see     https://codex.wordpress.org/Plugin_API/Action_Reference/admin_notices
 *
 * @param   mixed   $type           Type of message to display {info | success | warning | error}.
 * @param   boolean $message        Whether to halt PHP execution after displaying data.
 * @param   boolean $dismissable    Whether the notice can be dismissed.
 * @since   1.0.0
 * @version 1.0.0
 */
if ( !function_exists( 'molongui_display_notice' ) )
{
    function molongui_display_notice( $type, $message, $dismissable )
    {
        $class = 'notice notice-' . $type . ' ' . ( $dismissable ? 'is-dismissible' : '' ) ;
        ?>
        <div id="message" class="<?php echo $class; ?>">
            <p>
                <?php echo $message; ?>
            </p>
        </div>
        <?php
    }
}

/**
 * Renders the help icon.
 *
 * @param   string  $tip        Tip's text to display.
 * @param   boolean $allow_html Whether to allow HTML.
 * @return  string              HTML markup to render tip icon.
 * @since   1.0.0
 * @version 1.0.0
 */
if ( !function_exists( 'molongui_help_tip' ) )
{
    function molongui_help_tip( $tip, $allow_html = false )
    {
        if ( $allow_html )
        {
            $tip = molongui_sanitize_tooltip( $tip );
        }
        else
        {
            $tip = esc_attr( $tip );
        }
        return '<i class="molongui-icon-tip molongui-help-tip" data-tip="' . $tip . '"></i>';
    }
}

/**
 * Sanitize a string destined to be a tooltip.
 *
 * Tooltips are encoded with htmlspecialchars to prevent XSS. Should not be used in conjunction with esc_attr().
 *
 * @param   string  $var    Tip's text to display.
 * @return  string          Sanitized tooltip.
 * @since   1.0.0
 * @version 1.0.0
 */
if ( !function_exists( 'molongui_sanitize_tooltip' ) )
{
    function molongui_sanitize_tooltip( $var )
    {
        return htmlspecialchars( wp_kses( html_entity_decode( $var ), array(
            'br'     => array(),
            'em'     => array(),
            'strong' => array(),
            'small'  => array(),
            'span'   => array(),
            'ul'     => array(),
            'li'     => array(),
            'ol'     => array(),
            'p'      => array(),
        ) ) );
    }
}

/**
 * Retrieves the response from the specified URL using one of PHP's outbound
 * request facilities.
 *
 * 'wp_remote_get' is a simple WordPress API function that’s used to retrieve
 * data from a specified URL using the HTTP GET method. The request can fail,
 * and in those cases you should be prepared to handle that. First, you can
 * check to see if the request failed by using the is_wp_error function. If
 * it did fail, then you can proceed by trying to use file_get_contents. If
 * that fails, then you can use cURL, and if that fails, you may simply be
 * out of luck.
 *
 * @see     https://tommcfarlin.com/wp_remote_get/
 *
 * @param   string  $url    The URL of the feed to retrieve.
 * @return  mixed           The response from the URL; null if empty.
 * @since   1.0.0
 * @version 1.2.2
 */
if ( !function_exists( 'molongui_request_data' ) )
{
    function molongui_request_data( $url )
    {
        $response = null;

        // First, try to use wp_remote_get.
        $response = wp_remote_get( $url );
        if( is_wp_error( $response ) or !isset( $response ) or empty( $response ) )
        {
            // If that doesn't work, try file_get_contents.
            $response = file_get_contents( $url );
            if( false == $response )
            {
                // And if that doesn't work either, try curl.
                $response = molongui_curl( $url );
                if( null == $response )
                {
                    $response = 0;
                }
                // If curl worked, convert data before returning it.
                else
                {
                    // Debug vars.
                    //molongui_debug( array( "curl", $response ) );

                    // Convert data to object.
                    $response = unserialize( $response );
                }
            }
            // If file_get_contents worked, convert data before returning it.
            else
            {
                // Debug vars.
                //molongui_debug( array( "file_get_contents", $response ) );

                // Convert data to object.
                $response = unserialize( $response );
            }
        }
        // If wp_remote_get worked, convert data before returning it.
        else
        {
            // Debug vars.
            //molongui_debug( array( "wp_remote_get", $response ) );

            // Convert data to object.
            $response = unserialize( wp_remote_retrieve_body( $response ) );
        }

        // Return response data.
        return $response;
    }
}

/**
 * Defines the function used to initial the cURL library.
 *
 * @see     http://php.net/manual/es/function.curl-setopt.php
 *
 * @param   string  $url        To URL to which the request is being made.
 * @return  string  $response   The response, if available; otherwise, null.
 * @since   1.0.0
 * @version 1.0.0
 */
if ( !function_exists( 'molongui_curl' ) )
{
    function molongui_curl( $url )
    {
        $curl = curl_init( $url );

        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $curl, CURLOPT_HEADER, 0 );
        curl_setopt( $curl, CURLOPT_USERAGENT, '' );
        curl_setopt( $curl, CURLOPT_TIMEOUT, 10 );
        curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, true );

        $response = curl_exec( $curl );
        if( 0 !== curl_errno( $curl ) || 200 !== curl_getinfo( $curl, CURLINFO_HTTP_CODE ) )
        {
            $response = null;
        }
        curl_close( $curl );

        return $response;
    }
}

/**
 * Returns whether a variable acts as a boolean.
 *
 * @param   mixed   $var    The variable to check.
 * @return  bool            Whether the variable acts as a boolean.
 * @since   1.0.0
 * @version 1.0.0
 */
if ( !function_exists( 'molongui_is_bool' ) )
{
    function molongui_is_bool( $var )
    {
        if ( '0' === $var or '1' === $var ) return true;

        return false;
    }
}

/**
 * Retrieves existing public post types.
 *
 * @see     https://codex.wordpress.org/Function_Reference/get_post_types
 *
 * @param   string  $type       Post types to retrieve: {wp|cpt|all}.
 * @param   string  $output     The type of output to return, either 'names' or 'objects'.
 * @param   bool    $setting    Whether to format output as a setting's options.
 * @return  array               Existing post types.
 * @since   1.0.0
 * @version 1.1.0
 */
if ( !function_exists( 'molongui_get_post_types' ) )
{
    function molongui_get_post_types( $type = 'all', $output = 'names', $setting = false )
    {
        // Retrieve existing public post types.
        $wp_post_types     = ( ( $type == 'wp'  or $type == 'all' ) ? get_post_types( array( 'public' => true, '_builtin' => true ), $output )  : array() );
        $custom_post_types = ( ( $type == 'cpt' or $type == 'all' ) ? get_post_types( array( 'public' => true, '_builtin' => false ), $output ) : array() );

        // Merge arrays.
        $post_types = array_merge( $wp_post_types, $custom_post_types );

        // Return data as checkbox options.
        if ( $setting )
        {
            $options = array();

            foreach ( $post_types as $post_type )
            {
                $options[] = array( 'id' => $post_type->name, 'label' => $post_type->labels->name );
            }

            return $options;
        }

        // Return data.
        return $post_types;
    }
}

/**
 * Retrieves public post types to extend.
 *
 * @param   string  $plugin     Calling plugin id.
 * @param   string  $type       Post types to retrieve: {wp|cpt|all}.
 * @param   bool    $setting    Whether to format output as a setting's options.
 * @return  array               Extended post types.
 * @since   1.0.0
 * @version 1.1.0
 */
if ( !function_exists('molongui_supported_post_types') )
{
    function molongui_supported_post_types( $plugin, $type = 'all', $setting = false )
    {
        // Define array to return.
        $post_types = $options = array();

        // Get advanced plugin settings.
        $advanced_settings = (array) get_option( molongui_get_constant( $plugin, 'ADVANCED_SETTINGS', false ) );

        // Fill array of post types the plugin will be extended to.
        foreach( molongui_get_post_types( $type, 'objects', false ) as $post_type_name => $post_type_object )
        {
            if ( isset( $advanced_settings['extend_to_'.$post_type_name] ) and $advanced_settings['extend_to_'.$post_type_name] )
            {
                $post_types[] = $post_type_name;
                $options[]    = array( 'id' => $post_type_name, 'label' => $post_type_object->labels->name );
            }
        }

        // Return data.
        return ( $setting ? $options : $post_types );
    }
}

/**
 * Retrieves post categories.
 *
 * @param   bool    $setting    Whether to format output as a setting's options.
 * @return  array               Post categories.
 * @since   1.2.4
 * @version 1.2.4
 */
if ( !function_exists('molongui_post_categories') )
{
    function molongui_post_categories( $setting = false )
    {
        // Define array to return.
        $categories = $options = array();

        // Get categories.
	    $post_categories = get_categories( array
        (
		    'orderby' => 'name',
            'order'   => 'ASC',
	    ));

        // Fill array of post types the plugin will be extended to.
        foreach( $post_categories as $category )
        {
	        $categories[] = $category->name;
            $options[]    = array( 'id' => $category->cat_ID, 'label' => $category->name );
        }

        // Return data.
        return ( $setting ? $options : $categories );
    }
}

/**
 * Whether or not the plugin is enabled for this post type.
 *
 * Must be called after init.
 *
 * @access  public
 * @param   string  $plugin     Calling plugin id.
 * @param   string  $post_type  The name of the post type being evaluated.
 * @return  bool                Whether it is enabled.
 * @since   1.0.0
 * @version 1.0.0
 */
if ( !function_exists('molongui_is_post_type_enabled') )
{
    function molongui_is_post_type_enabled( $plugin, $post_type = null )
    {
        if ( !$post_type ) $post_type = get_post_type();

        return (bool) in_array( $post_type, molongui_supported_post_types( $plugin, 'all' ) );
    }
}

/**
 * Prints human-readable information about a variable.
 *
 * Data will be displayed only if current user is logged in and has admin rights. That way this function can be used in
 * production environments without affecting what other users are presented.
 *
 * This function should be used only in code development and to debug.
 *
 * To output more than one variable at once, call the function like: molongui_debug(array($var1, $var2)).
 *
 * @see     http://php.net/manual/en/function.print-r.php
 *          https://developer.wordpress.org/reference/functions/wp_debug_backtrace_summary/
 *
 * @param   mixed   $vars       Data to print.
 * @param   boolean $backtrace  Whether to display backtrace.
 * @param   boolean $is_admin   Whether to display any debug data on wp-admin.
 * @param   boolean $die        Whether to halt PHP execution after displaying data.
 * @since   1.0.0
 * @version 1.2.6
 */
if ( !function_exists( 'molongui_debug' ) )
{
    function molongui_debug( $vars, $backtrace = false, $in_admin = true, $die = false )
    {
	    // Leave if admin is not logged in.
	    if ( !is_user_logged_in() and !current_user_can( 'administrator' ) ) return;

	    // Leave if executing on wp-admin.
	    if ( !$in_admin and is_admin() ) return;

        // Open preformatted text.
        echo "<pre>";

        // Display debug backtrace summary.
        if ( $backtrace )
        {
            print_r( array_reverse( wp_debug_backtrace_summary( null, 0, false ) ) );
            echo "<br>";
        }

        // Display vars.
        print_r( $vars );

        // Close preformatted text.
        echo "</pre>";

        // Exit or die.
        if ( $die ) die;
    }
}

/**
 * Returns all site IDs from a multi-site network.
 *
 * @since   1.2.0
 * @version 1.2.0
 */
if ( !function_exists( 'molongui_get_sites' ) )
{
    function molongui_get_sites()
    {
	    // WordPress >= 4.6 provides easy to use functions for this.
	    if ( function_exists( 'get_sites' ) && function_exists( 'get_current_network_id' ) )
	    {
		    $site_ids = get_sites( array( 'fields' => 'ids', 'network_id' => get_current_network_id() ) );
	    }
	    // WordPress < 4.6. Backwards compatibility.
	    else
	    {
		    global $wpdb;
		    $site_ids = $wpdb->get_col( "SELECT blog_id FROM $wpdb->blogs WHERE site_id = $wpdb->siteid;" );
	    }

	    return $site_ids;
    }
}

/**
 * Returns the acronym of a given string.
 *
 * @param   string  $words      The string.
 * @param   int     $length     The maximum length of the acronym.
 * @return  string  $acronym    The acronym.
 * @since   1.0.0
 * @version 1.0.0
 */
if ( !function_exists( 'molongui_get_acronym' ) )
{
    function molongui_get_acronym ( $words, $length = 3 )
    {
        $acronym = '';
        foreach ( explode( ' ', $words ) as $word ) $acronym .= mb_substr( $word, 0, 1, 'utf-8' );

        return strtoupper( mb_substr( $acronym, 0, $length ) );
    }
}

/**
 * Transforms the php.ini string notation for numbers (like '2M') to an integer.
 *
 * @param   string  $size   Value to convert.
 * @return  int     $ret    Converted value.
 * @since   1.0.0
 * @version 1.0.0
 */
if ( !function_exists( 'molongui_let_to_num' ) )
{
    function molongui_let_to_num( $size )
    {
        $l   = substr( $size, - 1 );
        $ret = substr( $size, 0, - 1 );
        switch ( strtoupper( $l ) )
        {
            case 'P':
                $ret *= 1024;
            case 'T':
                $ret *= 1024;
            case 'G':
                $ret *= 1024;
            case 'M':
                $ret *= 1024;
            case 'K':
                $ret *= 1024;
        }

        return $ret;
    }
}

/**
 * Returns the IP address of the current visitor.
 *
 * @return  string  $ip     User's IP address.
 * @since   1.0.0
 * @version 1.0.0
 */
if ( !function_exists( 'molongui_get_ip' ) )
{
    function molongui_get_ip()
    {
        $ip = '127.0.0.1';

        if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) )
        {
            // Check ip from share internet.
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
        elseif ( !empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) )
        {
            // Check ip is pass from proxy.
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        elseif( !empty( $_SERVER['REMOTE_ADDR'] ) )
        {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return apply_filters( 'molongui_get_ip', $ip );
    }
}

/**
 * Returns a base64-encoded SVG using data URI.
 *
 * @see     https://developer.wordpress.org/reference/functions/add_menu_page/
 * @param   bool    $svg        SVG code formated image to base64 encode.
 * @param   bool    $base64     Whether or not to return base64'd output.
 * @return  string
 * @since   1.0.0
 * @version 1.0.0
 */
if ( !function_exists( 'molongui_get_base64_svg' ) )
{
    function molongui_get_base64_svg( $svg, $base64 = true )
    {
        if ( $base64 )
        {
            return 'data:image/svg+xml;base64,' . base64_encode( $svg );
        }

        return $svg;
    }
}

/**
 * Cleans variables using sanitize_text_field.
 *
 * Arrays are cleaned recursively. Non-scalar values are ignored.
 *
 * @param   string|array    $var
 * @return  string|array
 * @since   1.0.0
 * @version 1.0.0
 */
if ( !function_exists( 'molongui_clean' ) )
{
    function molongui_clean( $var )
    {
        if ( is_array( $var ) ) return array_map( 'molongui_clean', $var );
        else return is_scalar( $var ) ? sanitize_text_field( $var ) : $var;
    }
}

/**
 * Parses string data into an array.
 *
 * @param   string  $string     The string to parse into an array.
 * @return  array   $array      The parsed data.
 * @since   1.0.0
 * @version 1.0.0
 */
if ( !function_exists( 'molongui_parse_array_attribute' ) )
{
    function molongui_parse_array_attribute( $string )
    {
        // Remove whitespaces.
        $no_whitespaces = preg_replace( '/\s*,\s*/', ',', filter_var( $string, FILTER_SANITIZE_STRING ) );

        // Explode string.
        $array = explode( ',', $no_whitespaces );

        // Return data.
        return $array;
    }
}