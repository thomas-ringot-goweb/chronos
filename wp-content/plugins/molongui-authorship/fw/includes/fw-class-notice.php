<?php

namespace Molongui\Fw\Includes;

// Deny direct access to this file.
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Common Notice Class.
 *
 * This class defines all code necessary to handle admin notices.
 *
 * @see        https://github.com/collizo4sky/persist-admin-notices-dismissal
 *
 * @author     Amitzy
 * @package    Molongui Common Framework (/fw)
 * @subpackage /fw/includes
 * @since      1.0.0
 * @version    1.2.4
 */
if ( !class_exists( 'Molongui\Fw\Includes\Notice' ) )
{
	class Notice
	{
		/**
		 * Renders a WP admin notice.
		 *
		 * @access  public
		 * @param   string $id              Notice unique identifier. Dismissal time period must be appended to the end of this identifier with a hyphen (-) and
		 *                                  this value must be an integer. The only exception is the string 'forever'.
		 * @param   string $type            Type of notice. Defaults to 'info'. Available options are { info | success | warning | error }.
		 * @param   array  $content         Content to show.
		 * @param   bool   $dismissible     Whether the notice is dismissible.
		 * @param   mixed  $dismissal       Dismissal time period. Defaults to '0'.
		 * @param   string $class           Extra classes to add to the notice DOM element.
		 * @param   array  $pages           Where to display the notice. If empty, notice is displayed anywhere.
		 * @return  void
		 * @since   1.0.0
		 * @version 1.2.4
		 */
		public static function display( $id, $type = 'info', $content = array(), $dismissible = false, $dismissal = 0, $class = '', $pages = array() )
		{
			// Dismissed notices won't be displayed.
			if ( !self::is_dismissed( $id ) ) return;

			// Where to display check.
			if ( !empty( $pages ) )
			{
				global $current_screen;
				if ( !in_array( $current_screen->id, $pages ) ) return;
			}

			// Render notice.
			?>
            <div id="<?php echo $id; ?>" data-dismissible="<?php echo $dismissal; ?>" class="notice notice-<?php echo $type; ?> molongui-notice <?php echo $class ?> <?php echo ( $dismissible ? 'is-dismissible' : '' ); ?>">
			    <?php if ( isset( $content['image'] ) and !empty( $content['image'] ) ) : ?>
                    <div class="molongui-notice-image"><img src="<?php echo $content['image']; ?>" /></div>
			    <?php endif; ?>
                <div class="molongui-notice-content">
			        <?php if ( isset( $content['title'] ) and !empty( $content['title'] ) ) : ?>
                        <div class="molongui-notice-title"><h3><?php echo $content['title']; ?></h3></div>
			        <?php endif; ?>
			        <?php if ( isset( $content['message'] ) and !empty( $content['message'] ) ) : ?>
                        <div class="molongui-notice-message"><p><?php echo $content['message']; ?></p></div>
                    <?php endif; ?>
                    <?php if ( isset( $content['buttons'] ) and !empty( $content['buttons'] ) ) : ?>
                        <div class="molongui-notice-buttons">
                            <?php foreach ( $content['buttons'] as $button ) : ?>
                                <?php if ( isset( $button['hidden'] ) and $button['hidden'] ) continue; ?>
                                <a href="<?php echo $button['href']; ?>" target="<?php echo $button['target']; ?>" class="molongui-notice-button <?php echo $button['class']; ?>"><?php echo ( ( isset( $button['icon'] ) and !empty( $button['icon'] ) ) ? $button['icon'].' ' : '' ).$button['label']; ?></a>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
			<?php
		}

		/**
		 * Handles Ajax request to persist notices dismissal.
		 *
		 * Transients are used to 'remember' dismissals because it is not critical information.
		 *
		 * @see     https://codex.wordpress.org/Transients_API
		 *
		 * @access  public
		 * @return  void
		 * @since   1.0.0
		 * @version 1.2.2
		 */
		public static function dismiss_notice()
		{
			// Security check. Die if invalid nonce.
			check_ajax_referer( 'molongui-js-nonce', 'security', true );

			// Parse data.
			$transient          = 0;
			$dismissible_id     = sanitize_text_field( $_POST['dismissible_id'] );
			$dismissible_length = sanitize_text_field( $_POST['dismissible_length'] );

			if ( 'forever' != $dismissible_length )
			{
				// If $dismissible_length is not an integer default to 1.
				$dismissible_length = ( 0 == absint( $dismissible_length ) ) ? 1 : $dismissible_length;
				$transient          = absint( $dismissible_length ) * DAY_IN_SECONDS;
				$dismissible_length = strtotime( absint( $dismissible_length ) . ' days' );
			}

			// Save notice dismissal.
			set_site_transient( $dismissible_id, $dismissible_length, $transient );

			// Avoid 'admin-ajax.php' to append the value outputted with a "0".
			wp_die();
		}

		/**
		 * Returns whether the notice should be displayed.
         *
         * @see     https://codex.wordpress.org/Transients_API
		 *
		 * @access  public
		 * @param   string  $id    Notice id.
		 * @return  bool
		 * @since   1.0.0
		 * @version 1.2.2
		 */
		public static function is_dismissed( $id )
		{
			$db_record = get_site_transient( $id );

			if ( 'forever' == $db_record ) return false;
            elseif ( absint( $db_record ) >= time() ) return false;
			else return true;
		}
	}
}