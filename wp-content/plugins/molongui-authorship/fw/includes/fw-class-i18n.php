<?php

namespace Molongui\Fw\Includes;

// Deny direct access to this file
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * i18n class.
 *
 * Loads and defines the internationalization files for this plugin so that it is ready for translation.
 *
 * @author     Amitzy
 * @package    Molongui Common Framework (/fw)
 * @subpackage /fw/includes
 * @since      1.0.0
 * @version    1.0.0
 */
if ( !class_exists( 'Molongui\Fw\Includes\i18n' ) )
{
	class i18n
	{
		/**
		 * The domain specified for this plugin.
		 *
		 * @access  private
		 * @var     string  $domain The domain identifier for this plugin.
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		private $domain;

		/**
		 * Loads the plugin text domain for translation.
		 *
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		public function load_plugin_textdomain()
		{
			// Load fw textdomain.
			load_plugin_textdomain(
				'molongui-common-framework',
				false,
				dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/i18n/'
			);

			// Load plugin textdomain.
			load_plugin_textdomain(
				$this->domain,
				false,
				dirname( dirname( dirname( plugin_basename( __FILE__ ) ) ) ) . '/i18n/'
			);
		}

		/**
		 * Sets the domain equal to that of the specified domain.
		 *
		 * @param   string  $domain The domain that represents the locale of this plugin.
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		public function set_domain( $domain )
		{
			$this->domain = $domain;
		}
	}
}
