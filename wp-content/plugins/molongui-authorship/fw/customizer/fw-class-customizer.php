<?php

namespace Molongui\Fw\Customizer;

// Deny direct access to this file.
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Class Customizer.
 *
 * @author     Amitzy
 * @package    Molongui Common Framework (/fw)
 * @subpackage /fw/customizer
 * @since      1.2.2
 * @version    1.2.4
 */
if ( !class_exists( 'Molongui\Fw\Customizer\Customizer' ) )
{
	class Customizer
	{
		/**
		 * Information of the plugin loading this class.
		 *
		 * @access  private
		 * @var     object
		 * @since   1.2.2
		 * @version 1.2.2
		 */
		private $plugin;

		/**
		 * Class constructor.
		 *
		 * @access  public
		 * @param   string      $plugin_id      Name of the plugin loading this class.
		 * @since   1.2.2
		 * @version 1.2.2
		 */
		public function __construct( $plugin_id )
		{
			// Initialize information of the plugin loading this class.
			molongui_get_plugin( $plugin_id, $this->plugin );

			// Load all custom control classes.
			foreach ( glob( plugin_dir_path( __FILE__ ).'controls/classes/*.php' ) as $file )
			{
				require_once $file;
			}

			// Load general customizer callbacks.
			require_once( 'customizer-helper-callbacks.php' );

			// Load plugin-specific wp-customizer functions.
			require_once( molongui_get_constant( $this->plugin->id, 'DIR', false ) . 'customizer/plugin-customizer-callbacks.php' );

			// Add plugin settings to the WP Customizer.
			add_action( 'customize_register', array( $this, 'molongui_customizer_settings' ) );

			// Enqueue needed files to automate the live settings preview in the Theme Customizer.
			add_action( 'customize_preview_init', array( $this, 'molongui_customizer_preview' ) );

			// Enqueue needed scripts to manipulate some controls based on other settings customized value in the Theme Customizer.
			add_action( 'customize_controls_enqueue_scripts', array( $this, 'molongui_customizer_hide_settings' ) );
		}

		/**
		 * Adds plugin settings to the WP Customizer.
		 *
		 * @see        https://developer.wordpress.org/reference/classes/wp_customize_manager/
		 *             https://developer.wordpress.org/reference/classes/wp_customize_manager/add_panel/
		 *             https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
		 *             https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
		 *
		 * @access  private
		 * @since   1.2.0
		 * @version 1.2.4
		 */
		public function molongui_customizer_settings( $wp_customize )
		{
			// Load customizer settings.
			$styles = include( molongui_get_constant( $this->plugin->id, 'DIR', false ) . 'config/customizer.php' );

			// Integrity check.
			if ( !isset( $styles ) and empty( $styles ) ) return;

			// Add panel.
			if ( $styles['add_panel'] )
			{
				$wp_customize->add_panel( $styles['id'], array
				(
					'title'           => $styles['title'],
					'description'     => $styles['description'],
					'priority'        => ( ( isset( $styles['priority'] ) and !empty( $styles['priority'] ) ) ? $styles['priority'] : '10' ),
					'capability'      => ( ( isset( $styles['capability'] ) and !empty( $styles['capability'] ) ) ? $styles['capability'] : 'manage_options' ),
					'active_callback' => ( ( isset( $styles['active_callback'] ) and !empty( $styles['active_callback'] ) ) ? $styles['active_callback'] : '' ),
				));
			}
			$wp_customize->add_panel( $styles['id'], array
			(
				'title'           => $styles['title'],
				'description'     => $styles['description'],
				'priority'        => ( ( isset( $styles['priority'] ) and !empty( $styles['priority'] ) ) ? $styles['priority'] : '10' ),
				'capability'      => ( ( isset( $styles['capability'] ) and !empty( $styles['capability'] ) ) ? $styles['capability'] : 'manage_options' ),
				'active_callback' => ( ( isset( $styles['active_callback'] ) and !empty( $styles['active_callback'] ) ) ? $styles['active_callback'] : '' ),
			));

			// Add sections.
			if ( isset( $styles['sections'] ) and !empty( $styles['sections'] ) )
			{
				foreach( $styles['sections'] as $section )
				{
					// Jump section if configured to not be displayed.
					if ( !$section['display'] ) continue;

					// Section properties.
					$args = array
					(
						'title'              => $section['title'],
						'description'        => $section['description'],
						'priority'           => ( ( isset( $section['priority'] ) and !empty( $section['priority'] ) ) ? $section['priority'] : 10 ),
						'type'               => ( ( isset( $section['type'] ) and !empty( $section['type'] ) ) ? $section['type'] : '' ),
						'capability'         => ( ( isset( $section['capability'] ) and !empty( $section['capability'] ) ) ? $section['capability'] : 'manage_options' ),
						'active_callback'    => ( ( isset( $section['active_callback'] ) and !empty( $section['active_callback'] ) ) ? $section['active_callback'] : '' ),
						'description_hidden' => ( ( isset( $section['description_hidden'] ) and !empty( $section['description_hidden'] ) ) ? $section['description_hidden'] : false ),
					);

					// If there is a panel, include its id.
					if ( $styles['add_panel'] ) $args['panel'] = $styles['id'];

					// Add section.
					$wp_customize->add_section( $section['id'] , $args );

					// Add settings.
					if ( isset( $section['fields'] ) and !empty( $section['fields'] ) )
					{
						foreach ( $section['fields'] as $field )
						{
							// Jump field if configured to not be displayed.
							if ( !$field['display'] ) continue;

							// Add setting.
							if ( isset( $field['setting'] ) and !empty( $field['setting'] ) )
							{
								$wp_customize->add_setting( $field['id'], array
								(
									'type'                 => ( ( isset( $field['setting']['type'] ) and !empty( $field['setting']['type'] ) ) ? $field['setting']['type'] : 'option' ),
									'capability'           => ( ( isset( $field['setting']['capability'] ) and !empty( $field['setting']['capability'] ) ) ? $field['setting']['capability'] : 'manage_options' ),
									'default'              => ( ( isset( $field['setting']['default'] ) and !empty( $field['setting']['default'] ) ) ? $field['setting']['default'] : '' ),
									'transport'            => ( ( isset( $field['setting']['transport'] ) and !empty( $field['setting']['transport'] ) ) ? $field['setting']['transport'] : 'refresh' ),
									'validate_callback'    => ( ( isset( $field['setting']['validate_callback'] ) and !empty( $field['setting']['validate_callback'] ) ) ? $field['setting']['validate_callback'] : '' ),
									'sanitize_callback'    => ( ( isset( $field['setting']['sanitize_callback'] ) and !empty( $field['setting']['sanitize_callback'] ) ) ? $field['setting']['sanitize_callback'] : '' ),
									'sanitize_js_callback' => ( ( isset( $field['setting']['sanitize_js_callback'] ) and !empty( $field['setting']['sanitize_js_callback'] ) ) ? $field['setting']['sanitize_js_callback'] : '' ),
									'dirty'                => ( ( isset( $field['setting']['dirty'] ) and !empty( $field['setting']['dirty'] ) ) ? $field['setting']['dirty'] : false ),
								));

								// Add control.
								if ( isset( $field['control'] ) and !empty( $field['control'] ) )
								{
									// Get control class.
									$class = ( ( isset( $field['control']['class'] ) and !empty( $field['control']['class'] ) ) ? $field['control']['class'] : 'WP_Customize_Control' );

									// Add control.
									$wp_customize->add_control( new $class( $wp_customize, $field['id'], array
									(
										'settings'        => $field['id'], // or also: array( 'default' => $field['id'] ),
										//'setting'       => ( ( isset( $field['control']['setting'] ) and !empty( $field['control']['setting'] ) ) ? $field['control']['setting'] : $field['id'] ),
										'capability'      => ( ( isset( $field['setting']['capability'] ) and !empty( $field['setting']['capability'] ) ) ? $field['setting']['capability'] : 'manage_options' ),
										'priority'        => ( ( isset( $field['control']['priority'] ) and !empty( $field['control']['priority'] ) ) ? $field['control']['priority'] : 10 ),
										'section'         => ( ( isset( $field['control']['section'] ) and !empty( $field['control']['section'] ) ) ? $field['control']['section'] : $section['id'] ),
										'label'           => ( ( isset( $field['control']['label'] ) and !empty( $field['control']['label'] ) ) ? $field['control']['label'] : '' ),
										'description'     => ( ( isset( $field['control']['description'] ) and !empty( $field['control']['description'] ) ) ? $field['control']['description'] : '' ),
										'choices'         => ( ( isset( $field['control']['choices'] ) and !empty( $field['control']['choices'] ) ) ? $field['control']['choices'] : array() ),
										'input_attrs'     => ( ( isset( $field['control']['input_attrs'] ) and !empty( $field['control']['input_attrs'] ) ) ? $field['control']['input_attrs'] : array() ),
										'allow_addition'  => ( ( isset( $field['control']['allow_addition'] ) and !empty( $field['control']['allow_addition'] ) ) ? $field['control']['allow_addition'] : false ),
										'type'            => ( ( isset( $field['control']['type'] ) and !empty( $field['control']['type'] ) ) ? $field['control']['type'] : 'text' ),
										'active_callback' => ( ( isset( $field['control']['active_callback'] ) and !empty( $field['control']['active_callback'] ) ) ? $field['control']['active_callback'] : '' ),
									)));
								}
							}
						}
					}
				}
			}
		}

		/**
		 * Enqueues the javascript needed to automate the live settings preview in the Theme Customizer.
		 *
		 * @see     https://codex.wordpress.org/Plugin_API/Action_Reference/customize_preview_init
		 *
		 * @access  private
		 * @since   1.2.0
		 * @version 1.2.2
		 */
		public function molongui_customizer_preview()
		{
			// Get variables.
			$plugin_dir  = molongui_get_constant( $this->plugin->id, 'DIR', false );
			$plugin_url  = molongui_get_constant( $this->plugin->id, 'URL', false );
			$plugin_slug = molongui_get_constant( $this->plugin->id, 'SLUG', false );

			// Enqueue plugin-specific styles.
			if ( !molongui_is_premium( $plugin_dir ) )
			{
				$fpath = 'customizer/css/live-preview.min.css';
				if ( file_exists( $plugin_dir.$fpath ) )
					wp_enqueue_style( $plugin_slug.'-preview', $plugin_url.$fpath, array(), $this->plugin->version );
			}

			// Enqueue plugin-specific scripts.
			$fpath = 'customizer/js/live-preview.min.js';
			if ( file_exists( $plugin_dir.$fpath ) )
			{
				wp_enqueue_script( $plugin_slug.'-preview', $plugin_url.$fpath, array( 'jquery', 'customize-preview' ), $this->plugin->version );
			}
		}

		/**
		 * Enqueues a script needed to manipulate some controls based on other settings customized value in the Theme Customizer.
		 *
		 * @see     http://www.jasong-designs.com/2017/05/04/hide-show-controls-wordpress-customizer/
		 *          https://florianbrinkmann.com/en/3783/conditional-displaying-and-hiding-of-customizer-controls-via-javascript/
		 *          https://wordpress.stackexchange.com/a/211953
		 *
		 * @access  private
		 * @since   1.2.0
		 * @version 1.2.4
		 */
		public function molongui_customizer_hide_settings()
		{
			// Get variables.
			$plugin_dir  = molongui_get_constant( $this->plugin->id, 'DIR', false );
			$plugin_url  = molongui_get_constant( $this->plugin->id, 'URL', false );
			$plugin_slug = molongui_get_constant( $this->plugin->id, 'SLUG', false );
			$fw_version  = molongui_get_constant( $this->plugin->id, 'VERSION', true );

			// Enqueue common styles.
			$fpath = 'fw/customizer/css/styles.cb33.min.css';
			if ( file_exists( $plugin_dir.$fpath ) )
			{
				wp_enqueue_style( 'molongui-framework-preview', $plugin_url.$fpath, array(), $fw_version );
			}

			// Enqueue common scripts.
			$fpath = 'fw/customizer/js/scripts.da39.min.js';
			if ( file_exists( $plugin_dir.$fpath ) )
			{
				wp_enqueue_script( 'molongui-framework-preview', $plugin_url.$fpath, array( 'jquery', 'customize-preview' ), $fw_version );

				if ( $this->plugin->is_upgradable and !$this->plugin->is_premium )
				{
					// Append "Go Premium" fake-section at the end of plugin sections.
					$script = sprintf( '
								( function($) {
									$( window ).load( function()
									{
										$( "#sub-accordion-panel-%s" ).append(
											"<li class=\"molongui-accordion-section-divider\">" +
												"<span>%s</span>" +
												"<p>%s</p>" +
											"</li>" +
											"<li class=\"accordion-section control-section control-section- control-subsection molongui-upgrade-link\">" +
												"<a href=\"%s\" target=\"_blank\"><h3 class=\"accordion-section-title\" tabindex=\"0\">%s</h3></a>" +
											"</li>"
										);
									});
								})(jQuery);
								', $this->plugin->slug, __( 'Premium features', 'molongui-framework-preview' ), __( 'Take a look at all the available premium features.', 'molongui-framework-preview' ), $this->plugin->web, __( 'Go Premium', 'molongui-framework-preview' ) );
					wp_add_inline_script( 'molongui-framework-preview', $script );
				}
			}

			// Enqueue plugin-specific scripts.
			$fpath = 'customizer/js/customizer-scripts.min.js';
			if ( file_exists( $plugin_dir.$fpath ) )
			{
				wp_enqueue_script( $plugin_slug.'-scripts', $plugin_url.$fpath, array( 'jquery', 'customize-preview' ), $this->plugin->version );
			}
		}

	}
}

/**
 * Auto-instantiate this class.
 *
 * @since   1.2.2
 * @version 1.2.2
 */
if ( class_exists( 'Molongui\Fw\Customizer\Customizer' ) )
{
	$molongui_customizer = new Customizer( $this->plugin_name );
}