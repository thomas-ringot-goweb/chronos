<?php

// Deny direct access to this file.
if ( !defined( 'ABSPATH' ) ) exit;

// Make sure base class is loaded.
if ( !class_exists('WP_Customize_Control' ) ) include_once ABSPATH . 'wp-includes/class-wp-customize-control.php';

/**
 * Range custom control.
 *
 * This range control is the same than the default one except that this one displays the value of the slider.
 *
 * To load this control, an array of data like the one below is required:
 *
 *      array
 *      (
 *          'id'      => 'setting_id',
 *          'display' => true,
 *          'setting' => array
 *          (
 *              'type'                 => 'option',
 *              'capability'           => 'manage_options',
 *              'default'              => '12',
 *              'transport'            => { 'refresh' | 'postMessage' },
 *              'validate_callback'    => '',
 *              'sanitize_callback'    => '',
 *              'sanitize_js_callback' => '',
 *              'dirty'                => false,
 *          ),
 *          'control' => array
 *          (
 *              'label'           => __( 'Setting label', 'molongui-common-framework' ),
 *              'description'     => __( 'Setting description if necessary.', 'molongui-common-framework' ),
 *              'priority'        => 10,
 *              'class'           => 'Molongui_Customize_Range_Control',
 *              'type'            => { 'molongui-compact-range-plain' | 'molongui-range-plain' | 'molongui-compact-range-flat' | 'molongui-range-flat' },
 *              'allow_addition'  => { true | false },
 *              'active_callback' => '',
 *              'input_attrs'     => array
 *              (
 *                  'premium' => { true | false },
 *                  'min'     => 8,
 *                  'max'     => 100,
 *                  'step'    => 1,
 *                  'suffix'  => 'px',
 *              ),
 *              'choices'         => array(),
 *          ),
 *      )
 *
 * @see        http://ottopress.com/2012/making-a-custom-control-for-the-theme-customizer/
 *             https://w3guy.com/wordpress-customizer-range-control-selected-indicator/
 *             https://github.com/soderlind/class-customizer-range-value-control
 *
 * @author     Amitzy
 * @package    Molongui Common Framework (/fw)
 * @subpackage /fw/customizer/controls/classes/range
 * @since      1.2.0
 * @version    1.2.4
 */
if ( !class_exists( 'Molongui_Customize_Range_Control' ) )
{
	class Molongui_Customize_Range_Control extends WP_Customize_Control
    {
        public $type = 'molongui-range';

        /**
         * Enqueues custom scripts and styles for the control.
         *
         * @since   1.2.0
         * @version 1.2.2
         */
        public function enqueue()
        {
            wp_enqueue_script(
	            'molongui-custom-controls',
	            plugin_dir_url( dirname( __FILE__ ) ).'js/scripts.min.js',
                array( 'jquery' ),
                false,
                true
            );

            wp_enqueue_style(
	            'molongui-custom-controls',
	            plugin_dir_url( dirname( __FILE__ ) ).'css/styles.min.css',
                array(),
                false,
                'all'
            );
        }

        /**
         * Renders the control.
         *
         * @since   1.2.0
         * @version 1.2.4
         */
        public function render_content()
        {
	        // Prepare data.
	        $input_id         = '_customize-input-' . $this->id;
	        $description_id   = '_customize-description-' . $this->id;
	        $describedby_attr = ( ! empty( $this->description ) ) ? ' aria-describedby="' . esc_attr( $description_id ) . '" ' : '';

	        // Check whether it is a molongui-compact control.
	        $string  = 'molongui-compact-';
	        $compact = ( substr( $this->type, 0, strlen( $string ) ) === $string ? true : false );

	        // Retrieve control style/theme.
            $style = explode( '-', $this->type );
            $style = end( $style );

            ?>
            <div class="molongui-range-control">

                <label for="<?php echo esc_attr( $input_id ); ?>" >

	                <?php if ( !$compact ) : ?>

                        <?php if ( !empty( $this->label ) ) : ?>
                            <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
                            <?php if ( isset( $this->input_attrs['premium'] ) and $this->input_attrs['premium'] ) : ?>
                                <img class="molongui-premium-setting-label" src="<?php echo plugins_url( '/', dirname( dirname( plugin_dir_path( __FILE__ ) ) ) ); ?>admin/img/lock.png" title="<?php _e( 'Premium setting. Only preview available.', 'molongui_common_framework' ); ?>" />
                            <?php endif; ?>
                        <?php endif; ?>

                        <?php if ( !empty( $this->description ) ) : ?>
                            <span id="<?php echo esc_attr( $description_id ); ?>" class="description customize-control-description"><?php echo $this->description; ?></span>
                        <?php endif; ?>

	                <?php else : ?>

                        <div class="molongui-compact-setting">
                            <div class="molongui-compact-setting-label"><?php echo $this->label; ?></div>
                            <div class="molongui-compact-setting-input">

                    <?php endif; ?>

                    <?php switch( $style )
	                {
		                case 'plain': ?>
                            <div class="molongui-range-slider-plain">
                                <input id="<?php echo esc_attr( $input_id ); ?>" <?php echo $describedby_attr; ?> class="molongui-range-slider-plain__input" data-input-type="range" type="range" <?php $this->input_attrs(); ?> value="<?php echo esc_attr( $this->value() ); ?>" <?php $this->link(); ?> />
                                <div class="molongui-range-slider-plain__value"><?php echo esc_attr( $this->value() ).'%'; ?></div>
                            </div>
			            <?php break;

		                case 'flat': ?>
                            <div class="molongui-range-slider-flat">
                                <span class="molongui-range-slider-flat-wrapper">
                                    <input class="molongui-range-slider-flat__range" type="range" value="<?php echo esc_attr( $this->value() ); ?>" <?php $this->input_attrs(); $this->link(); ?>>
                                    <span class="molongui-range-slider-flat__value">0</span>
                                </span>
                            </div>
			            <?php break;

	                } ?>

	                <?php if ( $compact ) : ?>

                            </div> <!-- !.molongui-compact-setting-input -->
                        </div> <!-- !.molongui-compact-setting -->

                    <?php endif; ?>

                </label>

            </div> <!-- !.molongui-range-control -->
	        <?php
        }
    }
}