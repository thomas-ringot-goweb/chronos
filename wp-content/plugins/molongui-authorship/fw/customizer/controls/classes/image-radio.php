<?php

// Deny direct access to this file.
if ( !defined( 'ABSPATH' ) ) exit;

// Make sure base class is loaded.
if ( !class_exists('WP_Customize_Control' ) ) include_once ABSPATH . 'wp-includes/class-wp-customize-control.php';

/**
 * Image Radio Button custom control.
 *
 * To load this control, an array of data like the one below is required:
 *
 *      array
 *      (
 *          'id'      => 'setting_id',
 *          'display' => true,
 *          'setting' => array
 *          (
 *              'type'                 => 'option',
 *              'capability'           => 'manage_options',
 *              'default'              => 'option-1',
 *              'transport'            => { 'refresh' | 'postMessage' },
 *              'validate_callback'    => '',
 *              'sanitize_callback'    => '',
 *              'sanitize_js_callback' => '',
 *              'dirty'                => false,
 *          ),
 *          'control' => array
 *          (
 *              'label'           => __( 'Setting label', 'molongui-common-framework' ),
 *              'description'     => __( 'Setting description if necessary.', 'molongui-common-framework' ),
 *              'priority'        => 10,
 *              'class'           => 'Molongui_Customize_Image_Radio_Button_Control',
 *              'type'            => { 'molongui-compact-image-radio' | 'molongui-image-radio' },
 *              'allow_addition'  => { true | false },
 *              'active_callback' => '',
 *              'input_attrs'     => array
 *              (
 *                  'premium' => { true | false },
 *              ),
 *              'choices'         => array
 *              (
 *                  'option-1' => array
 *                  (
 *                      'label'    => __( 'Label option 1', 'molongui-common-framework' ),
 *                      'image'    => MOLONGUI_COMMON_FRAMEWORK_URL.'fw/customizer/img/group/variant_1.png',
 *                      'disabled' => { true | false },
 *                      'premium'  => { true | false },
 *                  ),
 *                  'option-2' => array
 *                  (
 *                      'label'    => __( 'Label option 2', 'molongui-common-framework' ),
 *                      'image'    => MOLONGUI_COMMON_FRAMEWORK_URL.'fw/customizer/img/group/variant_2.png',
 *                      'disabled' => { true | false },
 *                      'premium'  => { true | false },
 *                  ),
 *                  'option-3' => array
 *                  (
 *                      'label'    => __( 'Label option 3', 'molongui-common-framework' ),
 *                      'image'    => MOLONGUI_COMMON_FRAMEWORK_URL.'fw/customizer/img/group/variant_2.png',
 *                      'disabled' => { true | false },
 *                      'premium'  => { true | false },
 *                  ),
 *              ),
 *          ),
 *      )
 *
 * @see        https://github.com/maddisondesigns/customizer-custom-controls
 *
 * @author     Amitzy
 * @package    Molongui Common Framework (/fw)
 * @subpackage /fw/customizer/controls/classes/image-radio
 * @since      1.2.2
 * @version    1.2.4
 */
if ( !class_exists( 'Molongui_Customize_Image_Radio_Button_Control' ) )
{
	class Molongui_Customize_Image_Radio_Button_Control extends WP_Customize_Control
	{
		public $type = 'molongui-image-radio';

		/**
		 * Enqueues custom scripts and styles for the control.
		 *
		 * @since   1.2.2
		 * @version 1.2.2
		 */
		public function enqueue()
		{
			wp_enqueue_style(
				'molongui-custom-controls',
				plugin_dir_url( dirname( __FILE__ ) ).'css/styles.min.css',
				array(),
				false,
				'all'
			);
		}

		/**
		 * Renders the control.
		 *
		 * @since   1.2.2
		 * @version 1.2.4
		 */
		public function render_content()
		{
			// Leave if no select options to show.
			if ( empty( $this->choices ) ) return;

			// Prepare data.
			$input_id         = '_customize-input-' . $this->id;
			$description_id   = '_customize-description-' . $this->id;
			$describedby_attr = ( ! empty( $this->description ) ) ? ' aria-describedby="' . esc_attr( $description_id ) . '" ' : '';

			// Check whether it is a molongui-compact control.
			$string  = 'molongui-compact-';
			$compact = ( substr( $this->type, 0, strlen( $string ) ) === $string ? true : false );

			?>
            <div class="molongui-image-radio-button-control">

			    <?php if ( !$compact ) : ?>

                    <?php if ( isset( $this->label ) and !empty( $this->label ) ) : ?>
                        <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
                    <?php endif; ?>

                    <?php if ( isset( $this->description ) and !empty( $this->description ) ) : ?>
                        <span id="<?php echo esc_attr( $description_id ); ?>" class="description customize-control-description"><?php echo $this->description; ?></span>
                    <?php endif; ?>

                <?php else : ?>

                    <div class="molongui-compact-setting">
                        <div class="molongui-compact-setting-label"><?php echo $this->label; ?></div>
                        <div class="molongui-compact-setting-input">

                <?php endif; ?>

				<?php foreach ( $this->choices as $key => $value ) : ?>
                    <label class="radio-button-label">
                        <input type="radio" id="<?php echo esc_attr( $input_id ); ?>" name="<?php echo esc_attr( $this->id ); ?>" data-molongui-setting="<?php echo ( $value['premium'] ? 'premium' : 'free' ); ?>" value="<?php echo esc_attr( $key ); ?>" <?php checked( esc_attr( $key ), $this->value() ); ?> <?php echo $describedby_attr; ?> <?php $this->link(); ?> />
                        <img src="<?php echo esc_attr( $value['image'] ); ?>" alt="<?php echo esc_attr( $value['label'] ); ?>" title="<?php echo esc_attr( $value['label'] ); ?>" />
			            <?php if ( $value['premium'] ) : ?><img class="molongui-premium-setting-label" src="<?php echo plugins_url( '/', dirname( dirname( plugin_dir_path( __FILE__ ) ) ) ); ?>admin/img/lock.png" title="<?php _e( 'Premium option. Only preview available.', 'molongui_common_framework' ); ?>" /><?php endif; ?>
                    </label>
				<?php endforeach; ?>

	            <?php if ( $compact ) : ?>

                        </div> <!-- !.molongui-compact-setting-input -->
                    </div> <!-- !.molongui-compact-setting -->

                <?php endif; ?>

            </div> <!-- !.molongui-image-radio-button-control -->
			<?php
		}
	}
}