<?php

// Deny direct access to this file.
if ( !defined( 'ABSPATH' ) ) exit;

// Make sure base class is loaded.
if ( !class_exists('WP_Customize_Control' ) ) include_once ABSPATH . 'wp-includes/class-wp-customize-control.php';

/**
 * Group label custom control.
 *
 * @author     Amitzy
 * @package    Molongui Common Framework (/fw)
 * @subpackage /fw/customizer/controls/classes/group-label
 * @since      1.2.4
 * @version    1.2.4
 */
if ( !class_exists( 'Molongui_Customize_Group_Label_Control' ) )
{
	class Molongui_Customize_Group_Label_Control extends WP_Customize_Control
	{
		public $type = 'molongui-group-label';

		/**
		 * Enqueues custom scripts and styles for the control.
		 *
		 * @since   1.2.4
		 * @version 1.2.4
		 */
		public function enqueue()
		{
			wp_enqueue_style(
				'molongui-custom-controls',
				plugin_dir_url( dirname( __FILE__ ) ).'css/styles.min.css',
				array(),
				false,
				'all'
			);
		}

		/**
		 * Renders the control.
		 *
		 * @since   1.2.4
		 * @version 1.2.4
		 */
		public function render_content()
		{
			?>
			<div class="molongui-group-label-control">

				<?php if( isset( $this->label ) and !empty( $this->label ) ) : ?>
					<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<?php endif; ?>

				<?php if( isset( $this->description ) and !empty( $this->description ) ) : ?>
					<span class="customize-control-description"><?php echo esc_html( $this->description ); ?></span>
				<?php endif; ?>

			</div> <!-- !.molongui-group-label-control -->
			<?php
		}
	}
}