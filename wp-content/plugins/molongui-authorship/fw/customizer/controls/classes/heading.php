<?php

// Deny direct access to this file.
if ( !defined( 'ABSPATH' ) ) exit;

// Make sure base class is loaded.
if ( !class_exists('WP_Customize_Control' ) ) include_once ABSPATH . 'wp-includes/class-wp-customize-control.php';

/**
 * Divider custom control.
 *
 * Outputs arbitrary HTML into your Theme Customizer section without actually controlling a setting.
 *
 * @see        https://coreymckrill.com/2014/01/09/adding-arbitrary-html-to-a-wordpress-theme-customizer-section/
 *
 * @author     Amitzy
 * @package    Molongui Common Framework (/fw)
 * @subpackage /fw/customizer/controls/classes/divider
 * @since      1.2.0
 * @version    1.2.0
 */
if ( !class_exists( 'Molongui_Customize_Heading_Control' ) )
{
	class Molongui_Customize_Heading_Control extends WP_Customize_Control
	{
		public $type = 'molongui-heading';

		/**
		 * Renders the control.
		 *
		 * @since   1.2.0
		 * @version 1.2.0
		 */
		public function render_content()
		{
			?>
				<?php if ( !empty( $this->label ) ) : ?>
                    <h2 style="margin-top: 2em; margin-bottom: 0; border-bottom:1px solid lightgrey; padding-bottom:10px; <?php $this->input_attrs(); ?>">
                        <?php echo esc_html( $this->label ); ?>
                    </h2>
                    <?php if ( !empty( $this->description ) ) : ?>
                        <p style="margin-top:10px; margin-bottom:0; font-size:95%; color: #909090;"><?php echo esc_html( $this->description ); ?></p>
                    <?php endif; ?>
                <?php endif; ?>
			<?php
		}
	}
}