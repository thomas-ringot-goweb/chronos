<?php

// Deny direct access to this file.
if ( !defined( 'ABSPATH' ) ) exit;

// Make sure base class is loaded.
if ( !class_exists('WP_Customize_Control' ) ) include_once ABSPATH . 'wp-includes/class-wp-customize-control.php';

/**
 * Divider custom control.
 *
 * Outputs arbitrary HTML into your Theme Customizer section without actually controlling a setting.
 *
 * @see        https://coreymckrill.com/2014/01/09/adding-arbitrary-html-to-a-wordpress-theme-customizer-section/
 *
 * @author     Amitzy
 * @package    Molongui Common Framework (/fw)
 * @subpackage /fw/customizer/controls/classes/divider
 * @since      1.2.0
 * @version    1.2.2
 */
if ( !class_exists( 'Molongui_Customize_Divider_Control' ) )
{
	class Molongui_Customize_Divider_Control extends WP_Customize_Control
	{
		public $type = 'molongui-divider';

		/**
		 * Renders the control.
		 *
		 * @since   1.2.0
		 * @version 1.2.2
		 */
		public function render_content()
		{
			?>
                <hr>
			<?php
		}
	}
}