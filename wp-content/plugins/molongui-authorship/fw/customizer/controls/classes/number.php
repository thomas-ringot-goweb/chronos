<?php

// Deny direct access to this file.
if ( !defined( 'ABSPATH' ) ) exit;

// Make sure base class is loaded.
if ( !class_exists('WP_Customize_Control' ) ) include_once ABSPATH . 'wp-includes/class-wp-customize-control.php';

/**
 * Dropdown custom control.
 *
 * @see        https://developer.wordpress.org/reference/classes/wp_customize_control
 *
 * @author     Amitzy
 * @package    Molongui Common Framework (/fw)
 * @subpackage /fw/customizer/controls/classes/number
 * @since      1.2.0
 * @version    1.2.4
 */
if ( !class_exists( 'Molongui_Customize_Number_Control' ) )
{
	class Molongui_Customize_Number_Control extends WP_Customize_Control
	{
		/**
		 * The type of control being rendered.
		 *
		 * @since   1.2.0
		 * @version 1.2.2
		 */
		public $type = 'molongui-number';

		/**
		 * Renders the control.
		 *
		 * @since   1.2.0
		 * @version 1.2.4
		 */
		public function render_content()
		{
			$input_id         = '_customize-input-' . $this->id;
			$description_id   = '_customize-description-' . $this->id;
			$describedby_attr = ( ! empty( $this->description ) ) ? ' aria-describedby="' . esc_attr( $description_id ) . '" ' : '';

			// Handle default values.

			?>
			<?php if ( ! empty( $this->label ) ) : ?>
                <label for="<?php echo esc_attr( $input_id ); ?>" class="customize-control-title">
                    <?php echo esc_html( $this->label ); ?>
                    <?php if ( isset( $this->input_attrs['premium'] ) and $this->input_attrs['premium'] ) : ?>
                        <img class="molongui-premium-setting-label" style="width: 18px;" src="<?php echo plugins_url( '/', dirname( dirname( plugin_dir_path( __FILE__ ) ) ) ); ?>admin/img/lock.png" title="<?php _e( 'Premium setting. Only preview available.', 'molongui_common_framework' ); ?>" />
                    <?php endif; ?>
                </label>
			<?php endif; ?>

			<?php if ( ! empty( $this->description ) ) : ?>
				<span id="<?php echo esc_attr( $description_id ); ?>" class="description customize-control-description"><?php echo $this->description; ?></span>
			<?php endif; ?>

			<input type="number" id="<?php echo esc_attr( $input_id ); ?>" <?php echo $describedby_attr; ?>
			       min="<?php echo $this->input_attrs['min']; ?>" max="<?php echo $this->input_attrs['max']; ?>" step="<?php echo $this->input_attrs['step']; ?>"
				   <?php if ( !isset( $this->input_attrs['value'] ) ) : ?>
						value="<?php echo esc_attr( $this->value() ); ?>"
				   <?php endif; ?>
				   <?php $this->link(); ?>
			/>

			<?php
		}
	}
}