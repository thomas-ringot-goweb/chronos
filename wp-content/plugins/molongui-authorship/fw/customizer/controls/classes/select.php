<?php

// Deny direct access to this file.
if ( !defined( 'ABSPATH' ) ) exit;

// Make sure base class is loaded.
if ( !class_exists('WP_Customize_Control' ) ) include_once ABSPATH . 'wp-includes/class-wp-customize-control.php';

/**
 * Dropdown custom control.
 *
 * To load this control, an array of data like the one below is required:
 *
 *      array
 *      (
 *          'id'      => 'setting_id',
 *          'display' => true,
 *          'setting' => array
 *          (
 *              'type'                 => 'option',
 *              'capability'           => 'manage_options',
 *              'default'              => 'option-1',
 *              'transport'            => { 'refresh' | 'postMessage' },
 *              'validate_callback'    => '',
 *              'sanitize_callback'    => '',
 *              'sanitize_js_callback' => '',
 *              'dirty'                => false,
 *          ),
 *          'control' => array
 *          (
 *              'label'           => __( 'Setting label', 'molongui-common-framework' ),
 *              'description'     => __( 'Setting description if necessary.', 'molongui-common-framework' ),
 *              'priority'        => 10,
 *              'class'           => 'Molongui_Customize_Select_Control',
 *              'type'            => { 'molongui-compact-select' | 'molongui-select' },
 *              'allow_addition'  => { true | false },
 *              'active_callback' => '',
 *              'input_attrs'     => array
 *              (
 *                  'premium' => { true | false },
 *              ),
 *              'choices'         => array
 *              (
 *                  'option-1' => array
 *                  (
 *                      'label'    => __( 'Label option 1', 'molongui-common-framework' ),
 *                      'disabled' => { true | false },
 *                      'premium'  => { true | false },
 *                  ),
 *                  'option-2' => array
 *                  (
 *                      'label'    => __( 'Label option 2', 'molongui-common-framework' ),
 *                      'disabled' => { true | false },
 *                      'premium'  => { true | false },
 *                  ),
 *                  'option-3' => array
 *                  (
 *                      'label'    => __( 'Label option 3', 'molongui-common-framework' ),
 *                      'disabled' => { true | false },
 *                      'premium'  => { true | false },
 *                  ),
 *              ),
 *          ),
 *      )
 *
 * @see        https://developer.wordpress.org/reference/classes/wp_customize_control
 *
 * @author     Amitzy
 * @package    Molongui Common Framework (/fw)
 * @subpackage /fw/customizer/controls/classes/select
 * @since      1.2.0
 * @version    1.2.4
 */
if ( !class_exists( 'Molongui_Customize_Select_Control' ) )
{
	class Molongui_Customize_Select_Control extends WP_Customize_Control
	{
		/**
		 * The type of control being rendered.
		 *
		 * @since   1.2.0
		 * @version 1.2.2
		 */
		public $type = 'molongui-select';

		/**
		 * Renders the control.
		 *
		 * @since   1.2.0
		 * @version 1.2.4
		 */
		public function render_content()
		{
		    // Leave if no select options to show.
			if ( empty( $this->choices ) ) return;

			// Prepare data.
			$input_id         = '_customize-input-' . $this->id;
			$description_id   = '_customize-description-' . $this->id;
			$describedby_attr = ( ! empty( $this->description ) ) ? ' aria-describedby="' . esc_attr( $description_id ) . '" ' : '';

			// Check whether it is a molongui-compact control.
            $string  = 'molongui-compact-';
			$compact = ( substr( $this->type, 0, strlen( $string ) ) === $string ? true : false );

			?>
            <div class="molongui-select-control">

                <?php if ( !$compact ) : ?>

                    <?php if ( isset( $this->label ) and !empty( $this->label ) ) : ?>
                        <label for="<?php echo esc_attr( $input_id ); ?>" class="customize-control-title">
                            <?php echo esc_html( $this->label ); ?>
                            <?php if ( isset( $this->input_attrs['premium'] ) and $this->input_attrs['premium'] ) : ?>
                                <img class="molongui-premium-setting-label" style="width: 18px;" src="<?php echo plugins_url( '/', dirname( dirname( plugin_dir_path( __FILE__ ) ) ) ); ?>admin/img/lock.png" title="<?php _e( 'Premium setting. Only preview available.', 'molongui_common_framework' ); ?>" />
                            <?php endif; ?>
                        </label>
                    <?php endif; ?>

                    <?php if ( !empty( $this->description ) ) : ?>
                        <span id="<?php echo esc_attr( $description_id ); ?>" class="description customize-control-description"><?php echo $this->description; ?></span>
                    <?php endif; ?>

                <?php else : ?>

                    <div class="molongui-compact-setting">
                        <div class="molongui-compact-setting-label"><?php echo $this->label; ?></div>
                        <div class="molongui-compact-setting-input">

                <?php endif; ?>

                            <select id="<?php echo esc_attr( $input_id ); ?>" <?php echo $describedby_attr; ?> <?php $this->link(); ?>>
                                <?php foreach ( $this->choices as $value => $choice ) : ?>
                                    <option value="<?php echo esc_attr( $value ); ?>" <?php selected( $this->value(), $value, true ); ?> <?php echo ( ( isset( $choice['disabled'] ) and $choice['disabled'] ) ? 'disabled' : '' ); ?> ><?php echo esc_attr( $choice['label'] ); ?></option>
                                <?php endforeach; ?>
                            </select>

                <?php if ( $compact ) : ?>

                        </div> <!-- !.molongui-compact-setting-input -->
                    </div> <!-- !.molongui-compact-setting -->

		        <?php endif; ?>

            </div> <!-- !.molongui-select-control -->
            <?php
		}
	}
}