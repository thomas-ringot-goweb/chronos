<?php

// Deny direct access to this file.
if ( !defined( 'ABSPATH' ) ) exit;

// Make sure base class is loaded.
if ( !class_exists('WP_Customize_Control' ) ) include_once ABSPATH . 'wp-includes/class-wp-customize-control.php';

/**
 * Color picker custom control.
 *
 * To load this control, an array of data like the one below is required:
 *
 *      array
 *      (
 *          'id'      => 'setting_id',
 *          'display' => true,
 *          'setting' => array
 *          (
 *              'type'                 => 'option',
 *              'capability'           => 'manage_options',
 *              'default'              => 'inherit',
 *              'transport'            => 'postMessage',
 *              'validate_callback'    => '',
 *              'sanitize_callback'    => '',
 *              'sanitize_js_callback' => '',
 *              'dirty'                => false,
 *          ),
 *          'control' => array
 *          (
 *              'label'           => __( 'Setting label', 'molongui-common-framework' ),
 *              'description'     => __( 'Setting description if necessary.', 'molongui-common-framework' ),
 *              'priority'        => 10,
 *              'class'           => 'Molongui_Customize_Color_Control',
 *              'type'            => { 'molongui-compact-color' | 'molongui-color' },
 *              'allow_addition'  => { true | false },
 *              'active_callback' => '',
 *              'input_attrs'     => array
 *              (
 *                  'premium' => { true | false },
 *              ),
 *              'choices'         => array(),
 *          ),
 *      )
 *
 * @see        https://github.com/maddisondesigns/customizer-custom-controls
 *
 * @author     Amitzy
 * @package    Molongui Common Framework (/fw)
 * @subpackage /fw/customizer/controls/classes/range
 * @since      1.2.0
 * @version    1.2.5
 */
if ( !class_exists( 'Molongui_Customize_Color_Control' ) )
{
	class Molongui_Customize_Color_Control extends WP_Customize_Control
	{
		public $type = 'molongui-color';

		/**
		 * Add support for palettes to be passed in.
		 *
		 * Supported palette values are true, false, or an array of RGBa and Hex colors.
		 *
		 * @since   1.2.0
		 * @version 1.2.0
		 */
		public $palette;

		/**
		 * Add support for showing the opacity value on the slider handle.
		 *
		 * @since   1.2.0
		 * @version 1.2.0
		 */
		public $show_opacity;

		/**
		 * Enqueues custom scripts and styles for the control.
		 *
		 * @since   1.2.0
		 * @version 1.2.2
		 */
		public function enqueue()
		{
			wp_enqueue_script(
				'molongui-custom-controls',
				plugin_dir_url( dirname( __FILE__ ) ).'js/scripts.min.js',
				array( 'jquery', 'wp-color-picker' ),
				false,
				true
			);

			wp_enqueue_style(
				'molongui-custom-controls',
				plugin_dir_url( dirname( __FILE__ ) ).'css/styles.min.css',
				array( 'wp-color-picker' ),
				false,
				'all'
			);
		}

		/**
		 * Renders the control.
		 *
		 * @since   1.2.0
		 * @version 1.2.5
		 */
		public function render_content()
		{
			// Prepare data.
			$input_id         = '_customize-input-' . $this->id;
			$description_id   = '_customize-description-' . $this->id;
			$describedby_attr = ( ! empty( $this->description ) ) ? ' aria-describedby="' . esc_attr( $description_id ) . '" ' : '';

			// Check whether it is a molongui-compact control.
			$string  = 'molongui-compact-';
			$compact = ( substr( $this->type, 0, strlen( $string ) ) === $string ? true : false );

			// Process the palette.
			if ( is_array( $this->palette ) )
			{
				$palette = implode( '|', $this->palette );
			}
			else
			{
				// Default to true.
				$palette = ( false === $this->palette || 'false' === $this->palette ) ? 'false' : 'true';
			}

			// Support passing show_opacity as string or boolean. Default to true.
			$show_opacity = ( false === $this->show_opacity || 'false' === $this->show_opacity ) ? 'false' : 'true';

			?>
            <div class="molongui-color-control">

	            <?php if ( !$compact ) : ?>

		            <?php if ( isset( $this->label ) and !empty( $this->label ) ) : ?>
                        <label for="<?php echo esc_attr( $input_id ); ?>" class="customize-control-title">
                            <?php echo esc_html( $this->label ); ?>
                            <?php if ( isset( $this->input_attrs['premium'] ) and $this->input_attrs['premium'] ) : ?>
                                <img class="molongui-premium-setting-label" src="<?php echo plugins_url( '/', dirname( dirname( plugin_dir_path( __FILE__ ) ) ) ); ?>admin/img/lock.png" title="<?php _e( 'Premium setting. Only preview available.', 'molongui_common_framework' ); ?>" />
                            <?php endif; ?>
                        </label>
                    <?php endif; ?>

                    <?php if ( !empty( $this->description ) ) : ?>
                        <span id="<?php echo esc_attr( $description_id ); ?>" class="description customize-control-description"><?php echo $this->description; ?></span>
                    <?php endif; ?>

                <?php else : ?>

                    <div class="molongui-compact-setting molongui-compact-color-setting">
                        <div class="molongui-compact-setting-label"><?php echo $this->label; ?></div>
                        <div class="molongui-compact-setting-input">
			                <?php if ( isset( $this->input_attrs['premium'] ) and $this->input_attrs['premium'] ) : ?>
                                <div class="molongui-premium-setting-label" title="<?php _e( 'Premium setting. Only preview available.', 'molongui_common_framework' ); ?>"></div>
			                <?php endif; ?>

                <?php endif; ?>

                            <input id="<?php echo esc_attr( $input_id ); ?>" <?php echo $describedby_attr; ?> data-molongui-setting="<?php echo ( ( isset( $this->input_attrs['premium'] ) and $this->input_attrs['premium'] ) ? 'premium' : 'free' ); ?>" class="alpha-color-control" type="text" data-show-opacity="<?php echo $show_opacity; ?>" data-palette="<?php echo esc_attr( $palette ); ?>" <?php $this->link(); ?>  />

                <?php if ( $compact ) : ?>

                        </div> <!-- !.molongui-compact-setting-input -->
                    </div> <!-- !.molongui-compact-setting -->

                <?php endif; ?>

            </div> <!-- !.molongui-select-control -->
			<?php
		}
	}
}