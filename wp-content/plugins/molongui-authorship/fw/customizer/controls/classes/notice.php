<?php

// Deny direct access to this file.
if ( !defined( 'ABSPATH' ) ) exit;

// Make sure base class is loaded.
if ( !class_exists('WP_Customize_Control' ) ) include_once ABSPATH . 'wp-includes/class-wp-customize-control.php';

/**
 * Notice custom control.
 *
 * @see        https://github.com/maddisondesigns/customizer-custom-controls
 *
 * @author     Amitzy
 * @package    Molongui Common Framework (/fw)
 * @subpackage /fw/customizer/controls/classes/notice
 * @since      1.2.0
 * @version    1.2.2
 */
if ( !class_exists( 'Molongui_Customize_Notice_Control' ) )
{
	class Molongui_Customize_Notice_Control extends WP_Customize_Control
	{
		public $type = 'molongui-notice';

		/**
		 * Enqueues custom scripts and styles for the control.
		 *
		 * @since   1.2.0
		 * @version 1.2.2
		 */
		public function enqueue()
		{
			wp_enqueue_style(
				'molongui-custom-controls',
				plugin_dir_url( dirname( __FILE__ ) ).'css/styles.min.css',
				array(),
				false,
				'all'
			);
		}

		/**
		 * Renders the control.
		 *
		 * @since   1.2.0
		 * @version 1.2.0
		 */
		public function render_content()
		{
			$allowed_html = array(
				'a' => array
				(
					'href'   => array(),
					'target' => array(),
					'title'  => array(),
					'class'  => array(),
                    'style'  => array(),
				),
				'br'     => array(),
				'em'     => array(),
				'strong' => array(),
				'i'      => array
				(
					'class' => array(),
					'style' => array(),
				),
				'span'   => array
				(
					'class' => array(),
					'style' => array(),
				),
				'code'   => array(),
			);
			?>
			<div class="molongui-notice-control" style="<?php echo ( !empty( $this->input_attrs['bg'] ) ? 'background:'.$this->input_attrs['bg'].';' : '' ); ?> <?php echo ( !empty( $this->input_attrs['color'] ) ? 'color:'.$this->input_attrs['color'].';' : '' ); ?>">
				<?php if( !empty( $this->label ) ) : ?>
					<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<?php endif; ?>
				<?php if( !empty( $this->description ) ) : ?>
					<span class="customize-control-description"><?php echo wp_kses( $this->description, $allowed_html ); ?></span>
				<?php endif; ?>
			</div>
			<?php
		}
	}
}