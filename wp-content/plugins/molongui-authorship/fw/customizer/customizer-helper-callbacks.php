<?php

// Deny direct access to this file.
if ( !defined( 'ABSPATH' ) ) exit;

// Load useful functions.
require_once molongui_get_constant( $this->plugin->id, 'DIR', true ) . 'includes/fw-helper-functions.php';

/**
 * Plugin-specific functions for WP Customizer.
 *
 * @see        http://ottopress.com/2015/whats-new-with-the-customizer/
 *
 * @author     Amitzy
 * @package    Molongui Common Framework (/fw)
 * @subpackage /fw/customizer
 * @since      1.2.2
 * @version    1.2.2
 */

/***********************************************************************************************************************
 * VALIDATE CALLBACKS
 **********************************************************************************************************************/

// TODO...

/***********************************************************************************************************************
 * SANITIZE CALLBACKS
 **********************************************************************************************************************/

/**
 * Returns input if premium, setting's default otherwise.
 *
 * @param   mixed   $input
 * @param   object  $setting
 * @return  mixed   $input
 * @since   1.2.2
 * @version 1.2.2
 */
function molongui_sanitize_to_default( $input, $setting )
{
	// Return input if premium, default otherwise.
	return ( molongui_is_premium( molongui_get_constant( $this->plugin->id, 'DIR', false ) ) ? $input : $setting->default );
}

/**
 * Returns input if premium, setting's default otherwise.
 *
 * @param   string  $color
 * @return  string  $color
 * @since   1.2.2
 * @version 1.2.2
 */
function molongui_sanitize_color( $color )
{
	if ( empty( $color ) || is_array( $color ) ) return '';

	// If string does not start with 'rgba', then treat as hex.
	// sanitize the hex color and finally convert hex to rgba
	if ( false === strpos( $color, 'rgba' ) ) return sanitize_hex_color( $color );

	// By now we know the string is formatted as an rgba color so we need to further sanitize it.
	$color = str_replace( ' ', '', $color );
	sscanf( $color, 'rgba(%d,%d,%d,%f)', $red, $green, $blue, $alpha );
	return 'rgba('.$red.','.$green.','.$blue.','.$alpha.')';
}

/**
 * Returns sanitized input color if premium, setting's default otherwise.
 *
 * @param   mixed   $input
 * @param   object  $setting
 * @return  mixed   $input
 * @since   1.2.2
 * @version 1.2.2
 */
function molongui_sanitize_premium_color( $input, $setting )
{
	// Sanitize color input.
	$input = molongui_sanitize_color( $input );

	// Return input if premium, default otherwise.
	return molongui_sanitize_to_default( $input, $setting );
}

/**
 * Returns sanitized input select.
 *
 * @param   mixed   $input
 * @param   object  $setting
 * @return  mixed   $input
 * @since   1.2.2
 * @version 1.2.2
 */
function molongui_sanitize_select( $input, $setting )
{
	// Get the list of possible select options.
	$choices = $setting->manager->get_control( $setting->id )->choices;

	// Return input if valid or return default option
	return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
}

/**
 * Returns sanitized input select if premium, setting's default otherwise.
 *
 * @param   mixed   $input
 * @param   object  $setting
 * @return  mixed   $input
 * @since   1.2.2
 * @version 1.2.2
 */
function molongui_sanitize_premium_select( $input, $setting )
{
	// Sanitize selected input.
	$input = molongui_sanitize_select( $input, $setting );

	// Return input if premium, default otherwise.
	return molongui_sanitize_to_default( $input, $setting );
}

/**
 * Returns sanitized input radio.
 *
 * @param   mixed   $input
 * @param   object  $setting
 * @return  mixed   $input
 * @since   1.2.2
 * @version 1.2.2
 */
function molongui_sanitize_radio( $input, $setting )
{
	// TODO...
	// Get the list of possible select options.
	// $choices = $setting->manager->get_control( $setting->id )->choices;

	// Return input if valid or return default option
	//return ( array_key_exists( $input, $choices ) ? molongui_sanitize_to_default( $input, $setting ) : $setting->default );
	return $input;
}

/**
 * Returns sanitized input radio if premium, setting's default otherwise.
 *
 * @param   mixed   $input
 * @param   object  $setting
 * @return  mixed   $input
 * @since   1.2.2
 * @version 1.2.2
 */
function molongui_sanitize_premium_radio( $input, $setting )
{
	// Sanitize selected radio.
	$input = molongui_sanitize_radio( $input, $setting );

	// Return input if premium, default otherwise.
	return molongui_sanitize_to_default( $input, $setting );
}

/***********************************************************************************************************************
 * ACTIVE CALLBACKS
 **********************************************************************************************************************/

// TODO...