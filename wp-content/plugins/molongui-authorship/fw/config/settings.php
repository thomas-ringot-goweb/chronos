<?php

// Deny direct access to this file
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Common admin settings.
 *
 * This file contains common plugin settings.
 *
 * This file MUST be loaded after WordPress has initialized its translation routines. Try putting the declaration in an init action hook:
 *
 *      function settings_setup() { $settings = include( molongui_get_constant( $this->plugin->id, 'DIR', true ) . 'config/settings.php' ); ]
 *      add_action( 'admin_init', 'settings_setup' );
 *
 * @see http://stackoverflow.com/a/19160452
 *
 * Returned array must have the following structure:
 *
 * 'advanced' => array(
 *      'key'      => 'db_key',
 *      'slug'     => 'tab_uri_slug',
 *      'label'    => 'Tab label',
 *      'callback' => function,
 *      'sections' => array(
 *          array(
 *              'id'       => 'section_id',
 *              'display'  => true,
 *              'label'    => 'Section label',
 *              'desc'     => 'Section description',
 *              'callback' => 'render_description',
 *              'fields'   => array(
 *                  array(
 *                      'id'          => 'field_name_id',
 *                      'display'     => true,
 *                      'label'       => 'Field label',
 *                      'desc'        => 'Field description',
 *                      'tip'         => 'Field tip',
 *                      'premium'     => 'Field premium notice tip,
 *                      'class'       => 'Class to apply to the field',
 *                      'type'        => 'text',
 *                      'placeholder' => 'Field placeholder',
 *                  ),
 *              ),
 *          ),
 *          array(
 *              'id'       => 'section_id',
 *              'display'  => true,
 *              'label'    => 'Section label',
 *              'callback' => 'render_page',
 *              'cb_page'  => 'path_to_the_view_to_render',
 *              'cb_args'  => array(),
 *          ),
 *          array(
 *              'id'       => 'section_id',
 *              'display'  => true,
 *              'label'    => 'Section label',
 *              'callback' => 'render_class',
 *              'cb_class' => 'class_to_load_and_render',
 *              'cb_args'  => array(),
 *          ),
 *      )
 * )
 *
 * where the 'type' key of a field can be: {text | textarea | select | radio | checkbox | checkboxes | colorpicker | range | toggle | button | link | password | email | number | date }:
 *
 *  array(
 *      'id'          => 'field_name_id',
 *      'display'     => true,
 *      'label'       => __( 'Field label', 'textdomain' ),
 *      'desc'        => __( 'Field description', 'textdomain' ),
 *      'tip'         => __( 'Helping tip', textdomain ),
 *      'premium'     => $this->premium_setting_tip(),
 *      'class'       => 'class_name',
 *      'type'        => 'text',
 *      'placeholder' => 'Field placeholder',
 *      'icon'        => array(
 *                      'position' => 'right',
 *                      'type'     => 'status',
 *                  ),
 *  ),
 *
 *  array(
 *      'id'      => 'field_name_id',
 *      'display' => true,
 *      'label'   => __( 'Field label', 'textdomain' ),
 *      'desc'    => __( 'Field description', 'textdomain' ),
 *      'tip'     => 'Field tip',
 *      'premium' => $this->premium_setting_tip(),
 *      'class'   => 'class_name',
 *      'type'    => 'textarea',
 *  ),
 *
 *  array(
 *      'id'      => 'field_name_id',
 *      'display' => true,
 *      'label'   => __( 'Field label', 'textdomain' ),
 *      'desc'    => __( 'Field description', 'textdomain' ),
 *      'tip'     => __( 'Helping tip', textdomain ),
 *      'premium' => $this->premium_setting_tip(),
 *      'class'   => '{long-width | longer-width | longest-width | full-width}',
 *      'type'    => 'select',
 *      'options' => array(
 *                      array(
 *                          'id'    => 'select_option_1_id',
 *                          'label' => __( 'Select option 1 label', 'textdomain' ),
 *                          'value' => 'select_option_1_value',
 *                      ),
 *                      array(
 *                          'id'    => 'select_option_2_id',
 *                          'label' => __( 'Select option 2 label', 'textdomain' ),
 *                          'value' => 'select_option_2_value',
 *                      ),
 *                  ),
 *  ),
 *
 *  array(
 *      'id'      => 'field_name_id',
 *      'display' => true,
 *      'label'   => __( 'Field label', 'textdomain' ),
 *      'desc'    => __( 'Field description', 'textdomain' ),
 *      'tip'     => __( 'Helping tip', textdomain ),
 *      'premium' => $this->premium_setting_tip(),
 *      'class'   => 'class_name',
 *      'type'    => 'radio',
 *      'options' => array(
 *                      array(
 *                          'id'    => 'radio_option_1_id',
 *                          'label' => __( 'Radio option 1 label', 'textdomain' ),
 *                          'value' => 'radio_option_1_value',
 *                      ),
 *                      array(
 *                          'id'    => 'radio_option_2_id',
 *                          'label' => __( 'Radio option 2 label', 'textdomain' ),
 *                          'value' => 'radio_option_2_value',
 *                      ),
 *                  ),
 *  ),
 *
 *  array(
 *      'id'      => 'field_name_id',
 *      'display' => true,
 *      'label'   => __( 'Field label', 'textdomain' ),
 *      'desc'    => __( 'Field description', 'textdomain' ),
 *      'tip'     => __( 'Helping tip', textdomain ),
 *      'premium' => $this->premium_setting_tip(),
 *      'class'   => 'class_name',
 *      'type'    => 'checkbox',
 *  ),
 *
 *  array(
 *      'id'      => 'field_name_id',
 *      'display' => true,
 *      'label'   => __( 'Field label', 'textdomain' ),
 *      'desc'    => __( 'Field description', 'textdomain' ),
 *      'tip'     => __( 'Helping tip', textdomain ),
 *      'type'    => 'checkboxes',
 *      'options' => array(
 *                      array(
 *                          'id'    => 'checkbox_option_1_id',
 *                          'label' => __( 'Checkbox option 1 label', 'textdomain' ),
 *                      ),
 *                      array(
 *                          'id'    => 'checkbox_option_2_id',
 *                          'label' => __( 'Checkbox option 2 label', 'textdomain' ),
 *                      ),
 *                  ),
 *  ),
 *
 *  array(
 *      'id'      => 'field_name_id',
 *      'display' => true,
 *      'label'   => __( 'Field label', 'textdomain' ),
 *      'desc'    => __( 'Field description', 'textdomain' ),
 *      'tip'     => __( 'Helping tip', textdomain ),
 *      'premium' => $this->premium_setting_tip(),
 *      'class'   => 'class_name',
 *      'type'    => 'colorpicker',
 *  ),
 *
 * array(
 *      'id'      => 'field_name_id',
 *      'display' => true,
 *      'label'   => __( 'Field label', 'textdomain' ),
 *      'desc'    => __( 'Field description', 'textdomain' ),
 *      'tip'     => __( 'Helping tip', textdomain ),
 *      'premium' => $this->premium_setting_tip(),
 *      'default' => 'field_default_value',
 *      'type'    => 'range',
 *      'min'     => 'value',
 *      'max'     => 'value',
 *      'step'    => 'value',  // Available options: { 0.01 | 0.1 | 1 | 10 }
 *  ),
 *
 * array(
 *      'id'      => 'field_name_id',
 *      'display' => true,
 *      'label'   => __( 'Field label', 'textdomain' ),
 *      'desc'    => __( 'Field description', 'textdomain' ),
 *      'tip'     => __( 'Helping tip', textdomain ),
 *      'premium' => $this->premium_setting_tip(),
 *      'default' => 'field_default_value',
 *      'type'    => 'toggle',
 *      'style'   => 'yes-no',  // Available options: { round | round-flat | yes-no }
 *  ),
 *
 *  array(
 *      'id'      => 'field_name_id',
 *      'display' => true,
 *      'label'   => __( 'Field label', textdomain ),
 *      'desc'    => __( 'Field description', textdomain ),
 *      'tip'     => __( 'Helping tip', textdomain ),
 *      'premium' => $this->premium_setting_tip(),
 *      'class'   => 'class_name',
 *      'type'    => 'button',
 *      'args'    => array(
 *                      'id'    => 'button_id',
 *                      'label' => __( 'Button label', textdomain ),
 *                      'class' => 'button_class1 button_class2 ...',
 *                  ),
 *  ),
 *
 *  array(
 *      'id'      => 'field_name_id',
 *      'display' => true,
 *      'label'   => __( 'Field label', textdomain ),
 *      'desc'    => __( 'Field description', textdomain ),
 *      'tip'     => __( 'Helping tip', textdomain ),
 *      'premium' => $this->premium_setting_tip(),
 *      'class'   => 'class_name',
 *      'type'    => 'link',
 *      'args'    => array(
 *                      'id'     => 'link_id',
 *                      'url'    => '#',
 *                      'target' => '_self',
 *                      'text' => __( 'Link label', textdomain ),
 *                      'class' => 'button button-primary text_class1 text_class2 ...',
 *                  ),
 *  ),
 *
 *  array(
 *      'id'      => 'field_name_id',
 *      'display' => true,
 *      'label'   => __( 'Field label', 'textdomain' ),
 *      'desc'    => __( 'Field description', 'textdomain' ),
 *      'tip'     => __( 'Helping tip', textdomain ),
 *      'premium' => $this->premium_setting_tip(),
 *      'class'   => 'class_name',
 *      'type'    => 'number',
 *      'min'     => 'value',
 *      'max'     => 'value',
 *      'step'    => 'value',  // Available options: { 0.01 | 0.1 | 1 | 10 }
 *      'icon'    => array(
 *                      'position' => 'right',
 *                      'type'     => 'status',
 *                   ),
 *  ),
 *
 *  array(
 *      'id'      => 'field_name_id',
 *      'display' => true,
 *      'label'   => __( 'Field label', 'textdomain' ),
 *      'desc'    => __( 'Field description', 'textdomain' ),
 *      'tip'     => __( 'Helping tip', textdomain ),
 *      'premium' => $this->premium_setting_tip(),
 *      'class'   => 'class_name',
 *      'type'    => 'password',
 *      'icon'    => array(
 *                      'position' => 'right',
 *                      'type'     => 'status',
 *                   ),
 *  ),
 *
 *  array(
 *      'id'          => 'field_name_id',
 *      'display'     => true,
 *      'label'       => __( 'Field label', 'textdomain' ),
 *      'desc'        => __( 'Field description', 'textdomain' ),
 *      'tip'         => __( 'Helping tip', textdomain ),
 *      'premium'     => $this->premium_setting_tip(),
 *      'class'       => 'class_name',
 *      'type'        => 'email',
 *      'placeholder' => 'Field placeholder',
 *      'icon'        => array(
 *                      'position' => 'right',
 *                      'type'     => 'status',
 *                   ),
 *  ),
 *
 *  array(
 *      'id'      => 'field_name_id',
 *      'display' => true,
 *      'label'   => __( 'Field label', 'textdomain' ),
 *      'desc'    => __( 'Field description', 'textdomain' ),
 *      'tip'     => __( 'Helping tip', textdomain ),
 *      'premium' => $this->premium_setting_tip(),
 *      'class'   => 'class_name',
 *      'type'    => 'date',
 *      'min'     => 'yyyy-mm-dd',
 *      'max'     => 'yyyy-mm-dd',
 *      'icon'    => array(
 *                      'position' => 'right',
 *                      'type'     => 'status',
 *                   ),
 *  ),
 *
 * @author     Amitzy
 * @package    Molongui Common Framework (/fw)
 * @subpackage /config
 * @since      1.0.0
 * @version    1.2.2
 */

return array
(
	'advanced' => array
	(
		'key'      => $this->plugin->db_prefix . '_' . 'advanced',
		'slug'     => $this->plugin->id . '_' . 'advanced',
		'label'    => __( 'Advanced', 'molongui-common-framework' ),
		'callback' => array( $this, 'validate_advanced_tab' ),
		'sections' => array
		(
            array
            (
                'id'       => 'post_types',
                'display'  => true,
                'label'    => __( 'Post types', 'molongui-common-framework' ),
                'desc'     => __( '', 'molongui-common-framework' ),
                'callback' => 'render_description',
                'fields'   => array
                (
                    array
                    (
                        'id'      => 'extend_to',
                        'display' => true,
                        'label'   => __( 'Enable plugin on...', 'molongui-common-framework' ),
                        'desc'    => __( '', 'molongui-common-framework' ),
                        'tip'     => __( 'Choose which post types extend plugin functionality to.', 'molongui-common-framework' ),
                        'premium' => $this->premium_setting_tip(),
                        'class'   => '',
                        'type'    => 'checkboxes',
                        'options' => molongui_get_post_types( 'all', 'objects', true ),
                    ),
                ),
            ),
			array
			(
				'id'       => 'shortcodes',
                'display'  => version_compare( get_bloginfo('version'),'4.9', '<' ),
				'label'    => __( 'Shortcodes', 'molongui-common-framework' ),
				'desc'     => __( '', 'molongui-common-framework' ),
				'callback' => 'render_description',
				'fields'   => array
				(
					array
					(
						'id'      => 'enable_sc_text_widgets',
                        'display' => version_compare( get_bloginfo('version'),'4.9', '<' ),
						'label'   => __( 'Enable in text widgets', 'molongui-common-framework' ),
						'desc'    => __( '', 'molongui-common-framework' ),
						'tip'     => __( 'Whether to enable the use of shortcodes in text widgets.', 'molongui-common-framework' ),
						'premium' => $this->premium_setting_tip(),
						'class'   => '',
						'type'    => 'select',
						'options' => array
						(
							array
							(
								'id'    => 'yes',
								'label' => __( 'Yes', 'molongui-common-framework' ),
								'value' => '1',
							),
							array
							(
								'id'    => 'no',
								'label' => __( 'No', 'molongui-common-framework' ),
								'value' => '0',
							),
						),
					),
				),
			),
			array
			(
				'id'       => 'uninstalling',
                'display'  => true,
				'label'    => __( 'Uninstalling', 'molongui-common-framework' ),
				'desc'     => __( '', 'molongui-common-framework' ),
				'callback' => 'render_description',
				'fields'   => array
				(
					array
					(
						'id'      => 'keep_config',
                        'display' => true,
						'label'   => __( 'Keep configuration?', 'molongui-common-framework' ),
						'desc'    => __( '', 'molongui-common-framework' ),
						'tip'     => __( 'Whether to keep plugin configuration settings upon plugin uninstalling.', 'molongui-common-framework' ),
						'premium' => '',
						'class'   => '',
						'type'    => 'select',
						'options' => array
						(
							array
							(
								'id'    => 'yes',
								'label' => __( 'Yes', 'molongui-common-framework' ),
								'value' => '1',
							),
							array
							(
								'id'    => 'no',
								'label' => __( 'No', 'molongui-common-framework' ),
								'value' => '0',
							),
						),
					),
					array
					(
						'id'      => 'keep_data',
                        'display' => true,
						'label'   => __( 'Keep data?', 'molongui-common-framework' ),
						'desc'    => __( '', 'molongui-common-framework' ),
						'tip'     => __( 'Whether to keep plugin related data upon plugin uninstalling.', 'molongui-common-framework' ),
						'premium' => '',
						'class'   => '',
						'type'    => 'select',
						'options' => array
						(
							array
							(
								'id'    => 'yes',
								'label' => __( 'Yes', 'molongui-common-framework' ),
								'value' => '1',
							),
							array
							(
								'id'    => 'no',
								'label' => __( 'No', 'molongui-common-framework' ),
								'value' => '0',
							),
						),
					),
				),
			),
		),
	),
	'license' => array
	(
		'key'      => $this->plugin->db_prefix . '_' . 'license',
		'slug'     => $this->plugin->id . '_' . 'license',
		'label'    => __( 'License', 'molongui-common-framework' ),
		'callback' => array( $this, 'validate_license_tab' ),
		'sections' => array
		(
			array
			(
				'id'       => 'license_credentials',
                'display'  => true,
				'label'    => __( 'Credentials', 'molongui-common-framework' ),
				'desc'     => sprintf( __( 'Insert your license credentials to make the plugin work, you will find them by logging in %sto your account%s.', 'molongui-common-framework' ), '<a href="'.molongui_get_constant( $this->plugin->id, 'MOLONGUI_WEB', true ).'/my-account/" target="_blank">', '</a>' ),
				'callback' => 'render_description',
				'fields'   => array
				(
					array
					(
						'id'          => $this->config['db']['activation_key'],
                        'display'     => true,
						'label'       => __( 'License Key', 'molongui-common-framework' ),
						'desc'        => __( '', 'molongui-common-framework' ),
						'tip'         => __( 'The key we provided upon premium plugin purchase.', 'molongui-common-framework' ),
						'placeholder' => '',
						'type'        => 'text',
						'icon'        => array
						(
							'position' => 'right',
							'type'     => 'status',
						),
					),
					array
					(
						'id'          => $this->config['db']['activation_email'],
                        'display'     => true,
						'label'       => __( 'License email', 'molongui-common-framework' ),
						'desc'        => __( '', 'molongui-common-framework' ),
						'tip'         => __( 'The email you used to purchase the premium license.', 'molongui-common-framework' ),
						'placeholder' => '',
						'type'        => 'text',
						'icon'        => array
						(
							'position' => 'right',
							'type'     => 'status',
						),
					),
				),
			),
			array
			(
				'id'       => 'license_deactivation',
                'display'  => true,
				'label'    => __( 'Deactivation', 'molongui-common-framework' ),
				'desc'     => sprintf( __( 'Choose whether to deactivate the licence key upon plugin deactivation. Deactivating the license releases it so it can be used on another website but also removes it from this one, so should you reactivate the plugin, you will need to set again your credentials. %sRegardless of the value of this setting, the license will be released when uninstalling the plugin.', 'molongui-common-framework' ), '<br>' ),
				'callback' => 'render_description',
				'fields'   => array
				(
					array
					(
						'id'      => 'keep_license',
                        'display' => true,
						'label'   => __( 'Keep on deactivation', 'molongui-common-framework' ),
						'desc'    => __( '', 'molongui-common-framework' ),
						'tip'     => __( 'Whether to keep license credentials upon plugin deactivation.', 'molongui-common-framework' ),
						'class'   => '',
						'type'    => 'select',
						'options' => array
						(
							array
							(
								'id'    => 'yes',
								'label' => __( 'Yes', 'molongui-common-framework' ),
								'value' => '1',
							),
							array
							(
								'id'    => 'no',
								'label' => __( 'No', 'molongui-common-framework' ),
								'value' => '0',
							),
						),
					),
					array
					(
						'id'      => 'deactivate_license',
                        'display' => true,
						'label'   => __( 'Deactivate license', 'molongui-common-framework' ),
						'desc'    => __( '', 'molongui-common-framework' ),
						'tip'     => __( 'Deactivates your premium license so you can use it on another installation.', 'molongui-common-framework' ),
						'type'    => 'button',
						'args'    => array
						(
							'id'    => 'deactivate_license_button',
							'label' => __( 'Deactivate now', 'molongui-common-framework' ),
							'class' => '',
						),
					),
				),
			),
		),
	),
	'support' => array
	(
		'key'      => $this->plugin->db_prefix . '_' . 'support',
		'slug'     => $this->plugin->id . '_' . 'support',
		'label'    => __( 'Support', 'molongui-common-framework' ),
		'callback' => '',
		'sections' => array
		(
			array
			(
				'id'       => 'section_support',
                'display'  => true,
				'label'    => __( '', 'molongui-common-framework' ),
				'callback' => 'render_class',
				'cb_class' => 'Sysinfo',
				'cb_args'  => '',
			),
		),
	),
	'more' => array
	(
		'key'      => $this->plugin->db_prefix . '_' . 'more',
		'slug'     => $this->plugin->id . '_' . 'more',
		'label'    => __( 'More', 'molongui-common-framework' ),
		'callback' => '',
		'sections' => array
		(
			array
			(
				'id'       => 'section_more',
                'display'  => true,
				'label'    => __( '', 'molongui-common-framework' ),
				'callback' => 'render_page',
				'cb_page'  => molongui_get_constant( $this->plugin->id, 'DIR', true ) . 'admin/views/html-admin-tab-more.php',
				'cb_args'  => array( 'plugin_id' => $this->plugin->id, 'is_premium' => molongui_is_premium( $this->plugin->dir ) ),
			),
		),
	),
);