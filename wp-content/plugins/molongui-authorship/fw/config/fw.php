<?php

// Deny direct access to this file.
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Framework configuration.
 *
 * Contains the framework main configuration parameters and declares them as global constants.
 *
 * This file is loaded like:
 *      require_once( plugin_dir_path( __FILE__ ) . 'fw/config/fw.php' );
 *
 * IMPORTANT: This file must be loaded after plugin config file. Order is important because
 *            this code depends on a value defined in that file.
 *
 * @author     Amitzy
 * @package    Molongui Common Framework (/fw)
 * @subpackage /fw/config
 * @since      1.0.0
 * @version    1.2.5
 */

/**
 * Constants.
 *
 *  NAME            The human readable FW name.
 *  ID              The FW id.
 *  VERSION         The FW version.
 *  DIR             The 'fw' folder local path.
 *  URL             The 'fw' folder URL.
 *  MOLONGUI_WEB    The Molongui website URL.
 *  SUPPORT_MAIL    The e-mail used to send support reports to.
 *
 * @var     array
 * @since   1.0.0
 * @version 1.2.5
 */
$fw_config = array(
	'NAME'          => 'Molongui Common Framework',
	'ID'            => 'Common Framework',
	'VERSION'       => '1.2.5',
	'DIR'           => plugin_dir_path( plugin_dir_path( __FILE__ ) ),
	'URL'           => plugins_url( '/', plugin_dir_path( __FILE__ ) ),
	'MOLONGUI_WEB'  => 'https://molongui.amitzy.com/',
	'SUPPORT_MAIL'  => 'molongui@amitzy.com',
);

/**
 * Global constant namespace.
 *
 * String added before each constant to avoid collisions in the global PHP namespace.
 *
 * @var     string
 * @since   1.0.0
 * @version 1.0.0
 */
$constant_prefix = 'MOLONGUI_' . strtoupper( str_replace( ' ', '_', $config[ 'ID' ] ) ) . '_FW_';

/**
 * Define each constant if not already set
 *
 * @since   1.0.0
 * @version 1.0.0
 */
foreach( $fw_config as $param => $value )
{
	$param = $constant_prefix . $param;
	if( !defined( $param ) ) define( $param, $value );
}