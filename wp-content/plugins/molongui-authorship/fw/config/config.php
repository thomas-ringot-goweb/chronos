<?php

// Deny direct access to this file.
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Framework configuration.
 *
 * Defines some variables.
 *
 *  menu        Menu related variables.
 *  tabs        Admin settings page tabs related variables.
 *
 * This file is loaded like:
 *      $src = include dirname( plugin_dir_path( __FILE__ ) ) . "/fw/config/config.php";
 *
 * @author     Amitzy
 * @package    Molongui Common Framework (/fw)
 * @subpackage /fw/config
 * @since      1.0.0
 * @version    1.2.2
 */

return array
(
	'menu' => array
	(
		'slug' => 'admin.php?page=',
	),
	'tabs' => array
	(
		'license' => 'license',
	),
);