<?php

namespace Molongui\Fw\FrontEnd;

// Deny direct access to this file
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * The common FrontEnd functionality of a Molongui plugin.
 *
 * Defines the plugin name, version and all the needed hooks.
 *
 * @author     Amitzy
 * @package    Molongui Common Framework (/fw)
 * @subpackage /public
 * @since      1.2.2
 * @version    1.2.2
 */
if ( !class_exists( 'Molongui\Fw\FrontEnd\FrontEnd' ) )
{
    class FrontEnd
    {
        /**
         * Information of the plugin loading this class.
         *
         * @access  private
         * @var     object
         * @since   1.2.2
         * @version 1.2.2
         */
        private $plugin;

        /**
         * Initialize the class and set its properties.
         *
         * @access  public
         * @param   string  $plugin_id  The id of the plugin loading this class.
         * @since   1.2.2
         * @version 1.2.2
         */
        public function __construct( $plugin_id, $loader )
        {
            // Initialize information of the plugin loading this class.
            molongui_get_plugin( $plugin_id, $this->plugin );

            // Load required dependencies.
            $this->load_dependencies();

            if( $this->plugin->is_premium )
            {
                // Load premium dependencies.
                $this->load_premium_dependencies();
            }

            // Load admin CSS and JS scripts.
            $loader->add_action( 'wp_enqueue_scripts', $this, 'enqueue_styles'  );
            $loader->add_action( 'wp_enqueue_scripts', $this, 'enqueue_scripts' );

            // Instantiate required classes.
            // TODO...
        }

        /**
         * Loads required dependencies for this class.
         *
         * @access  private
         * @since   1.2.2
         * @version 1.2.2
         */
        private function load_dependencies()
        {
	        // Load here any required dependency.
	        // TODO...
        }

        /**
         * Loads required premium dependencies for this class.
         *
         * @access  private
         * @since   1.2.2
         * @version 1.2.2
         */
        private function load_premium_dependencies()
        {
            // Load here any required premium dependency.
	        // TODO...
        }

        /**
         * Registers the common styles used in the frontend.
         *
         * @access  public
         * @since   1.2.2
         * @version 1.2.2
         */
        public function enqueue_styles()
        {
	        // Get fw dir.
	        $fw_dir     = molongui_get_constant( $this->plugin->id, 'DIR', true );
	        $fw_url     = molongui_get_constant( $this->plugin->id, 'URL', true );
	        $fw_version = molongui_get_constant( $this->plugin->id, 'VERSION', true );

            // Enqueue common framework styles.
            $file = 'public/css/molongui-common-framework.638f.min.css';
            if ( file_exists( $fw_dir.$file ) ) wp_enqueue_style( 'molongui-common-framework-public', $fw_url.$file, array(), $fw_version, 'all' );
        }

        /**
         * Registers the common scripts used in the frontend.
         *
         * @access  public
         * @param   string  $hook
         * @since   1.2.2
         * @version 1.2.2
         */
        public function enqueue_scripts( $hook )
        {
	        // Get fw dir.
	        $fw_dir     = molongui_get_constant( $this->plugin->id, 'DIR', true );
	        $fw_url     = molongui_get_constant( $this->plugin->id, 'URL', true );
	        $fw_version = molongui_get_constant( $this->plugin->id, 'VERSION', true );

	        // Enqueue common framework styles.
	        $file = 'public/js/molongui-common-framework.c85f.min.js';
	        if ( file_exists( $fw_dir.$file ) ) wp_enqueue_script( 'molongui-common-framework-public', $fw_url.$file, array( 'jquery' ), $fw_version, true );
        }

    }
}