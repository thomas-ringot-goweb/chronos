# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#: fw/config/settings.php:311 fw/config/settings.php:320
#: fw/config/settings.php:334 fw/config/settings.php:343
#: fw/config/settings.php:371 fw/config/settings.php:380
#: fw/config/settings.php:406 fw/config/settings.php:453
#: fw/config/settings.php:468 fw/config/settings.php:494
#: fw/config/settings.php:519 fw/config/settings.php:545
#: fw/config/settings.php:564
msgid ""
msgstr ""
"Project-Id-Version: molongui-common-framework 1.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-06-06 12:39+0200\n"
"PO-Revision-Date: 2017-09-18 10:02+0100\n"
"Last-Translator: Amitzy <molongui@amitzy.com>\n"
"Language-Team: \n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.1\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: fw/admin/fw-class-admin.php:374
#, php-format
msgid ""
"There is a premium version of %s. %sUpgrade%s to unlock all features and "
"have premium support."
msgstr ""

#: fw/admin/fw-class-admin.php:448
#, php-format
msgid "Molongui is a trademark of %1$s Amitzy%2$s."
msgstr ""

#: fw/admin/fw-class-admin.php:457
#, php-format
msgid ""
"If you like <strong>%s</strong> please leave us a %s&#9733;&#9733;&#9733;"
"&#9733;&#9733;%s rating. A huge thank you from Molongui in advance!"
msgstr ""

#: fw/admin/fw-class-admin.php:459
msgid "Thanks :)"
msgstr "谢谢 :)"

#: fw/admin/fw-class-admin.php:484
msgid "Settings"
msgstr "設定"

#: fw/admin/fw-class-admin.php:485
msgid "Docs"
msgstr "文檔"

#: fw/admin/fw-class-admin.php:490
msgid "Go Premium"
msgstr "获取高级版本"

#: fw/admin/fw-class-admin.php:566
#, php-format
msgid ""
"%sPremium setting%s. You are using the free version of this plugin, so "
"changing this setting will have no effect and default value will be used. "
"Consider purchasing the %sPremium Version%s."
msgstr ""

#: fw/admin/fw-class-admin.php:574
#, php-format
msgid ""
"%sPremium setting%s. You are using the free version of this plugin, so "
"selecting any option marked as \"PREMIUM\" will have no effect and default "
"value will be used. Consider purchasing the %sPremium Version%s."
msgstr ""

#: fw/admin/views/html-admin-page-about.php:27
msgid "Plugins that make your site better!"
msgstr ""

#: fw/admin/views/html-admin-page-about.php:35
msgid ""
"Visit our site to find more plugins and themes we have created to improve "
"your site."
msgstr ""

#: fw/admin/views/html-admin-page-about.php:36
msgid "Visit our site"
msgstr "访问我们的网站"

#: fw/admin/views/html-admin-page-about.php:41
msgid ""
"As part of our ongoing effort to provide high quality, eye-catching "
"Wordpress plugins, here you have some you might find useful for your site."
msgstr ""

#: fw/admin/views/html-admin-page-plugins.php:25
#: fw/admin/views/html-admin-page-support.php:25
msgid "Need help? Need more?"
msgstr ""

#: fw/admin/views/html-admin-page-plugins.php:34
#: fw/admin/views/html-admin-page-support.php:34
#: fw/admin/views/html-admin-tab-support.php:32
msgid "Read the docs"
msgstr "阅读文档"

#: fw/admin/views/html-admin-page-plugins.php:35
#: fw/admin/views/html-admin-page-support.php:35
#: fw/admin/views/html-admin-tab-support.php:33
msgid "Learn the basics to help you make the most of Molongui plugins."
msgstr ""

#: fw/admin/views/html-admin-page-plugins.php:41
#: fw/admin/views/html-admin-page-support.php:41
#: fw/admin/views/html-admin-tab-support.php:53
msgid "Help request"
msgstr "帮助请求"

#: fw/admin/views/html-admin-page-plugins.php:42
#: fw/admin/views/html-admin-page-support.php:42
#: fw/admin/views/html-admin-tab-support.php:54
msgid ""
"Unable to find what you are looking for? Open a support ticket to get help."
msgstr ""

#: fw/admin/views/html-admin-page-plugins.php:48
#: fw/admin/views/html-admin-page-support.php:48
#: fw/admin/views/html-admin-tab-support.php:60
msgid "Upgrade!"
msgstr "升级版！"

#: fw/admin/views/html-admin-page-plugins.php:49
#: fw/admin/views/html-admin-page-support.php:49
#: fw/admin/views/html-admin-tab-support.php:61
msgid "Upgrade to unlock all premium features."
msgstr ""

#: fw/admin/views/html-admin-part-modal-preview.php:36
#: fw/admin/views/html-admin-part-modal-preview.php:44
msgid "Preview"
msgstr "预览"

#: fw/admin/views/html-admin-part-modal-preview.php:46
msgid "Note"
msgstr "備註"

#: fw/admin/views/html-admin-part-modal-preview.php:47
msgid ""
"Your theme might change how typography, colors and sizes are displayed in "
"this preview."
msgstr ""

#: fw/admin/views/html-admin-part-modal-preview.php:52
msgid "Close"
msgstr "关闭"

#: fw/admin/views/html-admin-part-sidebar-rate.php:24
msgid ""
"We are constantly looking for ways to improve the quality of our products "
"and services. How you rate our product and service is the most important "
"information we can obtain to support our goal."
msgstr ""

#: fw/admin/views/html-admin-part-sidebar-rate.php:28
msgid ""
"We would really appreciate it if you would take a second to rate this plugin "
"at the official directory of Wordpress plugins. A huge thank you from "
"Molongui in advance!"
msgstr ""

#: fw/admin/views/html-admin-part-sidebar-rate.php:32
msgid "Rate this plugin"
msgstr "为插件投票"

#: fw/admin/views/html-admin-part-sidebar-rate.php:36
msgid ""
"We would really appreciate any feedback you would like to send us. A huge "
"thank you from Molongui in advance!"
msgstr ""

#: fw/admin/views/html-admin-part-sidebar-rate.php:40
msgid "Send feedback"
msgstr "发送反馈"

#: fw/admin/views/html-admin-part-sidebar-upsell.php:26
#: fw/admin/views/html-admin-tab-more.php:26
msgid ""
"As part of our ongoing effort to provide high quality, eye-catching "
"Wordpress plugins, here you have some you might find useful for your site:"
msgstr ""

#: fw/admin/views/html-admin-part-upsells.php:34
msgid "Installed"
msgstr "安装"

#: fw/admin/views/html-admin-part-upsells.php:35
#: fw/admin/views/html-admin-tab-support.php:95
#: fw/includes/fw-class-settings.php:281
msgid "Free"
msgstr "免费"

#: fw/admin/views/html-admin-tab-more.php:24
msgid "Molongui plugins"
msgstr "Molongui 插件"

#: fw/admin/views/html-admin-tab-support.php:24
msgid "Need help?"
msgstr "需要帮忙？"

#: fw/admin/views/html-admin-tab-support.php:39
msgid "Get report"
msgstr "得到报告"

#: fw/admin/views/html-admin-tab-support.php:40
msgid ""
"Download and attach this information in your ticket when contacting support."
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:46
msgid "Send report"
msgstr "发送报告"

#: fw/admin/views/html-admin-tab-support.php:47
msgid "Submit report only if instructed so by the Molongui Support Team."
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:68
msgid ""
"Please download and attach this information in your ticket when contacting "
"support:"
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:70
msgid "Download report as text file"
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:71
msgid "Send report to Molongui"
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:76
msgid ""
"This page displays information about your WordPress configuration and Server "
"information that may be useful for debugging."
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:82
msgid "Plugin information"
msgstr "插件信息"

#: fw/admin/views/html-admin-tab-support.php:87
#: fw/admin/views/html-admin-tab-support.php:246
msgid "Name"
msgstr "名称"

#: fw/admin/views/html-admin-tab-support.php:88
msgid "The name of this plugin."
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:92 fw/config/settings.php:435
msgid "License"
msgstr "授權碼"

#: fw/admin/views/html-admin-tab-support.php:93
msgid "The license of this plugin."
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:95
#: fw/includes/fw-class-settings.php:281
msgid "Premium"
msgstr "高级"

#: fw/admin/views/html-admin-tab-support.php:98
msgid "Upgrade now!"
msgstr "现在升级"

#: fw/admin/views/html-admin-tab-support.php:104
#: fw/admin/views/html-admin-tab-support.php:164
#: fw/admin/views/html-admin-tab-support.php:251
msgid "Version"
msgstr "版本"

#: fw/admin/views/html-admin-tab-support.php:105
msgid "The version of the plugin installed on your site."
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:109
msgid "Framework"
msgstr "框架"

#: fw/admin/views/html-admin-tab-support.php:110
msgid "The Molongui Framework this plugin is using."
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:114
msgid "Database Version"
msgstr "数据库版本"

#: fw/admin/views/html-admin-tab-support.php:115
msgid "The version of the plugin that the database is formatted for."
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:125
msgid "Plugin settings"
msgstr "外掛設定"

#: fw/admin/views/html-admin-tab-support.php:149
msgid "WordPress Environment"
msgstr "WP环境"

#: fw/admin/views/html-admin-tab-support.php:154
msgid "Home URL"
msgstr "主页网址"

#: fw/admin/views/html-admin-tab-support.php:155
msgid "The URL of your site's homepage."
msgstr "你的网站首页的网址"

#: fw/admin/views/html-admin-tab-support.php:159
msgid "Site URL"
msgstr "网站网址"

#: fw/admin/views/html-admin-tab-support.php:160
msgid "The root URL of your site."
msgstr "您网站的根URL。"

#: fw/admin/views/html-admin-tab-support.php:165
msgid "The version of WordPress installed on your site."
msgstr "您网站上安装的WordPress版本。"

#: fw/admin/views/html-admin-tab-support.php:169
msgid "Multisite"
msgstr "多站點"

#: fw/admin/views/html-admin-tab-support.php:170
msgid "Whether or not you have WordPress Multisite enabled."
msgstr "是否啟用了WordPress 多站点。"

#: fw/admin/views/html-admin-tab-support.php:174
msgid "DB table prefix"
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:175
msgid "WordPress database table prefix."
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:179
msgid "Memory Limit"
msgstr "内存限制"

#: fw/admin/views/html-admin-tab-support.php:180
msgid "The maximum amount of memory (RAM) that your site can use at one time."
msgstr "您的站点可以一次使用的最大内存量（RAM）。"

#: fw/admin/views/html-admin-tab-support.php:186
#, php-format
msgid "%s - We recommend setting memory to at least 64MB. See: %s"
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:186
msgid "Increasing memory allocated to PHP"
msgstr "增加分配给PHP的内存"

#: fw/admin/views/html-admin-tab-support.php:193
msgid "Permalink strucutre"
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:194
msgid "WordPress permalink structure."
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:198
msgid "Registered post status"
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:199
msgid "WordPress registered post status types."
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:203
msgid "Cron"
msgstr "Cron"

#: fw/admin/views/html-admin-tab-support.php:204
msgid "Displays whether or not WP Cron Jobs are enabled."
msgstr "顯示是否啓用了 WP Cron 作業。"

#: fw/admin/views/html-admin-tab-support.php:214
msgid "Debug Mode"
msgstr "调试模式"

#: fw/admin/views/html-admin-tab-support.php:215
msgid "Displays whether or not WordPress is in Debug Mode."
msgstr "显示WordPress是否处于调试模式。"

#: fw/admin/views/html-admin-tab-support.php:225
msgid "Timezone"
msgstr "時區"

#: fw/admin/views/html-admin-tab-support.php:226
msgid "WordPress timezone."
msgstr "WordPress 的时区"

#: fw/admin/views/html-admin-tab-support.php:230
msgid "Language"
msgstr "语言"

#: fw/admin/views/html-admin-tab-support.php:231
msgid "The current language used by WordPress. Default = English"
msgstr "WordPress使用的当前语言。 默认=英文"

#: fw/admin/views/html-admin-tab-support.php:241
msgid "Active theme"
msgstr "活动的主题"

#: fw/admin/views/html-admin-tab-support.php:247
msgid "The name of the current active theme."
msgstr "当前已启用主题的名称。"

#: fw/admin/views/html-admin-tab-support.php:252
msgid "The installed version of the current active theme."
msgstr "当前已启用主题的版本信息。"

#: fw/admin/views/html-admin-tab-support.php:256
msgid "Author"
msgstr "作者"

#: fw/admin/views/html-admin-tab-support.php:257
msgid "The theme developers."
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:261
msgid "Author URL"
msgstr "作者链接"

#: fw/admin/views/html-admin-tab-support.php:262
msgid "The theme developers URL."
msgstr "主题开发者的URL。"

#: fw/admin/views/html-admin-tab-support.php:266
msgid "Child Theme"
msgstr "子主题"

#: fw/admin/views/html-admin-tab-support.php:267
msgid "Displays whether or not the current theme is a child theme."
msgstr "是否显示当前主题是否是一个子主题。"

#: fw/admin/views/html-admin-tab-support.php:272
msgid "Parent Theme Name"
msgstr "父主题名称"

#: fw/admin/views/html-admin-tab-support.php:273
msgid "The name of the parent theme."
msgstr "父主题的名称。"

#: fw/admin/views/html-admin-tab-support.php:277
msgid "Parent Theme Version"
msgstr "父主题版本"

#: fw/admin/views/html-admin-tab-support.php:278
msgid "The installed version of the parent theme."
msgstr "当前已安装的父主题版本"

#: fw/admin/views/html-admin-tab-support.php:282
msgid "Parent Theme Author URL"
msgstr "父主题作者链接"

#: fw/admin/views/html-admin-tab-support.php:283
msgid "The parent theme developers URL."
msgstr "父主题开发者的URL"

#: fw/admin/views/html-admin-tab-support.php:294
msgid "Installed themes"
msgstr "安装主图"

#: fw/admin/views/html-admin-tab-support.php:304
#: fw/admin/views/html-admin-tab-support.php:380
msgid "Disabled"
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:305
#: fw/admin/views/html-admin-tab-support.php:346
#: fw/admin/views/html-admin-tab-support.php:381
#, php-format
msgctxt "by author"
msgid "by %s"
msgstr "来自 %s"

#: fw/admin/views/html-admin-tab-support.php:317
msgid "Installed plugins"
msgstr "已安裝的插件"

#: fw/admin/views/html-admin-tab-support.php:334
#: fw/admin/views/html-admin-tab-support.php:361
msgid "Visit plugin homepage"
msgstr "访问插件主页"

#: fw/admin/views/html-admin-tab-support.php:340
#: fw/admin/views/html-admin-tab-support.php:374
msgid "Network enabled"
msgstr "网络功能"

#: fw/admin/views/html-admin-tab-support.php:345
msgid "Active"
msgstr "活跃"

#: fw/admin/views/html-admin-tab-support.php:368
msgid "Visit author homepage"
msgstr "访问作者主页"

#: fw/admin/views/html-admin-tab-support.php:394
msgid "Server Environment"
msgstr "服务器环境"

#: fw/admin/views/html-admin-tab-support.php:399
msgid "Server Info"
msgstr "服务器信息"

#: fw/admin/views/html-admin-tab-support.php:400
msgid "Information about the web server that is currently hosting your site."
msgstr "有关当前托管您的站点的Web服务器的信息。"

#: fw/admin/views/html-admin-tab-support.php:404
msgid "Database Info"
msgstr "数据库信息"

#: fw/admin/views/html-admin-tab-support.php:405
msgid "Information about the database installed on your hosting server."
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:409
msgid "PHP Version"
msgstr "PHP 版本"

#: fw/admin/views/html-admin-tab-support.php:410
msgid "The version of PHP installed on your hosting server."
msgstr "你当前服务器的PHP版本信息。"

#: fw/admin/views/html-admin-tab-support.php:420
#, php-format
msgid "%s - We recommend a minimum PHP version of 5.6. See: %s"
msgstr "%s - 我们建议 PHP 版本至少为 5.4. 请看: %s"

#: fw/admin/views/html-admin-tab-support.php:420
msgid "How to update your PHP version"
msgstr "如何更新你的PHP版本"

#: fw/admin/views/html-admin-tab-support.php:429
msgid "Couldn't determine PHP version because phpversion() doesn't exist."
msgstr "無法確定PHP版本，因為phpversion（）不存在。"

#: fw/admin/views/html-admin-tab-support.php:436
msgid "PHP Safe Mode"
msgstr "PHP 安全模式"

#: fw/admin/views/html-admin-tab-support.php:437
msgid "Whether PHP's safe mode is enabled."
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:441
msgid "PHP Memory Limit"
msgstr "PHP内存限制"

#: fw/admin/views/html-admin-tab-support.php:442
msgid "The maximum amount of memory (RAM) that PHP can use at one time."
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:446
msgid "PHP Post Max Size"
msgstr "PHP 文章最大規格"

#: fw/admin/views/html-admin-tab-support.php:447
msgid "The largest filesize that can be contained in one post."
msgstr "单篇文章可用的最大附件尺寸。"

#: fw/admin/views/html-admin-tab-support.php:451
msgid "PHP Upload Max Size"
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:452
msgid ""
"The largest filesize that can be uploaded to your WordPress installation."
msgstr "可以被上传至你的wordpress安装器的最大尺寸"

#: fw/admin/views/html-admin-tab-support.php:456
msgid "PHP Time Limit"
msgstr "PHP时间限制"

#: fw/admin/views/html-admin-tab-support.php:457
msgid ""
"The amount of time (in seconds) that your site will spend on a single "
"operation before timing out (to avoid server lockups)"
msgstr ""
"您的站點在超時之前在單個操作上花費的時間（以秒為單位）（以避免服務器鎖定）"

#: fw/admin/views/html-admin-tab-support.php:461
msgid "PHP Max Input Vars"
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:462
msgid ""
"The maximum number of variables your server can use for a single function to "
"avoid overloads."
msgstr "服务器可用的最大的变量值是一个用来避免过载的单独功能。"

#: fw/admin/views/html-admin-tab-support.php:466
msgid "PHP Arg Separator"
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:467
msgid "Character used as argument separator."
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:471
msgid "PHP Display Errors"
msgstr "PHP顯示錯誤"

#: fw/admin/views/html-admin-tab-support.php:472
msgid "Does your site prints errors to the screen as part of the output?"
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:476
msgid "PHP Allow URL File Open"
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:477
#: fw/admin/views/html-admin-tab-support.php:482
#: fw/admin/views/html-admin-tab-support.php:487
#: fw/admin/views/html-admin-tab-support.php:492
#: fw/admin/views/html-admin-tab-support.php:497
#: fw/admin/views/html-admin-tab-support.php:502
#: fw/admin/views/html-admin-tab-support.php:507
msgid "Does your site enable accessing URL object like files?"
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:481
msgid "PHP Session"
msgstr "PHP会话"

#: fw/admin/views/html-admin-tab-support.php:486
msgid "PHP Session Name"
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:491
msgid "PHP Cookie Path"
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:496
msgid "PHP Save Path"
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:501
msgid "PHP Use Cookies"
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:506
msgid "PHP Use Only Cookies"
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:512
msgid "SUHOSIN Installed"
msgstr "安装了SUHOSIN"

#: fw/admin/views/html-admin-tab-support.php:513
msgid ""
"Suhosin is an advanced protection system for PHP installations. It was "
"designed to protect your servers on the one hand against a number of well "
"known problems in PHP applications and on the other hand against potential "
"unknown vulnerabilities within these applications or the PHP core itself. If "
"enabled on your server, Suhosin may need to be configured to increase its "
"data submission limits."
msgstr ""
"Suhosin是一个先进的PHP安装保护系统。它被设计来保护您的服务器，一方面对一些PHP"
"应用程序中的已知问题，另一方面对潜在的未知的漏洞在这些应用程序或PHP核心本身。"
"如果您的服务器上启用，Suhosin可能需要配置以增加其数据提交限制。"

#: fw/admin/views/html-admin-tab-support.php:517
msgid "cURL Version"
msgstr "cURL版本"

#: fw/admin/views/html-admin-tab-support.php:518
msgid "The version of cURL installed on your server."
msgstr "这版本的cURL已安装在您的服务器上。"

#: fw/admin/views/html-admin-tab-support.php:526
msgid "N/A"
msgstr "无"

#: fw/admin/views/html-admin-tab-support.php:537
msgid "Some plugins can use cURL to communicate with remote servers."
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:543
msgid ""
"Your server does not have fsockopen or cURL enabled - some plugins which "
"communicate with other servers will not work. Contact your hosting provider."
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:548
msgid "Some webservices use SOAP to get information from remote servers."
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:554
#, php-format
msgid ""
"Your server does not have the %s class enabled - some plugins which use SOAP "
"may not work as expected."
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:559
msgid ""
"HTML/Multipart emails use DOMDocument to generate inline CSS in templates."
msgstr "HTML /多部分邮件使用DOMDocument生成内联CSS模板。"

#: fw/admin/views/html-admin-tab-support.php:565
#, php-format
msgid ""
"Your server does not have the %s class enabled - HTML/Multipart emails, and "
"also some extensions, will not work without DOMDocument."
msgstr ""
"您的服务器没有%s级启用- HTML /多部分邮件，也有一些扩展，将不工作DOMDocument。"

#: fw/admin/views/html-admin-tab-support.php:570
msgid "GZip (gzopen) is used to open the GEOIP database from MaxMind."
msgstr "GZip (gzopen) 用于从 MaxMind 打开 GEOIP 数据库。"

#: fw/admin/views/html-admin-tab-support.php:576
#, php-format
msgid ""
"Your server does not support the %s function - this is required to use the "
"GeoIP database from MaxMind."
msgstr "您的服务器不支持%s函数——这是需要使用来自MaxMind GeoIP数据库。"

#: fw/admin/views/html-admin-tab-support.php:581
msgid ""
"Multibyte String (mbstring) is used to convert character encoding, like for "
"emails or converting characters to lowercase."
msgstr ""
"Multibyte String（mbstring）是用来转换字符编码，如电子邮件或将字符转换为小"
"写。"

#: fw/admin/views/html-admin-tab-support.php:587
#, php-format
msgid ""
"Your server does not support the %s functions - this is required for better "
"character encoding. Some fallbacks will be used instead for it."
msgstr ""
"您的服务器不支持%s 的功能-这是需要更好的字符编码。一些回退将改为使用它。"

#: fw/admin/views/html-admin-tab-support.php:591
msgid "Remote Post"
msgstr "远程文章"

#: fw/admin/views/html-admin-tab-support.php:592
msgid ""
"PayPal uses this method of communicating when sending back transaction "
"information."
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:606
msgid ""
"wp_remote_post() failed. PayPal IPN won't work with your server. Contact "
"your hosting provider."
msgstr ""
"wp_remote_post（）失敗。 PayPal IPN將無法與您的服務器一起使用。 聯繫您的託管"
"服務提供商。"

#: fw/admin/views/html-admin-tab-support.php:608
#: fw/admin/views/html-admin-tab-support.php:626
#, php-format
msgid "Error: %s"
msgstr "错误: %s."

#: fw/admin/views/html-admin-tab-support.php:610
#: fw/admin/views/html-admin-tab-support.php:628
#, php-format
msgid "Status code: %s"
msgstr "狀態碼: %s"

#: fw/admin/views/html-admin-tab-support.php:616
msgid "Remote Get"
msgstr "远程get"

#: fw/admin/views/html-admin-tab-support.php:617
msgid ""
"Molongui plugins may use this method of communication when checking for "
"plugin updates."
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:624
msgid ""
"wp_remote_get() failed. The Molongui plugin updater won't work with your "
"server. Contact your hosting provider."
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:650
msgid "Default Timezone is UTC"
msgstr "默认时区是UTC"

#: fw/admin/views/html-admin-tab-support.php:651
msgid "The default timezone for your server."
msgstr "服务器的默认时区"

#: fw/admin/views/html-admin-tab-support.php:655
#, php-format
msgid "Default timezone is %s - it should be UTC"
msgstr "默认时区是 %s - 他将是 UTC"

#: fw/admin/views/html-admin-tab-support.php:668
msgid "Client Environment"
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:673
msgid "Platform"
msgstr "平台"

#: fw/admin/views/html-admin-tab-support.php:674
msgid "Information of the platform being used."
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:678
msgid "Browser"
msgstr "瀏覽器"

#: fw/admin/views/html-admin-tab-support.php:679
msgid "Information of the browser being used."
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:683
msgid "User Agent"
msgstr "用户代理"

#: fw/admin/views/html-admin-tab-support.php:684
msgid "Information of the user agent being used."
msgstr ""

#: fw/admin/views/html-admin-tab-support.php:688
msgid "IP address"
msgstr "IP 位址"

#: fw/admin/views/html-admin-tab-support.php:689
msgid "Your IP address."
msgstr ""

#: fw/config/settings.php:302
msgid "Advanced"
msgstr "高级"

#: fw/config/settings.php:310 fw/config/settings.php:319
msgid "Post types"
msgstr ""

#: fw/config/settings.php:321
msgid "Choose which post types extend plugin functionality to."
msgstr ""

#: fw/config/settings.php:333
msgid "Shortcodes"
msgstr "簡碼"

#: fw/config/settings.php:342
msgid "Enable in text widgets"
msgstr ""

#: fw/config/settings.php:344
msgid "Whether to enable the use of shortcodes in text widgets."
msgstr ""

#: fw/config/settings.php:353 fw/config/settings.php:390
#: fw/config/settings.php:416 fw/config/settings.php:503
msgid "Yes"
msgstr "是"

#: fw/config/settings.php:359 fw/config/settings.php:396
#: fw/config/settings.php:422 fw/config/settings.php:509
msgid "No"
msgstr "否"

#: fw/config/settings.php:370
msgid "Uninstalling"
msgstr "正在解除安裝"

#: fw/config/settings.php:379
msgid "Keep configuration?"
msgstr "保持配置"

#: fw/config/settings.php:381
msgid "Whether to keep plugin configuration settings upon plugin uninstalling."
msgstr ""

#: fw/config/settings.php:405
msgid "Keep data?"
msgstr "保留数据"

#: fw/config/settings.php:407
msgid "Whether to keep plugin related data upon plugin uninstalling."
msgstr ""

#: fw/config/settings.php:443
msgid "Credentials"
msgstr "登入資訊"

#: fw/config/settings.php:444
#, php-format
msgid ""
"Insert your license credentials to make the plugin work, you will find them "
"by logging in %sto your account%s."
msgstr ""

#: fw/config/settings.php:452
msgid "License Key"
msgstr "Key 详情"

#: fw/config/settings.php:454
msgid "The key we provided upon premium plugin purchase."
msgstr ""

#: fw/config/settings.php:467
msgid "License email"
msgstr "許可證郵件"

#: fw/config/settings.php:469
msgid "The email you used to purchase the premium license."
msgstr ""

#: fw/config/settings.php:484
msgid "Deactivation"
msgstr "停用"

#: fw/config/settings.php:485
#, php-format
msgid ""
"Choose whether to deactivate the licence key upon plugin deactivation. "
"Deactivating the license releases it so it can be used on another website "
"but also removes it from this one, so should you reactivate the plugin, you "
"will need to set again your credentials. %sRegardless of the value of this "
"setting, the license will be released when uninstalling the plugin."
msgstr ""

#: fw/config/settings.php:493
msgid "Keep on deactivation"
msgstr ""

#: fw/config/settings.php:495
msgid "Whether to keep license credentials upon plugin deactivation."
msgstr ""

#: fw/config/settings.php:518
msgid "Deactivate license"
msgstr "禁用许可证"

#: fw/config/settings.php:520
msgid ""
"Deactivates your premium license so you can use it on another installation."
msgstr ""

#: fw/config/settings.php:525
msgid "Deactivate now"
msgstr ""

#: fw/config/settings.php:537 fw/includes/fw-class-settings.php:102
msgid "Support"
msgstr "支持"

#: fw/config/settings.php:556
msgid "More"
msgstr "更多链接"

#: fw/includes/customizer/controls/color/class.php:102
#: fw/includes/customizer/controls/number/class.php:52
#: fw/includes/customizer/controls/range/class.php:75
#: fw/includes/customizer/controls/select/class.php:53
#: fw/includes/customizer/controls/toggle/class.php:70
#: fw/includes/customizer/controls/toggle/class.php:98
msgid "Premium setting. Only preview available"
msgstr ""

#: fw/includes/fw-class-compatibility.php:112
#, php-format
msgid ""
"%s%s%s - There can be only one instance of %s plugin installed. Please, "
"uninstall all other instances but the one you want to activate."
msgstr ""

#: fw/includes/fw-class-notice.php:82
msgid "I've already left a review"
msgstr ""

#: fw/includes/fw-class-notice.php:83
msgid "Maybe Later"
msgstr ""

#: fw/includes/fw-class-notice.php:84
msgid "Sure! I'd love to!"
msgstr ""

#: fw/includes/fw-class-settings.php:93
msgid "Molongui"
msgstr "Molongui"

#: fw/includes/fw-class-settings.php:96
msgid "Plugins"
msgstr "外掛"

#: fw/includes/fw-class-settings.php:99
msgid "About"
msgstr "关于"

#: fw/includes/fw-class-sysinfo.php:123
#, php-format
msgid "MOLONGUI - Support report for %s"
msgstr ""

#: fw/update/includes/fw-class-license.php:286
#, php-format
msgid ""
"The %s license has not been activated, so the plugin is inactive! %sClick "
"here%s to activate the license key and the plugin."
msgstr ""

#: fw/update/includes/fw-class-license.php:315
#, php-format
msgid ""
"Your license for %s is about to expire. %sGrab a new one%s to keep getting "
"updates and have direct support."
msgstr ""

#: fw/update/includes/fw-class-license.php:324
#, php-format
msgid ""
"Your license for %s has expired so %sgrab a new one%s to get plugin updates "
"and have premium support."
msgstr ""

#: fw/update/includes/fw-class-license.php:386
msgid "Plugin activated. "
msgstr "插件已啟用"

#: fw/update/includes/fw-class-license.php:396
msgid "Connection failed to the License Key API server. Try again later."
msgstr "连接失败到许可密钥API服务器。稍后再试。"

#: fw/update/includes/fw-class-license.php:469
msgid ""
"The license could not be deactivated. Use the License Deactivation button to "
"manually deactivate the license before activating a new license."
msgstr ""

#: fw/update/includes/fw-class-license.php:521
#, php-format
msgid "Your license has been deactivated. %s"
msgstr ""

#: fw/update/includes/fw-class-license.php:530
msgid "UNDEFINED ERROR. Please, try again or contact Molongui"
msgstr ""

#: fw/update/includes/fw-class-license.php:536
#, php-format
msgid "EMAIL ERROR: %s. %s"
msgstr ""

#: fw/update/includes/fw-class-license.php:539
#, php-format
msgid "KEY ERROR: %s. %s"
msgstr ""

#: fw/update/includes/fw-class-license.php:542
#, php-format
msgid "PURCHASE INCOMPLETE ERROR: %s. %s"
msgstr ""

#: fw/update/includes/fw-class-license.php:545
#, php-format
msgid "EXCEEDED ERROR: %s. %s"
msgstr ""

#: fw/update/includes/fw-class-license.php:548
#, php-format
msgid "KEY NOT ACTIVATED ERROR: %s. %s"
msgstr ""

#: fw/update/includes/fw-class-license.php:551
#, php-format
msgid "INVALID KEY ERROR: %s. %s"
msgstr ""

#: fw/update/includes/fw-class-license.php:554
#, php-format
msgid "NOT ACTIVE ERROR: %s. %s"
msgstr ""

#: fw/update/includes/fw-class-license.php:581
msgid "There is no active license to deactivate..."
msgstr ""

#: fw/update/includes/fw-class-update.php:540
msgid ""
"The plugin has been updated, but might have not been reactivated. Please "
"reactivate it manually if needed."
msgstr ""

#: fw/update/includes/fw-class-update.php:541
msgid "Plugin reactivated successfully."
msgstr "插件重新启动成功"

#: fw/update/includes/fw-class-update.php:574
#: fw/update/includes/fw-class-update.php:627
#, php-format
msgid ""
"A license key for %s could not be found. Maybe you forgot to enter a license "
"key when setting up %s, or the key was deactivated in your account. You can "
"reactivate or purchase a license key from your account <a href=\"%s\" target="
"\"_blank\">dashboard</a>."
msgstr ""
"無法找到%s的授權序號，也許您忘記於設置頁面輸入授權序號%s，或序號已於您帳戶中"
"被用了。 您可以為您的帳號再次啟動或購買一個授權序號 <a href=\"%s\" target="
"\"_blank\">儀表板</a>."

#: fw/update/includes/fw-class-update.php:577
#: fw/update/includes/fw-class-update.php:617
#, php-format
msgid ""
"A subscription for %s could not be found. You can purchase a subscription "
"from your account <a href=\"%s\" target=\"_blank\">dashboard</a>."
msgstr ""
"無法找到%s的訂閱，您可以在您的帳號購買新的訂閱 <a href=\"%s\" target=\"_blank"
"\">儀表板</a>."

#: fw/update/includes/fw-class-update.php:582
#, php-format
msgid ""
"The license key for %s has expired. You can reactivate or purchase a license "
"key from your account <a href=\"%s\" target=\"_blank\">dashboard</a>."
msgstr ""
"此%s的授權序號已過期。您可以在您的帳號再次啟動或購買授權序號 <a href=\"%s\" "
"target=\"_blank\">儀表板</a>."

#: fw/update/includes/fw-class-update.php:587
#, php-format
msgid ""
"The subscription for %s is on-hold. You can reactivate the subscription from "
"your account <a href=\"%s\" target=\"_blank\">dashboard</a>."
msgstr ""
"為%s所做的訂閱被保留中。您可以在您的帳號再次啟動訂閱 <a href=\"%s\" target="
"\"_blank\">儀表板</a>。"

#: fw/update/includes/fw-class-update.php:592
#, php-format
msgid ""
"The subscription for %s has been cancelled. You can renew the subscription "
"from your account <a href=\"%s\" target=\"_blank\">dashboard</a>. A new "
"license key will be emailed to you after your order has been completed."
msgstr ""
"為%s所做的訂閱被取消。您可以在您的帳號再次啟動訂閱 <a href=\"%s\" target="
"\"_blank\">儀表板</a>。在您訂購完成後一個新的授權序號將被寄送至您的電子郵件信"
"箱。"

#: fw/update/includes/fw-class-update.php:597
#, php-format
msgid ""
"The subscription for %s has expired. You can reactivate the subscription "
"from your account <a href=\"%s\" target=\"_blank\">dashboard</a>."
msgstr ""
"為%s所做的訂閱已過期。您可以在您的帳號再次啟動訂閱 <a href=\"%s\" target="
"\"_blank\">儀表板</a>."

#: fw/update/includes/fw-class-update.php:602
#, php-format
msgid ""
"The subscription for %s has been suspended. You can reactivate the "
"subscription from your account <a href=\"%s\" target=\"_blank\">dashboard</"
"a>."
msgstr ""
"為%s所做的訂閱被擱置。您可以在您的帳號再次啟動訂閱 <a href=\"%s\" target="
"\"_blank\">儀表板</a>."

#: fw/update/includes/fw-class-update.php:607
#, php-format
msgid ""
"The subscription for %s is still pending. You can check on the status of the "
"subscription from your account <a href=\"%s\" target=\"_blank\">dashboard</"
"a>."
msgstr ""
"%s的訂閱被擱置。您可以在您的帳號確認訂閱狀態 <a href=\"%s\" target=\"_blank"
"\">儀表板</a>."

#: fw/update/includes/fw-class-update.php:612
#, php-format
msgid ""
"The subscription for %s has been placed in the trash and will be deleted "
"soon. You can purchase a new subscription from your account <a href=\"%s\" "
"target=\"_blank\">dashboard</a>."
msgstr ""
"%s的訂閱已被移至垃圾桶，而且很快將會被删除。您可以在您的帳號購買新的訂閱 <a "
"href=\"%s\" target=\"_blank\">儀表板</a>."

#: fw/update/includes/fw-class-update.php:622
#, php-format
msgid ""
"%s has not been activated. Go to the settings page and enter the license key "
"and license email to activate %s."
msgstr "%s尚未被啟動。進入設置頁面，輸入授權序號與授權信箱以啟動 %s。"

#: fw/update/includes/fw-class-update.php:632
#, php-format
msgid ""
"Download permission for %s has been revoked possibly due to a license key or "
"subscription expiring. You can reactivate or purchase a license key from "
"your account <a href=\"%s\" target=\"_blank\">dashboard</a>."
msgstr ""
"為 %s 下載的權限已被撤銷，可能由于授權序號或訂閱已到期。 您可以為您的帳號再次"
"啟動或購買一個授權序號 <a href=\"%s\" target=\"_blank\">儀表板</a>."

#: fw/update/includes/fw-class-update.php:637
#, php-format
msgid ""
"You changed the subscription for %s, so you will need to enter your new API "
"License Key in the settings page. The License Key should have arrived in "
"your email inbox, if not you can get it by logging into your account <a href="
"\"%s\" target=\"_blank\">dashboard</a>."
msgstr ""
"你已變更了 %s 的訂閱方式, 所以你需要於設置頁面輸入你的新API授權序號。授權序號"
"應該已送達至您的電子郵件信，如果没有你可以透過登入到您的帳號 <a href=\"%s\" "
"target=\"_blank\">儀表板</a>."

#: fw/update/includes/fw-class-update.php:659
#, php-format
msgid ""
"An error occurred with your %s license. Please, open a support ticket %shere"
"%s so we can help you."
msgstr ""

#~ msgid "Inactive"
#~ msgstr "未启用"

#~ msgid "Keep configuration"
#~ msgstr "保持配置"

#~ msgid "Keep data"
#~ msgstr "保留数据"

#~ msgid "Theme"
#~ msgstr "主题"
