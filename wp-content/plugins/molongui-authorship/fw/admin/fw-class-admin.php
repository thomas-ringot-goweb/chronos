<?php

namespace Molongui\Fw\Admin;

use Molongui\Fw\Includes\License;
use Molongui\Fw\Includes\Key;
use Molongui\Fw\Includes\Update;
use Molongui\Fw\Includes\Password;
use Molongui\Fw\Includes\Settings;
use Molongui\Fw\Includes\Sysinfo;
use Molongui\Fw\Includes\Notice;
use Molongui\Fw\Includes\Upsell;

// Deny direct access to this file
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * The common admin functionality of a Molongui plugin.
 *
 * Defines the plugin name, version and all the needed hooks.
 *
 * @author     Amitzy
 * @package    Molongui Common Framework (/fw)
 * @subpackage /admin
 * @since      1.0.0
 * @version    1.2.5
 */
if ( !class_exists( 'Molongui\Fw\Admin\Admin' ) )
{
	class Admin
	{
		/**
		 * Information of the plugin loading this class.
		 *
		 * @access  private
		 * @var     object
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		private $plugin;

		/**
		 * Holds all the configuration set into '/premium/config/update.php' configuration file.
		 *
		 * @access  private
		 * @var     string
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		private $config;

		/**
		 * Instance of 'Key' class.
		 *
		 * @access  private
		 * @var     Key
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		private $plugin_key;

		/**
		 * Instance of 'License' class.
		 *
		 * @access  private
		 * @var     License
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		private $plugin_license;

		/**
		 * Instance of 'SysInfo' class.
		 *
		 * @access  private
		 * @var     Key
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		private $plugin_info;

		/**
		 * Instance of 'Notice' class.
		 *
		 * @access  private
		 * @var     License
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		private $plugin_notice;

		/**
		 * Update data used across this class.
		 *
		 * @access  public
		 * @var     mixed
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		public $update_license;
		public $update_basename;
		public $update_slug;
		public $update_product_id;
		public $update_renew_license_url;
		public $update_instance_id;
		public $update_domain;
		public $update_sw_version;
		public $update_plugin_or_theme;
		public $update_extra;

/**
 * The loader that's responsible for maintaining and registering all hooks that power the plugin.
 *
 * @access  protected
 * @var     Loader      $loader     Maintains and registers all hooks for the plugin.
 * @since   1.0.0
 * @version 1.0.0
 */
protected $loader;

		/**
		 * Initialize the class and set its properties.
		 *
		 * @access  public
		 * @param   string  $plugin_id  The id of the plugin loading this class.
		 * @since   1.0.0
		 * @version 1.2.2
		 */
		public function __construct( $plugin_id, $loader )
		{
			// Initialize information of the plugin loading this class.
			molongui_get_plugin( $plugin_id, $this->plugin );

$this->loader = $loader;

			// Load required dependencies.
			$this->load_dependencies();

			if( $this->plugin->is_premium )
			{
				// Load update configuration.
				$this->config = include( $this->plugin->dir . "premium/config/update.php" );

				// Load premium dependencies.
				$this->load_premium_dependencies();

				// Handle license stuff.
				$this->manage_license();

				// Instantiate required premium classes.
				$this->plugin_license = new License( $this->plugin->id );
				$this->plugin_key     = new Key( $this->update_product_id, $this->update_instance_id, $this->update_domain, $this->update_sw_version, $this->config['server']['url'] );

				// Enable license deactivation for logged in users.
				$this->loader->add_action( 'wp_ajax_deactivate_license_key', $this->plugin_license, 'deactivate_license_key' );
			}
			else
			{
				// Display "Go premium" notice if upgrade available.
				if( $this->plugin->is_upgradable ) $this->loader->add_action( 'admin_notices', $this, 'display_upgrade_notice' );
			}

			// Display "Welcome" notice if the plugin has been activated and notice has not been dismissed before.
			// If the transient does not exist, or has expired, then get_transient() will return false.
			if ( get_transient( $this->plugin->slug.'-activated' ) ) $this->loader->add_action( 'admin_notices', $this, 'display_install_notice' );

			// Display "What's new" notice if the plugin has been updated and notice has not been dismissed before.
			// If the transient does not exist, or has expired, then get_transient() will return false.
			if ( get_transient( $this->plugin->slug.'-updated' ) ) $this->loader->add_action( 'admin_notices', $this, 'display_whatsnew_notice' );

			// Reset "What's new" notice dismissal after update.
			$this->loader->add_filter( 'upgrader_post_install', $this, 'reset_whatsnew_notice', 10, 3 );

			// Load admin CSS and JS scripts.
			$this->loader->add_action( 'admin_enqueue_scripts', $this, 'enqueue_styles'  );
			$this->loader->add_action( 'admin_enqueue_scripts', $this, 'enqueue_scripts' );

			// Load plugin settings after registering any other plugin action (at 'plugin-class-core.php')
			// with default priority (10).
			$this->loader->add_action( 'init', $this, 'load_plugin_settings', 11 );

			// Register settings page.
			$this->loader->add_action( 'admin_menu', $this, 'add_menu_item' );
			$this->loader->add_action( 'admin_init', $this, 'add_page_tabs' );

			// Register additional action links (at "Plugins" page).
			$this->loader->add_filter( 'plugin_action_links_'.$this->plugin->basename, $this, 'add_action_links' );

			// Customize admin footer text.
			$this->loader->add_filter( 'admin_footer_text', $this, 'admin_footer_text', 1 );

			// Instantiate required classes.
			$this->plugin_info   = new Sysinfo( $this->plugin->id );
			$this->plugin_notice = new Notice();

			// Enable the sent of support reports.
			$this->loader->add_action( 'wp_ajax_send_support_report', $this->plugin_info, 'send_support_report' );

			// Enable persistent notice dismissal.
			$this->loader->add_action( 'wp_ajax_dismiss_notice', $this->plugin_notice, 'dismiss_notice' );
		}

		/**
		 * Load required dependencies for this class.
		 *
		 * Load other classes definitions used by this class.
		 *
		 * @access  private
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		private function load_dependencies()
		{
			/**
			 * The class responsible for getting all system information.
			 */
			if ( !class_exists( 'Molongui\Fw\Includes\Sysinfo' ) ) require_once( molongui_get_constant( $this->plugin->id, 'DIR', true ) . 'includes/fw-class-sysinfo.php' );
			if ( !class_exists( 'Molongui\Fw\Includes\Browser' ) ) require_once( molongui_get_constant( $this->plugin->id, 'DIR', true ) . 'includes/vendor-class-browser.php' );

			/**
			 * The class responsible for handling admin notices.
			 */
			if ( !class_exists( 'Molongui\Fw\Includes\Notice' ) ) require_once( molongui_get_constant( $this->plugin->id, 'DIR', true ) . 'includes/fw-class-notice.php' );

			/**
			 * The class responsible for getting a list with all the plugins developed by Molongui.
			 */
			if ( !class_exists( 'Molongui\Fw\Includes\Upsell' ) ) require_once( molongui_get_constant( $this->plugin->id, 'DIR', true ) . 'includes/fw-class-upsell.php' );
		}

		/**
		 * Load required premium dependencies for this class.
		 *
		 * Load other classes definitions used by this class.
		 *
		 * @access  private
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		private function load_premium_dependencies()
		{
			/**
			 * The class responsible for defining all actions related with the license key.
			 */
			if ( !class_exists( 'Molongui\Fw\Includes\Key' ) ) require_once( molongui_get_constant( $this->plugin->id, 'DIR', true ) . 'update/includes/fw-class-key.php' );

			/**
			 * The class responsible for defining update functionality of the plugin.
			 */
			if ( !class_exists( 'Molongui\Fw\Includes\Update' ) ) require_once( molongui_get_constant( $this->plugin->id, 'DIR', true ) . 'update/includes/fw-class-update.php' );

			/**
			 * The class responsible for creating the instance key of the plugin installation.
			 */
			if ( !class_exists( 'Molongui\Fw\Includes\Password' ) ) require_once( molongui_get_constant( $this->plugin->id, 'DIR', true ) . 'update/includes/fw-class-password.php' );

			/**
			 * The class responsible for handling license activation/deactivation.
			 */
			if ( !class_exists( 'Molongui\Fw\Includes\License' ) ) require_once( molongui_get_constant( $this->plugin->id, 'DIR', true ) . 'update/includes/fw-class-license.php' );
		}

		/**
		 * Register the stylesheets for the admin area.
		 *
		 * @access  public
		 * @since   1.0.0
		 * @version 1.2.2
		 */
		public function enqueue_styles()
		{
			// Get fw dir.
			$fw_dir = molongui_get_constant( $this->plugin->id, 'DIR', true );
			$fw_url = molongui_get_constant( $this->plugin->id, 'URL', true );

			// Enqueue color-picker styles.
			wp_enqueue_style( 'wp-color-picker' );

			// Load plugin internal configuration.
			$config = include $this->plugin->dir . 'config/config.php';

			// Enqueue "choices" if configured so.
			if ( isset( $config['styles']['back']['choices'] ) and $config['styles']['back']['choices'] )
			{
				$choices_css_url = $fw_url.'admin/css/vendor/choices/choices.min.css';
				wp_enqueue_style( 'molongui-choices', $choices_css_url, array(), $this->plugin->version, 'all' );
			}

			// Enqueue "select2" if configured so.
			if ( isset( $config['styles']['back']['select2'] ) and $config['styles']['back']['select2'] )
			{
				$select2_css_url = 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css';
				wp_enqueue_style( 'molongui-select2', $select2_css_url, array(), $this->plugin->version, 'all' );
			}

			// Enqueue common framework styles.
			$file = 'admin/css/molongui-common-framework.0cd7.min.css';
			if ( file_exists( molongui_get_constant( $this->plugin->id, 'DIR', true ).$file ) )
				wp_enqueue_style( 'molongui-common-framework-admin', molongui_get_constant( $this->plugin->id, 'URL', true ).$file, array(), molongui_get_constant( $this->plugin->id, 'VERSION', true ), 'all' );
		}

		/**
		 * Register the JavaScript for the admin area.
		 *
		 * @see     https://developer.wordpress.org/reference/functions/wp_enqueue_script/
		 *          https://developer.wordpress.org/reference/functions/wp_add_inline_script/
		 *          https://developer.wordpress.org/reference/functions/wp_localize_script/
		 *
		 * @access  public
		 * @param   string  $hook
		 * @since   1.0.0
		 * @version 1.2.0
		 */
		public function enqueue_scripts( $hook )
		{
			// Get fw dir.
			$fw_dir = molongui_get_constant( $this->plugin->id, 'DIR', true );
			$fw_url = molongui_get_constant( $this->plugin->id, 'URL', true );

			// Load plugin internal configuration.
			$config = include $this->plugin->dir . 'config/config.php';

			// Enqueue "css-element-queries" scripts if configured so and only on plugin pages (?page=molongui_page_plugin-slug).
			if ( isset( $config['scripts']['back']['element_queries'] ) and $config['scripts']['back']['element_queries'] and $hook == 'molongui_page_'.$this->plugin->slug )
			{
				wp_enqueue_script( 'molongui-resizesensor',   $fw_url.'public/js/vendor/element-queries/ResizeSensor.js',   array( 'jquery' ), $this->plugin->version, false );
				wp_enqueue_script( 'molongui-elementqueries', $fw_url.'public/js/vendor/element-queries/ElementQueries.js', array( 'jquery' ), $this->plugin->version, false );
			}

			// Enqueue "sweetalert" if configured so.
			if ( isset( $config['scripts']['back']['sweetalert'] ) and $config['scripts']['back']['sweetalert'] )
			{
				$sweetalert_js_url = 'https://unpkg.com/sweetalert/dist/sweetalert.min.js';
				wp_enqueue_script( 'molongui-sweetalert', $sweetalert_js_url, array( 'jquery' ), $this->plugin->version, false );

				// Avoid conflicts with other plugin instances loaded by third parties.
				wp_add_inline_script( 'molongui-sweetalert', 'var molongui_swal = swal;' );
			}

			// Enqueue "choices" if configured so.
			if ( isset( $config['scripts']['back']['choices'] ) and $config['scripts']['back']['choices'] )
			{
				$choices_js_url = $fw_url.'admin/js/vendor/choices/choices.min.js';
				wp_enqueue_script( 'molongui-choices', $choices_js_url, array(), $this->plugin->version, true );

				// Avoid conflicts with other plugin instances loaded by third parties.
				wp_add_inline_script( 'molongui-choices', 'var MolonguiChoices = Choices; delete Choices;' );
			}

			// Enqueue "select2" if configured so.
			if ( isset( $config['scripts']['back']['select2'] ) and $config['scripts']['back']['select2'] )
			{
				$select2_js_url = 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js';
				wp_enqueue_script( 'molongui-select2', $select2_js_url, array( 'jquery' ), $this->plugin->version, false );

				// Avoid conflicts with other plugin instances loaded by third parties.
				// @see: https://github.com/select2/select2/issues/3942
				//       http://jsbin.com/pelulolime/1/edit?html,js,output
				wp_add_inline_script( 'molongui-select2', 'var molongui_select2 = jQuery.fn.select2; delete jQuery.fn.select2;' );
			}

			// Enqueue "sortable" if configured so.
			if ( isset( $config['scripts']['back']['sortable'] ) and $config['scripts']['back']['sortable'] )
			{
				$sortable_js_url = 'https://cdn.jsdelivr.net/npm/sortablejs@1.6.1/Sortable.min.js';
				wp_enqueue_script( 'molongui-sortable', $sortable_js_url, array( 'jquery' ), $this->plugin->version, false );
			}

			// Enqueue common framework scripts.
			$file = 'admin/js/molongui-common-framework.2ee6.min.js';
			if ( file_exists( molongui_get_constant( $this->plugin->id, 'DIR', true ).$file ) )
			{
				wp_enqueue_script( 'molongui-common-framework', molongui_get_constant( $this->plugin->id, 'URL', true ).$file, array( 'jquery', 'wp-color-picker' ), molongui_get_constant( $this->plugin->id, 'VERSION', true ), true );

				// Add server-side data to previously enqueued script so it can used as variables.
				wp_localize_script( 'molongui-common-framework', 'myAjax', array
				(
					// Url to 'wp-admin/admin-ajax.php' so ajax calls can be processed.
					'ajaxurl'	=> admin_url( 'admin-ajax.php' ),

					// Nonce that will be used to enforce security on several requests.
					'ajaxnonce'	=> wp_create_nonce( 'molongui-js-nonce' ),
				));
			}
		}

		/**
		 * Shows a notice to anyone who has just installed the plugin for the first time.
		 *
		 * @see     https://catapultthemes.com/wordpress-plugin-update-hook-upgrader_process_complete/
		 *
		 * @access  public
		 * @since   1.2.2
		 * @version 1.2.5
		 */
		public function display_install_notice()
		{
			$n_content = array();

			// Get the name of the function to call to retrieve data to display within the notice.
			$plugin_function = "highlights_plugin";

			// Get the content to display.
			$class_name = '\Molongui\\'.$this->plugin->namespace.'\Includes\Highlights';
			if ( !class_exists( $class_name ) ) require_once $this->plugin->dir.'includes/plugin-class-highlights.php';
			if ( method_exists( $class_name, $plugin_function ) )
			{
				$plugin_class = new $class_name();
				$n_content    = $plugin_class->{$plugin_function}();
			}

			// Leave if there is nothing to show.
			if ( empty( $n_content ) ) return;

			// Load notices configuration.
			$config = include $this->plugin->dir . 'config/config.php';

			// Notice data.
			$n_slug = 'install';
			$notice = array
			(
				'id'          => $this->plugin->slug.'-'.$n_slug.'-notice',
				'type'        => 'success',
				'content'     => $n_content,
				'dismissible' => $config['notices'][$n_slug]['dismissible'],
				'dismissal'   => $config['notices'][$n_slug]['dismissal'],
				'class'       => 'molongui-notice-activated',
				'pages'       => array
				(
					'dashboard' => 'dashboard',
					'updates'   => 'update-core',
					'plugins'   => 'plugins',
					'plugin'    => 'molongui_page_'.$this->plugin->slug,
				),
			);

			// Render notice.
			Notice::display( $notice['id'], $notice['type'], $notice['content'], $notice['dismissible'], $notice['dismissal'], $notice['class'], $notice['pages'] );
		}

		/**
		 * Displays a notice featuring release highlights.
		 *
		 * @access  public
		 * @since   1.2.2
		 * @version 1.2.4
		 */
		public function display_whatsnew_notice()
		{
			$n_content = array();

			// Get the name of the function to call to retrieve data to display within the notice.
			$current_release = str_replace('.', '', $this->plugin->version );
			$plugin_function = "highlights_release_{$current_release}";

			// Get content to display for current release.
			$class_name = '\Molongui\\'.ucfirst($this->plugin->id).'\Includes\Highlights';
			if ( !class_exists( $class_name ) ) require_once $this->plugin->dir.'includes/plugin-class-highlights.php';
			if ( method_exists( $class_name, $plugin_function ) )
			{
				$plugin_class = new $class_name();
				$n_content    = $plugin_class->{$plugin_function}();
			}
/*			// If no highlights to display for current release, get the latest ones.
			else
			{
				// Get the name of all the methods/functions defined for the class.
				$class_methods = get_class_methods( $class_name );

				// Let's assume all methods in the class are ordered so the last one is the most recent one.
				$plugin_function = end( $class_methods );

				// Get latest content to display.
				if ( method_exists( $class_name, $plugin_function ) )
				{
					$plugin_class = new $class_name();
					$n_content = $plugin_class->{$plugin_function}();
				}
			}
*/
			// Leave if there is nothing to show.
			if ( empty( $n_content ) ) return;

			// Load notices configuration.
			$config = include $this->plugin->dir . 'config/config.php';

			// Notice data.
			$n_slug = 'whatsnew';
			$notice = array
			(
				'id'          => $this->plugin->slug.'-'.$n_slug.'-notice',
				'type'        => 'success',
				'content'     => $n_content,
				'dismissible' => $config['notices'][$n_slug]['dismissible'],
				'dismissal'   => $config['notices'][$n_slug]['dismissal'],
				'class'       => 'molongui-notice-whatsnew',
				'pages'       => array
				(
					'dashboard' => 'dashboard',
					'updates'   => 'update-core',
					'plugins'   => 'plugins',
					'plugin'    => 'molongui_page_'.$this->plugin->slug,
				),
			);

			// Render notice.
			Notice::display( $notice['id'], $notice['type'], $notice['content'], $notice['dismissible'], $notice['dismissal'], $notice['class'], $notice['pages'] );
		}

		/**
		 * Resets notice dismissal so it gets displayed again.
		 *
		 * @see     https://developer.wordpress.org/reference/hooks/upgrader_post_install/
		 *
		 * @param   boolean $response   Install response.
		 * @param   mixed   $hook_extra Extra arguments passed to hooked filters.
		 * @param   array   $result     Installation result data.
		 * @return  array   $result     Untouched nstallation result data.
		 * @return  void
		 * @since   1.2.2
		 * @version 1.2.4
		 */
		public function reset_whatsnew_notice( $response, $hook_extra, $result )
		{
			// Run only if updated plugin is this one.
			if ( $hook_extra['plugin'] != $this->plugin->basename ) return $result;

			// Delete "Welcome" transient (so the notice does not show again).
			delete_transient( $this->plugin->slug.'-activated' );

			// Set a transient so that we know we have to display the "What's new" notice.
			set_transient( $this->plugin->slug.'-updated', 1 );

			// Delete "What's new" dismissal transient (so the notice show again).
			delete_site_transient( $this->plugin->slug.'-whatsnew-notice' );

			// Return untouched result.
			return $result;
		}

		/**
		 * Displays an upgrade notice.
		 *
		 * @access  public
		 * @since   1.0.0
		 * @version 1.2.2
		 */
		public function display_upgrade_notice()
		{
			// Load notices configuration.
			$config = include $this->plugin->dir . 'config/config.php';

			// Notice data.
			$n_slug = 'upgrade';
			$notice = array
			(
				'id'          => $this->plugin->slug.'-'.$n_slug.'-notice',
				'type'        => 'info',
				'content'     => array
				(
					'image'   => '',
					'title'   => '',
					'message' => sprintf( __( 'There is a premium version of %s. %sUpgrade%s to unlock all features and have premium support.', 'molongui-common-framework' ),
						$this->plugin->name,
						'<a href="'.$this->plugin->web.'" target="_blank" >',
						'</a>' ),
					'buttons' => array(),
				),
				'dismissible' => $config['notices'][$n_slug]['dismissible'],
				'dismissal'   => $config['notices'][$n_slug]['dismissal'],
				'class'       => 'molongui-notice-upgrade',
				'pages'       => array
				(
					'dashboard' => 'dashboard',
					'updates'   => 'update-core',
					'plugins'   => 'plugins',
					'plugin'    => 'molongui_page_'.$this->plugin->slug,
				),
			);

			// Render notice.
			Notice::display( $notice['id'], $notice['type'], $notice['content'], $notice['dismissible'], $notice['dismissal'], $notice['class'], $notice['pages'] );
		}

		/**
		 * Handle license stuff for premium plugins.
		 *
		 * @access  public
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		public function manage_license()
		{
			// Set all software update data here.
			$this->update_license           = get_option( $this->config['db']['license_key'] );
			$this->update_basename          = $this->plugin->basename;                                                          // Same as plugin slug. If a theme, use a theme name like 'twentyeleven'.
			$this->update_product_id        = get_option( $this->config['db']['product_id_key'] );                              // Software Title.
			$this->update_renew_license_url = molongui_get_constant( $this->plugin->id, 'MOLONGUI_WEB', true ) . '/my-account'; // URL to renew a license.
			$this->update_instance_id       = get_option( $this->config['db']['instance_key'] );                                // Instance ID (unique to each blog activation).
			$this->update_domain            = site_url();                                                                       // Blog domain name.
			$this->update_sw_version        = $this->plugin->version;                                                           // The software version.
			$this->update_plugin_or_theme   = $this->config['sw']['type'];

			// Check for software updates.
			$options = get_option( $this->config['db']['license_key'] );

			if( !empty( $options ) && $options !== false )
			{
				new Update(
					$this->config['server']['url'],
					$this->update_basename,
					$this->update_product_id,
					$this->update_license[ $this->config['db']['activation_key'] ],
					$this->update_license[ $this->config['db']['activation_email'] ],
					$this->update_renew_license_url,
					$this->update_instance_id,
					$this->update_domain,
					$this->update_sw_version,
					$this->update_plugin_or_theme,
					'molongui-common-framework'
				);
			}
		}

		/**
		 * Customizes the admin footer text on Molongui related pages.
		 *
		 * @access  public
		 * @param   string  $footer_text
		 * @return  string
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		public function admin_footer_text( $footer_text )
		{
			global $current_screen;

			// Customize text on common framework pages ("Dashboard", "Support", "About", ...).
			$common_fw_pages = array( 'toplevel_page_molongui', 'molongui_page_molongui-support', 'molongui_page_molongui-about' );
			if ( in_array( $current_screen->id, $common_fw_pages ) )
			{
				return ( sprintf( __( 'Molongui is a trademark of %1$s Amitzy%2$s.', 'molongui-common-framework' ),
					'<a href="https://www.amitzy.com" target="_blank" class="molongui-admin-footer-link">',
					'</a>' )
				);
			}

			// Customize text on Molongui plugin's settings pages.
			if ( $current_screen->id == 'molongui_page_'.$this->plugin->slug )
			{
				return ( sprintf( __( 'If you like <strong>%s</strong> please leave us a %s&#9733;&#9733;&#9733;&#9733;&#9733;%s rating. A huge thank you from Molongui in advance!', 'molongui-common-framework' ),
					$this->plugin->name,
					'<a href="https://wordpress.org/support/view/plugin-reviews/'.strtolower( str_replace( ' ', '-', $this->plugin->name ) ).'?filter=5#postform" target="_blank" class="molongui-admin-footer-link" data-rated="' . esc_attr__( 'Thanks :)', 'molongui-common-framework' ) . '">',
					'</a>' )
				);
			}

			// Return default text.
			return $footer_text;
		}

		/**
		 * Add extra "action links" on Plugins page.
		 *
		 * @see     http://codex.wordpress.org/Plugin_API/Filter_Reference/plugin_action_links_%28plugin_file_name%29
		 * @param   array   $links
		 * @return  array   $links
		 * @access  public
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		public function add_action_links( $links )
		{
			// Load config.
			$config = include molongui_get_constant( $this->plugin->id, 'DIR', true ) . "config/config.php";

			$more_links = array(
				'settings' => '<a href="' . admin_url( $config['menu']['slug'] . $this->plugin->slug ) . '">' . __( 'Settings', 'molongui-common-framework' ) . '</a>',
				'docs'     => '<a href="' . molongui_get_constant( $this->plugin->id, 'MOLONGUI_WEB', true ) . '/docs" target="blank" >' . __( 'Docs', 'molongui-common-framework' ) . '</a>'
			);

			if( !$this->plugin->is_premium )
			{
				$more_links['gopro'] = '<a href="' . $this->plugin->web . '/" target="blank" style="font-weight:bold;color:orange">' . __( 'Go Premium', 'molongui-common-framework' ) . '</a>';
			}

			return array_merge(
				$more_links,
				$links
			);
		}

		/**
		 * Add menu link to the admin menu at the admin area.
		 *
		 * This function registers the menu link to the settings page and the settings page itself.
		 *
		 * @access  public
		 * @see     https://codex.wordpress.org/Function_Reference/add_menu_page
		 * @see     https://codex.wordpress.org/Function_Reference/add_submenu_page
		 * @since   1.0.0
		 * @version 1.2.2
		 */
		public function add_menu_item()
		{
			// Check user capabilities.
			if ( !current_user_can( 'manage_options' ) ) return;

			// Instantiate settings class.
			if( !class_exists( 'Molongui\Fw\Includes\Settings' ) ) require_once( molongui_get_constant( $this->plugin->id, 'DIR', true ) . '/includes/fw-class-settings.php' );
			$settings = new Settings( $this->plugin->id, $this->plugin->settings_map );

			// Add plugin menu.
			$settings->add_menu_item();
		}

		/**
		 * Register settings page tabs.
		 *
		 * @access  public
		 * @since   1.0.0
		 * @version 1.2.2
		 */
		public function add_page_tabs()
		{
			// Check user capabilities.
			if ( !current_user_can( 'manage_options' ) ) return;

			// Instantiate settings class
			if( !class_exists( 'Molongui\Fw\Includes\Settings' ) ) require_once( molongui_get_constant( $this->plugin->id, 'DIR', true ) . '/includes/fw-class-settings.php' );
			$settings = new Settings(  $this->plugin->id, $this->plugin->settings_map );

			// Add menu
			$settings->add_page_tabs();
		}

		/**
		 * Display a star icon to indicate it is a Premium setting.
		 *
		 * This function adds a star icon to the end of the filed label to mark a Premium setting on free plugins.
		 *
		 * @access  public
		 * @param   string  $type       Whether it is a a premium setting or a setting with premium options.
		 * @param   string  $default    Default value for the setting.
		 * @return  string  $tip        Premium tip.
		 * @since   1.0.0
		 * @version 1.0.0
		 */
		public function premium_setting_tip( $type = 'full', $default = '' )
		{
			switch ( $type )
			{
				case 'full':
					$tip = sprintf( __( '%sPremium setting%s. You are using the free version of this plugin, so changing this setting will have no effect and default value will be used. Consider purchasing the %sPremium Version%s.', 'molongui-common-framework' ),
						'<strong>',
						'</strong>',
						'<a href="'.$this->plugin->web.'" target="_blank">',
						'</a>' );
					break;

				case 'part':
					$tip = sprintf( __( '%sPremium setting%s. You are using the free version of this plugin, so selecting any option marked as "PREMIUM" will have no effect and default value will be used. Consider purchasing the %sPremium Version%s.', 'molongui-common-framework' ),
						'<strong>',
						'</strong>',
						'<a href="'.$this->plugin->web.'" target="_blank">',
						'</a>' );
					break;

				default:
					$tip = '';
					break;
			}

			return $tip;
		}

		/**
		 * License tab data validation.
		 *
		 * Sanitizes and validates data submitted using license tab form.
		 *
		 * @access  public
		 * @since   1.0.0
		 * @version 1.2.2
		 */
		public function validate_license_tab( $input )
		{
			// DEBUG: For testing activation status_extra data.
			//molongui_debug( array( $_REQUEST, $input ), true, false, true );

			$this->plugin_license->validate_settings( $input );

			// Save plugin version (useful on plugin updates).
			$input['plugin_version'] = $this->plugin->version;

			// DEBUG: For testing activation status_extra data.
			//molongui_debug( array( $input ), true, false, true );

			// Return sanitized and validated data (this will update database data).
			return $input;
		}

		/**
		 * Advanced tab data validation.
		 *
		 * Sanitizes and validates data submitted using the advanced tab form.
		 *
		 * @access  public
		 * @since   1.0.0
		 * @version 1.2.2
		 */
		public function validate_advanced_tab( $input )
		{
			// DEBUG: Check involved data.
			// molongui_debug( array( $_REQUEST, $input ), true, false, true );

			// Save plugin version (useful on plugin updates).
			$input[ 'plugin_version' ] = $this->plugin->version;

			// Load public post types.
			$pts = molongui_get_post_types( 'all', 'names', false );

			// Handle unchecked post types.
			// Unchecked checkboxes doesn't have any value, so it must be equaled to 0 in order to override default values.
			foreach( $pts as $pt )
			{
				if ( !isset( $input['extend_to_'.$pt] ) ) $input['extend_to_'.$pt] = '0';
			}

			// Force default settings for premium options.
			if( !$this->plugin->is_premium )
			{
				// Post types.
				foreach( $pts as $pt )
				{
					$input['extend_to_'.$pt] = '0';
				}
				$input['extend_to_post'] = '1';
				$input['extend_to_page'] = '1';

				// Shortcodes.
				$input['enable_sc_text_widgets'] = '0';

				// TODO: $input['option_name'] = 'value';
			}

			// Handle mandatory fields.
			// TODO: if ( !isset( $input['option_name'] ) ) $input['option_name'] = 'value';

			// Return validated data.
			return $input;
		}

		/**
		 * Load available plugin settings.
		 *
		 * As settings page is divided into tabs, each tab has its own options group.
		 *
		 * @see     http://stackoverflow.com/a/19160452
		 *
		 * @access  public
		 * @since   1.0.0
		 * @version 1.2.2
		 */
		public function load_plugin_settings()
		{
			/**
			 * Settings array needs to be defined after WordPress has initialized its translation routines.
			 * So plugin settings must be loaded in an init action hook (like this function is).
			 *
			 * @see http://stackoverflow.com/a/19160452
			 */

			// Load common framework settings.
			if ( file_exists( $file = molongui_get_constant( $this->plugin->id, 'DIR', true ) . 'config/settings.php' ) ) $common_settings = include $file;

			// Load plugin settings.
			if ( file_exists( $file = $this->plugin->dir . '/config/settings.php' ) ) $plugin_settings = include $file;

			// Merge settings.
			$this->plugin->settings_map = array_merge_recursive( ( isset( $plugin_settings ) ? $plugin_settings : array() ), ( isset( $common_settings ) ? $common_settings : array() ) );
		}

	}
}