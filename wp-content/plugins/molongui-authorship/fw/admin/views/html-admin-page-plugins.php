<?php

// Deny direct access to this file
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * About page.
 *
 * Contains the markup to be displayed on the "About" page of the framework's top-level menu ("Molongui").
 *
 * @author     Amitzy
 * @package    Molongui Common Framework (/fw)
 * @subpackage /fw/admin/views
 * @since      1.0.0
 * @version    1.1.0
 */

?>

<div class="molongui-page-centered-wrapper">

	<div class="molongui-page-centered-inner">

        <img class="molongui-page-header" src="<?php echo molongui_get_constant( $this->plugin->id, 'URL', true ) . 'admin/img/logo_molongui.png' ?>" alt="Molongui logo" />
		<h1><?php _e( 'Need help? Need more?', 'molongui-common-framework' ); ?></h1>
		<p>Get instant answers for the most common questions and learn how to use Molongui plugins like a pro. Upgrade to Premium and unleash powerful features.</p>

		<!-- Quick buttons -->
		<div class="molongui-blurb-holder">

			<!-- Documentation -->
			<a class="molongui-blurb" target="_blank" href="<?php echo molongui_get_constant( $this->plugin->id, 'MOLONGUI_WEB', true ); ?>/docs/">
				<div class="molongui-blurb-icon"><i class="molongui-icon-book-open turquoise"></i></div>
				<div class="molongui-blurb-title"><?php _e( 'Read the docs', 'molongui-common-framework' ); ?></div>
				<div class="molongui-blurb-text"><?php _e( 'Learn the basics to help you make the most of Molongui plugins.', 'molongui-common-framework' ); ?></div>
			</a>

			<!-- Help request -->
			<a class="molongui-blurb" target="_blank" href="<?php echo molongui_get_constant( $this->plugin->id, 'MOLONGUI_WEB', true ); ?>/support/">
				<div class="molongui-blurb-icon"><i class="molongui-icon-tip-circled blue"></i></div>
				<div class="molongui-blurb-title"><?php _e( 'Help request', 'molongui-common-framework' ); ?></div>
				<div class="molongui-blurb-text"><?php _e( 'Unable to find what you are looking for? Open a support ticket to get help.', 'molongui-common-framework' ); ?></div>
			</a>

			<!-- Upgrade -->
			<a class="molongui-blurb" target="_blank" href="<?php echo molongui_get_constant( $this->plugin->id, 'MOLONGUI_WEB', true ); ?>">
				<div class="molongui-blurb-icon"><i class="molongui-icon-star orange"></i></div>
				<div class="molongui-blurb-title"><?php _e( 'Upgrade!', 'molongui-common-framework' ); ?></div>
				<div class="molongui-blurb-text"><?php _e( 'Upgrade to unlock all premium features.', 'molongui-common-framework' ); ?></div>
			</a>

		</div>

	</div>

</div>