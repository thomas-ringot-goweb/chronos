<?php

use Molongui\Fw\Includes\Upsell;

// Deny direct access to this file
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Sidebar.
 *
 * Shows a sidebar with upsells in the settings page.
 *
 * @author     Amitzy
 * @package    Molongui Common Framework (/fw)
 * @subpackage /fw/admin/views
 * @since      1.0.0
 * @version    1.0.0
 */

?>

<div class="sidebar">
	<div class="upsells">

		<p class="text">
			<?php _e( 'As part of our ongoing effort to provide high quality, eye-catching Wordpress plugins, here you have some you might find useful for your site:', 'molongui-common-framework' ); ?>
		</p>

		<?php
			$upsell = new Upsell( $this->plugin->id );
			$upsell->output( 'featured', 2, 36, null );
		?>

	</div><!-- /.upsells -->
</div><!-- /.sidebar -->