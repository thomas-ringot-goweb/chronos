<?php

use Molongui\Fw\Includes\Upsell;

// Deny direct access to this file
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * More page.
 *
 * This file is used to markup the "More" page of the plugin.
 *
 * @author     Amitzy
 * @package    Molongui Common Framework (/fw)
 * @subpackage /fw/admin/views
 * @since      1.0.0
 * @version    1.0.0
 */

?>

<div class="molongui-more-wrap">

	<h3><?php _e( 'Molongui plugins', 'molongui-common-framework' ); ?></h3>
	<p class="text">
		<?php _e( 'As part of our ongoing effort to provide high quality, eye-catching Wordpress plugins, here you have some you might find useful for your site:', 'molongui-common-framework' ); ?>
	</p>

	<!-- Upsells -->
	<?php
	$upsell = new Upsell( $plugin_id );
	$upsell->output( 'all', 'all', 36, null );
	?>

</div><!-- !.upsells -->