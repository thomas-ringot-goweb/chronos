<?php

// Deny direct access to this file.
if ( !defined( 'ABSPATH' ) ) exit;

// Load useful functions.
//require_once MOLONGUI_AUTHORSHIP_FW_DIR.'includes/fw-helper-functions.php';

/**
 * Plugin-specific functions for WP Customizer.
 *
 * @see        http://ottopress.com/2015/whats-new-with-the-customizer/
 *
 * @author     Amitzy
 * @package    Authorship
 * @subpackage /includes
 * @since      2.1.0
 * @version    3.0.0
 */

/***********************************************************************************************************************
 * VALIDATE CALLBACKS
 **********************************************************************************************************************/

/**
 * Validates author box width input value.
 *
 * @param   mixed   $validity
 * @param   mixed   $value
 * @return  mixed   $validity
 * @since   2.1.0
 * @version 2.1.0
 */
function molongui_authorship_validate_box_width( $validity, $value )
{
	$value = intval( $value );
	if ( ( empty( $value ) and ( $value !== 0 ) ) || !is_numeric( $value ) )
	{
		$validity->add( 'required', __( 'You must supply a valid width.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) );
	}
	elseif ( $value < 0 )
	{
		$validity->add( 'box_width_too_small', __( 'Box width is too small.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) );
	}
	elseif ( $value > 100 )
	{
		$validity->add( 'box_width_too_big', __( 'Box width cannot be bigger than 100%.', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) );
	}
	return $validity;
}

/***********************************************************************************************************************
 * SANITIZE CALLBACKS
 **********************************************************************************************************************/

/**
 * Returns user's input if premium, setting's stored value if valid or default otherwise.
 *
 * @param   mixed   $input
 * @param   object  $setting
 * @return  mixed   $input
 * @since   2.1.0
 * @version 2.1.0
 */
function molongui_authorship_sanitize_setting( $input, $setting )
{
	// Get setting type.
	$setting_type = $setting->manager->get_control( $setting->id )->type;

	// Get the list of possible options associated with the setting.
	$choices = $setting->manager->get_control( $setting->id )->choices;
	if ( !empty( $choices ) )
	{
		$accepted = array();
		foreach ( $choices as $choice => $atts )
		{
			if ( !isset( $atts['premium'] ) or ( isset( $atts['premium'] ) and !$atts['premium'] ) ) $accepted[] = $choice;
		}

		// Return user input if accepted option selected.
		if ( in_array( $input, $accepted ) ) return $input;

		// Handle multi-option input.
		if ( $setting_type == 'molongui-image-checkbox' )
		{
			if ( !empty( $input ) )
			{
				$input_values = explode( ',', $input );
				foreach ( $input_values as $key => $input_value )
				{
					if ( !in_array( $input_value, $accepted ) ) unset( $input_values[$key] );
				}
			}
			// No option is OK on any case. Save it.
			else return $input;

			// Save only those acceptable options.
			return implode(',', $input_values );
		}

		// Get stored option for the setting.
		$box = get_option( MOLONGUI_AUTHORSHIP_BOX_SETTINGS );
		preg_match('/\[(.*?)\]/', $setting->id, $setting_name);

		// Try to revert to stored value if it is an accepted option.
		if ( isset( $box[$setting_name[1]] ) and !empty( $box[$setting_name[1]] ) )
		{
			if ( in_array( $box[$setting_name[1]], $accepted ) ) return $box[$setting_name[1]];
		}

		// Revert to default.
		return $setting->default ;
	}
	// Setting without choices: input, color, toggle, range...
	else
	{
		// Sanitize color if necessary.
		if ( $setting_type == 'molongui-color' ) $input = molongui_sanitize_color( $input );

		// Get the input attributes associated with the setting.
		$input_attrs = $setting->manager->get_control( $setting->id )->input_attrs;

		// Return user input if premium. Default otherwise.
		if ( !isset( $input_attrs['premium'] ) or ( isset( $input_attrs['premium'] ) and !$input_attrs['premium'] ) ) return $input;
		else return $setting->default;
	}
}

/**
 * Sanitizes text strings avoiding empty strings to be saved (default value is taken).
 *
 * @param   mixed   $input
 * @param   object  $setting
 * @return  mixed   $input
 * @since   3.0.0
 * @version 3.0.0
 */
function molongui_authorship_sanitize_string( $input, $setting )
{
	// If no empty value, sanitize and return.
	if ( !empty( $input ) ) return wp_filter_nohtml_kses( $input );

	// Return default value.
	return $setting->default;
}

/***********************************************************************************************************************
 * ACTIVE CALLBACKS
 **********************************************************************************************************************/

//----------------------------------------------------------------------------------------------------------------------
// BOX
//----------------------------------------------------------------------------------------------------------------------

/**
 * Returns whether to display a setting based on "box_layout" setting value.
 *
 * @param   $control
 * @return  bool
 * @since   3.0.0
 * @version 3.0.0
 */
function molongui_active_box_slim_layout_setting( $control )
{
	return ( $control->manager->get_setting('molongui_authorship_box[layout]')->value() == 'slim' ? true : false );
}

function molongui_active_box_tabbed_layout_setting( $control )
{
	return ( $control->manager->get_setting('molongui_authorship_box[layout]')->value() == 'tabbed' ? true : false );
}

function molongui_active_box_stacked_layout_setting( $control )
{
	return ( $control->manager->get_setting('molongui_authorship_box[layout]')->value() == 'stacked' ? true : false );
}

/**
 * Returns whether to display a setting based on "box_border" setting value.
 *
 * @param   $control
 * @return  bool
 * @since   3.0.0
 * @version 3.0.0
 */
function molongui_active_box_border_setting( $control )
{
	return ( $control->manager->get_setting('molongui_authorship_box[box_border]')->value() == 'none' ? false : true );
}

/**
 * Returns whether to display a "ribbon layout" related setting based on "layout" setting value.
 *
 * @param   $control
 * @return  bool
 * @since   2.1.0
 * @version 3.0.0
 */
function molongui_active_ribbon_layout_setting( $control )
{
	$layout = $control->manager->get_setting('molongui_authorship_box[profile_layout]')->value();

	return ( ( $layout == 'layout-7' or $layout == 'layout-8' ) ? true : false );
}

/**
 * Returns whether to display a setting based on "bottom_border_style" setting value.
 *
 * @param   $control
 * @return  bool
 * @since   2.1.0
 * @version 2.1.0
 */
function molongui_active_ribbon_border_setting( $control )
{
	return ( $control->manager->get_setting('molongui_authorship_box[bottom_border_style]')->value() == 'none' ? false : true );
}

//----------------------------------------------------------------------------------------------------------------------
// HEADLINE
//----------------------------------------------------------------------------------------------------------------------

/**
 * Returns whether to display a headline-related setting based on "show_headline" setting value.
 *
 * @param   $control
 * @return  bool
 * @since   2.1.0
 * @version 2.1.0
 */
function molongui_active_headline_setting( $control )
{
	return ( ( $control->manager->get_setting('molongui_authorship_box[show_headline]')->value() != '1' or $control->manager->get_setting('molongui_authorship_box[layout]')->value() == 'template-3' ) ? false : true );
}

//----------------------------------------------------------------------------------------------------------------------
// TABS
//----------------------------------------------------------------------------------------------------------------------

/**
 * Returns whether to display a setting based on "tabs_border_style" setting value.
 *
 * @param   $control
 * @return  bool
 * @since   3.0.0
 * @version 3.0.0
 */
function molongui_active_tabs_border_setting( $control )
{
	return ( $control->manager->get_setting('molongui_authorship_box[tabs_border]')->value() == 'none' ? false : true );
}
function molongui_active_tabs_border_color_setting( $control )
{
	$box_border  = $control->manager->get_setting('molongui_authorship_box[box_border]')->value();
	$tab_border  = $control->manager->get_setting('molongui_authorship_box[tabs_border]')->value();

	$box_borders = array( 'all', 'horizontals', 'top' );
	$tab_borders = array( 'none', 'around' );

	return ( in_array( $box_border, $box_borders ) and in_array( $tab_border, $tab_borders ) ? false : true );
}
function molongui_active_tabs_border_style_width_setting( $control )
{
	$box_border  = $control->manager->get_setting('molongui_authorship_box[box_border]')->value();
	$tab_border  = $control->manager->get_setting('molongui_authorship_box[tabs_border]')->value();

	$box_borders = array( 'all', 'horizontals', 'top' );
	$tab_borders = array( 'none', 'around', 'bottom' );

	return ( in_array( $box_border, $box_borders ) and in_array( $tab_border, $tab_borders ) ? false : true );
}

/**
 * Returns whether to display a setting based on "tabs_border" setting value.
 *
 * @param   $control
 * @return  bool
 * @since   3.0.0
 * @version 3.0.0
 */
function molongui_active_tabs_background_setting( $control )
{
	return ( $control->manager->get_setting('molongui_authorship_box[tabs_position]')->value() == 'top-full' ? false : true );
}

//----------------------------------------------------------------------------------------------------------------------
// AVATAR
//----------------------------------------------------------------------------------------------------------------------

/**
 * Returns whether to display "avatar_border_width" setting based on "avatar_border_style" setting value.
 *
 * @param   $control
 * @return  bool
 * @since   2.1.0
 * @version 2.1.0
 */
function molongui_active_avatar_border_width_setting( $control )
{
	return ( $control->manager->get_setting('molongui_authorship_box[avatar_border_style]')->value() == 'none' ? false : true );
}

/**
 * Returns whether to display "avatar_border_color" setting based on "avatar_border_style" setting value.
 *
 * @param   $control
 * @return  bool
 * @since   2.1.0
 * @version 2.1.0
 */
function molongui_active_avatar_border_color_setting( $control )
{
	return ( $control->manager->get_setting('molongui_authorship_box[avatar_border_style]')->value() == 'none' ? false : true );
}

/**
 * Returns whether to display an "acronym" related setting based on "avatar_default_img" setting value.
 *
 * @param   $control
 * @return  bool
 * @since   2.1.0
 * @version 2.1.0
 */
function molongui_active_acronym_setting( $control )
{
	return ( $control->manager->get_setting('molongui_authorship_box[avatar_default_img]')->value() == 'acronym' ? true : false );
}

//----------------------------------------------------------------------------------------------------------------------
// ICONS
//----------------------------------------------------------------------------------------------------------------------

/**
 * Returns whether to display a social icons-related setting based on "show_icons" setting value.
 *
 * @param   $control
 * @return  bool
 * @since   2.1.0
 * @version 2.1.0
 */
function molongui_active_icons_setting( $control )
{
	return ( $control->manager->get_setting('molongui_authorship_box[show_icons]')->value() == '1' ? true : false );
}

/**
 * Returns whether to display the social icons color setting based on "show_icons" and "icons_style" settings values.
 *
 * @param   $control
 * @return  bool
 * @since   2.1.0
 * @version 2.1.0
 */
function molongui_active_icons_color_setting( $control )
{
	$no_color_styles = array( 'branded', 'branded-boxed', 'branded-squared-reverse', 'branded-circled-reverse' );
	return ( ( in_array( $control->manager->get_setting('molongui_authorship_box[icons_style]')->value(), $no_color_styles ) or $control->manager->get_setting('molongui_authorship_box[show_icons]')->value() != '1' ) ? false : true );
}