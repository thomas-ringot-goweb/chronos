<?php

namespace Molongui\Authorship\FrontEnd;

use Molongui\Authorship\Includes\Author;

// Deny direct access to this file.
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version and all the needed hooks.
 *
 * @author     Amitzy
 * @package    Authorship
 * @subpackage /public
 * @since      1.0.0
 * @version    3.0.0
 */
class FrontEnd
{
    /**
     * The ID of this plugin.
     *
     * @access  private
     * @var     string  $plugin_name    The ID of this plugin.
     * @since   1.0.0
     * @version 1.0.0
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @access  private
     * @var     string  $plugin_version The current version of this plugin.
     * @since   1.0.0
     * @version 2.0.0
     */
    private $plugin_version;

    /**
     * Whether this plugin is premium.
     *
     * @access  private
     * @var     boolean
     * @since   2.0.0
     * @version 2.0.0
     */
    private $is_premium;

    /**
     * Initialize the class and set its properties.
     *
     * @param   string    $plugin_name     The name of the plugin.
     * @param   string    $plugin_version  The version of this plugin.
     * @since   1.0.0
     * @version 2.0.0
     */
    public function __construct( $plugin_name, $plugin_version )
    {
        $this->plugin_name    = $plugin_name;
        $this->plugin_version = $plugin_version;
        $this->is_premium     = molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR );

        // Load the required dependencies.
        $this->load_dependencies();

        if( $this->is_premium )
        {
            // Load premium dependencies.
            $this->load_premium_dependencies();
        }
    }

    /**
     * Load the required dependencies for this class.
     *
     * Loads other classes definitions used by this class.
     *
     * @access  private
     * @since   1.2.12
     * @version 2.0.2
     */
    private function load_dependencies()
    {
        /**
         * The library containing plugin's global functions.
         */
        require_once( MOLONGUI_AUTHORSHIP_DIR . 'includes/functions.php' );

        /**
         * The class responsible for defining all author stuff.
         */
        require_once( MOLONGUI_AUTHORSHIP_DIR . 'includes/class-author.php' );

        /**
         * The library containing template tag functions.
         */
        require_once( MOLONGUI_AUTHORSHIP_DIR . 'includes/template-tags.php' );

        /**
         * The library containing compatibility functions.
         *
         * @since 2.0.2
         */
        require_once( MOLONGUI_AUTHORSHIP_DIR . 'includes/compatibility.php' );
    }

    /**
     * Load the required premium dependencies for this class.
     *
     * Loads other classes definitions used by this class.
     *
     * @access  private
     * @since   1.2.2
     * @version 2.2.0
     */
    private function load_premium_dependencies()
    {
        /**
         * The library responsible for defining all available shortcodes.
         */
        require_once( MOLONGUI_AUTHORSHIP_DIR . 'premium/includes/shortcodes.php' );

        // Load plugin settings.
        $guest_settings    = get_option( MOLONGUI_AUTHORSHIP_GUEST_SETTINGS );
	    $archives_settings = get_option( MOLONGUI_AUTHORSHIP_ARCHIVES_SETTINGS );
	    $settings          = array_merge( $guest_settings, $archives_settings );

        /**
         * The library responsible for initiating virtual pages functionality.
         */
        if ( isset( $settings['enable_guest_authors_feature'] ) and $settings['enable_guest_authors_feature'] and
             isset( $settings['guest_archive_enabled'] ) and $settings['guest_archive_enabled'] )
        {
	        require_once( MOLONGUI_AUTHORSHIP_DIR . 'premium/includes/guest-archives.php' );
        }

        /**
         * The library responsible for disabling registered author archive pages functionality.
         */
        require_once( MOLONGUI_AUTHORSHIP_DIR . 'premium/includes/author-archives.php' );
    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @access  public
     * @since   1.0.0
     * @version 3.0.0
     */
    public function enqueue_styles()
    {
    	// Enqueue plugin styles.
        if ( !$this->is_premium )
        {
            $fpath = 'public/css/molongui-authorship.fada.min.css';
            if ( file_exists( MOLONGUI_AUTHORSHIP_DIR . $fpath ) )
                wp_enqueue_style( strtolower( $this->plugin_name ), MOLONGUI_AUTHORSHIP_URL . $fpath, array(), $this->plugin_version, 'all' );
        }
        else
        {
            $fpath = 'premium/public/css/molongui-authorship-premium.7e29.min.css';
            if ( file_exists( MOLONGUI_AUTHORSHIP_DIR . $fpath ) )
                wp_enqueue_style( strtolower( $this->plugin_name ), MOLONGUI_AUTHORSHIP_URL . $fpath, array(), $this->plugin_version, 'all' );
        }

	    // Generate on the fly some styles and add them to the HTML markup.
	    $onthefly_css = '';

        // Add styles to hide configured elements/selectors.
	    $settings = get_option( MOLONGUI_AUTHORSHIP_ADVANCED_SETTINGS );
	    if ( isset( $settings['hide_elements'] ) and !empty( $settings['hide_elements'] ) )
	    {
		    $selectors = $settings['hide_elements'];
		    $onthefly_css .= "{$selectors} { display: none !important; }";
	    }

	    // Add customized styles for the author box based on set configuration.
	    $onthefly_css .= $this->generate_on_the_fly_css();

	    // Add styles to HTML markup.
	    wp_add_inline_style( strtolower( $this->plugin_name ), $onthefly_css );
    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @access  public
     * @since   1.0.0
     * @version 3.0.0
     */
    public function enqueue_scripts()
    {
        // Load plugin internal configuration.
        $config = include MOLONGUI_AUTHORSHIP_DIR . "config/config.php";

        // Enqueue css-element-queries scripts.
        if ( $config['scripts']['front']['element_queries'] )
        {
            wp_enqueue_script( 'molongui-resizesensor',   MOLONGUI_AUTHORSHIP_FW_URL . 'public/js/vendor/element-queries/ResizeSensor.js',   array( 'jquery' ), $this->plugin_version, false );
            wp_enqueue_script( 'molongui-elementqueries', MOLONGUI_AUTHORSHIP_FW_URL . 'public/js/vendor/element-queries/ElementQueries.js', array( 'jquery' ), $this->plugin_version, false );
        }

	    // Enqueue plugin scripts.
        if ( !$this->is_premium )
        {
            $fpath = 'public/js/molongui-authorship.xxxx.min.js';
            if ( file_exists( MOLONGUI_AUTHORSHIP_DIR . $fpath ) )
                wp_enqueue_script( strtolower( $this->plugin_name ), MOLONGUI_AUTHORSHIP_URL . $fpath, array( 'jquery' ), $this->plugin_version, true );
        }
        else
        {
            $fpath = 'premium/public/js/molongui-authorship-premium.xxxx.min.js';
            if ( file_exists( MOLONGUI_AUTHORSHIP_DIR . $fpath ) )
                wp_enqueue_script( strtolower( $this->plugin_name ), MOLONGUI_AUTHORSHIP_URL . $fpath, array( 'jquery' ), $this->plugin_version, true );
        }

	    // Load plugin settings.
	    $settings = get_option( MOLONGUI_AUTHORSHIP_ADVANCED_SETTINGS );

	    // Generate on the fly a script and add it to the HTML markup.
	    if ( isset( $settings['hide_elements'] ) and !empty( $settings['hide_elements'] ) )
	    {
		    $selectors = $settings['hide_elements'];
		    $onthefly_js = "var s = '{$selectors}';
		                    var match = s.split(', ');
		                    for (var a in match) { jQuery(match[a]).hide(); }";
		    wp_add_inline_script( strtolower( $this->plugin_name ), $onthefly_js );
	    }
    }

    /**
     * Generates CSS rules based on set plugin configuration.
     *
     * @access  private
     * @return  string      CSS rules to add.
     * @since   3.0.0
     * @version 3.1.0
     */
    private function generate_on_the_fly_css()
    {
    	// Init vars.
		$css = '';

		// Load plugin settings.
	    $settings = get_option( MOLONGUI_AUTHORSHIP_BOX_SETTINGS );

		//--------------------------------------------------------------------------------------------------------------
	    // Author box tabs - Borders.
	    //--------------------------------------------------------------------------------------------------------------

	    // Get tabs relative position.
	    if ( isset( $settings['tabs_position'] ) and !empty( $settings['tabs_position'] ) ) $position = explode('-', $settings['tabs_position'] );
	    if ( isset( $position ) or !empty( $position[0] ) ) $position = $position[0];
	    else $position = 'top';

	    // Get tabs border style.
	    if ( isset( $settings['tabs_border'] ) and !empty( $settings['tabs_border'] ) ) $border = $settings['tabs_border'];
	    else $border = 'around';

	    // Init vars.
		$nav_style    = '';
		$tab_style    = '';
		$active_style = '';

		// Nav and tabs background.
	    if ( isset( $settings['tabs_background'] ) and !empty( $settings['tabs_background'] ) ) $nav_style .= ' background-color:'.$settings['tabs_background'].';';
	    if ( isset( $settings['tabs_color'] ) and !empty( $settings['tabs_color'] ) ) $tab_style .= ' background-color:'.$settings['tabs_color'].';';
/*
	    // Tabs border (style, width and color).
	    switch ( $position )
	    {
		    case 'left':

			    // TODO...

			break;

		    case 'right':

			    // TODO...

			break;

		    case 'bottom':

			    // TODO...

			break;

		    case 'top':
		    default:

			    switch ( $border )
			    {
				    case 'none':

					    $nav_style .= 'border: 0;';
					    $nav_style .= 'margin-bottom: 0;';
					    $tab_style .= 'border: 0;';
					    $tab_style .= 'margin-bottom: 0;';

					    if ( $settings['box_border'] === 'all' || $settings['box_border'] === 'horizontals' || $settings['box_border'] === 'top' )
					    {
						    $active_style .= 'margin-bottom: -'.$settings['box_border_width'].'px;';
					    }

				    break;

				    case 'top':

					    $nav_style    .= 'border: 0;';
					    $nav_style    .= 'margin-bottom: 0;';
					    $tab_style    .= 'border: 0; border-top: '.$settings['tabs_border_width'].'px '.$settings['tabs_border_style'].' transparent;';
					    $tab_style    .= 'margin-bottom: 0;';
					    $active_style .= 'border-top: '.$settings['tabs_border_width'].'px '.$settings['tabs_border_style'].' '.$settings['tabs_border_color'].';';

					    if ( $settings['box_border'] === 'all' || $settings['box_border'] === 'horizontals' || $settings['box_border'] === 'top' )
					    {
						    $nav_style    .= 'border-bottom: '.$settings['box_border_width'].'px '.$settings['box_border_style'].' '.$settings['box_border_color'].';';
						    $nav_style    .= 'margin-bottom: -'.$settings['box_border_width'].'px;';
						    $tab_style    .= 'border-bottom: '.$settings['box_border_width'].'px '.$settings['box_border_style'].' '.$settings['box_border_color'].';';
						    $tab_style    .= 'margin-bottom: -'.$settings['box_border_width'].'px;';
						    $active_style .= 'border-bottom: '.$settings['box_border_width'].'px '.$settings['box_border_style'].' transparent;';
					    }

				    break;

				    case 'topline':

					    $nav_style    .= 'border: 0; border-bottom: 1px solid lightgrey;';
					    $nav_style    .= 'margin-bottom: 0;';
					    $tab_style    .= 'border: 0; border-top: '.$settings['tabs_border_width'].'px '.$settings['tabs_border_style'].' transparent; border-bottom: 1px solid lightgrey;';
					    $tab_style    .= 'margin-bottom: -1px;';
					    $active_style .= 'border: 1px solid lightgrey; border-top: '.$settings['tabs_border_width'].'px '.$settings['tabs_border_style'].' '.$settings['tabs_border_color'].'; border-bottom: 0;';

					    if ( $settings['box_border'] === 'all' || $settings['box_border'] === 'horizontals' || $settings['box_border'] === 'top' )
					    {
						    $nav_style .= 'margin-bottom: -'.($settings['box_border_width']+1).'px;';
					    }

				    break;

				    case 'bottom':

					    $nav_style    .= 'border: 0;';
					    $nav_style    .= 'margin-bottom: 0;';
					    $tab_style    .= 'border: 0; border-bottom: '.$settings['tabs_border_width'].'px '.$settings['tabs_border_style'].' transparent;';
					    $tab_style    .= 'margin-bottom: 0px;';
					    $active_style .= 'border: 0; border-bottom: '.$settings['tabs_border_width'].'px '.$settings['tabs_border_style'].' '.$settings['tabs_border_color'].';';

					    if ( $settings['box_border'] === 'all' || $settings['box_border'] === 'horizontals' || $settings['box_border'] === 'top' )
					    {
						    $tab_style .= 'border-bottom: '.$settings['box_border_width'].'px '.$settings['box_border_style'].' '.$settings['box_border_color'].';';
						    $tab_style .= 'margin-bottom: -'.$settings['box_border_width'].'px;';
					    }

				    break;

				    case 'around':
				    default:

					    $nav_style    .= 'border: 0; border-bottom: '.$settings['tabs_border_width'].'px '.$settings['tabs_border_style'].' '.$settings['tabs_border_color'].';';
					    $nav_style    .= 'margin-bottom: -'.$settings['tabs_border_width'].'px;';
				        $tab_style    .= 'padding: '.(15+$settings['tabs_border_width']).'px '.(25+$settings['tabs_border_width']).'px '.(15).'px;';
			    		$tab_style    .= 'border: 0; border-bottom: '.$settings['tabs_border_width'].'px '.$settings['tabs_border_style'].' '.$settings['tabs_border_color'].';';
						$tab_style    .= 'margin-bottom: -'.$settings['tabs_border_width'].'px;';
					    $active_style .= 'padding: '.(15).'px '.(25).'px;';
					    $active_style .= 'border: '.$settings['tabs_border_width'].'px '.$settings['tabs_border_style'].' '.$settings['tabs_border_color'].'; border-bottom: 0;';
					    $active_style .= 'background: '.( ( isset( $settings['tabs_color'] ) and !empty( $settings['tabs_color'] ) ) ? $settings['tabs_color'] : 'white' ).';';

					    if ( $settings['box_border'] === 'all' || $settings['box_border'] === 'horizontals' || $settings['box_border'] === 'top' )
					    {
						    $nav_style    .= 'border: 0;';
						    $nav_style    .= 'margin-bottom: 0;';
						    $tab_style    .= 'border-bottom: '.$settings['box_border_width'].'px '.$settings['box_border_style'].' '.$settings['box_border_color'].';';
						    $tab_style    .= 'margin-bottom: -'.$settings['box_border_width'].'px;';
						    $active_style .= 'border: '.$settings['box_border_width'].'px '.$settings['box_border_style'].' '.$settings['box_border_color'].'; border-bottom: 0;';
					    }

					break;
			    }

			break;
	    }
*/
	    //--------------------------------------------------------------------------------------------------------------
	    // Author box tabs - Background color.
	    //--------------------------------------------------------------------------------------------------------------
	    $tabs_background_style        = 'background-color: '.$settings['tabs_color'].';';
	    $tabs_active_background_style = 'background-color: '.$settings['tabs_active_color'].';';

	    // CSS markup.
	    $css .= "
	        .molongui-author-box .molongui-author-box-tabs nav.molongui-author-box-tabs-{$position} { {$nav_style} }
	        .molongui-author-box .molongui-author-box-tabs nav label { {$tab_style} }
	        .molongui-author-box .molongui-author-box-tabs input[id^='mab-tab-profile-']:checked ~ nav label[for^='mab-tab-profile-'],
            .molongui-author-box .molongui-author-box-tabs input[id^='mab-tab-related-']:checked ~ nav label[for^='mab-tab-related-'],
            .molongui-author-box .molongui-author-box-tabs input[id^='mab-tab-contact-']:checked ~ nav label[for^='mab-tab-contact-']
            {
                {$active_style}
            }
            
            .molongui-author-box .molongui-author-box-tabs nav label.molongui-author-box-tab { {$tabs_background_style} }
            .molongui-author-box .molongui-author-box-tabs nav label.molongui-author-box-tab.molongui-author-box-tab-active { {$tabs_active_background_style} }
        ";

	    // Return customized CSS classes.
	    return $css;
    }
}