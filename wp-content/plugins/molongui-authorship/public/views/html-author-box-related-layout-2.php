<?php

/**
 * Author box related tab layout 2.
 *
 * This template must have the loop over the $author['posts'].
 *
 * @author     Amitzy
 * @package    Authorship
 * @subpackage /public/views
 * @since      3.0.0
 * @version    3.0.0
 */

foreach( $author['posts'] as $related )
{
    ?>
    <li>
        <div class="molongui-author-box-related-entry">

            <!-- Related entry thumb -->
            <div class="molongui-author-box-related-entry-thumb">
                <?php if ( has_post_thumbnail( $related->ID ) ) : ?>
                    <a href="<?php echo get_permalink( $related->ID ); ?>">
                        <?php echo get_the_post_thumbnail( $related->ID, 'thumbnail', $attr = '' ) ?>
                    </a>
                <?php else : ?>
                    <img src="<?php echo MOLONGUI_AUTHORSHIP_URL.'public/img/related_placeholder.svg'; ?>" width="<?php echo get_option( 'thumbnail_size_w' ).'px'; ?>">
                <?php endif; ?>
            </div>

            <div class="">
                <!-- Related entry date -->
                <div class="molongui-author-box-related-entry-date">
                    <?php echo get_the_date( '', $related->ID ); ?>
                </div>

                <!-- Related entry title -->
                <div class="molongui-author-box-related-entry-title">
                    <a class="molongui-remove-text-underline" href="<?php echo get_permalink( $related->ID ); ?>">
                        <?php echo $related->post_title; ?>
                    </a>
                </div>
            </div>

        </div>
    </li>
    <?php
}