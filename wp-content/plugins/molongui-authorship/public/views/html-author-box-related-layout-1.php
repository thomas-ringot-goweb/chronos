<?php

/**
 * Author box related tab layout 1.
 *
 * This template must have the loop over the $author['posts'].
 *
 * @author     Amitzy
 * @package    Authorship
 * @subpackage /public/views
 * @since      3.0.0
 * @version    3.0.0
 */

foreach( $author['posts'] as $related )
{
    ?>
    <li>
        <div class="molongui-author-box-related-entry">
            <div class="molongui-author-box-related-entry-title">
                <i class="molongui-authorship-icon-doc"></i>
                <a class="molongui-remove-text-underline" href="<?php echo get_permalink( $related->ID ); ?>">
                    <?php echo $related->post_title; ?>
                </a>
            </div>
        </div>
    </li>
    <?php
}