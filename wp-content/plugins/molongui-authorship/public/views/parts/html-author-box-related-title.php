<?php

/**
 * HTML template part.
 *
 * Author box related title markup.
 *
 * @author     Amitzy
 * @package    Authorship
 * @subpackage /public/views/parts
 * @since      3.0.0
 * @version    3.0.0
 */

?>

<div class="molongui-author-box-related-title">
    <?php echo ( $settings['related_title'] ? $settings['related_title'] : __( 'Related posts', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) ); ?>
</div>