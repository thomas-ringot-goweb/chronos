<?php

/**
 * HTML template part.
 *
 * Author box headline markup.
 *
 * @author     Amitzy
 * @package    Molongui_Authorship
 * @subpackage Molongui_Authorship/public/views/parts
 * @since      1.3.5
 * @version    2.1.0
 */

$headline_text_style = '';
if ( isset( $settings['headline_text_style'] ) and !empty( $settings['headline_text_style'] ) )
{
    foreach ( explode(',', $settings['headline_text_style'] ) as $style ) $headline_text_style .= ' molongui-text-style-'.$style;
}
?>

<?php if ( isset( $settings['show_headline'] ) and !empty( $settings['show_headline'] ) and $settings['show_headline'] == 1  ) : ?>
<!-- Author headline -->
<div class="molongui-author-box-item molongui-author-box-headline">
    <h3 class="molongui-font-size-<?php echo $settings['headline_text_size']; ?>-px
               molongui-text-align-<?php echo $settings['headline_text_align']; ?>
               <?php echo $headline_text_style; ?>
               molongui-text-case-<?php echo $settings['headline_text_case']; ?>"
        style="color: <?php echo $settings['headline_text_color']; ?>">
        <span class="molongui-author-box-string-headline"><?php echo ( $settings['headline'] ? $settings['headline'] : __( 'About the author', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) ); ?></span>
    </h3>
</div>
<?php endif; ?>