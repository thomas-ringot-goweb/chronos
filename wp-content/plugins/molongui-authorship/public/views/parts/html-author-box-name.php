<?php

/**
 * HTML author box template part.
 *
 * Author name markup.
 *
 * @author     Amitzy
 * @package    Authorship
 * @subpackage /public/views/parts
 * @since      1.2.17
 * @version    3.0.0
 */

$name_text_style = '';
if ( isset( $settings['name_text_style'] ) and !empty( $settings['name_text_style'] ) )
{
	foreach ( explode(',', $settings['name_text_style'] ) as $style ) $name_text_style .= ' molongui-text-style-'.$style;
}

?>

<div class="molongui-author-box-title">
	<h5 class="molongui-font-size-<?php echo $settings['name_text_size']; ?>-px
               <?php echo $name_text_style; ?>
               molongui-text-case-<?php echo $settings['name_text_case']; ?>"
        style="color: <?php echo $settings['name_text_color']; ?>" itemprop="name">
        <?php
            if ( !$settings['name_link_to_archive'] or
	           ( $author['type'] === 'guest' and !molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ) ) or
	           ( $author['type'] === 'guest' and !$settings['guest_archive_enabled'] ) or
	           ( $author['type'] === 'user'  and !$settings['user_archive_enabled'] ) )
            {
	            ?>
                <span class="molongui-font-size-<?php echo $settings['name_text_size']; ?>-px" style="color:<?php echo$settings['name_text_color']; ?>;">
			        <?php echo $author['name']; ?>
                </span>
	            <?php
            }
            elseif ( is_customize_preview() and $author['type'] === 'guest' and molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ) and $settings['guest_archive_enabled'] )
            {
	            ?>
                <a href="#" class="customize-unpreviewable <?php echo ( $settings['name_inherited_underline'] == 'remove' ? 'molongui-remove-text-underline' : '' ); ?> molongui-font-size-<?php echo $settings['name_text_size']; ?>-px <?php echo $name_text_style; ?>" style="color:<?php echo $settings['name_text_color']; ?>;" itemprop="url">
		            <?php echo $author['name']; ?>
                </a>
	            <?php
            }
            else
            {
	            ?>
                <a href="<?php echo esc_url( $author['url'] ); ?>" class="<?php echo ( $settings['name_inherited_underline'] == 'remove' ? 'molongui-remove-text-underline' : '' ); ?> molongui-font-size-<?php echo $settings['name_text_size']; ?>-px <?php echo $name_text_style; ?>" style="color:<?php echo $settings['name_text_color']; ?>;" itemprop="url">
		            <?php echo $author['name']; ?>
                </a>
	            <?php
            }
        ?>
	</h5>
</div>