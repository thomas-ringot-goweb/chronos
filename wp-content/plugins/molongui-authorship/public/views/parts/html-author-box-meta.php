<?php

/**
 * HTML template part.
 *
 * Author box meta markup.
 *
 * @author     Amitzy
 * @package    Molongui_Authorship
 * @subpackage Molongui_Authorship/public/views/parts
 * @since      1.2.17
 * @version    3.0.0
 */

$meta_text_style = '';
if ( isset( $settings['meta_text_style'] ) and !empty( $settings['meta_text_style'] ) )
{
	foreach ( explode(',', $settings['meta_text_style'] ) as $style ) $meta_text_style .= ' molongui-text-style-'.$style;
}

?>

<?php if ( !isset( $settings['show_meta'] ) or ( isset( $settings['show_meta'] ) and $settings['show_meta'] ) ) : ?>
<div class="molongui-author-box-item molongui-author-box-meta
            molongui-font-size-<?php echo $settings['meta_text_size']; ?>-px
            <?php echo $meta_text_style; ?>
            molongui-text-case-<?php echo $settings['meta_text_case']; ?>"
     style="color: <?php echo $settings['meta_text_color']; ?>">

	<span itemprop="jobTitle"><?php echo $author['job']; ?></span>
	<?php if ( $author['job'] && $author['company'] ) echo ' <span class="molongui-author-box-string-at">'.( $settings[ 'at' ] ? $settings[ 'at' ] : __('at', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) ).'</span> '; ?>
	<span itemprop="worksFor" itemscope itemtype="https://schema.org/Organization">
		<?php if ( $author['company_link'] ) echo '<a href="' . esc_url( $author['company_link'] ) . '" target="_blank" itemprop="url" style="color: '.$settings['meta_text_color'].';">'; ?>
		<span itemprop="name"><?php echo $author['company']; ?></span>
		<?php if ( $author['company_link'] ) echo '</a>'; ?>
	</span>

	<?php if ( $author['phone'] and $author['show_phone'] ) : ?>
		<?php if ( $author['job'] or $author['company'] ) echo ' <span class="molongui-author-box-meta-separator">'.$settings['meta_separator'].'</span> '; ?>
        <a href="tel:<?php echo $author['phone']; ?>" itemprop="telephone" content="<?php echo $author['phone']; ?>" style="color: <?php echo $settings['meta_text_color']; ?>"><?php echo $author['phone']; ?></a>
	<?php endif; ?>

	<?php if ( $author['mail'] and $author['show_mail'] or ( $author['phone'] and $author['show_phone'] ) ) : ?>
		<?php if ( $author['job'] or $author['company'] ) echo ' <span class="molongui-author-box-meta-separator">'.$settings['meta_separator'].'</span> '; ?>
		<a href="mailto:<?php echo $author['mail']; ?>" target="_top" itemprop="email" style="color: <?php echo $settings['meta_text_color']; ?>"><?php echo $author['mail']; ?></a>
	<?php endif; ?>

	<?php if ( $author['link'] ) : ?>
		<?php if ( $author['job'] or $author['company'] or ( $author['phone'] and $author['show_phone'] ) or ( $author['mail'] and $author['show_mail'] ) ) echo ' <span class="molongui-author-box-meta-separator">'.$settings['meta_separator'].'</span> '; ?>
		<a href="<?php echo esc_url( $author['link'] ); ?>" target="_blank" itemprop="url" style="color: <?php echo $settings['meta_text_color']; ?>"><?php echo ' <span class="molongui-author-box-string-web">'.( $settings[ 'web' ] ? $settings[ 'web' ] : __( 'Website', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) ).'</span>'; ?></a>
	<?php endif; ?>

	<?php if ( $settings['show_related'] and $settings['layout'] == 'slim' and isset( $author['posts'] ) and !empty( $author['posts'] ) ) : ?>
		<?php if ( $author['job'] or $author['company'] or $author['link'] ) echo ' <span class="molongui-author-box-meta-separator">'.$settings['meta_separator'].'</span> '; ?>
		<script type="text/javascript" language="JavaScript">
			if (typeof window.ToggleAuthorshipData === 'undefined')
			{
				function ToggleAuthorshipData(id, author)
				{
					// Get author box.
					var box_selector = '#mab-' + id;
                    var box = document.querySelector(box_selector);

                    // Modify box selector if multiauthor box.
                    if ( box.getAttribute('data-multiauthor') ) box_selector = '#mab-' + id + ' [data-author-ref="' + author + '"]';

					// Toggle link label.
					var label = document.querySelector(box_selector + ' ' + '.molongui-author-box-data-toggle');
					label.innerHTML = ( label.text.trim() === "<?php echo ( $settings['more_posts'] ? $settings['more_posts'] : __( '+ posts', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) ); ?>" ? " <span class=\"molongui-author-box-string-bio\"><?php echo ( $settings['bio'] ? $settings['bio'] : __( 'Bio', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) ); ?></span>" : " <span class=\"molongui-author-box-string-more-posts\"><?php echo ( $settings['more_posts'] ? $settings['more_posts'] : __( '+ posts', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) ); ?></span>" );

					// Toggle data to display.
					var bio     = document.querySelector(box_selector + ' ' + '.molongui-author-box-bio');
					var related = document.querySelector(box_selector + ' ' + '.molongui-author-box-related-entries');

					if( related.style.display === "none" )
					{
						related.style.display = "block";
						bio.style.display     = "none";
					}
					else
					{
						related.style.display = "none";
						bio.style.display     = "block";
					}
				}
			}
		</script>
		<a href="javascript:ToggleAuthorshipData(<?php echo $random_id; ?>, '<?php echo $author['type'].'-'.$author['id']; ?>')" class="molongui-author-box-data-toggle" style="color: <?php echo $settings['meta_text_color']; ?>"><?php echo ' <span class="molongui-author-box-string-more-posts">'.( $settings[ 'more_posts' ] ? $settings[ 'more_posts' ] : __( '+ posts', MOLONGUI_AUTHORSHIP_TEXTDOMAIN ) ).'</span> '; ?></a>
	<?php endif; ?>

</div><!-- End of .molongui-author-box-meta -->
<?php endif; ?>