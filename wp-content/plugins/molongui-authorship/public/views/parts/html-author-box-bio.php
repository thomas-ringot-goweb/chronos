<?php

/**
 * HTML template part.
 *
 * Author box biography markup.
 *
 * @see        https://codex.wordpress.org/Function_Reference/wpautop
 *
 * @author     Amitzy
 * @package    Molongui_Authorship
 * @subpackage Molongui_Authorship/public/views/parts
 * @since      1.2.17
 * @version    3.0.0
 */

$bio_text_style = '';
if ( isset( $settings['bio_text_style'] ) and !empty( $settings['bio_text_style'] ) )
{
	foreach ( explode(',', $settings['bio_text_style'] ) as $style ) $bio_text_style .= ' molongui-text-style-'.$style;
}

?>

<div class="molongui-author-box-bio" itemprop="description">
	<div class="molongui-font-size-<?php echo $settings['bio_text_size']; ?>-px molongui-text-align-<?php echo $settings['bio_text_align']; ?> <?php echo $bio_text_style; ?>" style="color: <?php echo $settings['bio_text_color']; ?>">
	    <?php
        if ( !isset( $settings['show_bio'] ) or ( isset( $settings['show_bio'] ) and $settings['show_bio'] ) )
        {
            if ( molongui_is_premium( MOLONGUI_AUTHORSHIP_DIR ) )
            {
                echo do_shortcode( str_replace( array("\n\r", "\r\n", "\n\n", "\r\r"), "<br>", wpautop(  $author['bio'] ) ) );
            }
            else
            {
                echo str_replace( array("\n\r", "\r\n", "\n\n", "\r\r"), "<br>", wpautop(  $author['bio'] ) );
            }
        }

	    /**
	     * Display extra content after (or in replace of) bio.
         *
         * 'Extra content' can only be set as shortcode attribute: extra_content
         *
         * @since   3.0.0
         * @version 3.0.0
	     */
	    if ( isset( $settings['extra_content'] ) and $settings['extra_content'] )
	    {
	        echo $settings['extra_content'];
	    }
        ?>
	</div>
</div>
