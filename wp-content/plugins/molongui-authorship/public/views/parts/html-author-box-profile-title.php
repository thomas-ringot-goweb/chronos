<?php

/**
 * HTML template part.
 *
 * Author box profile title markup.
 *
 * @author     Amitzy
 * @package    Authorship
 * @subpackage /public/views/parts
 * @since      3.0.0
 * @version    3.0.0
 */

?>

<div class="molongui-author-box-profile-title"><?php echo $settings['profile_title']; ?></div>