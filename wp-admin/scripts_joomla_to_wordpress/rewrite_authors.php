<?php
global $wpdb;

require_once( dirname( __FILE__ ) . '/../admin.php' );


/*---------------------------------------*\
	► CREATION AUTEURS
\*---------------------------------------*/

$userList = [
	[ "first_name" => "Adrien", "name" => "JOLY" ],
	[ "first_name" => "Anne Laury", "name" => "LEQUIEN" ],
	[ "first_name" => "Aurélien", "name" => "BOUDEWEEL" ],
	[ "first_name" => "Alexandre", "name" => "PETIT" ],
	[ "first_name" => "Ana María", "name" => "FIERRO BUELVAS" ],
	[ "first_name" => "Camille", "name" => "WATTIEZ" ],
	[ "first_name" => "Caroline", "name" => "DEVE" ],
	[ "first_name" => "Clara", "name" => "DUBRULLE" ],
	[ "first_name" => "Christine", "name" => "MARTIN" ],
	[ "first_name" => "Delphine", "name" => "VISSOL" ],
	[ "first_name" => "Diane", "name" => "PICANDET" ],
	[ "first_name" => "Eric", "name" => "DELESALLE" ],
	[ "first_name" => "Etienne", "name" => "CHARBONNEL" ],
	[ "first_name" => "Esther", "name" => "LEBLOND" ],
	[ "first_name" => "Franck", "name" => "MAES" ],
	[ "first_name" => "François", "name" => "ALMALEH" ],
	[ "first_name" => "Frédéric", "name" => "Vauvillé" ],
	[ "first_name" => "Frédérique", "name" => "Eudier" ],
	[ "first_name" => "Geneviève", "name" => "FERRETTI" ],
	[ "first_name" => "Harald", "name" => "MIQUET" ],
	[ "first_name" => "Jacques Eric", "name" => "MARTINOT" ],
	[ "first_name" => "Kathia", "name" => "BEULQUE" ],
	[ "first_name" => "LAURA", "name" => "COTZA" ],
	[ "first_name" => "Laurent", "name" => "RUET" ],
	[ "first_name" => "Lauriane", "name" => "LEPRETRE" ],
	[ "first_name" => "Manuel", "name" => "DELAMARRE" ],
	[ "first_name" => "Patricia", "name" => "VIANE CAUVAIN" ],
	[ "first_name" => "Sabrina", "name" => "MAHIEU" ],
	[ "first_name" => "Sarah", "name" => "Buchon" ],
	[ "first_name" => "Servane", "name" => "MOREL" ],
	[ "first_name" => "Stéphanie", "name" => "TRAN" ],
	[ "first_name" => "Sylvain", "name" => "VERBRUGGHE" ],
	[ "first_name" => "Sylvie", "name" => "LHERMIE" ],
	[ "first_name" => "Thomas", "name" => "LAILLER" ],
	[ "first_name" => "Thomas", "name" => "T’JAMPENS" ],
	[ "first_name" => "Victoria", "name" => "GODEFROOD BERRA" ],
	[ "first_name" => "Virginie", "name" => "PERDRIEUX" ],
	[ "first_name" => "Constance", "name" => "LACHERE" ],
	[ "first_name" => "Equipe", "name" => "vivaldi" ],
];

foreach ( $userList as $author ) {
	$firstName   = ucfirst( mb_strtolower( $author['first_name'], 'UTF-8' ) );
	$lastName    = mb_strtoupper( $author['name'], 'UTF-8' );
	$email       = mb_strtolower( $author['first_name'], 'UTF-8' ) . '-' . mb_strtolower( $author['name'], 'UTF-8' ) . '@nomail.com';
	$password    = 'vivaldi';
	$userName    = mb_strtolower( $firstName[0] . $lastName, 'UTF-8' );
	$displayName = $firstName . ' ' . $lastName;
	$user_data   = array(
		'user_pass'    => $password,
		'user_login'   => $userName,
		'display_name' => $displayName,
		'first_name'   => $firstName,
		'last_name'    => $lastName,
		'role'         => 'author',
		'user_email'   => $email,
	);
	if ( ! email_exists( $email ) ) {
		$user_id = wp_insert_user( $user_data );
	}
}


/*---------------------------------------*\
	► INSPECTION META ALIAS POUR MAPPING
\*---------------------------------------*/
$results = $wpdb->get_results( "SELECT meta_value FROM vc_postmeta WHERE meta_key='author_alias' GROUP BY meta_value" );
$listAuthorAlias = array();
foreach ( $results as $result ) {
	$listAuthorAlias[] = $result->meta_value;
}

$mapping = [
	"    Sylvain VERBRUGGHE"                   => [ "sylvain-verbrugghe@nomail.com" ],
	"   Eric DELFLY"                           => [ "eric-delfly@nomail.com" ],
	"   Etienne CHARBONNEL"                    => [ "etienne-charbonnel@nomail.com" ],
	"   Sylvain VERBRUGGHE"                    => [ "sylvain-verbrugghe@nomail.com" ],
	" Diane PICANDET"                          => [ "diane-picandet@nomail.com" ],
	" Harald MIQUET"                           => [ "harald-miquet@nomail.com" ],
	" Kathia BEULQUE"                          => [ "kathia-beulque@nomail.com" ],
	" Patricia VIANE-CAUVAIN"                  => [ "patricia-vianecauvain@nomail.com" ],
	" Vivaldi-chronos"                         => [ "equipe-vivaldi@nomail.com" ],
	"administrator"                            => [ "equipe-vivaldi@nomail.com" ],
	"Adrien JOLY Sarah Buchon"                 => [
		"adrien-joly@nomail.com",
		"sarah-buchon@nomail.com"
	],
	"Alexandre PETIT"                          => [ "alexandre-petit@nomail.com" ],
	"Ana María FIERRO BUELVAS"                 => [ "anamara-fierrobuelvas@nomail.com" ],
	"Anne-Laury LEQUIEN"                       => [ "annelaury-lequien@nomail.com" ],
	"Aurélien BOUDEWEEL"                       => [ "aurlien-boudeweel@nomail.com" ],
	"Camille WATTIEZ"                          => [ "camille-wattiez@nomail.com" ],
	"Caroline DEVE"                            => [ "caroline-deve@nomail.com" ],
	"Caroline DEVE,"                           => [ "caroline-deve@nomail.com" ],
	"Christine MARTIN"                         => [ "christine-martin@nomail.com" ],
	"Christine MARTIN, Franck MAES"            => [
		"christine-martin@nomail.com",
		"franck-maes@nomail.com"
	],
	"Chronos"                                  => [ "equipe-vivaldi@nomail.com" ],
	"Clara DUBRULLE"                           => [ "clara-dubrulle@nomail.com" ],
	"Delphine VISSOL"                          => [ "delphine-vissol@nomail.com" ],
	"Delphine VISSOL."                         => [ "delphine-vissol@nomail.com" ],
	"Diane PIACANDET"                          => [ "diane-picandet@nomail.com" ],
	"Diane PICANDET"                           => [ "diane-picandet@nomail.com" ],
	"ED - Clara D"                             => [
		"eric-delfly@nomail.com",
		"clara-dubrulle@nomail.com"
	],
	"ED/SV"                                    => [
		"eric-delfly@nomail.com",
		"sylvain-verbrugghe@nomail.com"
	],
	"Eric DELESALLE"                           => [ "eric-delesalle@nomail.com" ],
	"Eric DELFLY"                              => [ "eric-delfly@nomail.com" ],
	"Eric DELFLY / Sarah BUCHON"               => [ "eric-delfly@nomail.com", "sarah-buchon@nomail.com" ],
	"Eric DELFLY et Victoria GODEFROOD-BERRA " => [
		"eric-delfly@nomail.com",
		"victoria-godefroodberra@nomail.com"
	],
	"Eric DELFLY Sabrina MAHIEU"               => [
		"eric-delfly@nomail.com",
		"sabrina-mahieu@nomail.com"
	],
	"Eric DELFLY, Constance LACHERE"           => [
		"eric-delfly@nomail.com",
		"constance-lachere@nomail.com"
	],
	"Eric DELFLY, LAURA COTZA"                 => [ "eric-delfly@nomail.com", "laura-cotza@nomail.com" ],
	"Eric DELFY"                               => [ "eric-delfly@nomail.com" ],
	"Esther LEBLOND"                           => [ "esther-leblond@nomail.com" ],
	"Etienne CHARBONNEL"                       => [ "etienne-charbonnel@nomail.com" ],
	"Franck MAES"                              => [ "franck-maes@nomail.com" ],
	"Franck MAES / Christine MARTIN"           => [
		"franck-maes@nomail.com",
		"christine-martin@nomail.com"
	],
	"Franck MAES, Christine MARTIN"            => [
		"franck-maes@nomail.com",
		"christine-martin@nomail.com"
	],
	"François ALMALEH"                         => [ "franois-almaleh@nomail.com" ],
	"Frédéric Vauvillé"                        => [ "frdric-vauvill@nomail.com" ],
	"Frédéric Vauvillé & Frédérique Eudier"    => [
		"frdric-vauvill@nomail.com",
		"frdrique-eudier@nomail.com"
	],
	"Geneviève FERETTI"                        => [ "genevive-ferretti@nomail.com" ],
	"Geneviève FERRETTI"                       => [ "genevive-ferretti@nomail.com" ],
	"Harald MIQUET "                           => [ "harald-miquet@nomail.com" ],
	"Jacques Eric MARTINOT"                    => [ "jacqueseric-martinot@nomail.com" ],
	"Jacques-Eric MARTINOT"                    => [ "jacqueseric-martinot@nomail.com" ],
	"Jacques-Eric MARTINOT."                   => [ "jacqueseric-martinot@nomail.com" ],
	"Kathia BEULQUE"                           => [ "kathia-beulque@nomail.com" ],
	"Kathia BEULQUE Sylvain VERBRUGGHE"        => [
		"kathia-beulque@nomail.com",
		"sylvain-verbrugghe@nomail.com"
	],
	"Katia BEULQUE"                            => [ "kathia-beulque@nomail.com" ],
	"L'équipe Chronos"                         => [ "equipe-vivaldi@nomail.com" ],
	"L'équipe Vivaldi-Chronos"                 => [ "equipe-vivaldi@nomail.com" ],
	"Lailler Delfly"                           => [
		"thomas-lailler@nomail.com",
		"eric-delfly@nomail.com"
	],
	"Laurent RUET"                             => [ "laurent-ruet@nomail.com" ],
	"Lauriane LEPRETRE"                        => [ "lauriane-lepretre@nomail.com" ],
	"MAES / MARTIN"                            => [
		"franck-maes@nomail.com",
		"christine-martin@nomail.com"
	],
	"Manuel DELAMARRE"                         => [ "manuel-delamarre@nomail.com" ],
	"MOREL"                                    => [ "sylvain-verbrugghe@nomail.com" ],
	"Patricia VIANE CAUVAIN"                   => [ "patricia-vianecauvain@nomail.com" ],
	"Patricia VIANE-CAUVAIN"                   => [ "patricia-vianecauvain@nomail.com" ],
	"Patridia VIANE-CAUVAIN"                   => [ "patricia-vianecauvain@nomail.com" ],
	"Pôle banque"                              => [ "equipe-vivaldi@nomail.com" ],
	"Sabrina MAHIEU"                           => [ "sabrina-mahieu@nomail.com" ],
	"Sabrina MAHIEUX"                          => [ "sabrina-mahieu@nomail.com" ],
	"Sarah Buchon"                             => [ "sarah-buchon@nomail.com" ],
	"Servane MOREL"                            => [ "sylvain-verbrugghe@nomail.com" ],
	"Stéphanie TRAN"                           => [ "stphanie-tran@nomail.com" ],
	"Sylvain VERBRUGGGHE"                      => [ "sylvain-verbrugghe@nomail.com" ],
	"Sylvain VERBRUGGHE"                       => [ "sylvain-verbrugghe@nomail.com" ],
	"Sylvain VERRBUGGHE"                       => [ "sylvain-verbrugghe@nomail.com" ],
	"Sylvie LHERMIE"                           => [ "sylvie-lhermie@nomail.com" ],
	"test"                                     => [ "equipe-vivaldi@nomail.com" ],
	"Thomas LAILLER"                           => [ "thomas-lailler@nomail.com" ],
	"Thomas T’JAMPENS"                         => [ "thomas-tjampens@nomail.com" ],
	"Victoria GODEFROOD-BERRA"                 => [ "victoria-godefroodberra@nomail.com" ],
	"Virginie PERDRIEUX"                       => [ "virginie-perdrieux@nomail.com" ],
	"vivaldi"                                  => [ "equipe-vivaldi@nomail.com" ],
	"Vivaldi Chronos"                          => [ "equipe-vivaldi@nomail.com" ],
	"Vivaldi-Avocats"                          => [ "equipe-vivaldi@nomail.com" ],
	"Vivaldi Avocats"                          => [ "equipe-vivaldi@nomail.com" ],
	"Vivaldi-Chronos"                          => [ "equipe-vivaldi@nomail.com" ],
];

foreach ( $listAuthorAlias as $alias ) {
	$listPostMeta = $wpdb->get_results( "SELECT * FROM vc_postmeta WHERE meta_key='author_alias' AND meta_value='$alias'" );
	$listPost     = [];
	foreach ( $listPostMeta as $postMeta ) {
		$listPost[] = $postMeta->post_id;
	}
	$realUsers = $mapping[ $alias ];
	$user  = get_user_by_email( $realUsers[0] );

	foreach ( $listPost as $postId ) {
		/*---------------------------------------*\
			► MAJ AUTEUR
		\*---------------------------------------*/
		$post = get_post( $postId );
		$post->post_author = $user->ID;
		wp_update_post( $post );
		/*---------------------------------------*\
			► Suppression de toutes les post meta précédentes sur le multi author et sur l'alias
		\*---------------------------------------*/
		delete_post_meta( $postId, '_molongui_author_box_position' );
		delete_post_meta( $postId, '_molongui_author_box_display' );
		delete_post_meta( $postId, '_molongui_author' );
		delete_post_meta( $postId, '_molongui_main_author' );

		delete_post_meta( $postId, 'author_alias');

		add_post_meta( $postId, '_molongui_author_box_position', null );
		add_post_meta( $postId, '_molongui_author_box_display', 'hide' );
		add_post_meta( $postId, '_molongui_author', 'user-'.$user->ID );
		add_post_meta( $postId, '_molongui_main_author', 'user-'.$user->ID );

		if (count($realUsers) > 1){
			foreach (array_slice($realUsers, 1) as $userMail){
				$user  = get_user_by_email( $userMail );
				add_post_meta( $postId, '_molongui_author', 'user-'.$user->ID );
			}
		}
	}
}
echo 'ok';
