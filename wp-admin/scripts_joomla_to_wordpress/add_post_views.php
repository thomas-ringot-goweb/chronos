<?php
global $wpdb;

require_once( dirname( __FILE__ ) . '/../admin.php' );


$listPostViews = $wpdb->get_results( "SELECT * FROM vc_postmeta WHERE meta_key='views'" );

foreach ( $listPostViews as $postView ) {

	$date = explode( '-', date( 'W-d-m-Y', current_time( 'timestamp' ) ) );

	$postId    = (int) $postView->post_id;
	$count = (int) $postView->meta_value;

	foreach (
		array(
			0 => $date[3] . $date[2] . $date[1], // day like 20140324
			1 => $date[3] . $date[0], // week like 201439
			2 => $date[3] . $date[2], // month like 201405
			3 => $date[3], // year like 2014
			4 => 'total'   // total views
		) as $type => $period
	) {
		$wpdb->query(
			$wpdb->prepare( "
				INSERT INTO " . $wpdb->prefix . "post_views (id, type, period, count)
				VALUES (%d, %d, %s, %d)
				ON DUPLICATE KEY UPDATE count = count + %d", $postId, $type, $period, $count, $count
			)
		);
	}
	delete_post_meta( $postId, 'views' );
}
echo 'ok';
